<?php
class Redactor_Helper_Articles_index extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$sql = $this->query ( "select `id`, `name` from `articles` where `active`=1  order by `pos` asc " );
		$out = "";
		if ($sql != false && $sql->rowCount () > 0) {
			
			$out .= '<ul>';
			$i = 0;
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$i ++;
				if ($i <= 5) {
					$out .= '<li><a href="' . $this->getUrl ( array (
							'module' => 'articles',
							'articles' => $row ['id'] 
					) ) . '">' . $row ['name'] . '</a></li>';
				}
			}
			
			$out .= '<li><a href="' . $this->getUrl ( array (
					'module' => 'articles' 
			) ) . '">��� ������ (' . $sql->rowCount () . ')</a></li></ul>';
		}
		$this->over = $out;
	}
}