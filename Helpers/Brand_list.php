<?php
class Redactor_Helper_Brand_list extends Redactor_Helper {
	var $over = '';
	function __construct() {
		$words = array (
				
				'A',
				'B',
				'C',
				'D',
				'E',
				'F',
				'G',
				'H',
				'I',
				'J',
				'K',
				'L',
				'M',
				'N',
				'O',
				'P',
				'Q',
				'R',
				'S',
				'T',
				'U',
				'V',
				'W',
				'X',
				'Y',
				'Z' 
		);
		$this->over = '<div class="menu2"><ul>
            	<li><span>������ �� ��������:</span></li>
            	
            ';
		foreach ( $words as $word ) {
			$this->over .= '<li><a href="/brands/' . $word . '/">' . $word . '</a>' . $this->getSub ( $word ) . '</li>';
		}
		$this->over .= '</ul><div class="clear"></div></div>';
	}
	function getSub($word) {
		$out = '';
		$word = mb_strtolower ( $word );
		$sth = $this->query ( "select `Id`, `Title` from `Brands_Items` where `Active`=1 and LOWER(`Title`) LIKE '{$word}%' " );
		
		if ($sth != false && $sth->rowCount () > 0) {
			$out .= '	<div class="submenu2">
						<ul>';
			foreach ( $sth->fetchAll () as $row ) {
				$out .= '<li><a href="' . $this->getUrl ( array (
						'module' => 'brands',
						'brands' => $row->Id 
				) ) . '">' . $row->Title . '</a></li>';
			}
			$out .= '</ul></div>';
		}
		
		return $out;
	}
}