<?php
class Redactor_Helper_Breadcrumbs extends Redactor_Helper {
	function getRoad() {
		return Breadcrumbs::getPath ();
	}
	function getView() {
		$out = '';
		if (! isset ( $_GET ['isIndex'] )) {
			$out = '<div class="breadcrumbs"><ul>
			<li><a itemprop="url" href="/">Главная</a></li>';
			$bread = $this->getRoad ();
			if (count ( $bread ) > 0) {
				foreach ( $bread as $path ) {
					if($path==end($bread))
					{
						$out .= '<li><span>' . $path . '</span></li>';
					}else {
						$out .= '<li>' . $path . '</li>';
					}
				}
			}
			$out .= '</div>';
		}
		return $out;
	}
}
?>