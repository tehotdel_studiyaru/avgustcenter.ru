<?php
class Redactor_Helper_Categories extends Redactor_Helper {
	var $over = '';
	function __construct() {
		$out = '';
		$module = $this->getModule ( 'Catalog' );
		
		if ($module != false) {
			
			$current = $module->getCurrentCategory ();
			$parents = array ();
			$this->getParents ( $current, $parents );
			$this->cats = $parents;
			$cats = $module->getCategories ( 0 );
			
			if ($cats != false) {
				
				foreach ( $cats as $row ) {
					$url = $this->getUrl ( array (
							'module' => "catalog",
							"catid" => $row->Id 
					) );
					if (in_array($row->Id, $parents)){
						$out.='<li class="expanded">';
					}
					else {
						$out.='<li>';
					}
					if ($module->hasCategoryChildren ( $row->Id ) > 0) {
						$out .= '
					<div>' . (! empty ( $row->Icon ) && file_exists ( "files/catalog/{$row->Icon}" ) ? '<a href="' . $url . '"><img src="'.(in_array($row->Id, $parents)?'/files/catalog/' . $row->Icon . '':'/change.php?url=files/catalog/' . $row->Icon ).'" source="files/catalog/' . $row->Icon . '" alt="" /></a>' : '') . '<a href="' . $url . '">' . $row->Title . '</a></div>';
						$out .= $this->getChilds ( $row->Id );
					} else {
						$out .= ' 
                                   <div>' . (! empty ( $row->Icon ) && file_exists ( "files/catalog/{$row->Icon}" ) ? '<a href="' . $url . '"><img src="'.(in_array($row->Id, $parents)?'/files/catalog/' . $row->Icon . '':'/change.php?url=files/catalog/' . $row->Icon ).'" source="files/catalog/' . $row->Icon . '" alt="" /></a>' : '') . '<a href="' . $url . '">' . $row->Title . '</a></div>';
					}
					$out .= '</li>';
				}
				if ($current!=0 && count($parents)>0){
					$out.='				<li class="servItem"><a href="#"><span>���� �������</span></a></li>
				';
				}
			}
		}
		$this->over = $out;
	}
	function getParents($Id, &$p) {
		$p [] = $Id;
		$sth = $this->query ( "select `parentId` from `Catalog_Categories` WHERE `Id`='{$Id}' LIMIT 1" );
		if ($sth && $sth->rowCount () > 0) {
			$par = $sth->fetchColumn ();
			if ($par != 0) {
				$this->getParents ( $par, $p );
			}
		}
	}
	function getChilds($id, $sub = false) {
		$out = '';
		$module = $this->getModule ( 'Catalog' );
		
		if ($module != false) {
			
			$cats = $module->getCategories ( $id );
			
			if ($cats != false) {
				$out .= '<ul>';
				foreach ( $cats as $row ) {
					$url = $this->getUrl ( array (
							'module' => "catalog",
							"catid" => $row->Id 
					) );
					$out .= '<li><a href="' . $url . '">' . $row->Title . '</a>';
					
					$out .= '</li>';
				}
				$out .= '</ul>';
			}
		}
		return $out;
	}
} 