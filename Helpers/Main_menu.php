<?php
class Redactor_Helper_Main_menu extends Redactor_Helper {
	var $over = "";
	var $li = array ();
	var $a = array ();
	var $idPages = array ();
	static $Pages = array ();
	function __construct() {
		$out = '';
		
		$sth = $this->query ( "select `Id`, `Title`, `Icon` from `Catalog_Categories` where `parentId`=0 and `Id`!=22 " );
		if ($sth != false && $sth->rowCount () > 0) {
			$out = '<div class="menu1">
			<ul>';
			foreach ( $sth->fetchAll () as $row ) {
				$out .= '<li><a href="' . $this->getUrl ( array (
						'module' => 'catalog',
						'catid' => $row->Id 
				) ) . '">' . $row->Title . '</a>' . $this->getSub ( $row->Id, $row->Icon ) . '</li>';
			}
			
			$out .= '</ul><div class="clear"></div></div>';
		}
		$this->over = $out;
	}
	function getSub($id, $icon) {
		$out = '';
		 $sth = $this->prepare ( "select `c`.`Id`, `c`.`Title` , count(*) as `count` , `c`.`Icon` from `Catalog_Categories` as `c` inner join `Catalog_Items` as `i` ON `i`.`CategoryId`=`c`.`Id` where `c`.`parentId`=? group by `c`.`Id`" );
		//$sth = $this->prepare ( "select `c`.`Id`, `c`.`Title` from `Catalog_Categories` as `c` where `c`.`parentId`=? " );
		$sth->execute ( array (
				$id 
		) );
		if ($sth->rowCount () > 0) {
			$count = ceil ( $sth->rowCount () / 2 );
			$total = $sth->rowCount ();
			$i = 0;
			$out = '<div class="submenu1"><table><tr>';
			if (! empty ( $icon )) {
				$out.='<td><img src="/thumbs/315x186/files/catalog/'.$icon.'"></td>';
			}
			foreach ( $sth->fetchAll () as $row ) {
				
				$i ++;
				
				if ($i == 1 || $count + 1 == $i) {
					$out .= '<td><ul>';
				}
				$out .= '<li><a href="' . $this->getUrl ( array (
						'module' => 'catalog',
						'catid' => $row->Id 
				) ) . '">' . $row->Title . ' </a></li>';
				if ($i == $total || $count == $i) {
					$out .= '</ul></td>';
				}
			}
			$out .= '</tr></table></div>';
		}
		return $out;
	}
}	
		