<?php

class Redactor_Helper_Menu extends Redactor_Helper
{

    var $over = "";

    var $li = array();

    var $a = array();

    var $idPages = array();

    static $Pages = array();

    function __construct()
    {
        $out = '';
        $id = 0;
        $sth = $this->prepare("select `Id`, `Title`, `url` from 	`pages` where `parentId`=0 and `inMenu`=1 order by `module`");
        $sth->execute(array(
            $id
        ));
       
        $page_id = isset($_GET['pages']) ? (int) $_GET['pages'] : 0;
        
        //echo '<pre>' . print_r($sth->fetchAll(), true) . '</pre>';
        
        
        if ($sth->rowCount() > 0) {
			echo '<div class="centerBlock"><ul>';
			
			foreach ($sth->fetchAll() as $row) {
				$url = '';			
				if($row->url != '') {
					$url = '/' . $row->url;
				}
				else {
					$url = '/pages/' . $row->Id;
				}
				echo '<li><a href="' . $url . '">' . $row->Title . '</a></li>';
			}
			
			echo '</ul></div>';
			
		}
        
        //if ($sth->rowCount() > 0) {
			
            //echo '<div class="centerBlock"><ul>';
            //$i = 0;
            //foreach ($sth->fetchAll() as $row) {
                //$this->uds = array();
                
                //$cls = '';
                //$this->getSub($row->Id);
                //if ($row->Id == $page_id) {
                    //$cls = 'class="active"';
                //}
                
                //if (in_array($page_id, $this->uds)) {
                    //$cls = 'class="active"';
                //}
                
                //$i ++;
                //if ($i == 1) {
                    //echo '<li><a ' . $cls . ' >' . $row->Title . '</a>' . $this->getSub($row->Id) . '</li>';
                //} elseif ($i == 2) {
                    //if (isset($_GET['module']) && $_GET['module'] == 'catalog') {
                        //$cls = 'class="active"';
                    //}
                    //echo '<li><a ' . $cls . ' >' . $row->Title . '</a>' . $this->getParentId() . '</li>';
                //} else {
                    
                    //echo '<li><a ' . $cls . ' href="' . $this->getUrl(array(
                        //'module' => 'pages',
                        //'pages' => $row->Id
                    //)) . '">' . $row->Title . '</a>' . ($row->Id == 3 ? $this->getParentId() : $this->getSub($row->Id)) . '</li>';
                //}
                //if ($i == 3) {
                    //$cls='';
                    //if (isset($_GET['module']) && $_GET['module'] == 'gallery') {
                        
                        //$cls = 'class="active"';
                    //}                  
                //}
            //}
            
            //echo '</ul></div>';
        //}
        
        $this->over = $out;
    }

    function getSub($id)
    {
        $out = '';
        $sth = $this->prepare("select `Id`, `Title` from 	`pages` where `parentId`=? and `inMenu`=1 order by `module`");
        $sth->execute(array(
            $id
        ));
        if ($sth->rowCount() > 0) {
            $out .= '<ul class="centerBlock">';
            foreach ($sth->fetchAll() as $row) {
                $this->uds[] = $row->Id;
                $out .= '<li><a href="' . $this->getUrl(array(
                    'module' => 'pages',
                    'pages' => $row->Id
                )) . '">' . $row->Title . '</a>';
            }
            $out .= '</ul>';
        }
        return $out;
    }

    function getParentId()
    {
        $out = '';
        $sth = $this->prepare("select `Id`,`Title` from `Catalog_Categories` where `parentId`=0");
        $sth->execute();
        if ($sth->rowCount() > 0) {
           // $out .= '<ul class="centerBlock">';
            foreach ($sth->fetchAll() as $row) {
                
                $out .= '<li><a href="' . $this->getUrl(array(
                    'module' => 'catalog',
                    'catid' => $row->Id
                )) . '">' . $row->Title . '</a>';
            }
            //$out .= '</ul>';
        }
        return $out;
    }
}	
		
