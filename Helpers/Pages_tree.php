<?php
class Redactor_Helper_Pages_tree extends Redactor_Helper {
	var $over = '';
	function __construct() {
		$this->pages = $this->getModule ( 'pages' );
		$this->over = $this->getPages ();
	}
	function getPages($id = 0) {
		$out = '';
		$sth = $this->prepare ( "select `Id`, `Title` from `pages` where `Active`='1' and `inMenu`='1' and `parentId`=? order by `module`" );
		$sth->execute ( array (
				$id 
		) );
		if ($sth->rowCount () > 0) {
			if ($id != 0) {
				$out .= ' <menu class="catalog-menu active">';
			}
			foreach ( $sth->fetchAll () as $row ) {
				if ($id == 0) {
					$out .= ' <li class="catalog-block">
                                    <a href="' . $this->getUrl ( array (
							'module' => 'pages',
							'pages' => $row->Id 
					) ) . '" class="catalog-category' . (in_array ( $row->Id, $this->pages->parents ) ? ' active' : '') . '"><span>' . $row->Title . '</span></a>';
				} else {
					$out .= ' <li class="catalog-menu-item"><a class="catalog-category-link' . (in_array ( $row->Id, $this->pages->parents ) ? ' active' : '') . '" href="' . $this->getUrl ( array (
							'module' => 'pages',
							'pages' => $row->Id 
					) ) . '">' . $row->Title . '</a>';
				}
				if ($id == 0 && in_array ( $row->Id, $this->pages->parents )) {
					$out .= $this->getPages ( $row->Id );
				}
				
				$out .= '</li>';
			}
			if ($id != 0) {
				$out .= '</menu>';
			}
		}
		
		return $out;
	}
}