<?php
$this->iUser = $this->getModule ( 'users' )->iUser ();
if ($this->iUser) {
	$this->UserRow = $this->getModule ( 'users' )->getUserRow ();
	if (isset ( $_POST ['Ajax'] )) {
		$this->setDisableView ();
		$this->setDisableTemplate ();
		header ( 'Content-Type: application/json' );
		if (isset ( $_POST ['Action'] )) {
			switch ($_POST ['Action']) {
				case "edit" :
					$Id = $this->UserRow->Id;
					if ($Id == 0) {
						echo json_encode ( array (
								"failure" => true 
						) );
						return false;
					}
					if (isset ( $_POST ['Email'] )) {
						unset ( $_POST ["Email"] );
					}
					decodeArray ( $_POST );
					$this->getModule ( 'users' )->updateUser ( $_POST, $Id );
					$this->UserRow = $this->getModule ( 'users' )->getUserRow ();
					$data = array ();
					$notAllow = array (
							'Id',
							'Password',
							'UpdatedDate',
							'Sort',
							'CategoryID' 
					);
					foreach ( ( array ) $this->UserRow as $name => $value ) {
						if (in_array ( $name, $notAllow )) {
							continue;
						}
						$data [$name] = $value;
					}
					$a = array (
							trim ( $this->UserRow->uf_42 ),
							trim ( $this->UserRow->uf_41 ),
							trim ( $this->UserRow->uf_43 ) 
					);
					$a = array_filter ( $a );
					$data ['fullname'] = implode ( ' ', $a );
					encodeArray ( $data );
					echo json_encode ( array (
							"success" => true,
							'data' => $data 
					) );
					break;
					
						case "subscribe" :
							$Id = $this->UserRow->Id;
							if ($Id == 0) {
								echo json_encode ( array (
										"failure" => true
								) );
								return false;
							}
							if (!isset($_POST['Newsletter'])){
								$_POST['Newsletter']=0;
							}
							
							decodeArray ( $_POST );
							$this->getModule ( 'users' )->updateUser ( $_POST, $Id );
					
							
							echo json_encode ( array (
									"success" => true
									
							) );
							break;
				case 'load' :
					$data = array ();
					$notAllow = array (
							'Id',
							'Password',
							'UpdatedDate',
							'Sort',
							'CategoryID' 
					);
					foreach ( ( array ) $this->UserRow as $name => $value ) {
						if (in_array ( $name, $notAllow )) {
							continue;
						}
						$data [$name] = $value;
					}
					
					encodeArray ( $data );
					
					echo json_encode ( array (
							"success" => true,
							'data' => $data 
					) );
					break;
				case "updateCompany" :
					$Id = 0;
					if (isset ( $_POST ['Id'] )) {
						$Id = ( int ) $_POST ['Id'];
						unset ( $_POST ['Id'] );
					}
					if ($Id == 0) {
						
						decodeArray ( $_POST );
						if (! isset ( $_POST ['Title'] ) or isset ( $_POST ['Title'] ) && empty ( $_POST ['Title'] )) {
							echo json_encode ( array (
									"failure" => true,
									'msg' => iconv ( 'windows-1251', 'utf-8', '�� ��������� ���� ��������' ) 
							) );
							return false;
						}
						$this->getModule ( 'company' )->addCompany ( $_POST, $this->UserRow->Id );
					} else {
						decodeArray ( $_POST );
						$this->getModule ( 'company' )->updateCompany ( $_POST, $Id, $this->UserRow->Id );
					}
					if (!onlyNumber($_POST['uf_71']) || strlen($_POST['uf_71'])!=12 || !onlyNumber($_POST['uf_72'])  || strlen($_POST['uf_72'])!=9 ){
						echo json_encode ( array (
								"failure" => true,
								'msg' => iconv ( 'windows-1251', 'utf-8', "���� ��� ������ �������� �� ���� � �� ����� 12 ��������\n���� ��� ������ �������� ������ �� ���� � �� ����� 9 ��������" )
						) );
						return false;
					}
					if (!onlyNumber($_POST['uf_59']) || strlen($_POST['uf_59'])!=6){
						echo json_encode ( array (
								"failure" => true,
								'msg' => iconv ( 'windows-1251', 'utf-8', "���� ������ ������ �������� �� ���� � �� ����� 6 ��������" )
						) );
						return false;
					}
					echo json_encode ( array (
							"success" => true 
					) );
					break;
				case "loadSelectCompany" :
					
					$companies = $this->getModule ( 'company' )->getCompaniesByUserID ( $this->UserRow->Id );
					$opt = array ();
					$first = 0;
					
					if ($companies) {
						$i = 0;
						foreach ( $companies as $row ) {
							$i ++;
							$cls = '';
							if ($i == 1) {
								$first = $row->Id;
								$cls = ' selected="selected"';
							}
							$opt [] = '<option value="' . $row->Id . '"' . $cls . '>' . $row->Title . '</option>';
						}
					}
					$select = $this->win2utf ( implode ( "", $opt )  );
					$data = '';
					if ($first != 0) {
						
						$company = $this->getModule ( 'company' )->getCompanyById ( $first, array (
								'Id' 
						), true, $this->UserRow->Id );
						if ($company) {
							
							$data .= '<span class="order-info-title info">���������</span>
								<ul class="address-list">';
							
							foreach ( $company as $name => $value ) {
								if (preg_match ( '#uf_#', $name )) {
									if (is_object ( $value )) {
										$data .= '<li><span class="address-title">' . $value->Title . '</span> <span>' . $value->Value . '</span></li>';
									}
								}
							}
							
							$data .= '</ul>
								<span class="order-btn change" onclick="editCompany(' . $company->Id . ')">��������</span>';
						}
					}
					$data = $this->win2utf($data);
					echo json_encode ( array (
							"success" => true,
							'data' => $data,
							'select'=>$select
					) );
					
					break;
				case "loadFormCompany" :
					$Id = 0;
					if (isset ( $_POST ['Id'] )) {
						$Id = ( int ) $_POST ['Id'];
					}
					if ($Id == 0) {
						echo json_encode ( array (
								"failure" => true 
						) );
						return false;
					}
					
					$company = $this->getModule ( 'company' )->getCompanyById ( $Id, array (
							'Id',
							'Title' 
					), true, $this->UserRow->Id );
					if ($company) {
						$data = array ();
						
						foreach ( ( array ) $company as $name => $value ) {
							if (is_object ( $value )) {
								$data [$name] = $value->Value;
							} else {
								$data [$name] = $value;
							}
						}
						encodeArray ( $data );
						echo json_encode ( array (
								"success" => true,
								'data' => $data 
						) );
					} else {
						echo json_encode ( array (
								"failure" => true 
						) );
					}
					break;
				case "loadCompany" :
					$Id = 0;
					if (isset ( $_POST ['Id'] )) {
						$Id = ( int ) $_POST ['Id'];
					}
					if ($Id == 0) {
						echo json_encode ( array (
								"failure" => true 
						) );
						return false;
					}
					
					$out = '';
					
					$company = $this->getModule ( 'company' )->getCompanyById ( $Id, array (
							'Id' 
					), true, $this->UserRow->Id );
					if ($company) {
						
						$out .= '<span class="order-info-title info">���������</span>
								<ul class="address-list">';
						
						foreach ( $company as $name => $value ) {
							if (preg_match ( '#uf_#', $name )) {
								if (is_object ( $value )) {
									$out .= '<li><span class="address-title">' . $value->Title . '</span> <span>' . $value->Value . '</span></li>';
								}
							}
						}
						
						$out .= '</ul>
								'.(!isset($_POST['notChange'])?'<span class="order-btn change" onclick="editCompany(' . $company->Id . ')">��������</span>':'');
						$out = iconv ( 'windows-1251', 'utf-8', $out );
						echo json_encode ( array (
								"success" => true,
								'data' => $out 
						) );
					} else {
						echo json_encode ( array (
								"failure" => true 
						) );
					}
					break;
					
					/*
					 * ������
					 * 
					 */
					case "updateAddress" :
						$Id = 0;
						if (isset ( $_POST ['Id'] )) {
							$Id = ( int ) $_POST ['Id'];
							unset ( $_POST ['Id'] );
						}
						if ($Id == 0) {
					
							decodeArray ( $_POST );
							if (! isset ( $_POST ['Title'] ) or isset ( $_POST ['Title'] ) && empty ( $_POST ['Title'] )) {
								echo json_encode ( array (
										"failure" => true,
										'msg' => iconv ( 'windows-1251', 'utf-8', '�� ��������� ���� ��������' )
								) );
								return false;
							}
							$this->getModule ( 'address' )->addAddress ( $_POST, $this->UserRow->Id );
						} else {
							decodeArray ( $_POST );
							$this->getModule ( 'address' )->updateAddress ( $_POST, $Id, $this->UserRow->Id );
						}
						echo json_encode ( array (
								"success" => true
						) );
						break;
					case "loadSelectAddress" :
							
						$addresses = $this->getModule ( 'address' )->getAddressesByUserID ( $this->UserRow->Id );
						$opt = array ();
						$first = 0;
							
						if ($addresses) {
							$i = 0;
							foreach ( $addresses as $row ) {
								$i ++;
								$cls = '';
								if ($i == 1) {
									$first = $row->Id;
									if (!isset($_POST['notAdd'])){
									   $cls = ' selected="selected"';
									}
								}
								$opt [] = '<option value="' . $row->Id . '"' . $cls . '>' . $row->Title . '</option>';
							}
						}
						$select = $this->win2utf ( implode ( "", $opt )  );
						$data = '';
						if ($first != 0) {
					
							$address = $this->getModule ( 'address' )->getAddressById ( $first, array (
									'Id'
							), true, $this->UserRow->Id );
							if ($address) {
									
								$data .= '<span class="order-info-title info">����� ��������</span>
								<ul class="address-list">';
									
								foreach ( $address as $name => $value ) {
									if (preg_match ( '#uf_#', $name )) {
										if (is_object ( $value )) {
											$data .= '<li><span class="address-title">' . $value->Title . '</span> <span>' . $value->Value . '</span></li>';
										}
									}
								}
									
								$data .= '</ul>
								<span class="order-btn change" onclick="editAddress(' . $address->Id . ')">��������</span>';
							}
						}
						$data = $this->win2utf($data);
						echo json_encode ( array (
								"success" => true,
								'data' => $data,
								'select'=>$select
						) );
							
						break;
					case "loadFormAddress" :
						$Id = 0;
						if (isset ( $_POST ['Id'] )) {
							$Id = ( int ) $_POST ['Id'];
						}
						if ($Id == 0) {
							echo json_encode ( array (
									"failure" => true
							) );
							return false;
						}
							
						$address = $this->getModule ( 'address' )->getAddressById ( $Id, array (
								'Id',
								'Title'
						), true, $this->UserRow->Id );
						if ($address) {
							$data = array ();
					
							foreach ( ( array ) $address as $name => $value ) {
								if (is_object ( $value )) {
									$data [$name] = $value->Value;
								} else {
									$data [$name] = $value;
								}
							}
							encodeArray ( $data );
							echo json_encode ( array (
									"success" => true,
									'data' => $data
							) );
						} else {
							echo json_encode ( array (
									"failure" => true
							) );
						}
						break;
					case "loadAddress" :
						$Id = 0;
						if (isset ( $_POST ['Id'] )) {
							$Id = ( int ) $_POST ['Id'];
						}
						if ($Id == 0) {
							echo json_encode ( array (
									"failure" => true
							) );
							return false;
						}
							
						$out = '';
							
						$company = $this->getModule ( 'address' )->getAddressById ( $Id, array (
								'Id'
						), true, $this->UserRow->Id );
						if ($company) {
					
							$out .= '<span class="order-info-title info">����� ��������</span>
								<ul class="address-list">';
					
							foreach ( $company as $name => $value ) {
								if (preg_match ( '#uf_#', $name )) {
									if (is_object ( $value )) {
										$out .= '<li><span class="address-title">' . $value->Title . '</span> <span>' . $value->Value . '</span></li>';
									}
								}
							}
					
							$out .= '</ul>
								'.(!isset($_POST['notChange'])?'<span class="order-btn change" onclick="editAddress(' . $company->Id . ')">��������</span>':'');
							$out = iconv ( 'windows-1251', 'utf-8', $out );
							echo json_encode ( array (
									"success" => true,
									'data' => $out
							) );
						} else {
							echo json_encode ( array (
									"failure" => true
							) );
						}
						break;
					
					
					
				default :
					echo json_encode ( array (
							"failure" => true 
					) );
					return false;
					break;
			}
		}
	}
}
function onlyNumber($string){
	$string = str_split($string);
	$number = array(0,1,2,3,4,5,6,7,8,9,'0','1','2','3','4','5','6','7','8','9');
	foreach ($string as $n){
		if (!in_array($n, $number)){
			return false;
		}
	}
	return true;
}
function decodeArray(&$array) {
	if (is_array ( $array )) {
		foreach ( $array as $name => $value ) {
			decodeArray ( $array [$name] );
		}
	} elseif (is_string ( $array )) {
		$array = iconv ( 'utf-8', 'windows-1251', $array );
	}
}
function encodeArray(&$array) {
	if (is_array ( $array )) {
		foreach ( $array as $name => $value ) {
			encodeArray ( $array [$name] );
		}
	} elseif (is_string ( $array )) {
		$array = iconv ( 'windows-1251', 'utf-8', $array );
	}
}
$this->setFileTemplate ( 'account' );

