<?php

$this->setLastModified ();
$queryParams = array ();
$this->setFileTemplate ( 'catalog' );
$mainParams = array (
		'module' => 'catalog' 
);

if ($this->getCurrentID ()) {
	$this->setView ( 'card' );
} elseif ($this->getCurrentCategory () != 0) {
	
	$mainParams ['catid'] = $this->getCurrentCategory ();
}
if (isset ( $_GET ['type'] )) {
	$mainParams ['type'] = $_GET ['type'];
}
if (isset ( $_GET ['dateend'] )) {
	$mainParams ['dateend'] = $_GET ['dateend'];
}
if (isset ( $_GET ['datestart'] )) {
	$mainParams ['datestart'] = $_GET ['datestart'];
}
if (isset ( $_GET ['search'] )) {
	$mainParams ['search'] = $_GET ['search'];
}
if (isset ( $_GET ['filter'] )) {
	$mainParams ['filter'] = $_GET ['filter'];
}
if (isset ( $_GET ['Price_From'] )) {
	$mainParams ['Price_From'] = $_GET ['Price_From'];
}
if (isset ( $_GET ['Price_To'] )) {
	$mainParams ['Price_To'] = $_GET ['Price_To'];
}
$this->set ( 'mainParams', $mainParams );
$this->set ( 'queryParams', $queryParams );

$this->setBreadcrumbs ();


