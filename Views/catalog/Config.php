<?php
$this->setLimitRecords('all');

if (isset($_GET['catid']) && intval($_GET['catid']) > 0) {
    $sth = $this->prepare("select count(1) from `Catalog_Categories` WHERE `parentId`=?");
    $sth->execute(array(
        $_GET['catid']
    ));
    if ($sth->fetchColumn() == 0) {
        $this->setView('list');
    } else {
        $this->setView('index');
    }
} else {
    $this->setView('card');
}

$this->setFileTemplate('catalog');
// BreadcrumbsTitle::set('������� �������');
$this->setOrderBy(array(
    array(
        "Sort",
        "asc"
    )
));
