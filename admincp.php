<?php
header("Content-Type: text/html; charset=utf-8");
session_start ();
require ("includes/Engine.php");
$try = false;
$b = array ();
$b = array_keys ( array_merge ( $b, $_POST, $_GET ) );

$a = array (
		"module",
		"task",
		"xaction" 
);
$exec = false;
foreach ( $a as $h ) {
	if (in_array ( $h, $b )) {
		$exec = true;
		break;
	}
}
$glb = new Redactor_Admin ();
if (isset ( $_POST ['user'] ) && isset ( $_POST ['passwd'] )) {
	
	if (SystemUsers::iUser ( $_POST ['user'], $_POST ['passwd'] )) {
		$glb->login = true;
		header ( "Location: /admincp.php" );
	} else {
		$try = true;
	}
}

if (SystemUsers::iUser ()) {
	$glb->login = true;
	if ($exec == true) {
		exit ();
	}
}
if ($glb->login == false && $exec == true) {
	echo "{failure:true, login:false}";
	exit ();
} elseif (! SystemUsers::iUser ()) {
	
	?>
<html>
<head>
<style>
#loading {
	position: absolute;
	left: 40%;
	top: 40%;
	padding: 2px;
	z-index: 20001;
	height: auto;
}

#loading a {
	color: #225588;
}

#loading .loading-indicator {
	background: white;
	color: #444;
	font: bold 13px tahoma, arial, helvetica;
	padding: 10px;
	width: 350px;
	margin: 0;
	height: auto;
}

#loading-msg {
	font: normal 10px arial, tahoma, sans-serif;
}
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=utf-8">
<title>Панель управления сайтом</title>
<link rel="stylesheet" type="text/css" href="core/css/ext-all.css" />

<link rel="stylesheet" type="text/css" href="core/css/coreadmin.css" />
<link rel="stylesheet" type="text/css"
	href="core/adminjs/ux/Ext.ux.grid.GridSummary.css" />
<link rel="stylesheet" type="text/css"
	href="core/css/virtualkeyboard.css" />


<!--       

    	<script type="text/javascript">var ErrorAdmin="";</script>-->
</head>
<body>
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div class="loading-indicator" style="">
			<form method="post">

				<center>
					<img align="center" src="core/re.png" width="150"
						style="margin-right: 8px; vertical-align: top;">
				</center>
				<br> <br />
    <?php
	if ($try) {
		echo "<p align='center'>Не правильный логин или пароль</p>";
	}
	?>
    
    <table>
					<tr>
						<td>Имя пользователя:</td>
						<td width="10">&nbsp;</td>
						<td><input type="text" name="user" /></td>
					</tr>
					<tr>
						<td>Пароль:</td>
						<td width="10">&nbsp;</td>
						<td><input type="password" name="passwd" /></td>
					</tr>
					<tr>
						<td colspan="3">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3"><center>
								<input type="submit" name="Login" value="Войти" />
							</center></td>
					</tr>
				</table>
			</form>
		</div>

	</div>
</body>
</html>
<?php
	exit ();
}
?>
<html>
<head>
<style>
#loading {
	position: absolute;
	left: 40%;
	top: 40%;
	padding: 2px;
	z-index: 20001;
	height: auto;
}

#loading a {
	color: #225588;
}

#loading .loading-indicator {
	background: white;
	color: #444;
	font: bold 13px tahoma, arial, helvetica;
	padding: 10px;
	width: 250px;
	margin: 0;
	height: auto;
}

#loading-msg {
	font: normal 10px arial, tahoma, sans-serif;
}

.swfupload {
	position: absolute;
	z-index: 1;
}
</style>
<meta http-equiv="Content-Type"
	content="text/html; charset=utf-8">
<title>Панель управления сайтом</title>

<link rel="stylesheet" type="text/css" href="core/css/ext-all.css" />
<link rel="stylesheet" type="text/css" href="core/css/ux-all.css" />

<link rel="stylesheet" type="text/css" href="core/css/coreadmin.css" />
<link rel="stylesheet" type="text/css"
	href="core/adminjs/ux/Ext.ux.grid.GridSummary.css" />
<link rel="stylesheet" type="text/css"
	href="core/css/virtualkeyboard.css" />


<!--       

    	<script type="text/javascript">var ErrorAdmin="";</script>-->
</head>
<body>
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div class="loading-indicator" style="">
			<img src="core/re.png" width="150"
				style="margin-right: 8px; float: left; vertical-align: top;"><br> <br />
			<span id="loading-msg">Загрузка</span>
		</div>

	</div>
	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Загрузка интерфейса...';</script>
	<script type="text/javascript" src="core/ext-base.js"></script>

	<script type="text/javascript" src="core/ext-all-debug.js"></script>
	<script type="text/javascript" src="core/ux-all.js"></script>

	<script>
	   Ext.override(Ext.grid.ColumnModel, {
		    // private
		    destroyConfig: function() {
		        for (var i = 0, len = this.config.length; i < len; i++) {
		            Ext.destroy(this.config[i]);
		        }
		    },

		    destroy : function() {
		        this.destroyConfig();
		        this.purgeListeners();
		    },
		    
		    setConfig: function(config, initial) {
		        var i, c, len;
		        if (!initial) { // cleanup
		            delete this.totalWidth;
		            this.destroyConfig();
		        }

		        // backward compatibility
		        this.defaults = Ext.apply({
		            width: this.defaultWidth,
		            sortable: this.defaultSortable
		        }, this.defaults);

		        this.config = config;
		        this.lookup = {};

		        for (i = 0, len = config.length; i < len; i++) {
		            c = Ext.applyIf(config[i], this.defaults);
		            // if no id, create one using column's ordinal position
		            if (Ext.isEmpty(c.id)) {
		                c.id = i;
		            }
		            if (!c.isColumn) {
		                var Cls = Ext.grid.Column.types[c.xtype || 'gridcolumn'];
		                c = new Cls(c);
		                config[i] = c;
		            }
		            this.lookup[c.id] = c;
		        }
		        if (!initial) {
		            this.fireEvent('configchange', this);
		        }
		    }
		});
	   </script>
	<script type="text/javascript" src="core/ext-lang-ru.js"></script>
	<script type="text/javascript" src="core/App.js"></script>

	<script type="text/javascript" src="core/adminjs/ux/virtualkeyboard.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/plugins/virtualkeyboard.js"></script>

	<script type="text/javascript"
		src="core/adminjs/ux/Ext.ux.form.LoginDialog.js"></script>

	<script type="text/javascript" src="core/adminjs/ux/miframe.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript" src="core/adminjs/ux/Ext.ux.TinyMCE.js"></script>

	<script type="text/javascript" src="core/swfupload.js"></script>
	<script type="text/javascript" src="core/swfobject.js"></script>
	<script type="text/javascript" src="core/plugins/swfupload.queue.js"></script>
	<script type="text/javascript" src="core/plugins/swfupload.cookies.js"></script>

	<script type="text/javascript" src="core/adminjs/all.js"></script>

	<script type="text/javascript"
		src="core/adminjs/ux/Ext.ux.grid.RowActions.js"></script>
	<script type="text/javascript" src="core/adminjs/ux/FileUploadField.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/data-view-plugins.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/Ext.ux.grid.GridSummary.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/Ext.ux.Plugin.RemoteComponent.js"></script>
	<script type="text/javascript"
		src="core/adminjs/ux/Ext.ux.UploadDialog.js"></script>

	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Загрузка модулей...';</script>
	<script type="text/javascript" src="core/jquery.js"></script>
	<script type="text/javascript" src="core/adminjs/stat_orders.js"></script>
	     
	       
	<?php
	$files = array ();
	// 'site_settings'=>'core/adminjs/site_settings.js',
	// 'pages'=>'core/adminjs/pages.js',
	// 'blocks'=>'core/adminjs/blocks.js',
	// 'news'=>'core/adminjs/news.js',
	// 'articles'=>'core/adminjs/articles.js',
	// 'faq'=>'core/adminjs/faq.js',
	// 'slider'=>'core/adminjs/slider.js',
	
	// 'price'=>'core/adminjs/price.js',
	// 'shop'=>'core/adminjs/shop.js',
	// 'files'=>'core/adminjs/files.js',
	// 'reviews'=>'core/adminjs/reviews.js',
	// 'banners'=>'core/adminjs/banners.js',
	// 'slider2'=>'core/adminjs/slider2.js',
	// 'pricelistfiles'=>'core/adminjs/pricelistfiles.js',
	// 'newsletter'=>'core/adminjs/newsletter.js',
	// 'users'=>'core/adminjs/users.js',
	// 'systemusers'=>'core/adminjs/systemusers.js',
	
	$names = array (
			"pages" => 'Основные разделы',
			"blocks" => "Блоки",
			"news" => "Новости",
			"articles" => "Спецпредложения",
			// "price"=>"Каталог",
			"shop" => "Магазин",
			"faq" => "Вопрос-ответ",
			"files" => "Публикации",
			"banners" => "Баннеры",
			"slider" => "Основные направления",
			"pricelistfiles" => "Прайс-листы",
			"newsletter" => "Подписка",
			"reviews" => "Отзывы",
			"users" => "Пользователи",
			"systemusers" => "Администраторы" 
	);
	
	$a = array ();
	foreach ( $files as $resource => $file ) {
		
		if (SystemUsers::isAllowed ( $resource ) or $resource == 'slider2') {
			
			echo ' <script type="text/javascript" src="' . $file . '"></script>';
		}
	}
	
	?>
	
	
	<!--  Catalog -->
	
<!--
	<script type="text/javascript" src="core/adminjs/Brands.js"></script>
-->
	<script type="text/javascript" src="core/adminjs/Catalog.init.js"></script>
	<script type="text/javascript" src="core/adminjs/Catalog.items.js"></script>
	<script type="text/javascript" src="core/adminjs/Catalog.uf.js"></script>

	<script type="text/javascript" src="core/adminjs/Catalog.view.js"></script>

	
	<!-- <script type="text/javascript" src="core/adminjs/Users.js"></script>
	<script src="/core/adminjs/newsletter.js"></script>
	<script type="text/javascript" src="core/adminjs/Catalog.Orders.js"></script> -->
	<script>
ModulesRightMenu += '<li style="margin: 0px;height: 1px;padding: 5px;padding-bottom: 17px;"><div style="height: 1px;border-bottom:2px solid #ccc;padding-bottom:3px;">&nbsp;</div></li>';	

	</script>
	
	
	<script type="text/javascript" src="core/adminjs/pages.js"></script>
	
	<script type="text/javascript" src="core/adminjs/blocks.js"></script>	
	<!-- <script type="text/javascript" src="core/adminjs/Oplata.js"></script> -->
	<script type="text/javascript" src="core/adminjs/news.js"></script>
	<script type="text/javascript" src="core/adminjs/articles.js"></script>
	<script type="text/javascript" src="core/adminjs/slider.js"></script>
<!--
	<script type="text/javascript" src="core/adminjs/slider2.js"></script>
-->
	<!-- <script type="text/javascript" src="core/adminjs/gallery.js"></script>
	<script type="text/javascript" src="core/adminjs/contacts.js"></script> -->
	<!-- SEO -->
	
	
	<script type="text/javascript" src="core/adminjs/redirects.js"></script>

	<script type="text/javascript" src="core/adminjs/VirtPages.js"></script>
	<script type="text/javascript" src="core/adminjs/seo.js"></script>

	<!-- End Seo -->

	<script type="text/javascript" src="core/adminjs/install.js"></script>
	<script type="text/javascript" src="core/adminjs/site_settings.js"></script>
	<script type="text/javascript" src="core/adminjs/pass.js"></script>


	<script type="text/javascript">document.getElementById('loading-msg').innerHTML = 'Иницилизация...';</script>
	<script type="text/javascript" src="core/adminjs/admin.js"></script>

</body>
</html>
