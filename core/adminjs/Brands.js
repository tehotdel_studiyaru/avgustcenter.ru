var Brands = {};
Brands.Plugins = new Array();
Brands.functions = new Array();




// База данных для Таблицы с записями
//База данных для Таблицы с записями
Brands.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Brands'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'Link'
	}, {
		name : 'CountImages'
	}

	])

});



Brands.UploadPhoto = function(newFile) {

	var items = new Array();
	if (newFile) {
		items.push({
			xtype : 'fileuploadfield',
			emptyText : 'Выберите файл для загрузки',
			fieldLabel : 'Файл',
			name : 'photo-path',
			width : '500',
			anchor : '95%',
			allowBlank : false,
			buttonCfg : {
				text : ' ',
				iconCls : 'upload-icon'
			}
		});
	}

	items.push({
		xtype : 'textfield',
		name : 'Title',
		fieldLabel : 'Заголовок',
		anchor : '90%'
	});
	items.push({
		xtype : 'hidden',
		name : 'Id'
	});

	items.push(htmled({
		name : 'Description',
		label : 'Описание',
		height : 200
	}));
	var form = new Ext.FormPanel({
		fileUpload : true,
		labelAlign : 'top',
		frame : true,
		shim : true,
		id : 'Brands.UploadFileForm',

		items : [ items ]

	});
	return new Ext.Window(
			{

				layout : 'fit',
				shim : false,
				modal : true,
				title : 'Загрузка фотографий',
				id : 'Brands.UploadImageWindow',
				width : 860,
				height : 550,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				listeners : {
					'close' : function() {
						Brands.ImagesStore.reload();
					}
				},
				items : [ form ],
				buttons : [
						{
							text : 'Загрузить',
							handler : function() {
								if (form.getForm().isValid()) {
									tinyMCE.triggerSave();
									var idd = Ext.getCmp('Brands.EditRecord')
											.getForm().findField('Id')
											.getValue();

									form
											.getForm()
											.submit(
													{
														url : 'admincp.php',
														method : 'POST',
														params : {
															ItemID : idd,
															module : 'Brands',
															task : 'SaveFile'
														},
														waitTitle : 'Загрузка фотографии',
														waitMsg : 'Пожалуйста подождите, идёт загрузка фотографии...',
														success : function(
																fotoupload, o) {
															Brands.ImagesStore
																	.reload();
															Ext
																	.getCmp(
																			'Brands.UploadImageWindow')
																	.close();

														},
														failure : function(
																fotoupload2, o) {
															Ext.MessageBox
																	.alert(
																			'Ошибка',
																			'Не удалось загрузить фотографию');
														}
													});
								}
							}
						},
						{
							text : 'Закрыть',
							handler : function() {
								Brands.ImagesStore.reload();
								Ext.getCmp('Brands.UploadImageWindow').close();
							}
						} ]
			}).show();
}

Brands.ImagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadImages",
		module : 'Brands'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'

	}, {
		name : 'image'
	}, {
		name : 'Sort'
	}, {
		name : 'MainImage'
	}, {
		name : 'Title'
	}, {
		name : 'ItemID'
	} ])

});

Brands.EditRecord = function(Item, User) {

	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : Brands.ImagesStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : 'Сделать основной'
		}, '-', {
			iconCls : 'delete',
			qtip : 'Удалить'
		}, {
			iconCls : 'edit',
			qtip : 'Редактировать'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'Вы уверены что хотите удалить эту фотографию',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'Brands',
										task : 'DeleteImage',
										Id : record.data.Id
									},
									method : 'post',
									success : function() {
										Brands.ImagesStore.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'Brands',
						task : 'SaveFile',
						Id : record.data.Id,
						ItemID : record.get('ItemID'),
						MainImage : 1
					},
					method : 'post',
					success : function() {
						Brands.ImagesStore.reload();
					}
				});
			}
			if (action == 'edit') {
				Brands.UploadPhoto();
				Ext.getCmp('Brands.UploadFileForm').getForm().load({
					url : '/admincp.php',
					method : 'post',
					params : {
						module : 'Brands',
						task : 'LoadRecord',
						Id : record.get('Id'),
						to : 'File'
					},
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : Brands.ImagesStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'BrandsGridImages',

				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : 'Пожалуйста подождите...',
									url : 'admincp.php',
									params : {
										xaction : "SaveFile",
										Id : oGrid_event.record.data.Id,
										
										module : 'Brands',
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result) {
											if (result.success) {
												Brands.ImagesStore
														.commitChanges();
											}
										}

									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 100,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/brands/"
										+ value + "' width='80'></center>";
							}
						}, {

							header : "Файл",
							width : 80,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "/files/Brands/" + value + "";
							}
						}, {

							header : "Заголовок",
							width : 150,
							sortable : false,
							dataIndex : 'Title'
						}, {
							header : "Поз.",
							width : 50,
							sortable : true,
							dataIndex : 'Sort',
							editor : new Ext.form.NumberField()
						}, {

							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'MainImage',
							renderer : function(value) {
								if (value == 1) {
									return "<b>Основная</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : 'Загрузить новую фотографию',
					handler : function() {
						Brands.UploadPhoto(1);

					},
					iconCls : 'add'
				}, {
					text : 'Загрузить архив фотографий',
					handler : function() {
						alert('Функция временно не доступна');

					},
					iconCls : 'add'
				} ]

			});

	var form = new Ext.form.FormPanel({
		id : 'Brands.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		TypeUF: 0,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'CategoryID',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : 'Описание',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 360,

						items : [ {
							xtype : 'textfield',
							fieldLabel : 'Название',
							name : 'Title',
							width : 340
						} ]
					} ]
				}, {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},
					items : [ {
						layout : 'form',
						width : 200,
						items : [{
				            typeAhead: true,
				            triggerAction: 'all',
				            value:'0',
				            store: new Ext.data.ArrayStore({
				                fields: ['Id', 'Title'],
				              
				                data: [['0', 'Не выбрана']]
				            }),
				            name:'Type',
				            editable:false,
				            xtype:'combo',
				            hiddenName:'TypeUF',
				            allowBlank:false,
				            listeners:{
				            	change:function(combo, record){
				            	
				            	   form.TypeUF = 1;
				            	},
				            	select:function(combo, newvalue){
				            		
				            		form.TypeUF = 1;
				            		
				            	}
				            },
				            mode: 'local',
				            fieldLabel:'Тип Записи',
				            displayField: 'Title',
				            valueField: 'Id',
				            lazyRender: true,
				            listClass: 'x-combo-list-small'
				        } ]
					} ]
				}, htmled({
					name : 'Notice',
					label : 'Короткое описание',
					height : 200
				}) , htmled({
					name : 'Description',
					label : 'Описание',
					height : 200
				}) ]
			}, {
				title : 'Дополнительно',
				id:'UFields',
				items:[{xtype:'hidden', name:'uffields', value:true}],
				layout:'form'
			}, {

				height : 460,
				layout : 'fit',
				title : 'Фотографии',
				items : [ grid ],
				iconCls : 'images',
				listeners : {
					activate : function() {
						Brands.ImagesStore.reload();
					}
				}
			}, {
				title : 'Статистика',
				layout : 'form',
				items : [ {
					xtype : 'textfield',
					name : 'CreatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : 'Дата создания'
				}, {
					xtype : 'textfield',
					name : 'UpdatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : 'Дата обновления'
				} ]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				width : 920,
				border : false,

				title : 'Создание/Редактирование записи',
				iconCls : 'add',
				id : 'Brands.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						Ext.getCmp('UFields').addListener('activate',function(){
							
							if (form.TypeUF==0){
								
								return false;
							}
							form.TypeUF =0;
							
							Ext.getCmp('UFields').doLayout();
						var record = Ext.getCmp('Brands.EditRecord').getForm().findField('TypeUF').getValue();
						Ext.getCmp('UFields').items.each(function(item){
	            			if (typeof(item.destroy)=='function'){
	            				
	            				item.destroy();
	            				Ext.getCmp('UFields').doLayout();
	            			}
	            		});
	            	    form
						.getForm()
						.load(
								{
									url : '/admincp.php',

									waitMsg : 'Подождите.. идёт загрузка данных',
									params : {
										module : 'Brands',
										task : 'LoadRecord',
										to : 'Record',
										
										TypeUF:record,
										Id : Item
									},
									success : function(o, p) {
										
										if (p.result) {
											if (p.result.data) {

													
													if (p.result.data.UFData && p.result.data.UFData.length>0){
														Ext.getCmp('UFields').items.each(function(item){
									            			if (typeof(item.destroy)=='function'){
									            				item.destroy();
									            				Ext.getCmp('UFields').doLayout();
									            			}
									            		});
														
														Ext.each(p.result.data.UFData, function(i,s){
															
															 Ext.getCmp('UFields').add(p.result.data.UFData[s]);
															 Ext.getCmp('UFields').doLayout();
														});
													}
													
													form.getForm().loadRecord(p.result); 
													
												
											}
										}
										
									}
								}
								);
						
						});
						Ext.getCmp('UFields').items.addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						Ext.getCmp('UFields').addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						
						
						form
								.getForm()
								.load(
										{
											url : '/admincp.php',
											params : {
												module : 'Brands',
												task : 'LoadRecord',
												to : 'Record',
												Id : Item
											},
											success : function(o, p) {
												if (p.result) {
													if (p.result.data) {

														if (p.result.data.Id) {
															
															if (p.result.data.UFData && p.result.data.UFData.length>0){
																Ext.getCmp('UFields').items.each(function(item){
											            			if (typeof(item.destroy)=='function'){
											            				item.destroy();
											            				Ext.getCmp('UFields').doLayout();
											            			}
											            		});
															     Ext.getCmp('UFields').add(p.result.data.UFData);
															     
															}
															if (p.result.data.UFGroups && p.result.data.UFGroups.results && p.result.data.UFGroups.results.length>0){
																Ext.getCmp('Brands.EditRecord').getForm().findField('TypeUF').getStore().removeAll();
																Ext.getCmp('Brands.EditRecord').getForm().findField('TypeUF').getStore().loadData(p.result.data.UFGroups.results);
															}
															form.getForm().loadRecord(p.result); 
															
															Brands.ImagesStore.baseParams.ItemID = p.result.data.Id;
															Brands.ImagesStore
																	.reload();
															if (!Item) {
																var tree = Brands.Tree
																		.getSelectionModel()
																		.getSelectedNode();
																if (tree) {
																	var id = tree.id;
																	var name = tree.text;

																	Ext
																			.getCmp(
																					'Brands.EditRecord')
																			.getForm()
																			.findField(
																					'CategoryID')
																			.setValue(
																					id);

																	Ext
																			.getCmp(
																					'Brands.EditRecord')
																			.getForm()
																			.findField(
																					'categoryName')
																			.setValue(
																					name);
																}
															}

														} else {
															Brands.ImagesStore.baseParams.ItemID = '';
														}
													} else {
														Brands.ImagesStore.baseParams.ItemID = '';
													}
												} else {
													Brands.ImagesStore.baseParams.ItemID = '';

												}
											},
											waitMsg : 'Подождите.. идёт загрузка данных'
										});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : 'Сохранить',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid() != false) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												waitMsg : 'Подождите.. идёт отправка данных',
												params : {
													module : 'Brands',
													task : 'SaveData',
													to : 'Item'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Brands.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'Запись успешно Добавлена');
															Brands.Store
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													} else {
														App
																.setAlert('',
																		'Во время оброботки данных произошла ошибка');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'Страница успешно Добавлена');
												}
											});
						} else {
							App
									.setAlert('',
											'Проверьте правильно ли вы заполнили форму');
						}

					}
				} ]
			}).show();
}

Brands.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Brands.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// Действия для записей
Brands.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : 'Удалить'
	}, {
		iconCls : 'edit',
		qtip : 'Редактировать'
	} ],
	header : "<center><b>Операции</b></center>",
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Brands.RowAction
		.on({
			action : function(grid, record, action, row, col) {
				// Удаление записи
				if (action == 'delete') {
					
						Ext.MessageBox.confirm('',
								'Вы уверены что хотите удалить эту запись',
								function(btn) {
									if (btn == "yes") {
										Ext.Ajax.request({
											url : 'admincp.php',
											params : {
												module : 'Brands',
												task : 'DeleteRecord',
												Id : record.data.Id
											},
											method : 'post',
											success : function() {
												Brands.Store.reload();
											}
										});
									}
								});
					
				}
				// Редактирование записи
				if (action == 'edit') {
					Brands.EditRecord(record.data.Id);
				}
				if (action=='key'){
					Brands.ChangePassword(record.data.Id);
				}
			}
		});

// Таблица с данными
Brands.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Brands.Store,
			
			frame : false,
			loadMask : true,
			id : 'Brands.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,

			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,
			
			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'pos',
						header : "<b>Поз</b>",
						width : 40,
						sortable : true,
						dataIndex : 'Sort',
						editor : new Ext.form.TextField,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>Наименование</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'link',
						header : "<b>Ссылка</b>",
						width : 100,
						sortable : false,
						dataIndex : 'Link',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}

					},
					{
						header : "<center><b>Создание</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Обновление</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b></b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="BrandsLink'
									+ rec.get('Id')
									+ '" target="_blank"></form><img style="cursor:pointer;" ext:qtip="Предосмотр" onclick="document.getElementById(\'BrandsLink'
									+ rec.get('Id')
									+ '\').submit();" src="/core/images/icons/Internet.png" /></a> <span style="'
									+ style
									+ '"><img  onclick="Brands.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? 'Активировать'
											: 'Деактивировать')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, Brands.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),
			listeners:{
				afteredit:function(oGrid_event) {
					Ext.Ajax.request({
						waitMsg : 'Пожалуйста подождите...',
						url : 'admincp.php',
						params : {
							task : "SaveData",
							Id : oGrid_event.record.data.Id,
							to : 'Item',
							module : 'Brands',
							Sort : oGrid_event.record.data.Sort
						},
						success : function(response) {
							var result = Ext.decode(response.responseText);
							if (result.success) {
								
								Brands.Store.commitChanges(); 
							}
						},
						failure : function(response) {
							var result = response.responseText;
							Ext.MessageBox.alert('error',
									'could not connect to the database. retry later');
						}
					});
				}
		
			
			},
			
			viewConfig : {
				forceFit : false
			},
			enableDragDrop:true,
			ddGroup:'Brands.Grid2Tree',
			
			bbar : Brands.pagingBar,
			plugins : Brands.RowAction,
			enableDragDrop   : true,
	        stripeRows       : true,
			split : true,

			tbar : [ {
				text : 'Добавить новую запись',
				handler : function() {
					Brands.EditRecord(0);
				},
				iconCls : 'add'
			},'-',new Ext.ux.form.SearchField({
				hideTrigger1: false,
				store: Brands.Store,
				emptyText:'Поиск по адресам',
				paramName: 'search',
				onTrigger1Click: function() {
					if (this.hasSearch) {
						this.el.dom.value = '';
						var o = {
							start: 0
						};
						this.store.baseParams = this.store.baseParams || {};
						this.store.baseParams[this.paramName] = '';
						this.store.reload({
							params: o
						});
						this.hasSearch = false;
					}
				},
				onTrigger2Click: function() {
					var v = this.getRawValue();
					if (v.length < 1) {
						this.onTrigger1Click();
						return;
					}
					var o = {
						start: 0
					};
					this.store.baseParams = this.store.baseParams || {};
					this.store.baseParams[this.paramName] = v;
					this.store.reload({
						params: o
					});
					this.hasSearch = true;
				}
			}) ],
			region : 'center'

		});

Brands.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : 'Пожалуйста подождите...',
		url : 'admincp.php',
		params : {
			task : 'SaveData',
			to : 'Item',
			Id : Id,
			Active : Status,
			module : 'Brands',
		},
		success : function(response) {
			var result = Ext.decode(response.responseText);
			if (result.success) {
				Brands.Store.getById(Id).set('Active', Status)
				Brands.Store.commitChanges(); // changes successful, get rid
				// of the red triangles
			}

		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}









Brands.View = {

		title : 'Бренды',
		layout : 'fit',
		border:false,
		bodyBorder: false,
		defaults: {
			
			split: true,
			animFloat: false,
			autoHide: false,
			useSplitTips: true
			
		},
		items : [ Brands.Grid ]
	};
	Brands.Plugins.push(Brands.View);

	Brands.functions.push({
		init : function() {
			
			Brands.Store.load({
				params : {
					start : 0,
					limit : 25
				}
			});
			
		}
	})

var BrandsUserField = UserFields('Brands');



Brands.Plugins.push(BrandsUserField.tab);

Brands.endpoint = {
		id:'Brands',
		xtype:'tabpanel',
		
		activeTab:0,
		bodyBorder: false,
		defaults: {
			
			split: true,
			animFloat: false,
			autoHide: false,
			useSplitTips: true
			
		},
		items: [Brands.Plugins]
}


Brands.functions.push({
	init : BrandsUserField.init
})
init_modules.push(Brands.endpoint);
Ext.apply(actions, {
	'Brands' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'Brands') {
			Ext.getCmp('Content').layout.setActiveItem('Brands');

			Brands.Store.load({
				params : {
					start : 0,
					limit : 25
				}
			});
			

		}
	}
});
ModulesRightMenu += '<li><img src="core/icons/brend.png" /><a id="Brands" href="#">Производители</a></li>';
