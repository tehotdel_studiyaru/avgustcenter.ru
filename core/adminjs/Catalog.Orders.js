if (!Catalog){
	var Catalog = {};
}
Catalog.Orders = {};
Catalog.Orders.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadOrders",
		module : 'Catalog'
	},
	remoteSort : false,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Date'
	}, {
		name : 'Sum'
	}, {
		name : 'Info'
	}, {
		name : 'Status'
	},
	{name:'email'},
	{name:'Address'},
	{name:'fio'},
	{name:'Payment'},
	{name:'Delivery'},
	{name:'Index'},
	{name:'total'}

	])

});


Catalog.Orders.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Catalog.Orders.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// Действия для записей
Catalog.Orders.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'printer',
		qtip : 'Печать счета'
	} ],
	header : "<center><b>Операции</b></center>",
	//widthIntercept : Ext.isSafari ? 4 : 2,
	width:120,
	autoWidth:false,
	id : 'actions'
});
Catalog.Orders.RowAction
		.on({
			action : function(grid, record, action, row, col) {
				if (action=='printer'){
				    window.open('/orderpdf.php?order='+record.data.Id);
				}
			}
		});

Catalog.Orders.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Catalog.Orders.Store,
			title : 'Заказы',
			frame : false,
			loadMask : true,
			id : 'Catalog.Orders.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,

			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,

			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id'
					},
					{

						header : "<b>Дата</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Date'
					},
					{

						header : "<b>Ф.И.О</b>",
						width : 100,
						sortable : true,
						dataIndex : 'fio'
						
					},
					{

						header : "<b>Email</b>",
						width : 100,
						sortable : true,
						dataIndex : 'email'
						
					},
					{
						id : 'name',
						header : "<b>Информация о адресе</b>",
						width : 300,
						sortable : true,
						dataIndex : 'Address'
					},{
						
						header : "<b>Индекс</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Index'
					}
,{
						
						header : "<b>Способ оплаты</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Payment'
					}
,{
						
						header : "<b>Способ доставки</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Delivery'
					},
					{

						header : "<b>Позиций</b>",
						width : 100,
						sortable : true,
						dataIndex : 'total'
						
					},
					{

						header : "<b>Сумма заказа</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Sum'
						
					},{ header: '<center><b>Статус</b></center>', dataIndex:'Status',
						// width: 150,
						editor: new Ext.form.ComboBox({
							typeAhead: true,
							triggerAction: 'all',
							store:new Ext.data.SimpleStore({
								fields:['partyValue', 'partyName'],
								data: [['0','Активный'],['1','Выполненный'], ['2', 'Отмененный']]
							}),
							mode: 'local',
							displayField: 'partyName',
							valueField: 'partyValue',
							lazyRender:true,
							listClass: 'x-combo-list-small'
						}),  renderer: function(value) {
							if (value == 0) {
								return "Активный";
							}
							if (value==1){
								return 'Выполненный';
							}
							if (value==2){
								return 'отмененный';
							}
							return "Активный";
						}

						}, Catalog.Orders.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),

			listeners : {
				afteredit : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : 'Пожалуйста подождите...',
								url : 'admincp.php',
								params : {
									task : "SaveDataOrder",
									Id : oGrid_event.record.data.Id,
									
									module : 'Catalog',
									Status: oGrid_event.record.data.Status
								},
								success : function(response) {
									var result = Ext
											.decode(response.responseText);
									if (result.success) {

										Catalog.Orders.Store.commitChanges();
									}
								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				}

				
			},

			viewConfig : {
				forceFit : false
			},
			
    tbar:[new Ext.ux.form.SearchField({
    	hideTrigger1: false,
    	store: Catalog.Orders.Store,
    	emptyText:'Поиск по Email',
    	paramName: 'search',
    	onTrigger1Click: function() {
    		if (this.hasSearch) {
    			this.el.dom.value = '';
    			var o = {
    				start: 0
    			};
    			this.store.baseParams = this.store.baseParams || {};
    			this.store.baseParams[this.paramName] = '';
    			this.store.reload({
    				params: o
    			});
    			this.hasSearch = false;
    		}
    	},
    	onTrigger2Click: function() {
    		var v = this.getRawValue();
    		if (v.length < 1) {
    			this.onTrigger1Click();
    			return;
    		}
    		var o = {
    			start: 0
    		};
    		this.store.baseParams = this.store.baseParams || {};
    		this.store.baseParams[this.paramName] = v;
    		this.store.reload({
    			params: o
    		});
    		this.hasSearch = true;
    	}
    })],
			bbar : Catalog.Orders.pagingBar,
			plugins : Catalog.Orders.RowAction,
			enableDragDrop : true,
			stripeRows : true,
			split : true,

			
			region : 'center'

		});
Catalog.Orders.View = {

		title : 'Заказы',
		layout : 'fit',
		id:'orders',
		items : [  Catalog.Orders.Grid ]
	};
	


	
init_modules[init_modules.length] = Catalog.Orders.View ;


Ext.apply(actions, {
	'orders' : function() {

		
			Ext.getCmp('Content').layout.setActiveItem('orders');
			Catalog.Orders.Store.load({
				params : {
					start : 0,
					limit : 25
				}
			});
			

		
		
	}
});
ModulesRightMenu += '<li><img src="core/images/icons/page.png"/><a id="orders" href="#">Заказы</a></li>';	
	
