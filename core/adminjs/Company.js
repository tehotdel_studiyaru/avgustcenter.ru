if (!Users){
var Users = {};
}
if (!Users.Plugins){
Users.Plugins = new Array();
}
if (!Users.functions){
Users.functions = new Array();
}

var Company = {};
Company.Plugins = new Array();
Company.functions = new Array();


// ��������� ��� ���������
Company.TreeLoader = new Ext.tree.TreeLoader({
	url : '/admincp.php',
	baseParams : {
		xaction : 'LoadCategories',
		module : 'Company'
	},
	preloadChildren : true
});

// ������ ���������
Company.Tree = new Ext.ux.tree.TreeGrid(
		{
			title : '������ �������������',
			width : 320,
			containerScroll : false,
			lines : false,
			singleExpand : true,
			useArrows : true,
			//autoScroll : true,
			animate : true,

			columnResize : false,
			enableSort : false,
			enableDragDrop : true,
			enableDD : true,
			ddGroup : 'Company.Grid2Tree',
			listeners : {
				beforenodedrop : function(e) {

					if (Ext.isArray(e.data.selections)) {
						if (e.target == this.getRootNode()) {
							return false;
						}

						var r = new Array();
						for ( var i = 0; i < e.data.selections.length; i++) {
							r.push(e.data.selections[i].data.Id);
						}
						var CategoryID = e.target.id;
						Ext.Ajax.request({
							url : '/admincp.php',
							method : 'post',
							params : {
								module : 'Company',
								task : 'ChangeCategoryIDItems',
								CategoryID : CategoryID,
								items : Ext.encode(r)
							},
							success : function() {
								Company.Store.reload();
							}
						});
						return true;
					}
				}
			},
			split : true,
			collapsible : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			tbar : [ {
				text : '�������� ������',
				iconCls : 'add',
				handler : function() {
					Company.EditCategory(0);
				}
			} ],

			region : 'west',

			columns : [
					{
						header : '��������',
						dataIndex : 'Title',
						
						width : 180

					},
					{
						header : '',
						tpl : new Ext.XTemplate(
								'{Id:this.formatHours}',
								{
									formatHours : function(v) {

										return "<img src=\"/core/images/icons/delete.png\" title=\"�������\" style='cursor:pointer' onclick='Company.DeleteCategory("
												+ v
												+ ")'/>&nbsp;<img src=\"/core/images/icons/new-pencil.png\" title=\"�������������\" style='cursor:pointer' onclick='Company.EditCategory("
												+ v + ")'/>";
									}
								}),

						width : 50,
						dataIndex : 'Id',
					} ],

			loader : Company.TreeLoader

		});
// set the root node
var root = new Ext.tree.AsyncTreeNode({
	text : '������',
	draggable : false, // disable root node dragging
	id : 0
});
Company.Tree.setRootNode(root);
Company.Tree.on('click', function(n) {

	Company.Store.baseParams = {
		xaction : "LoadRecords",
		module : 'Company',
		CategoryID : n.id
	};
	Company.Store.load({
		params : {
			start : 0,
			limit : 25
		}
	});

});
Company.Tree.on('nodeDrop', function(n, dd, e, data) {

	var id = n.dropNode.id;
	var parentId = n.dropNode.parentNode.id;
	Ext.Ajax.request({
		url : '/admincp.php',
		method : 'post',
		params : {
			module : 'Company',
			task : 'SaveData',
			to : 'Category',
			Id : id,
			parentId : parentId
		}
	});
});
Company.Tree.on("enddrag", function(tree) {

	function simplifyNodes(node) {
		var resultNode = new Array();
		var kids = node.childNodes;
		var len = kids.length;
		for ( var i = 0; i < len; i++) {

			resultNode.push({
				Id : kids[i].id,
				Sort : (i + 1),
				childs : simplifyNodes(kids[i])
			});

		}
		return resultNode;
	}

	var encNodes = Ext.encode(simplifyNodes(Company.Tree.root));

	Ext.Ajax.request({
		method : 'POST',
		url : 'admincp.php',
		params : {
			module : 'Company',
			task : 'UpdateSortCategories',
			nodes : encNodes
		}

	});

});

// ���� ������ ��� ������� � ��������
Company.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Company'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	},{
		name:'Email',
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'CurrentCategoryID',
	}

	])

});



Company.UploadPhoto = function(newFile) {

	var items = new Array();
	if (newFile) {
		items.push({
			xtype : 'fileuploadfield',
			emptyText : '�������� ���� ��� ��������',
			fieldLabel : '����',
			name : 'photo-path',
			width : '500',
			anchor : '95%',
			allowBlank : false,
			buttonCfg : {
				text : ' ',
				iconCls : 'upload-icon'
			}
		});
	}

	items.push({
		xtype : 'textfield',
		name : 'Title',
		fieldLabel : '���������',
		anchor : '90%'
	});
	items.push({
		xtype : 'hidden',
		name : 'Id'
	});

	items.push(htmled({
		name : 'Description',
		label : '��������',
		height : 200
	}));
	var form = new Ext.FormPanel({
		fileUpload : true,
		labelAlign : 'top',
		frame : true,
		shim : true,
		id : 'Company.UploadFileForm',

		items : [ items ]

	});
	return new Ext.Window(
			{

				layout : 'fit',
				shim : false,
				modal : true,
				title : '�������� ����������',
				id : 'Company.UploadImageWindow',
				width : 860,
				height : 550,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				listeners : {
					'close' : function() {
						Company.ImagesStore.reload();
					}
				},
				items : [ form ],
				buttons : [
						{
							text : '���������',
							handler : function() {
								if (form.getForm().isValid()) {
									tinyMCE.triggerSave();
									var idd = Ext.getCmp('Company.EditRecord')
											.getForm().findField('Id')
											.getValue();

									form
											.getForm()
											.submit(
													{
														url : 'admincp.php',
														method : 'POST',
														params : {
															ItemID : idd,
															module : 'Company',
															task : 'SaveFile'
														},
														waitTitle : '�������� ����������',
														waitMsg : '���������� ���������, ��� �������� ����������...',
														success : function(
																fotoupload, o) {
															Company.ImagesStore
																	.reload();
															Ext
																	.getCmp(
																			'Company.UploadImageWindow')
																	.close();

														},
														failure : function(
																fotoupload2, o) {
															Ext.MessageBox
																	.alert(
																			'������',
																			'�� ������� ��������� ����������');
														}
													});
								}
							}
						},
						{
							text : '�������',
							handler : function() {
								Company.ImagesStore.reload();
								Ext.getCmp('Company.UploadImageWindow').close();
							}
						} ]
			}).show();
}

Company.ImagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadImages",
		module : 'Company'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'

	}, {
		name : 'image'
	}, {
		name : 'Sort'
	}, {
		name : 'MainImage'
	}, {
		name : 'Title'
	}, {
		name : 'ItemID'
	} ])

});

Company.EditRecord = function(Item, User) {
if (!User){
	User = 0;
}
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : Company.ImagesStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : '������� ��������'
		}, '-', {
			iconCls : 'delete',
			qtip : '�������'
		}, {
			iconCls : 'edit',
			qtip : '�������������'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'�� ������� ��� ������ ������� ��� ����������',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'Company',
										task : 'DeleteImage',
										Id : record.data.Id
									},
									method : 'post',
									success : function() {
										Company.ImagesStore.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'Company',
						task : 'SaveFile',
						Id : record.data.Id,
						ItemID : record.get('ItemID'),
						MainImage : 1
					},
					method : 'post',
					success : function() {
						Company.ImagesStore.reload();
					}
				});
			}
			if (action == 'edit') {
				Company.UploadPhoto();
				Ext.getCmp('Company.UploadFileForm').getForm().load({
					url : '/admincp.php',
					method : 'post',
					params : {
						module : 'Company',
						task : 'LoadRecord',
						Id : record.get('Id'),
						to : 'File'
					},
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : Company.ImagesStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'CompanyGridImages',

				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : '���������� ���������...',
									url : 'admincp.php',
									params : {
										xaction : "SaveFile",
										Id : oGrid_event.record.data.Id,
										
										module : 'Company',
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result) {
											if (result.success) {
												Company.ImagesStore
														.commitChanges();
											}
										}

									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 100,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/company/"
										+ value + "' width='80'></center>";
							}
						}, {

							header : "����",
							width : 80,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "/files/company/" + value + "";
							}
						}, {

							header : "���������",
							width : 150,
							sortable : false,
							dataIndex : 'Title'
						}, {
							header : "���.",
							width : 50,
							sortable : true,
							dataIndex : 'Sort',
							editor : new Ext.form.NumberField()
						}, {

							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'MainImage',
							renderer : function(value) {
								if (value == 1) {
									return "<b>��������</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : '��������� ����� ����������',
					handler : function() {
						Company.UploadPhoto(1);

					},
					iconCls : 'add'
				}, {
					text : '��������� ����� ����������',
					handler : function() {
						alert('������� �������� �� ��������');

					},
					iconCls : 'add'
				} ]

			});

	var form = new Ext.form.FormPanel({
		id : 'Company.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		TypeUF: 0,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'CategoryID',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : '��������',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 360,

						items : [ {
							xtype : 'textfield',
							fieldLabel : '��������',
							name : 'Title',
							width : 340
						} ]
					} ]
				}, {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},
					items : [ {
						layout : 'form',
						width : 200,
						items : [{
				            typeAhead: true,
				            triggerAction: 'all',
				            value:'0',
				            store: new Ext.data.ArrayStore({
				                fields: ['Id', 'Title'],
				              
				                data: [['0', '�� �������']]
				            }),
				            name:'Type',
				            editable:false,
				            xtype:'combo',
				            hiddenName:'TypeUF',
				            allowBlank:false,
				            listeners:{
				            	change:function(combo, record){
				            	
				            	   form.TypeUF = 1;
				            	},
				            	select:function(combo, newvalue){
				            		
				            		form.TypeUF = 1;
				            		
				            	}
				            },
				            mode: 'local',
				            fieldLabel:'��� ��������',
				            displayField: 'Title',
				            valueField: 'Id',
				            lazyRender: true,
				            listClass: 'x-combo-list-small'
				        } ]
					} ]
				} ]
			}, {
				title : '�������������',
				id:'UFields',
				items:[{xtype:'hidden', name:'uffields', value:true}],
				layout:'form'
			}, {

				height : 460,
				layout : 'fit',
				title : '����������',
				items : [ grid ],
				iconCls : 'images',
				listeners : {
					activate : function() {
						Company.ImagesStore.reload();
					}
				}
			}, {
				title : '����������',
				layout : 'form',
				items : [ {
					xtype : 'textfield',
					name : 'CreatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : '���� ��������'
				}, {
					xtype : 'textfield',
					name : 'UpdatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : '���� ����������'
				} ]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				width : 920,
				border : false,

				title : '��������/�������������� ������',
				iconCls : 'add',
				id : 'Company.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						Ext.getCmp('UFields').addListener('activate',function(){
							
							if (form.TypeUF==0){
								
								return false;
							}
							form.TypeUF =0;
							
							Ext.getCmp('UFields').doLayout();
						var record = Ext.getCmp('Company.EditRecord').getForm().findField('TypeUF').getValue();
						Ext.getCmp('UFields').items.each(function(item){
	            			if (typeof(item.destroy)=='function'){
	            				
	            				item.destroy();
	            				Ext.getCmp('UFields').doLayout();
	            			}
	            		});
	            	    form
						.getForm()
						.load(
								{
									url : '/admincp.php',

									waitMsg : '���������.. ��� �������� ������',
									params : {
										module : 'Company',
										task : 'LoadRecord',
										to : 'Record',
										
										TypeUF:record,
										Id : Item
									},
									success : function(o, p) {
										
										if (p.result) {
											if (p.result.data) {

													
													if (p.result.data.UFData && p.result.data.UFData.length>0){
														Ext.getCmp('UFields').items.each(function(item){
									            			if (typeof(item.destroy)=='function'){
									            				item.destroy();
									            				Ext.getCmp('UFields').doLayout();
									            			}
									            		});
														
														Ext.each(p.result.data.UFData, function(i,s){
															
															 Ext.getCmp('UFields').add(p.result.data.UFData[s]);
															 Ext.getCmp('UFields').doLayout();
														});
													}
													
													form.getForm().loadRecord(p.result); 
													
												
											}
										}
										
									}
								}
								);
						
						});
						Ext.getCmp('UFields').items.addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						Ext.getCmp('UFields').addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						
						
						form
								.getForm()
								.load(
										{
											url : '/admincp.php',
											params : {
												module : 'Company',
												task : 'LoadRecord',
												to : 'Record',
												UserID:User,
												Id : Item
											},
											success : function(o, p) {
												if (p.result) {
													if (p.result.data) {

														if (p.result.data.Id) {
															
															if (p.result.data.UFData && p.result.data.UFData.length>0){
																Ext.getCmp('UFields').items.each(function(item){
											            			if (typeof(item.destroy)=='function'){
											            				item.destroy();
											            				Ext.getCmp('UFields').doLayout();
											            			}
											            		});
															     Ext.getCmp('UFields').add(p.result.data.UFData);
															     
															}
															if (p.result.data.UFGroups && p.result.data.UFGroups.results && p.result.data.UFGroups.results.length>0){
																Ext.getCmp('Company.EditRecord').getForm().findField('TypeUF').getStore().removeAll();
																Ext.getCmp('Company.EditRecord').getForm().findField('TypeUF').getStore().loadData(p.result.data.UFGroups.results);
															}
															form.getForm().loadRecord(p.result); 
															
															Company.ImagesStore.baseParams.ItemID = p.result.data.Id;
															Company.ImagesStore
																	.reload();
															if (!Item) {
																var tree = Company.Tree
																		.getSelectionModel()
																		.getSelectedNode();
																if (tree) {
																	var id = tree.id;
																	var name = tree.text;

																	Ext
																			.getCmp(
																					'Company.EditRecord')
																			.getForm()
																			.findField(
																					'CategoryID')
																			.setValue(
																					id);

																	Ext
																			.getCmp(
																					'Company.EditRecord')
																			.getForm()
																			.findField(
																					'categoryName')
																			.setValue(
																					name);
																}
															}

														} else {
															Company.ImagesStore.baseParams.ItemID = '';
														}
													} else {
														Company.ImagesStore.baseParams.ItemID = '';
													}
												} else {
													Company.ImagesStore.baseParams.ItemID = '';

												}
											},
											waitMsg : '���������.. ��� �������� ������'
										});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '���������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid() != false) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												waitMsg : '���������.. ��� �������� ������',
												params : {
													module : 'Company',
													task : 'SaveData',
													to : 'Item'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Company.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'������ ������� ���������');
															Company.Store
																	.reload();
															if (Ext.getCmp('UsersGridCompany')){
																
																Ext.getCmp('UsersGridCompany').store.reload(); // changes successful, get rid
																// of the red triangles
															}
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert('',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�������� ������� ���������');
												}
											});
						} else {
							App
									.setAlert('',
											'��������� ��������� �� �� ��������� �����');
						}

					}
				} ]
			}).show();
}
Company.DeleteCategory = function(Id) {
	Ext.MessageBox.confirm('', '�� ������� ��� ������ ������� ��� ������',
			function(btn) {
				if (btn == "yes") {
					Ext.Ajax.request({
						url : 'admincp.php',
						params : {
							module : 'Company',
							task : 'DeleteCategory',
							Id : Id
						},
						method : 'post',
						success : function() {
							Company.Tree.root.reload();
						}
					});
				}
			})
}
Company.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Company.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// �������� ��� �������
Company.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : '�������'
	}, {
		iconCls : 'edit',
		qtip : '�������������'
	} ],
	header : "<center><b>��������</b></center>",
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Company.RowAction
		.on({
			action : function(grid, record, action, row, col) {
				// �������� ������
				if (action == 'delete') {
					
						Ext.MessageBox.confirm('',
								'�� ������� ��� ������ ������� ��� ������',
								function(btn) {
									if (btn == "yes") {
										Ext.Ajax.request({
											url : 'admincp.php',
											params : {
												module : 'Company',
												task : 'DeleteRecord',
												Id : record.data.Id
											},
											method : 'post',
											success : function() {
												Company.Store.reload();
											}
										});
									}
								});
					
				}
				// �������������� ������
				if (action == 'edit') {
					Company.EditRecord(record.data.Id);
				}
				
			}
		});

// ������� � �������
Company.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Company.Store,
			title : '������',
			frame : false,
			loadMask : true,
			id : 'Company.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,
tbar:[new Ext.ux.form.SearchField({
	hideTrigger1: false,
	store: Company.Store,
	emptyText:'����� �� ���������',
	paramName: 'search',
	onTrigger1Click: function() {
		if (this.hasSearch) {
			this.el.dom.value = '';
			var o = {
				start: 0
			};
			this.store.baseParams = this.store.baseParams || {};
			this.store.baseParams[this.paramName] = '';
			this.store.reload({
				params: o
			});
			this.hasSearch = false;
		}
	},
	onTrigger2Click: function() {
		var v = this.getRawValue();
		if (v.length < 1) {
			this.onTrigger1Click();
			return;
		}
		var o = {
			start: 0
		};
		this.store.baseParams = this.store.baseParams || {};
		this.store.baseParams[this.paramName] = v;
		this.store.reload({
			params: o
		});
		this.hasSearch = true;
	}
})],
			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,

			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>������������</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
					
						header : "<b>Email</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Email',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>��������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>����������</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b>������</b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="CompanyLink'
									+ rec.get('Id')
									+ '" target="_blank"></form> <span style="'
									+ style
									+ '"><img  onclick="Company.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? '������������'
											: '��������������')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, Company.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),

			listeners : {
				afteredit : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : '���������� ���������...',
								url : 'admincp.php',
								params : {
									task : "SaveData",
									Id : oGrid_event.record.data.Id,
									to : 'Item',
									module : 'Company',
									Sort : oGrid_event.record.data.Sort
								},
								success : function(response) {
									var result = Ext
											.decode(response.responseText);
									if (result.success) {

										Company.Store.commitChanges();
									}
								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				},

				render : function() {
					Company.gridTargetEl = Company.Grid.getEl();

					Company.DropZone = new Ext.dd.DropTarget(
							Company.gridTargetEl, {
								ddGroup : 'Company.Grid2Tree',

								notifyDrop : function(ddSource, e, data) {

									return (true);
								}
							});
				}
			},

			viewConfig : {
				forceFit : false
			},
			enableDragDrop : true,
			ddGroup : 'Company.Grid2Tree',

			bbar : Company.pagingBar,
			plugins : Company.RowAction,
			enableDragDrop : true,
			stripeRows : true,
			split : true,

			
			region : 'center'

		});

Company.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : '���������� ���������...',
		url : 'admincp.php',
		params : {
			task : 'SaveData',
			to : 'Item',
			Id : Id,
			Active : Status,
			module : 'Company',
		},
		success : function(response) {
			var result = Ext.decode(response.responseText);
			if (result.success) {
				if (Ext.getCmp('UsersGridCompany')){
					Ext.getCmp('UsersGridCompany').store.getById(Id).set('Active', Status)
					Ext.getCmp('UsersGridCompany').store.commitChanges(); // changes successful, get rid
					// of the red triangles
				}
				if (Company.Store.getById(Id)){
				Company.Store.getById(Id).set('Active', Status)
				Company.Store.commitChanges(); // changes successful, get rid
				// of the red triangles
				}
			}

		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}

// ����� ���������
Company.ChangeCategory = function(To, Additional) {
	var params = {
		xaction : 'LoadCategories',
		module : 'Company'
	};
	if (Additional) {
		params.checked = true;
		params.Id = Ext.getCmp('Company.EditRecord').getForm().findField('Id')
				.getValue();
	}
	if (To == 'Category') {
		params.Id = Ext.getCmp('Company.EditRecord').getForm().findField('Id')
				.getValue();
	}
	var loader = new Ext.tree.TreeLoader({
		url : '/admincp.php',
		baseParams : params,
		preloadChildren : true
	});
	var Tree4CCOS = new Ext.tree.TreePanel({
		autoScroll : true,
		animate : true,
		enableDD : false,
		width : 500,
		floatable : false,
		margins : '5 0 0 0',
		cmargins : '5 5 0 0',
		split : true,
		expanded : true,
		containerScroll : true,
		lines : false,
		singleExpand : true,
		useArrows : true,

		loader : loader,

		root : {
			nodeType : 'async',
			text : '�������� ������',
			expanded : true,
			draggable : false,
			id : '0'
		}
	});

	var ChangeCatOfShopItem = new Ext.Window(
			{
				layout : 'fit',
				id : 'ChangeParentOfAux',
				title : '�������� ������',
				shim : false,
				modal : true,
				width : 500,
				height : 250,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				items : Tree4CCOS,
				buttons : [ {
					text : '�������',
					iconCls : 'apply',
					handler : function() {
						if (Additional) {
							var result = new Array(), names = new Array(), selNodes = Tree4CCOS
									.getChecked();
							if (selNodes && selNodes.length > 0) {
								Ext.each(selNodes, function(node) {
									result.push(node.id);
									names.push(node.text);
								});

								Ext.getCmp('Company.EditRecord').getForm()
										.findField('VirtualCategoriesName')
										.setValue(names.join(', '));
								Ext.getCmp('Company.EditRecord').getForm()
										.findField('VirtualCategoriesId')
										.setValue(Ext.encode(result));
							}
							Ext.getCmp('ChangeParentOfAux').close();
						} else {
							var tr = Tree4CCOS.getSelectionModel()
									.getSelectedNode();
							if (!tr) {
								return Ext.MessageBox.alert('',
										'�������� ������');
							}
							var id = tr.id;
							var name = tr.text;

							if (To == 'Category') {
								Ext.getCmp('Company.EditRecord').getForm()
										.findField('parentId').setValue(id);
							} else {
								Ext.getCmp('Company.EditRecord').getForm()
										.findField('CategoryID').setValue(id);
							}
							Ext.getCmp('Company.EditRecord').getForm()
									.findField('categoryName').setValue(name);
							Ext.getCmp('ChangeParentOfAux').close();
						}
					}
				} ]
			}).show();
}


Company.ChangePassword = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'UF.EditValue',
		frame : true,
		border : false,

		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : Item
		}, {
			xtype : 'textfield',
			fieldLabel : '����� ������',
			name : 'Password',
			anchor : '90%',
			allowBlank : false
		} ]

	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 300,
				title : '����� ������',
				iconCls : 'add',
				id : 'Company.EditWindowPassword',
				height : 150,
				
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : '��������',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Company',
													task : 'changePassword'
												},
												success : function(form,
														action) {
													if (action.result) {
														if (action.result.success) {
															Company.Grid.store
																	.reload();
															Ext
																	.getCmp(
																			'Company.EditWindowPassword')
																	.close();

															App
																	.setAlert(
																			'',
																			'������ ������� �������');
															if (store) {
																store
																		.reload()
															}
															;
														} else {
															App
																	.setAlert(
																			'',
																			'�� ����� ��������� ������ ��������� ������');
														}
													} else {
														App
																.setAlert(
																		'',
																		'�� ����� ��������� ������ ��������� ������');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'�� ����� ��������� ������ ��������� ������');
												}
											});
						} else {
							App
									.setAlert('',
											'��������� ������������ ���������� �����');
						}
					}
				} ]
			}).show();
}

Company.View = {

	title : '��������',
	layout : 'fit',
	border:false,
	bodyBorder: false,
	defaults: {
		
		split: true,
		animFloat: false,
		autoHide: false,
		useSplitTips: true
		
	},
	items : [ Company.Grid ]
};
Company.Plugins.push(Company.View);

Company.functions.push({
	init : function() {
		
		Company.Store.load({
			params : {
				start : 0,
				limit : 25
			}
		});
		
	}
})



var CompanyUserField = UserFields('Company');



Company.Plugins.push(CompanyUserField.tab);

Company.endpoint = {
		id:'Company',
		xtype:'tabpanel',
		
		activeTab:0,
		bodyBorder: false,
		defaults: {
			
			split: true,
			animFloat: false,
			autoHide: false,
			useSplitTips: true
			
		},
		items: [Company.Plugins]
}


Company.functions.push({
	init : CompanyUserField.init
})
init_modules.push(Company.endpoint);
Ext.apply(actions, {
	'Company' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'Company') {
			Ext.getCmp('Content').layout.setActiveItem('Company');

			for (var i = 0; i<Users.functions.length;i++){
				Company.functions[i].init();
			}

		}
	}
});
ModulesRightMenu += '<li><img src="core/icons/company.png" /><a id="Company" href="#">��������</a></li>';