if (!Users){
var Users = {};
}
if (!Users.Plugins){
Users.Plugins = new Array();
}
if (!Users.functions){
Users.functions = new Array();
}

// Загрузчик для Категорий
Users.TreeLoader = new Ext.tree.TreeLoader({
	url : '/admincp.php',
	baseParams : {
		xaction : 'LoadCategories',
		module : 'Users'
	},
	preloadChildren : true
});

// Список категорий
Users.Tree = new Ext.ux.tree.TreeGrid(
		{
			title : 'Группы пользователей',
			width : 320,
			containerScroll : false,
			lines : false,
			singleExpand : true,
			useArrows : true,
			//autoScroll : true,
			animate : true,

			columnResize : false,
			enableSort : false,
			enableDragDrop : true,
			enableDD : true,
			ddGroup : 'Users.Grid2Tree',
			listeners : {
				beforenodedrop : function(e) {

					if (Ext.isArray(e.data.selections)) {
						if (e.target == this.getRootNode()) {
							return false;
						}

						var r = new Array();
						for ( var i = 0; i < e.data.selections.length; i++) {
							r.push(e.data.selections[i].data.Id);
						}
						var CategoryID = e.target.id;
						Ext.Ajax.request({
							url : '/admincp.php',
							method : 'post',
							params : {
								module : 'Users',
								task : 'ChangeCategoryIDItems',
								CategoryID : CategoryID,
								items : Ext.encode(r)
							},
							success : function() {
								Users.Store.reload();
							}
						});
						return true;
					}
				}
			},
			split : true,
			collapsible : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			tbar : [ {
				text : 'Добавить группу',
				iconCls : 'add',
				handler : function() {
					Users.EditCategory(0);
				}
			} ],

			region : 'west',

			columns : [
					{
						header : 'Название',
						dataIndex : 'Title',
						
						width : 180

					},
					{
						header : '',
						tpl : new Ext.XTemplate(
								'{Id:this.formatHours}',
								{
									formatHours : function(v) {

										return "<img src=\"/core/images/icons/delete.png\" title=\"Удалить\" style='cursor:pointer' onclick='Users.DeleteCategory("
												+ v
												+ ")'/>&nbsp;<img src=\"/core/images/icons/new-pencil.png\" title=\"Редактировать\" style='cursor:pointer' onclick='Users.EditCategory("
												+ v + ")'/>";
									}
								}),

						width : 50,
						dataIndex : 'Id',
					} ],

			loader : Users.TreeLoader

		});
// set the root node
var root = new Ext.tree.AsyncTreeNode({
	text : 'Корень',
	draggable : false, // disable root node dragging
	id : 0
});
Users.Tree.setRootNode(root);
Users.Tree.on('click', function(n) {

	Users.Store.baseParams = {
		xaction : "LoadRecords",
		module : 'Users',
		CategoryID : n.id
	};
	Users.Store.load({
		params : {
			start : 0,
			limit : 25
		}
	});

});
Users.Tree.on('nodeDrop', function(n, dd, e, data) {

	var id = n.dropNode.id;
	var parentId = n.dropNode.parentNode.id;
	Ext.Ajax.request({
		url : '/admincp.php',
		method : 'post',
		params : {
			module : 'Users',
			task : 'SaveData',
			to : 'Category',
			Id : id,
			parentId : parentId
		}
	});
});
Users.Tree.on("enddrag", function(tree) {

	function simplifyNodes(node) {
		var resultNode = new Array();
		var kids = node.childNodes;
		var len = kids.length;
		for ( var i = 0; i < len; i++) {

			resultNode.push({
				Id : kids[i].id,
				Sort : (i + 1),
				childs : simplifyNodes(kids[i])
			});

		}
		return resultNode;
	}

	var encNodes = Ext.encode(simplifyNodes(Users.Tree.root));

	Ext.Ajax.request({
		method : 'POST',
		url : 'admincp.php',
		params : {
			module : 'Users',
			task : 'UpdateSortCategories',
			nodes : encNodes
		}

	});

});

// База данных для Таблицы с записями
Users.Store = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Users'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'UserName'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'Password'
	}, {
		name : 'Email'
	}, {
		name : 'CurrentCategoryID',
	}

	])

});

Users.EditCategory = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'Users.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'parentId',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : 'Описание',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 2,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 400,

						items : [ {
							xtype : 'textfield',
							fieldLabel : 'Заголовок',
							name : 'Title',
							width : 380
						} ]
					}, {
						layout : 'form',
						width : 320,
						items : [ {

							xtype : 'trigger',
							fieldLabel : 'Группа',
							name : 'categoryName',
							triggerClass : 'x-form-search-trigger',
							onTriggerClick : function() {
								Users.ChangeCategory('Category');
							},

							editable : false,
							allowBlank : true,
							width : 300,
						} ]
					} ]
				}, htmled({
					name : 'Description_Top',
					label : 'Описание внизу',
					height : 200
				}), htmled({
					name : 'Description_Bottom',
					label : 'Описание вверху',
					height : 200
				}) ]
			}, {

				title : 'Параметры SEO',
				layout : 'form',
				iconCls : 'seo',

				items : [ {
					xtype : 'textfield',
					fieldLabel : 'URL страницы (ЧПУ)',
					name : 'url',
					dataIndex : 'url',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'H1',
					name : 'H1',
					dataIndex : 'H1',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'Title',
					name : 'TitlePage',
					dataIndex : 'TitlePage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Description',
					name : 'DescPage',
					dataIndex : 'DescPage',
					width : 850
				}, {
					xtype : 'textarea',
					fieldLabel : 'Keywords',
					name : 'KeysPage',
					dataIndex : 'KeysPage',
					width : 850
				}, {
					xtype : 'textfield',
					fieldLabel : 'Теги для контекстной перелинковки',
					name : 'Tags',
					dataIndex : 'Tags',
					width : 850
				} ]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 850,
				title : 'Создание/Редактирование группы',
				iconCls : 'add',
				id : 'Users.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						form.getForm().load({
							url : '/admincp.php',
							params : {
								module : 'Users',
								task : 'LoadRecord',
								to : 'Category',
								Id : Item
							},
							waitMsg : 'Подождите.. идёт загрузка данных'
						});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : 'Сохранить',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Users',
													task : 'SaveData',
													to : 'Category'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Users.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'Группа успешно создана');
															Users.Tree.root
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													} else {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'Во время обработки данных произошла ошибка');
												}
											});
						} else {
							App.setAlert('',
									'Проверьте правильность заполнения формы');
						}
					}
				} ]
			}).show();
}

Users.UploadPhoto = function(newFile) {

	var items = new Array();
	if (newFile) {
		items.push({
			xtype : 'fileuploadfield',
			emptyText : 'Выберите файл для загрузки',
			fieldLabel : 'Файл',
			name : 'photo-path',
			width : '500',
			anchor : '95%',
			allowBlank : false,
			buttonCfg : {
				text : ' ',
				iconCls : 'upload-icon'
			}
		});
	}

	items.push({
		xtype : 'textfield',
		name : 'Title',
		fieldLabel : 'Заголовок',
		anchor : '90%'
	});
	items.push({
		xtype : 'hidden',
		name : 'Id'
	});

	items.push(htmled({
		name : 'Description',
		label : 'Описание',
		height : 200
	}));
	var form = new Ext.FormPanel({
		fileUpload : true,
		labelAlign : 'top',
		frame : true,
		shim : true,
		id : 'Users.UploadFileForm',

		items : [ items ]

	});
	return new Ext.Window(
			{

				layout : 'fit',
				shim : false,
				modal : true,
				title : 'Загрузка фотографий',
				id : 'Users.UploadImageWindow',
				width : 860,
				height : 550,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				listeners : {
					'close' : function() {
						Users.ImagesStore.reload();
					}
				},
				items : [ form ],
				buttons : [
						{
							text : 'Загрузить',
							handler : function() {
								if (form.getForm().isValid()) {
									tinyMCE.triggerSave();
									var idd = Ext.getCmp('Users.EditRecord')
											.getForm().findField('Id')
											.getValue();

									form
											.getForm()
											.submit(
													{
														url : 'admincp.php',
														method : 'POST',
														params : {
															ItemID : idd,
															module : 'Users',
															task : 'SaveFile'
														},
														waitTitle : 'Загрузка фотографии',
														waitMsg : 'Пожалуйста подождите, идёт загрузка фотографии...',
														success : function(
																fotoupload, o) {
															Users.ImagesStore
																	.reload();
															Ext
																	.getCmp(
																			'Users.UploadImageWindow')
																	.close();

														},
														failure : function(
																fotoupload2, o) {
															Ext.MessageBox
																	.alert(
																			'Ошибка',
																			'Не удалось загрузить фотографию');
														}
													});
								}
							}
						},
						{
							text : 'Закрыть',
							handler : function() {
								Users.ImagesStore.reload();
								Ext.getCmp('Users.UploadImageWindow').close();
							}
						} ]
			}).show();
}
Users.AddressStore = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Address'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'CurrentCategoryID',
	}

	])

})
Users.CompanyStore = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadRecords",
		module : 'Company'
	},
	remoteSort : true,
	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'
	}, {
		name : 'Title'
	}, {
		name : 'Sort'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'Active'
	}, {
		name : 'CurrentCategoryID',
	}

	])

})
Users.ImagesStore = new Ext.data.Store({
	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "LoadImages",
		module : 'Users'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ {
		name : 'Id'

	}, {
		name : 'image'
	}, {
		name : 'Sort'
	}, {
		name : 'MainImage'
	}, {
		name : 'Title'
	}, {
		name : 'ItemID'
	} ])

});

Users.EditRecord = function(Item, disable) {
   if (disable){
	   disable = true;
   }
   else {
	   disable = false;
   }
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : Users.ImagesStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : 'Сделать основной'
		}, '-', {
			iconCls : 'delete',
			qtip : 'Удалить'
		}, {
			iconCls : 'edit',
			qtip : 'Редактировать'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'Вы уверены что хотите удалить эту фотографию',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'Users',
										task : 'DeleteImage',
										Id : record.data.Id
									},
									method : 'post',
									success : function() {
										Users.ImagesStore.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'Users',
						task : 'SaveFile',
						Id : record.data.Id,
						ItemID : record.get('ItemID'),
						MainImage : 1
					},
					method : 'post',
					success : function() {
						Users.ImagesStore.reload();
					}
				});
			}
			if (action == 'edit') {
				Users.UploadPhoto();
				Ext.getCmp('Users.UploadFileForm').getForm().load({
					url : '/admincp.php',
					method : 'post',
					params : {
						module : 'Users',
						task : 'LoadRecord',
						Id : record.get('Id'),
						to : 'File'
					},
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : Users.ImagesStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'UsersGridImages',

				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : 'Пожалуйста подождите...',
									url : 'admincp.php',
									params : {
										xaction : "SaveFile",
										Id : oGrid_event.record.data.Id,
										module : 'Users',
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result) {
											if (result.success) {
												Users.ImagesStore
														.commitChanges();
											}
										}

									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 100,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/users/"
										+ value + "' width='80'></center>";
							}
						}, {

							header : "Файл",
							width : 80,
							sortable : false,
							dataIndex : 'image',
							renderer : function(value) {
								return "/files/users/" + value + "";
							}
						}, {

							header : "Заголовок",
							width : 150,
							sortable : false,
							dataIndex : 'Title'
						}, {
							header : "Поз.",
							width : 50,
							sortable : true,
							dataIndex : 'Sort',
							editor : new Ext.form.NumberField()
						}, {

							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'MainImage',
							renderer : function(value) {
								if (value == 1) {
									return "<b>Основная</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : 'Загрузить новую фотографию',
					handler : function() {
						Users.UploadPhoto(1);

					},
					iconCls : 'add'
				}, {
					text : 'Загрузить архив фотографий',
					handler : function() {
						alert('Функция временно не доступна');

					},
					iconCls : 'add'
				} ]

			});
	
	
	
	

	var pagingBar2 = new Ext.PagingToolbar({
		pageSize : 25,
		store : Users.CompanyStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var RowAction2 = new Ext.ux.grid.RowActions({

		actions : [  {
			iconCls : 'delete',
			qtip : 'Удалить'
		}, {
			iconCls : 'edit',
			qtip : 'Редактировать'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction2.on({
		action : function(grid, record, action, row, col) {
			// Удаление записи
			if (action == 'delete') {
				
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить эту запись',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : 'Company',
											task : 'DeleteRecord',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											Users.CompanyStore.reload();
										}
									});
								}
							});
				
			}
			// Редактирование записи
			if (action == 'edit') {
				Company.EditRecord(record.data.Id);
			}
		}
	});

	var grid2 = new Ext.grid.EditorGridPanel(
			{
				store : Users.CompanyStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'UsersGridCompany',
				 disabled:disable,
				loadMask : true,
				autoWidth : true,
				
				columns : [
						{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>Наименование</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Создание</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Обновление</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b>Статус</b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="CompanyLink'
									+ rec.get('Id')
									+ '" target="_blank"></form> <span style="'
									+ style
									+ '"><img  onclick="Company.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? 'Активировать'
											: 'Деактивировать')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, RowAction2 ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar2,
				plugins : RowAction2,
				iconCls : 'icon-grid',
				split : true,
				tbar : [{
					iconCls:'add',
					text:'Добавить компанию',
					handler:function (){
						var UserID = Ext.getCmp('Users.EditRecord').getForm().findField('Id').getValue();
						Company.EditRecord(0, UserID);
					}
				} ]

			});
	
	var pagingBar3 = new Ext.PagingToolbar({
		pageSize : 25,
		store : Users.AddressStore,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	var rowAction3 = new Ext.ux.grid.RowActions({

		actions : [  {
			iconCls : 'delete',
			qtip : 'Удалить'
		}, {
			iconCls : 'edit',
			qtip : 'Редактировать'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	rowAction3.on({
		action : function(grid, record, action, row, col) {
			// Удаление записи
			if (action == 'delete') {
				
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить эту запись',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : 'Address',
											task : 'DeleteRecord',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											Users.AddressStore.reload();
										}
									});
								}
							});
				
			}
			// Редактирование записи
			if (action == 'edit') {
				Address.EditRecord(record.data.Id);
			}
		}
	});

	var grid3 = new Ext.grid.EditorGridPanel(
			{
				store : Users.AddressStore,

				enableColLock : false,
				clicksToEdit : 1,
				height : 430,
				frame : true,
				id : 'UsersGridAddress',
				 disabled:disable,
				loadMask : true,
				autoWidth : true,
				
				columns : [
						{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>Наименование</b>",
						width : 200,
						sortable : true,
						dataIndex : 'Title',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Создание</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Обновление</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b>Статус</b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="AddressLink'
									+ rec.get('Id')
									+ '" target="_blank"></form> <span style="'
									+ style
									+ '"><img  onclick="Address.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? 'Активировать'
											: 'Деактивировать')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, rowAction3 ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},

				bbar : pagingBar3,
				plugins : rowAction3,
				iconCls : 'icon-grid',
				split : true,
				tbar : [{
					iconCls:'add',
					text:'Добавить компанию',
					handler:function (){
						var UserID = Ext.getCmp('Users.EditRecord').getForm().findField('Id').getValue();
						Address.EditRecord(0, UserID);
					}
				} ]

			});

	var form = new Ext.form.FormPanel({
		id : 'Users.EditRecord',
		frame : true,
		border : false,
		layout : 'fit',
		height : 550,
		plain : true,
		TypeUF: 0,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : 0
		}, {
			xtype : 'hidden',
			name : 'CategoryID',
			value : 0
		}, {
			xtype : 'tabpanel',
			activeItem : 0,
			border : false,

			autoTabs : true,
			defaults : {
				frame : true,
				width : 850,
				autoHeight : true,
				border : false,
				layout : 'form'

			},
			items : [ {
				title : 'Описание',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},

					items : [ {
						layout : 'form',
						width : 360,

						items : [ {
							xtype : 'textfield',
							fieldLabel : 'Имя пользователя',
							name : 'UserName',
							disabled:disable,
							width : 340
						} ]
					}, {
						layout : 'form',
						width : 200,
						items : [ {

							xtype : 'trigger',
							fieldLabel : 'Группа',
							name : 'categoryName',
							disabled:disable,
							triggerClass : 'x-form-search-trigger',
							onTriggerClick : function() {
								Users.ChangeCategory('Item');
							},

							editable : false,
							allowBlank : true,
							width : 180
						} ]
					} ]
				}, {
					layout : 'table',
					layoutConfig : {
						columns : 3,
						tableAttrs : {
							style : {
								width : 600
							}
						}
					},
					items : [ {
						layout : 'form',
						width : 360,

						items : [ {
							xtype : 'textfield',
							fieldLabel : 'Email',
							name : 'Email',
							disabled:disable,
							width : 340
						} ]
					}, {
						layout : 'form',
						width : 200,
						items : [{
				            typeAhead: true,
				            triggerAction: 'all',
				            value:'0',
				            store: new Ext.data.ArrayStore({
				                fields: ['Id', 'Title'],
				              
				                data: [['0', 'Не выбрана']]
				            }),
				            name:'Type',
				            editable:false,
				            disabled:disable,
				            xtype:'combo',
				            hiddenName:'TypeUF',
				            allowBlank:false,
				            listeners:{
				            	change:function(combo, record){
				            	
				            	   form.TypeUF = 1;
				            	},
				            	select:function(combo, newvalue){
				            		
				            		form.TypeUF = 1;
				            		
				            	}
				            },
				            mode: 'local',
				            fieldLabel:'Тип пользователя',
				            displayField: 'Title',
				            valueField: 'Id',
				            lazyRender: true,
				            disabled:disable,
				            listClass: 'x-combo-list-small'
				        } ]
					} ]
				} ]
			}, {
				title : 'Дополнительно',
				id:'UFields',
				
				items:[{xtype:'hidden', name:'uffields', value:true}],
				layout:'form'
			}, {

				height : 460,
				layout : 'fit',
				title : 'Компании',
				items : [ grid2 ],
				icon: 'core/icons/company.png',
				listeners : {
					activate : function() {
						Users.CompanyStore.reload();
					}
				}
			}, {

				height : 460,
				layout : 'fit',
				title : 'Адреса доставки',
				items : [ grid3 ],
				icon: 'core/icons/company.png',
				listeners : {
					activate : function() {
						Users.AddressStore.reload();
					}
				}
			}, {

				height : 460,
				layout : 'fit',
				title : 'Фотографии',
				items : [ grid ],
				disabled:disable,
				iconCls : 'images',
				listeners : {
					activate : function() {
						Users.ImagesStore.reload();
					}
				}
			}, {
				title : 'Статистика',
				layout : 'form',
				items : [ {
					xtype : 'textfield',
					name : 'CreatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : 'Дата создания'
				}, {
					xtype : 'textfield',
					name : 'UpdatedDate2',
					disabled : true,
					width : 140,
					fieldLabel : 'Дата обновления'
				} ]
			} ]
		} ]
	});
	return new Ext.Window(
			{
				modal : true,
				width : 920,
				border : false,

				title : 'Создание/Редактирование записи',
				iconCls : 'add',
				id : 'Users.EditWindowRecord',
				height : 550,
				listeners : {
					'show' : function() {
						Ext.getCmp('UFields').addListener('activate',function(){
							
							if (form.TypeUF==0){
								
								return false;
							}
							form.TypeUF =0;
							
							Ext.getCmp('UFields').doLayout();
						var record = Ext.getCmp('Users.EditRecord').getForm().findField('TypeUF').getValue();
						Ext.getCmp('UFields').items.each(function(item){
	            			if (typeof(item.destroy)=='function'){
	            				
	            				item.destroy();
	            				Ext.getCmp('UFields').doLayout();
	            			}
	            		});
	            	    form
						.getForm()
						.load(
								{
									url : '/admincp.php',

									waitMsg : 'Подождите.. идёт загрузка данных',
									params : {
										module : 'Users',
										task : 'LoadRecord',
										to : 'Record',
										
										TypeUF:record,
										Id : Item
									},
									success : function(o, p) {
										
										if (p.result) {
											if (p.result.data) {

													
													if (p.result.data.UFData && p.result.data.UFData.length>0){
														Ext.getCmp('UFields').items.each(function(item){
									            			if (typeof(item.destroy)=='function'){
									            				item.destroy();
									            				Ext.getCmp('UFields').doLayout();
									            			}
									            		});
														
														Ext.each(p.result.data.UFData, function(i,s){
															
															 Ext.getCmp('UFields').add(p.result.data.UFData[s]);
															 Ext.getCmp('UFields').doLayout();
														});
														
													}
													
													form.getForm().loadRecord(p.result); 
													
												
											}
										}
										
									}
								}
								);
						
						});
						Ext.getCmp('UFields').items.addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						Ext.getCmp('UFields').addListener('add',function(){Ext.getCmp('UFields').doLayout();});
						
						
						form
								.getForm()
								.load(
										{
											url : '/admincp.php',
											params : {
												module : 'Users',
												task : 'LoadRecord',
												to : 'Record',
												Id : Item
											},
											success : function(o, p) {
												if (p.result) {
													if (p.result.data) {

														if (p.result.data.Id) {
															
															if (p.result.data.UFData && p.result.data.UFData.length>0){
																Ext.getCmp('UFields').items.each(function(item){
											            			if (typeof(item.destroy)=='function'){
											            				item.destroy();
											            				Ext.getCmp('UFields').doLayout();
											            			}
											            		});
															     Ext.getCmp('UFields').add(p.result.data.UFData);
															     
															}
															if (p.result.data.UFGroups && p.result.data.UFGroups.results && p.result.data.UFGroups.results.length>0){
																Ext.getCmp('Users.EditRecord').getForm().findField('TypeUF').getStore().removeAll();
																Ext.getCmp('Users.EditRecord').getForm().findField('TypeUF').getStore().loadData(p.result.data.UFGroups.results);
															}
															form.getForm().loadRecord(p.result); 
															Users.CompanyStore.baseParams.UserID = p.result.data.Id;
															Users.CompanyStore.reload();
															
															Users.AddressStore.baseParams.UserID = p.result.data.Id;
															Users.AddressStore.reload();
															
															Users.ImagesStore.baseParams.ItemID = p.result.data.Id;
															Users.ImagesStore
																	.reload();
															if (!Item) {
																var tree = Users.Tree
																		.getSelectionModel()
																		.getSelectedNode();
																if (tree) {
																	var id = tree.id;
																	var name = tree.text;

																	Ext
																			.getCmp(
																					'Users.EditRecord')
																			.getForm()
																			.findField(
																					'CategoryID')
																			.setValue(
																					id);

																	Ext
																			.getCmp(
																					'Users.EditRecord')
																			.getForm()
																			.findField(
																					'categoryName')
																			.setValue(
																					name);
																}
															}
															if (disable){
																Ext.each(Ext.getCmp('UFields').items.items, function(i,s){
																	
																	Ext.getCmp('UFields').items.items[s].disable(true);
																});
															}

														} else {
															Users.ImagesStore.baseParams.ItemID = '';
															Users.CompanyStore.baseParams.UserID ='';
														}
													} else {
														Users.ImagesStore.baseParams.ItemID = '';
														Users.CompanyStore.baseParams.UserID ='';
													}
												} else {
													Users.ImagesStore.baseParams.ItemID = '';
													Users.CompanyStore.baseParams.UserID ='';

												}
											},
											waitMsg : 'Подождите.. идёт загрузка данных'
										});
					}
				},
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : 'Сохранить',
					 disabled:disable,
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid() != false) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												waitMsg : 'Подождите.. идёт отправка данных',
												params : {
													module : 'Users',
													task : 'SaveData',
													to : 'Item'
												},
												success : function(form, action) {
													if (action.result) {
														if (action.result.success) {
															Ext
																	.getCmp(
																			'Users.EditWindowRecord')
																	.close();
															App
																	.setAlert(
																			'',
																			'Запись успешно Добавлена');
															Users.Store
																	.reload();
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													} else {
														App
																.setAlert('',
																		'Во время оброботки данных произошла ошибка');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'Страница успешно Добавлена');
												}
											});
						} else {
							App
									.setAlert('',
											'Проверьте правильно ли вы заполнили форму');
						}

					}
				} ]
			}).show();
}
Users.DeleteCategory = function(Id) {
	Ext.MessageBox.confirm('', 'Вы уверены что хотите удалить эту группу',
			function(btn) {
				if (btn == "yes") {
					Ext.Ajax.request({
						url : 'admincp.php',
						params : {
							module : 'Users',
							task : 'DeleteCategory',
							Id : Id
						},
						method : 'post',
						success : function() {
							Users.Tree.root.reload();
						}
					});
				}
			})
}
Users.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : Users.Store,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});

// Действия для записей
Users.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : 'Удалить'
	}, {
		iconCls : 'key',
		qtip : 'Задать пароль'
	}, {
		iconCls : 'edit',
		qtip : 'Редактировать'
	} ],
	header : "<center><b>Операции</b></center>",
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions'
});
Users.RowAction
		.on({
			action : function(grid, record, action, row, col) {
				// Удаление записи
				if (action == 'delete') {
					
						Ext.MessageBox.confirm('',
								'Вы уверены что хотите удалить эту запись',
								function(btn) {
									if (btn == "yes") {
										Ext.Ajax.request({
											url : 'admincp.php',
											params : {
												module : 'Users',
												task : 'DeleteRecord',
												Id : record.data.Id
											},
											method : 'post',
											success : function() {
												Users.Store.reload();
											}
										});
									}
								});
					
				}
				// Редактирование записи
				if (action == 'edit') {
					Users.EditRecord(record.data.Id);
				}
				if (action=='key'){
					Users.ChangePassword(record.data.Id);
				}
			}
		});

// Таблица с данными
Users.Grid = new Ext.grid.EditorGridPanel(
		{
			store : Users.Store,
			title : 'Записи',
			frame : false,
			loadMask : true,
			id : 'Users.Grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,

			split : true,
			margins : '3 0 3 3',
			cmargins : '3 3 3 3',
			autoWidth : true,

			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 30,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'name',
						header : "<b>Имя пользователя</b>",
						width : 200,
						sortable : true,
						dataIndex : 'UserName',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<b>Email</b>",
						width : 150,
						sortable : true,
						dataIndex : 'Email',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						id : 'link',
						header : "<b>Пароль</b>",
						width : 100,
						sortable : false,
						dataIndex : 'Password',
						renderer : function(v, tag, rec) {
							var style = "";
							 if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							if (v==1){
								v = 'Задан';
							}
							else {
								v = '<b>Не задан</b>';
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}

					},
					{
						header : "<center><b>Создание</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'CreatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{
						header : "<center><b>Обновление</b></center>",
						width : 80,
						sortable : true,
						dataIndex : 'UpdatedDate',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}
					},
					{

						header : "<center><b>Статус</b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.Active == 0 && rec.data.isVirtual == 1) {
								style = "color:red";
							} else if (rec.data.isVirtual == 1) {
								style = 'color: #3764A0';
							} else if (rec.data.Active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('Link')
									+ '" id="UsersLink'
									+ rec.get('Id')
									+ '" target="_blank"></form> <span style="'
									+ style
									+ '"><img  onclick="Users.UpdateStatus('
									+ rec.data.Id
									+ ', '
									+ (rec.data.Active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.Active == 0 ? 'Активировать'
											: 'Деактивировать')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.Active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, Users.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : false
			}),

			listeners : {
				afteredit : function(oGrid_event) {
					Ext.Ajax
							.request({
								waitMsg : 'Пожалуйста подождите...',
								url : 'admincp.php',
								params : {
									task : "SaveData",
									Id : oGrid_event.record.data.Id,
									to : 'Item',
									module : 'Users',
									Sort : oGrid_event.record.data.Sort
								},
								success : function(response) {
									var result = Ext
											.decode(response.responseText);
									if (result.success) {

										Users.Store.commitChanges();
									}
								},
								failure : function(response) {
									var result = response.responseText;
									Ext.MessageBox
											.alert('error',
													'could not connect to the database. retry later');
								}
							});
				},

				render : function() {
					Users.gridTargetEl = Users.Grid.getEl();

					Users.DropZone = new Ext.dd.DropTarget(
							Users.gridTargetEl, {
								ddGroup : 'Users.Grid2Tree',

								notifyDrop : function(ddSource, e, data) {

									return (true);
								}
							});
				}
			},

			viewConfig : {
				forceFit : false
			},
			enableDragDrop : true,
			ddGroup : 'Users.Grid2Tree',

			bbar : Users.pagingBar,
			plugins : Users.RowAction,
			enableDragDrop : true,
			stripeRows : true,
			split : true,

			tbar : [ {
				text : 'Добавить новую запись',
				handler : function() {
					Users.EditRecord(0);

				},
				iconCls : 'add'
			},'-',new Ext.ux.form.SearchField({
				hideTrigger1: false,
				store: Users.Store,
				emptyText:'Поиск по пользователям',
				paramName: 'search',
				onTrigger1Click: function() {
					if (this.hasSearch) {
						this.el.dom.value = '';
						var o = {
							start: 0
						};
						this.store.baseParams = this.store.baseParams || {};
						this.store.baseParams[this.paramName] = '';
						this.store.reload({
							params: o
						});
						this.hasSearch = false;
					}
				},
				onTrigger2Click: function() {
					var v = this.getRawValue();
					if (v.length < 1) {
						this.onTrigger1Click();
						return;
					}
					var o = {
						start: 0
					};
					this.store.baseParams = this.store.baseParams || {};
					this.store.baseParams[this.paramName] = v;
					this.store.reload({
						params: o
					});
					this.hasSearch = true;
				}
			}) ],
			region : 'center'

		});
Users.Grid.on('rowdblclick',function(grid, rowIndex, e){
	var rec =grid.getStore().getAt(rowIndex);
	Users.EditRecord(rec.data.Id, true);
	
});
Users.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : 'Пожалуйста подождите...',
		url : 'admincp.php',
		params : {
			task : 'SaveData',
			to : 'Item',
			Id : Id,
			Active : Status,
			module : 'Users',
		},
		success : function(response) {
			var result = Ext.decode(response.responseText);
			if (result.success) {
				Users.Store.getById(Id).set('Active', Status)
				Users.Store.commitChanges(); // changes successful, get rid
				// of the red triangles
			}

		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}

// Смена категории
Users.ChangeCategory = function(To, Additional) {
	var params = {
		xaction : 'LoadCategories',
		module : 'Users'
	};
	if (Additional) {
		params.checked = true;
		params.Id = Ext.getCmp('Users.EditRecord').getForm().findField('Id')
				.getValue();
	}
	if (To == 'Category') {
		params.Id = Ext.getCmp('Users.EditRecord').getForm().findField('Id')
				.getValue();
	}
	var loader = new Ext.tree.TreeLoader({
		url : '/admincp.php',
		baseParams : params,
		preloadChildren : true
	});
	var Tree4CCOS = new Ext.tree.TreePanel({
		autoScroll : true,
		animate : true,
		enableDD : false,
		width : 500,
		floatable : false,
		margins : '5 0 0 0',
		cmargins : '5 5 0 0',
		split : true,
		expanded : true,
		containerScroll : true,
		lines : false,
		singleExpand : true,
		useArrows : true,

		loader : loader,

		root : {
			nodeType : 'async',
			text : 'Основной раздел',
			expanded : true,
			draggable : false,
			id : '0'
		}
	});

	var ChangeCatOfShopItem = new Ext.Window(
			{
				layout : 'fit',
				id : 'ChangeParentOfAux',
				title : 'Выберите группу',
				shim : false,
				modal : true,
				width : 500,
				height : 250,
				autoScroll : true,
				closeAction : 'close',
				plain : true,
				items : Tree4CCOS,
				buttons : [ {
					text : 'Выбрать',
					iconCls : 'apply',
					handler : function() {
						if (Additional) {
							var result = new Array(), names = new Array(), selNodes = Tree4CCOS
									.getChecked();
							if (selNodes && selNodes.length > 0) {
								Ext.each(selNodes, function(node) {
									result.push(node.id);
									names.push(node.text);
								});

								Ext.getCmp('Users.EditRecord').getForm()
										.findField('VirtualCategoriesName')
										.setValue(names.join(', '));
								Ext.getCmp('Users.EditRecord').getForm()
										.findField('VirtualCategoriesId')
										.setValue(Ext.encode(result));
							}
							Ext.getCmp('ChangeParentOfAux').close();
						} else {
							var tr = Tree4CCOS.getSelectionModel()
									.getSelectedNode();
							if (!tr) {
								return Ext.MessageBox.alert('',
										'Выберите группу');
							}
							var id = tr.id;
							var name = tr.text;

							if (To == 'Category') {
								Ext.getCmp('Users.EditRecord').getForm()
										.findField('parentId').setValue(id);
							} else {
								Ext.getCmp('Users.EditRecord').getForm()
										.findField('CategoryID').setValue(id);
							}
							Ext.getCmp('Users.EditRecord').getForm()
									.findField('categoryName').setValue(name);
							Ext.getCmp('ChangeParentOfAux').close();
						}
					}
				} ]
			}).show();
}


Users.ChangePassword = function(Item) {
	var form = new Ext.form.FormPanel({
		id : 'UF.EditValue',
		frame : true,
		border : false,

		height : 550,
		plain : true,
		autoScroll : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			name : 'Id',
			value : Item
		}, {
			xtype : 'textfield',
			fieldLabel : 'Новый пароль',
			name : 'Password',
			anchor : '90%',
			allowBlank : false
		} ]

	});
	return new Ext.Window(
			{
				modal : true,
				border : false,
				width : 300,
				title : 'Смена пароля',
				iconCls : 'add',
				id : 'Users.EditWindowPassword',
				height : 150,
				
				layout : 'fit',
				items : [ form ],
				buttons : [ {
					text : 'Изменить',
					iconCls : 'apply',
					handler : function() {
						if (form.getForm().isValid()) {
							tinyMCE.triggerSave();
							form
									.getForm()
									.submit(
											{
												url : '/admincp.php',
												method : 'post',
												params : {
													module : 'Users',
													task : 'changePassword'
												},
												success : function(form,
														action) {
													if (action.result) {
														if (action.result.success) {
															Users.Grid.store
																	.reload();
															Ext
																	.getCmp(
																			'Users.EditWindowPassword')
																	.close();

															App
																	.setAlert(
																			'',
																			'Пароль успешно изменен');
															if (store) {
																store
																		.reload()
															}
															;
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													} else {
														App
																.setAlert(
																		'',
																		'Во время обработки данных произошла ошибка');
													}
												},
												failure : function() {
													App
															.setAlert('',
																	'Во время обработки данных произошла ошибка');
												}
											});
						} else {
							App
									.setAlert('',
											'Проверьте правильность заполнения формы');
						}
					}
				} ]
			}).show();
}

Users.View = {

	title : 'Пользователи',
	layout : 'border',

	items : [ Users.Tree, Users.Grid ]
};
Users.Plugins.push(Users.View);

Users.functions.push({
	init : function() {
		
		Users.Store.load({
			params : {
				start : 0,
				limit : 25
			}
		});
		Users.Tree.root.expand();
	}
})



var UsersUserField = UserFields('Users');
Users.Plugins.push(UsersUserField.tab);
Users.functions.push({
	init : UsersUserField.init
})



Users.Plugins.push({
	title:'Настройки',
	listeners:{
		render:function(){
			
		}
	},
	border:false
	
	
});
Users.endpoint = {
		id:'Users',
		xtype:'tabpanel',
		
		activeTab:0,
		bodyBorder: false,
		defaults: {
			
			split: true,
			animFloat: false,
			autoHide: false,
			useSplitTips: true
			
		},
		items: [Users.Plugins]
}
init_modules.push(Users.endpoint);
Ext.apply(actions, {
	'Users' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'Users') {
			Ext.getCmp('Content').layout.setActiveItem('Users');

			for (var i = 0; i<Users.functions.length;i++){
				Users.functions[i].init();
			}

		}
	}
});
ModulesRightMenu += '<li><img src="core/icons/users.png" /><a id="Users" href="#">Пользователи</a></li>';
