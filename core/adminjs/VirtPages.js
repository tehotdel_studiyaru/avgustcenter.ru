var virtpages = {};

virtpages.updateRecord = function(oGrid_event) {
	Ext.Ajax.request({
		waitMsg : 'Пожалуйста подождите...',
		url : 'admincp.php',
		params : {
			xaction : "Update",
			id : oGrid_event.record.data.Id,
			active : oGrid_event.record.data.Active,
			module : 'virtpages'
		},
		success : function(response) {
			var result = eval(response.responseText);
			switch (result) {
			case 33:
				virtpages.base.commitChanges(); // changes successful, get
				// rid of the red triangles
				virtpages.base.reload(); // reload our datastore.
				break;
			default:
				Ext.MessageBox.alert('Ошибка',
						'Не возможно сохранить изменения...');
				break;
			}
		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
};

virtpages.addedit = function(id) {
	var form = new Ext.FormPanel({
		id : 'virtpages.form',

		autoScroll : true,
		frame : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'hidden',
			'name' : 'Id'
		}, {
			xtype : 'fieldset',
			title : 'Основные данные',
			collapsible : false,
			autoHeight : true,
			items : [ {

				xtype : 'textfield',
				fieldLabel : 'Виртуальная страница',
				name : 'To',
				anchor : '95%'
			}, {

				xtype : 'textfield',
				fieldLabel : 'Источник',
				name : 'From',
				anchor : '95%'
			} ]
		}, {
			xtype : 'fieldset',
			title : 'Метатеги',
			collapsible : false,
			autoHeight : true,
			items : [ {
				fieldLabel : 'H1',
				xtype : 'textfield',
				anchor : '95%',
				name : 'H1'
			}, {
				fieldLabel : 'Заголовок страницы',
				xtype : 'textfield',
				anchor : '95%',
				name : 'Title'
			}, {
				fieldLabel : 'Ключевые слова страницы',
				xtype : 'textfield',
				anchor : '95%',
				name : 'Keywords'
			}, {
				fieldLabel : 'Описание страницы',
				xtype : 'textarea',
				anchor : '95%',
				name : 'Description'
			} ]
		}, {
			xtype : 'fieldset',
			title : 'Дополнительно',
			collapsible : false,
			autoHeight : true,
			items : [ htmled({
				name : 'Text',
				label : 'Текст для страницы',
				height : 200
			}) ]
		} ]
	});
	new Ext.Window({
		width : 1024,
		height : 560,
		frame : true,
		layout : 'fit',

		constrainHeader : true,
		closeAction : 'close',
		modal : true,
		id : 'virtpages.WindowAddEdit',
		items : [ form ],
		buttonAlign : 'right',
		buttons : [ {
			text : 'Сохранить',
			iconCls : 'accept',
			handler : function() {
				Ext.ux.TinyMCE.initTinyMCE();
				tinyMCE.triggerSave();
				form.getForm().submit({
					url : 'admincp.php',
					method : 'POST',
					params : {
						module : 'virtpages',
						task : 'save'
					},
					waitMsg : 'Пожалуйста подождите',
					success : function() {
						Ext.getCmp('virtpages.WindowAddEdit').close();
						virtpages.base.reload();

					}
				});
			}
		} ]
	}).show();
}

virtpages.base = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "Listing",
		module : 'virtpages'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'Id'
	}, [ 'Id', 'Type', 'From', 'To', 'Active', 'Text', 'H1', 'Title',
			'Keywords', 'Description' ])

});

// PagingBar for articlegrid
virtpages.pagingBar = new Ext.PagingToolbar({
	pageSize : 500,
	store : virtpages.base,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});
// End
// ArtclisRowAction

virtpages.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : 'Удалить'
	}, {
		iconCls : 'edit',
		qtip : 'Редактировать'
	} ],
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions',
	header : '<center><b>Операции</b></center>'
});
virtpages.RowAction.on({
	action : function(grid, record, action, row, col) {
		if (action == 'delete') {
			Ext.MessageBox.confirm('',
					'Вы уверены что хотите удалить эту запись', function(btn) {
						if (btn == "yes") {
							Ext.Ajax.request({
								url : 'admincp.php',
								params : {
									module : 'virtpages',
									task : 'deleteItem',
									id : record.data.Id
								},
								method : 'post',
								success : function() {
									virtpages.base.reload();
								}
							});
						}
					})
		}
		if (action == 'edit') {

			virtpages.addedit(record.data.id);
			Ext.getCmp('virtpages.form').getForm().loadRecord(record)

		}
	}
});
virtpages.grid = new Ext.grid.EditorGridPanel({
	store : virtpages.base,
	title : '',
	frame : true,
	loadMask : true,
	id : 'virtpages.grid',
	layout : 'fit',
	enableColLock : false,
	clicksToEdit : 1,
	autoWidth : true,
	columns : [ {
		header : '<b>Id</b>',
		width : 40,
		dataIndex : 'Id',
		sortable : false,
		renderer : function(v) {
			return '<span style="color:#ccc">' + v + '</span>';
		}
	}, {

		header : "<b>Виртуальная страница</b>",
		width : 200,
		sortable : false,
		dataIndex : 'To'
	}, {

		header : "<b>Источник</b>",
		width : 200,
		sortable : false,
		dataIndex : 'From'
	}, {

		header : '<b>Статус</b>',
		sortable : false,
		dataIndex : 'Active',
		width : 100,
		editor : new Ext.form.ComboBox({
			typeAhead : true,
			triggerAction : 'all',
			store : new Ext.data.SimpleStore({
				fields : [ 'partyValue', 'partyName' ],
				data : [ [ '1', 'Активная' ], [ '0', 'Не активная' ] ]
			}),
			mode : 'local',
			displayField : 'partyName',
			valueField : 'partyValue',
			lazyRender : true,
			listClass : 'x-combo-list-small'
		}),
		renderer : function(value) {
			if (value == 1) {
				return "Активная";
			}
			return "Не активная";
		}

	}, virtpages.RowAction ],

	sm : new Ext.grid.RowSelectionModel({
		singleSelect : true
	}),
	viewConfig : {
		forceFit : false
	},
	height : 150,
	bbar : virtpages.pagingBar,
	plugins : virtpages.RowAction,
	iconCls : 'icon-grid',
	split : true,
	tbar : [ {
		text : 'Добавить новую запись',
		handler : function() {

			virtpages.addedit(0);

		},
		iconCls : 'add'
	}, '-', new Ext.ux.form.SearchField({
		hideTrigger1 : false,
		store : virtpages.base,
		emptyText : 'Поиск',
		paramName : 'search',
		onTrigger1Click : function() {
			if (this.hasSearch) {
				this.el.dom.value = '';
				var o = {
					start : 0
				};
				this.store.baseParams = this.store.baseParams || {};
				this.store.baseParams[this.paramName] = '';
				this.store.reload({
					params : o
				});
				this.hasSearch = false;
			}
		},
		onTrigger2Click : function() {
			var v = this.getRawValue();
			if (v.length < 1) {
				this.onTrigger1Click();
				return;
			}
			var o = {
				start : 0
			};
			this.store.baseParams = this.store.baseParams || {};
			this.store.baseParams[this.paramName] = v;
			this.store.reload({
				params : o
			});
			this.hasSearch = true;
		}
	}) ],
	region : 'center'

});
virtpages.grid.on('afteredit', virtpages.updateRecord);

// End articleGrid

virtpages.view = {
	id : 'virtpages',
	title : 'Виртуальные страницы',
	layout : 'fit',

	items : [ virtpages.grid ]
}
init_modules[init_modules.length] = virtpages.view;
init_nav_modules[init_nav_modules.length] = {
	text : 'Виртуальные страницы',
	iconCls : 'pages',
	handler : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'virtpages') {
			Ext.getCmp('Content').layout.setActiveItem('virtpages');
			if (virtpages.base.data.length < 1) {
				virtpages.base.load({
					params : {
						start : 0,
						limit : 500
					}
				});

			}
			;

		}
	}
};

Ext.apply(actions, {
	'virtpages' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'virtpages') {
			Ext.getCmp('Content').layout.setActiveItem('virtpages');
			if (virtpages.base.data.length < 1) {
				virtpages.base.load({
					params : {
						start : 0,
						limit : 500
					}
				});
			}
			;

		}
	}
});
ModulesRightMenu2 += '<li><img src="core/icons/virtpages.png"/><a id="virtpages" href="#">Виртуальные страницы</a></li>';
