

var actions={};
var init_modules =new Array();
var init_nav_modules =new Array();
var init_nav_settings =new Array();

function htmled(m)
{
	if (m['width']){var width = m['width'];}else{var width = "100%";}
	if (m['height']){var height = m['height'];}else{var height =100;}
	if (m['label']){var label = m['label'];}else if (m['fieldLabel']){var label = m['fieldLabel'];}else{var label = "";}
	if (m['value']){var value = m['value'];}else{var value="";}
	if (m['addmod']){var addmod=m['addmod'];}else{var addmod="";}
	return new Ext.ux.TinyMCE({
		name:m['name'],
		
		fieldLabel: label,
		
		tinymceSettings: {
			convert_urls : 0,
			relative_urls : 0,
			height:height,
			width:850,
			
			forced_root_block : false,
	         force_br_newlines : false,
	         force_p_newlines :true,
            extended_valid_elements : "a[charset|coords|href|hreflang|name|rel|rev|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],abbr[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],acronym[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],address[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],applet[code|object|align|alt|archive|codebase|height|hspace|name|vspace|width|class|id|style|title],area[alt|coords|href|nohref|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],b[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],base[href|target],basefont[color|face|size|class|dir|id|lang|style|title],bdo[dir|class|id|lang|style|title],big[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],blockquote[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],body[alink|background|bgcolor|link|text|vlink|class|dir|id|lang|style|title|onclick|ondblclick|onload|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onunload],br[class|id|style|title],button[disabled|name|type|value|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],caption[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],center[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],cite[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],code[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],col[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],colgroup[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],del[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dfn[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dir[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],div[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dl[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],em[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],fieldset[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],font[color|face|size|class|dir|id|lang|style|title],form[action|accept|accept-charset|enctype|method|name|target|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onreset|onsubmit],frame[frameborder|longdesc|marginheight|marginwidth|name|noresize|scrolling|src|class|id|style|title],frameset[cols|rows|class|id|style|title|onload|onunload],head[profile|dir|lang],h1[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h2[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h3[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h4[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h5[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h6[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],hr[align|noshade|size|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],html[xmlns|dir|lang],i[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],iframe[align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width|class|id|style|title],img[alt|src|align|border|height|hspace|ismap|longdesc|usemap|vspace|width|class|dir|id|lang|style|title|onabort|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],input[],ins[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],kbd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],label[for|accesskey|class|dir|id|lang|style|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],legend[align|accesskey|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],li[type|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],link[charset|href|hreflang|media|rel|rev|target|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],map[name|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],menu[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],meta[content|http-equiv|name|scheme|dir|lang],noframes[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],noscript[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],object[align|archive|border|classid|codebase|codetype|data|declare|height|hspace|name|standby|type|usemap|vspace|width|class|dir|id|lang|style|tabindex|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ol[compact|start|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],optgroup[label|disabled|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],option[disabled|label|selected|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],p[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],param[name|type|value|valuetype|id],pre[width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],q[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],s[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],samp[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],script[type|charset|defer|src|xml:space],select[disabled|multiple|name|size|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],small[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],span[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strike[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strong[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|media|dir|lang|title],sub[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],sup[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],table[align|bgcolor|border|cellpadding|cellspacing|frame|rules|summary|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tbody[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],td[abbr|align|axis|bgcolor|char|charoff|colspan|headers|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],textarea[cols|rows|disabled|name|readonly|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onselect],tfoot[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],th[abbr|align|axis|bgcolor|char|charoff|colspan|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],thead[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],title[dir|lang],tr[align|bgcolor|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],u[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ul[compact|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],var[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup]",
			theme : "advanced",
			plugins: "safari,pagebreak,filemanager, imagemanager,fullscreen,"+addmod+"style,layer,table,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
			theme_advanced_buttons1 : "insertfile,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,pasteword,bullist,numlist,outdent,indent,blockquote,forecolor,backcolor,sub,|,fullscreen",
			theme_advanced_buttons2 : "filemanager,imagemanager, undo,redo,link,unlink,anchor,image,cleanup,code,preview,tablecontrols,hr,removeformat,visualaid,advhr,print,insertlayer,moveforward,movebackward,absolute,cite,visualchars",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : false,
			extended_valid_elements : "a[charset|coords|href|hreflang|name|rel|rev|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],abbr[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],acronym[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],address[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],applet[code|object|align|alt|archive|codebase|height|hspace|name|vspace|width|class|id|style|title],area[alt|coords|href|nohref|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],b[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],base[href|target],basefont[color|face|size|class|dir|id|lang|style|title],bdo[dir|class|id|lang|style|title],big[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],blockquote[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],body[alink|background|bgcolor|link|text|vlink|class|dir|id|lang|style|title|onclick|ondblclick|onload|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onunload],br[class|id|style|title],button[disabled|name|type|value|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],caption[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],center[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],cite[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],code[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],col[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],colgroup[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],del[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dfn[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dir[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],div[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dl[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],em[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],fieldset[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],font[color|face|size|class|dir|id|lang|style|title],form[action|accept|accept-charset|enctype|method|name|target|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onreset|onsubmit],frame[frameborder|longdesc|marginheight|marginwidth|name|noresize|scrolling|src|class|id|style|title],frameset[cols|rows|class|id|style|title|onload|onunload],head[profile|dir|lang],h1[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h2[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h3[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h4[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h5[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h6[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],hr[align|noshade|size|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],html[xmlns|dir|lang],i[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],iframe[align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width|class|id|style|title],img[alt|src|align|border|height|hspace|ismap|longdesc|usemap|vspace|width|class|dir|id|lang|style|title|onabort|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],input[],ins[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],kbd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],label[for|accesskey|class|dir|id|lang|style|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],legend[align|accesskey|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],li[type|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],link[charset|href|hreflang|media|rel|rev|target|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],map[name|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],menu[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],meta[content|http-equiv|name|scheme|dir|lang],noframes[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],noscript[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],object[align|archive|border|classid|codebase|codetype|data|declare|height|hspace|name|standby|type|usemap|vspace|width|class|dir|id|lang|style|tabindex|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ol[compact|start|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],optgroup[label|disabled|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],option[disabled|label|selected|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],p[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],param[name|type|value|valuetype|id],pre[width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],q[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],s[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],samp[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],script[type|charset|defer|src|xml:space],select[disabled|multiple|name|size|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],small[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],span[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strike[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strong[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|media|dir|lang|title],sub[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],sup[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],table[align|bgcolor|border|cellpadding|cellspacing|frame|rules|summary|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tbody[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],td[abbr|align|axis|bgcolor|char|charoff|colspan|headers|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],textarea[cols|rows|disabled|name|readonly|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onselect],tfoot[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],th[abbr|align|axis|bgcolor|char|charoff|colspan|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],thead[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],title[dir|lang],tr[align|bgcolor|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],u[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ul[compact|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],var[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup]",

			template_external_list_url : "example_template_list.js",
			theme_advanced_buttons3 : "",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_path : false,
			theme_advanced_resize_horizontal : false,
			theme_advanced_resizing : true,
			setup : function(ed) {
				ed.addButton('file_manager', {
					title : 'Медиа библеотека',
					image : 'core/images/icons/image_add.png',
					onclick : function() {
						medialib(ed);
					}
				}
				)
				ed.addButton('widgets_and_blocks', {
					title : 'Блоки и Виджеты',
					image : 'core/images/icons/folder_table.png',
					onclick : function() {
						widgets_blocks();
					}
				}
				)
			}
		},
		value: value
	
});
}
function widgets_blocks()
{
	var StoreBlocks = new Ext.data.Store({
		proxy: new Ext.data.HttpProxy({
			url: 'admincp.php',
			method: 'POST'
		}),
		baseParams:{module:'additional', xaction: "LoadStoreBlocks"},
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'id'
		}, [
		{name: 'title', mapping: 'title'},
		{name: 'link', mapping: 'link'}
		])

	});
	var GridBlocks =  new Ext.grid.GridPanel({
		store:StoreBlocks,
		loadMask: true,
		border:false,
		listeners:{
		"rowclick":function(){
			Ext.getCmp('ButtWAB_B').enable();
		}
		},
		enableColLock:false,
		columns: [
		{id:'title', header: "Описание", width: 300, sortable: true, dataIndex: 'title'},
		{id:'link', header: "Ссылка", width: 300, sortable: false, dataIndex: 'link'},

		],
		sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
		viewConfig: {
			forceFit: true
		},
		height:150,
		iconCls:'icon-grid',
		split: true
		,buttons:[{
			text:'Вставить'
			,id:'ButtWAB_B'
			,disabled:true
			,handler:function()
			{
				var name = GridBlocks.getSelectionModel().getSelected().get('link');
				tinyMCE.activeEditor.execCommand('mceInsertContent',false, name);
				Ext.getCmp('WindowBlocks&Widgets').close();
			}
		}]
	});
	var StoreWidgets = new Ext.data.Store({
		proxy: new Ext.data.HttpProxy({
			url: 'admincp.php',
			method: 'POST'
		}),

		baseParams:{module:'additional', xaction: "LoadStoreWidgets"},
		reader: new Ext.data.JsonReader({
			root: 'results',
			totalProperty: 'total',
			id: 'id'
		}, [
		{name: 'title', mapping: 'title'},
		{name: 'link', mapping: 'link'}
		])

	});
	var GridWidgets =  new Ext.grid.GridPanel({
		store: StoreWidgets,
		loadMask: true,
		border:false,
		listeners:{
		"rowclick":function(){
			Ext.getCmp('ButtWAB_W').enable();
		}
		},
		enableColLock:false,
		clicksToEdit:1,
		columns: [
		{id:'title', header: "Описание", width: 300, sortable: true, dataIndex: 'title'},
		{id:'link', header: "Ссылка", width: 300, sortable: false, dataIndex: 'link'}
		],
		sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
		viewConfig: {
			forceFit: true
		},
		height:150,
		iconCls:'icon-grid',
		split: true
		,buttons:[{
			text:'Вставить'
			,id:'ButtWAB_W'
			,disabled:true
			,handler:function()
			{
				var name = GridWidgets.getSelectionModel().getSelected().get('link');
				tinyMCE.activeEditor.execCommand('mceInsertContent',false, name);
				Ext.getCmp('WindowBlocks&Widgets').close();
			}
		}]
	});

	var TabPanel = new Ext.TabPanel({
		activeTab:0
		,border:false
		,frame:false
		,defaults:{
			layout:'fit'
			,width:500
			,height:230
			,frame:false
			,autoScroll:true
			,border:false
		}
		,items:[
		{
			listeners:{
			'activate': function()
			{
				if (GridBlocks.store.getCount()==0)
				{
				GridBlocks.store.load();
				}
				Ext.getCmp('ButtWAB_B').disable();
			}
			}

			,title:'Блоки'
			,items:[GridBlocks]
		},
		{
			listeners:{
			'activate': function()
			{
				if (GridWidgets.store.getCount()==0)
				{
				  GridWidgets.store.load();
				}
				Ext.getCmp('ButtWAB_W').disable();
			}
			}
			,title:'Виджеты'
			,items:[GridWidgets]
		}
		]
	});

	new Ext.Window({
		title:'Выберите виджет или блок'
		,width:500
		,id:'WindowBlocks&Widgets'
		,height:300
		,modal:true
		,closeAction:'close'
		,frame:true
		,items:[TabPanel]

	}).show();
}

var form = function (m, items)
{
	if (m['width']){var width = m['width'];}else{var width = "100%";}
	if (m['height']){var height = m['height'];}else{var height = "";}
	if (m['title']){var title = m['title'];}else{var title = "";}
	if (m['id']){var id = m['id'];}else{var id = "";}
	if (m['fileUpload']){var fileUpload = m['fileUpload'];}else{var fileUpload = false;}
	if (m['frame']){var frame = m['id'];}else{var frame = true;}
	if (m['autoWidth']){var autoWidth = m['autoWidth'];}else{var autoWidth = true;}
	if (m['autoHeight']){var autoHeight = m['autoWidth'];}else{var autoHeight = true;}
	if (m['labelAlign']){var labelAlign = m['labelAlign'];}else{var labelAlign = "top";}
	if (m['bodyStyle']){var bodyStyle = m['bodyStyle'];}else{var bodyStyle = "";}
	if (m['baseCls']){var baseCls = m['baseCls'];}else{var baseCls = "x-panel";}
	return new Ext.FormPanel({labelAlign: labelAlign,frame:frame,baseCls:baseCls,fileUpload:fileUpload, id:id, width: width,height: height,bodyStyle:bodyStyle,autoWidth: autoWidth,autoHeight: true,items: items});
}

var ModulesRightMenu = "";
var ModulesRightMenu2 = "";
var ModulesRightMenuS = "";
var App = new Ext.App({});
//Ext.WindowMgr.zseed = 10801;
var UFModules = {};

function UserFields(Module) {

	var UserFields = {};

	UserFields.DeleteCategory = function(Id) {
		Ext.MessageBox.confirm('',
				'Вы уверены что хотите удалить эту группу товаров', function(
						btn) {
					if (btn == "yes") {
						Ext.Ajax.request({
							url : 'admincp.php',
							params : {
								module : Module,
								task : 'DeleteUFCategory',
								Id : Id
							},
							method : 'post',
							success : function() {
								UserFields.Tree.root.reload();
							}
						});
					}
				})
	}

	UserFields.EditCategory = function(Item, Edit) {
		if (Edit) {
			return Ext.MessageBox.alert('',
					'Данную группу товаров нельзя редактировать');
		}
		var form = new Ext.form.FormPanel({
			id : 'UF.EditGroup',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Наименование',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование группы товаров',
					iconCls : 'add',
					id : 'UF.WindowGroup',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFGroup',
														to : 'Category',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFGroup'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																Ext
																		.getCmp(
																				'UF.WindowGroup')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	// Загрузчик для Категорий
	UserFields.TreeLoader = new Ext.tree.TreeLoader({
		url : '/admincp.php',
		baseParams : {
			xaction : 'LoadUFCategories',
			module : Module
		},
		preloadChildren : true
	});
	UserFields.Tree = new Ext.ux.tree.TreeGrid(
			{
				title : 'Группы',
				width : 250,
				containerScroll : true,
				lines : false,
				singleExpand : true,
				useArrows : true,
				// autoScroll : true,
				animate : true,

				columnResize : false,
				enableSort : false,

				split : true,
				collapsible : true,
				margins : '3 0 3 3',
				cmargins : '3 3 3 3',
				tbar : [ {
					text : 'Добавить группу',
					iconCls : 'add',
					handler : function() {
						UserFields.EditCategory(0);
					}
				} ],

				region : 'west',

				columns : [
						{
							header : '<b>Название группы</b>',
							dataIndex : 'Title',

							width : 180

						},
						{
							header : '',
							tpl : new Ext.XTemplate(
									'{Id:this.formatHours}',
									{
										formatHours : function(v) {

											return "<img src=\"/core/images/icons/delete.png\" title=\"Удалить\" style='cursor:pointer' onclick=\"UFModules['"+Module+"'].DeleteCategory("
													+ v
													+ ")\"/>&nbsp;<img src=\"/core/images/icons/new-pencil.png\" title=\"Редактировать\" style='cursor:pointer' onclick=\"UFModules['"+Module+"'].EditCategory("
													+ v
													+ (v == 0 ? ', true' : '')
													+ ")\"/>";
										}
									}),

							width : 50,
							dataIndex : 'Id',
						} ],

				loader : UserFields.TreeLoader

			});
	// set the root node

	UserFields.Tree.setRootNode(new Ext.tree.AsyncTreeNode({
		text : 'Корень',
		draggable : false, // disable root node dragging
		id : 0
	}));
	UserFields.Tree.on('click', function(n) {

		UserFields.Store.baseParams = {
			xaction : "LoadUFRecords",
			module : Module,

			Group : n.id
		};
		UserFields.Store.load({
			params : {
				start : 0,
				limit : 25
			}
		});

	});

	// База данных для Таблицы с записями
	UserFields.Store = new Ext.data.Store({

		proxy : new Ext.data.HttpProxy({
			url : 'admincp.php',
			method : 'POST'
		}),
		baseParams : {
			xaction : "LoadUFRecords",
			module : Module
		},
		remoteSort : true,
		reader : new Ext.data.JsonReader({
			root : 'results',
			totalProperty : 'total',
			id : 'Id'
		}, [ {
			name : 'Id'
		}, {
			name : 'Title'
		}, {
			name : 'Sort'
		}, {
			name : 'Type'
		} ])

	});

	// Действия для записей
	UserFields.RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'delete',
			qtip : 'Удалить'
		}, {
			iconCls : 'edit',
			qtip : 'Редактировать'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	UserFields.RowAction
			.on({
				action : function(grid, record, action, row, col) {
					// Удаление записи
					switch (action) {
					case "edit":
						if (record.data.Type == 1) {
							UserFields.EditTextarea(record.data.Id);
						} else if (record.data.Type == 2) {
							UserFields.EditCheckbox(record.data.Id);
						} else if (record.data.Type == 3) {
							UserFields.EditHTMLEditor(record.data.Id);
						} else if (record.data.Type == 4) {
							UserFields.EditSelect(record.data.Id);
						} else if (record.data.Type == 5) {
							UserFields.EditRadiobox(record.data.Id);
						} else if (record.data.Type == 6) {
							UserFields.EditTextfield(record.data.Id);
						}  else if (record.data.Type == 8) {
							UserFields.EditCheckboxColor(record.data.Id);
						}else {
							UserFields.EditTextfield(record.data.Id);
						}

						break;
					case "delete":
						Ext.MessageBox
								.confirm(
										'',
										'Вы уверены что хотите удалить это поле?<br/>Все данные из записей по этому полю будут удалены!',
										function(btn) {
											if (btn == "yes") {
												Ext.Ajax.request({
													url : 'admincp.php',
													params : {
														module : Module,
														task : 'DeleteUF',
														Id : record.data.Id
													},
													method : 'post',
													success : function() {
														UserFields.Store
																.reload();
													}
												});
											}
										})
						break;
					}
				}
			});

	// Пагинация
	UserFields.pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : UserFields.Store,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});

	// Таблица с полями
	UserFields.Grid = new Ext.grid.EditorGridPanel(
			{
				store : UserFields.Store,
				title : '',
				frame : false,
				loadMask : true,

				layout : 'fit',
				enableColLock : false,
				clicksToEdit : 1,
				title : 'Поля',
				split : true,
				margins : '3 0 3 3',
				cmargins : '3 3 3 3',
				autoWidth : true,
				listeners : {
					afteredit : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : 'Пожалуйста подождите...',
									url : 'admincp.php',
									params : {
										task : "SaveUFField",
										Id : oGrid_event.record.data.Id,

										module : Module,
										Sort : oGrid_event.record.data.Sort
									},
									success : function(response) {
										var result = Ext
												.decode(response.responseText);
										if (result.success) {

											UserFields.Store.commitChanges();
										}
									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [ {
					id : 'id',
					header : "<b>Id</b>",
					width : 30,
					sortable : true,
					dataIndex : 'Id'
				}, {

					header : "<b>Поз</b>",
					width : 40,
					sortable : true,
					dataIndex : 'Sort',
					editor : new Ext.form.TextField
				}, {

					header : "<b>Тип</b>",
					width : 100,
					sortable : true,
					dataIndex : 'Type',
					renderer : function(v) {
						switch (v) {
						case "0":
							return 'Текстовое поле';
							break;
						case "1":
							return 'Многострочное поле';
							break;
						case "2":
							return 'Флажки';
							break;
						case "3":
							return 'HTML Редактор';
							break;
						case "4":
							return 'Выпадающее меню';
							break;
						case "5":
							return 'Переключатели';
							break;
						case "6":
							return 'Числовое поле';
							break;
						case "7":
							return 'Дата';
							break;
						case "8":
							return 'Флажки для цвета';
							break;	
						default:
							return 'Текстовое поле';
						}
					}
				}, {
					id : 'name',
					header : "<b>Название поля</b>",
					width : 200,
					sortable : true,
					dataIndex : 'Title'
				}, {

					header : "<b>Название поле</b>",
					width : 50,
					sortable : true,
					dataIndex : 'Id',
					renderer : function(v, tag, rec) {
						if (rec.data.Type == 2) {
							return 'Значения';
						}
						return 'uf_' + rec.data.Id;
					}
				}, UserFields.RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),

				viewConfig : {
					forceFit : false
				},

				bbar : UserFields.pagingBar,
				plugins : UserFields.RowAction,

				stripeRows : true,
				split : true,
				region : 'center',
				tbar : [ {
					text : 'Добавить новую запись',
					handler : function() {
						UserFields.SelectType();
					},
					iconCls : 'add'
				} ]

			});

	UserFields.SelectGroup = function() {

		var params = {
			xaction : 'LoadUFCategories',
			module : Module
		};

		var loader = new Ext.tree.TreeLoader({
			url : '/admincp.php',
			baseParams : params,
			preloadChildren : true
		});
		var Tree4CCOS = new Ext.tree.TreePanel({
			autoScroll : true,
			animate : true,
			enableDD : false,
			width : 500,
			floatable : false,
			margins : '5 0 0 0',
			cmargins : '5 5 0 0',
			rootVisible : false,
			split : true,
			expanded : true,

			containerScroll : false,
			selModel : new Ext.tree.DefaultSelectionModel({
				listeners : {
					selectionchange : function(ui, n) {
						Ext.getCmp('UF.FormSelectType').getForm().findField(
								'Group').setValue(n.id);

						Ext.getCmp('UF.FormSelectType').getForm().findField(
								'categoryName').setValue('' + n.text);
						Ext.getCmp('UF.ChangeGroup').close();
					}
				}
			}),

			loader : loader,

			root : {
				nodeType : 'async',
				text : 'Общие поля',
				expanded : true,
				draggable : false,
				id : '0'
			}
		});

		var ChangeCatOfShopItem = new Ext.Window({
			layout : 'fit',
			id : 'UF.ChangeGroup',
			title : 'Выберите группу товаров',
			shim : false,
			modal : true,

			width : 500,
			height : 250,
			autoScroll : true,
			closeAction : 'close',
			plain : true,
			items : Tree4CCOS

		}).show();

	}

	// Форма для выбора типа записи
	UserFields.SelectType = function() {
		var form = new Ext.form.FormPanel({
			id : 'UF.FormSelectType',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [
					{
						xtype : 'hidden',
						name : 'Group',
						value : 0
					},
					{
						xtype : 'textfield',
						fieldLabel : 'Заголовок поля',
						name : 'Title',
						width : 150,
						allowBlank : false
					},
					{
						typeAhead : true,
						triggerAction : 'all',
						value : '0',
						store : new Ext.data.SimpleStore({
							fields : [ 'partyValue', 'partyName' ],
							data : [ [ '0', 'Текстовое поле' ],
									[ '6', 'Числовое поле' ],
									[ '1', 'Многострочное текстовое поле' ],
									[ '2', 'Флажки (Checkbox)' ],
									[ '3', 'HTML Редактор' ],
									[ '4', 'Выпадающие меню' ],
									[ '5', 'Переключатели' ],
									[ '7', 'Поле для выбора даты' ] ,
									[ '8', 'Флажки для цвета' ] ]
						}),
						name : 'Type',
						editable : false,
						xtype : 'combo',
						width : 200,
						hiddenName : 'Type',
						allowBlank : false,
						mode : 'local',
						fieldLabel : 'Тип поля',
						displayField : 'partyName',
						valueField : 'partyValue',
						lazyRender : true,
						listClass : 'x-combo-list-small'
					}, {

						xtype : 'trigger',
						fieldLabel : 'Группа товаров',
						name : 'categoryName',
						value : '',
						width : 200,
						allowBloank : false,
						triggerClass : 'x-form-search-trigger',
						onTriggerClick : function() {
							UserFields.SelectGroup();
						},

						editable : false,
						allowBlank : true

					} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 400,
					title : 'Новое поле',
					iconCls : 'add',
					id : 'UF.WindowSelectType',
					height : 250,

					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'addUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																var Id = action.result.Id;
																var Type = Ext
																		.getCmp(
																				'UF.FormSelectType')
																		.getForm()
																		.findField(
																				'Type')
																		.getValue();
																if (Type == 0) {
																	UserFields
																			.EditTextfield(Id);
																} else if (Type == 1) {
																	UserFields
																			.EditTextarea(Id);
																} else if (Type == 2) {
																	UserFields
																			.EditCheckbox(Id);
																} else if (Type == 3) {
																	UserFields
																			.EditHTMLEditor(Id);
																} else if (Type == 4) {
																	UserFields
																			.EditSelect(Id);
																} else if (Type == 5) {
																	UserFields
																			.EditRadiobox(Id);
																} else if (Type == 6) {
																	UserFields
																			.EditTextfield(Id);
																} else if (Type == 7) {
																	UserFields
																			.EditDatefield(Id);
																} else if (Type == 8) {
																	UserFields
																	.EditCheckboxColor(Id);
														}
																Ext
																		.getCmp(
																				'UF.WindowSelectType')
																		.close();

															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	UserFields.EditCheckboxValue = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditCheckboxValue',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Value',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование значения',
					iconCls : 'add',
					id : 'UF.EditWindowCheckboxValue',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFValue',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFValue'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowCheckboxValue')
																		.close();

																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																Ext
																		.getCmp('UF.GridCheckboxValues').store
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	
	//edit colot  checkbox
	UserFields.EditCheckboxValueColor = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditCheckboxValueColor',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Value',
				anchor : '90%',
				allowBlank : false
			}, {
				xtype : 'textfield',
				fieldLabel : 'Html-code цвета',
				name : 'Code',
				anchor : '90%',
				allowBlank : true
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование значения',
					iconCls : 'add',
					id : 'UF.EditWindowCheckboxValueColor',
					height : 200,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFValue',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFValue'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowCheckboxValueColor')
																		.close();

																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																Ext
																		.getCmp('UF.GridCheckboxValues').store
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	
	UserFields.EditValue = function(Item, store) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditValue',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Value',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование значения',
					iconCls : 'add',
					id : 'UF.EditWindowValue',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFValue',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFValue'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowValue')
																		.close();

																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																if (store) {
																	store
																			.reload()
																}
																;
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	
	
	
	// Форма для редактирование типа textfield
	UserFields.EditTextfield = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditTextfield',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowTextfield',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowTextfield')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	UserFields.EditDatefield = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditTextfield',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowTextfield',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowTextfield')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	// Форма для редактирование типа textarea
	UserFields.EditTextarea = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditTextarea',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowTextarea',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowTextarea')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	UserFields.EditHTMLEditor = function(Item) {
		var form = new Ext.form.FormPanel({
			id : 'UF.EditHTMLEditor',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			} ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 300,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowHTMLEditor',
					height : 150,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',

														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowHTMLEditor')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}

	// Форма для редактирование типа select
	UserFields.EditSelect = function(Item) {

		var store = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'admincp.php',
				method : 'POST'

			}),
			baseParams : {
				xaction : "LoadUFValues",
				module : Module,
				Id : Item
			},

			reader : new Ext.data.JsonReader({
				root : 'results',
				totalProperty : 'total',
				id : 'Id'
			}, [ {
				name : 'Id'

			}, {
				name : 'UFId'
			}, {
				name : 'Sort'
			}, {
				name : 'Value'
			} ])

		});
		var pagingBar = new Ext.PagingToolbar({
			pageSize : 25,
			store : store,
			paramNames : {
				start : 'start',
				limit : 'limit'
			},
			displayInfo : true

		});

		var RowAction = new Ext.ux.grid.RowActions({

			actions : [ {
				iconCls : 'delete',
				qtip : 'Удалить'
			}, {
				iconCls : 'edit',
				qtip : 'Редактировать'
			} ],
			widthIntercept : Ext.isSafari ? 4 : 2,
			id : 'actions'
		});
		RowAction.on({
			action : function(grid, record, action, row, col) {
				if (action == 'delete') {
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить это значение',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : Module,
											task : 'DeleteUFValue',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											store.reload();
										}
									});
								}
							})
				}

				else if (action == 'edit') {
					UserFields.EditSelectValue(record.data.Id, store)
				}
			}
		});

		var grid = new Ext.grid.EditorGridPanel(
				{
					store : store,
					enableColLock : false,
					clicksToEdit : 1,
					height : 200,
					title : 'Значения',
					frame : true,
					id : 'UF.GridSelectValues',

					loadMask : true,
					autoWidth : true,
					listeners : {
						"afteredit" : function(oGrid_event) {
							Ext.Ajax
									.request({
										waitMsg : 'Пожалуйста подождите...',
										url : 'admincp.php',
										params : {
											xaction : "SaveUFValue",
											Id : oGrid_event.record.data.Id,
											module : Module,
											Sort : oGrid_event.record.data.Sort
										},
										success : function(response) {
											var result = Ext
													.decode(response.responseText);
											if (result) {
												if (result.success) {
													store.commitChanges();
												}
											}

										},
										failure : function(response) {
											var result = response.responseText;
											Ext.MessageBox
													.alert('error',
															'could not connect to the database. retry later');
										}
									});
						}
					},
					columns : [ {

						header : "Id",
						width : 80,
						sortable : false,
						dataIndex : 'Id'

					}, {

						header : "Заголовок",
						width : 150,
						sortable : false,

						dataIndex : 'Value'
					}, {
						header : "Поз.",
						width : 50,

						sortable : true,
						dataIndex : 'Sort',
						editor : {
							xtype : 'numberfield'
						}
					}, RowAction ],

					sm : new Ext.grid.RowSelectionModel({
						singleSelect : true
					}),
					viewConfig : {
						forceFit : false
					},

					bbar : pagingBar,
					plugins : [ RowAction ],
					iconCls : 'icon-grid',
					split : true,
					tbar : [ {
						text : 'Добавить',
						iconCls : 'add',
						handler : function() {
							if (Item != 0) {
								Ext.Ajax.request({
									url : '/admincp.php',
									params : {
										module : Module,
										task : 'CreateUFValue',
										Id : Item

									},
									success : function(o) {
										store.reload();
										var id = Number(o.responseText);
										UserFields.EditValue(id, store);

									}

								});
							}
						}
					} ]

				});

		var form = new Ext.form.FormPanel({
			id : 'UF.EditSelect',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			}, grid ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 650,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowSelect',
					height : 450,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',
														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
								store.baseParams.Id = Item;
								store.reload();
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowSelect')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	// Форма для редактирование типа checkboxgroup
	UserFields.EditCheckbox = function(Item) {

		var store = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'admincp.php',
				method : 'POST',
				Id : Item
			}),
			baseParams : {
				xaction : "LoadUFValues",
				module : Module
			},

			reader : new Ext.data.JsonReader({
				root : 'results',
				totalProperty : 'total',
				id : 'Id'
			}, [ {
				name : 'Id'

			}, {
				name : 'UFId'
			}, {
				name : 'Sort'
			}, {
				name : 'Value'
			} ])

		});
		var pagingBar = new Ext.PagingToolbar({
			pageSize : 25,
			store : store,
			paramNames : {
				start : 'start',
				limit : 'limit'
			},
			displayInfo : true

		});

		var RowAction = new Ext.ux.grid.RowActions({

			actions : [ {
				iconCls : 'delete',
				qtip : 'Удалить'
			}, {
				iconCls : 'edit',
				qtip : 'Редактировать'
			} ],
			widthIntercept : Ext.isSafari ? 4 : 2,
			id : 'actions'
		});
		RowAction.on({
			action : function(grid, record, action, row, col) {
				if (action == 'delete') {
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить это значение',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : Module,
											task : 'DeleteUFValue',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											store.reload();
										}
									});
								}
							})
				}

				else if (action == 'edit') {
					UserFields.EditCheckboxValue(record.data.Id)
				}
			}
		});

		var grid = new Ext.grid.EditorGridPanel(
				{
					store : store,
					enableColLock : false,
					clicksToEdit : 1,
					height : 200,
					title : 'Значения',
					frame : true,
					id : 'UF.GridCheckboxValues',

					loadMask : true,
					autoWidth : true,
					listeners : {
						"afteredit" : function(oGrid_event) {
							Ext.Ajax
									.request({
										waitMsg : 'Пожалуйста подождите...',
										url : 'admincp.php',
										params : {
											xaction : "SaveUFValue",
											Id : oGrid_event.record.data.Id,
											module : Module,
											Sort : oGrid_event.record.data.Sort
										},
										success : function(response) {
											var result = Ext
													.decode(response.responseText);
											if (result) {
												if (result.success) {
													store.commitChanges();
												}
											}

										},
										failure : function(response) {
											var result = response.responseText;
											Ext.MessageBox
													.alert('error',
															'could not connect to the database. retry later');
										}
									});
						}
					},
					columns : [ {

						header : "Id",
						width : 80,
						sortable : false,
						dataIndex : 'Id'

					}, {

						header : "Заголовок",
						width : 150,
						sortable : false,

						dataIndex : 'Value'
					}, {
						header : "Поз.",
						width : 50,

						sortable : true,
						dataIndex : 'Sort',
						editor : {
							xtype : 'numberfield'
						}
					}, {

						header : "<b>Название поле</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {

							return 'uf_' + rec.data.UFId + '_' + rec.data.Id;
						}
					}, RowAction ],

					sm : new Ext.grid.RowSelectionModel({
						singleSelect : true
					}),
					viewConfig : {
						forceFit : false
					},

					bbar : pagingBar,
					plugins : [ RowAction ],
					iconCls : 'icon-grid',
					split : true,
					tbar : [ {
						text : 'Добавить',
						iconCls : 'add',
						handler : function() {
							if (Item != 0) {
								Ext.Ajax.request({
									url : '/admincp.php',
									params : {
										module : Module,
										task : 'CreateUFCheckboxValue',
										Id : Item

									},
									success : function(o) {
										store.reload();
										var id = Number(o.responseText);
										UserFields.EditCheckboxValue(id);

									}

								});
							}
						}
					} ]

				});

		var form = new Ext.form.FormPanel({
			id : 'UF.EditCheckbox',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			}, grid ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 650,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowCheckbox',
					height : 450,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',
														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
								store.baseParams.Id = Item;
								store.reload();
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowCheckbox')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	//форма для редактирования checkboxgroup color

	UserFields.EditCheckboxColor = function(Item) {

		var store = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'admincp.php',
				method : 'POST',
				Id : Item
			}),
			baseParams : {
				xaction : "LoadUFValues",
				module : Module
			},

			reader : new Ext.data.JsonReader({
				root : 'results',
				totalProperty : 'total',
				id : 'Id'
			}, [ {
				name : 'Id'

			}, {
				name : 'UFId'
			}, {
				name : 'Sort'
			}, {
				name : 'Value'
			},{
				name : 'Code'
			} ])

		});
		var pagingBar = new Ext.PagingToolbar({
			pageSize : 25,
			store : store,
			paramNames : {
				start : 'start',
				limit : 'limit'
			},
			displayInfo : true

		});

		var RowAction = new Ext.ux.grid.RowActions({

			actions : [ {
				iconCls : 'delete',
				qtip : 'Удалить'
			}, {
				iconCls : 'edit',
				qtip : 'Редактировать'
			} ],
			widthIntercept : Ext.isSafari ? 4 : 2,
			id : 'actions'
		});
		RowAction.on({
			action : function(grid, record, action, row, col) {
				if (action == 'delete') {
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить это значение',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : Module,
											task : 'DeleteUFValue',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											store.reload();
										}
									});
								}
							})
				}

				else if (action == 'edit') {
					UserFields.EditCheckboxValueColor(record.data.Id)
				}
			}
		});

		var grid = new Ext.grid.EditorGridPanel(
				{
					store : store,
					enableColLock : false,
					clicksToEdit : 1,
					height : 200,
					title : 'Значения',
					frame : true,
					id : 'UF.GridCheckboxValues',

					loadMask : true,
					autoWidth : true,
					listeners : {
						"afteredit" : function(oGrid_event) {
							Ext.Ajax
									.request({
										waitMsg : 'Пожалуйста подождите...',
										url : 'admincp.php',
										params : {
											xaction : "SaveUFValue",
											Id : oGrid_event.record.data.Id,
											module : Module,
											Sort : oGrid_event.record.data.Sort
										},
										success : function(response) {
											var result = Ext
													.decode(response.responseText);
											if (result) {
												if (result.success) {
													store.commitChanges();
												}
											}

										},
										failure : function(response) {
											var result = response.responseText;
											Ext.MessageBox
													.alert('error',
															'could not connect to the database. retry later');
										}
									});
						}
					},
					columns : [ {

						header : "Id",
						width : 80,
						sortable : false,
						dataIndex : 'Id'

					}, {

						header : "Заголовок",
						width : 150,
						sortable : false,

						dataIndex : 'Value'
					}, {
						header : "Поз.",
						width : 50,

						sortable : true,
						dataIndex : 'Sort',
						editor : {
							xtype : 'numberfield'
						}
					}, {

						header : "<b>Название поле</b>",
						width : 100,
						sortable : true,
						dataIndex : 'Id',
						renderer : function(v, tag, rec) {

							return 'uf_' + rec.data.UFId + '_' + rec.data.Id;
						}
					}, RowAction ],

					sm : new Ext.grid.RowSelectionModel({
						singleSelect : true
					}),
					viewConfig : {
						forceFit : false
					},

					bbar : pagingBar,
					plugins : [ RowAction ],
					iconCls : 'icon-grid',
					split : true,
					tbar : [ {
						text : 'Добавить',
						iconCls : 'add',
						handler : function() {
							if (Item != 0) {
								Ext.Ajax.request({
									url : '/admincp.php',
									params : {
										module : Module,
										task : 'CreateUFCheckboxValue',
										Id : Item

									},
									success : function(o) {
										store.reload();
										var id = Number(o.responseText);
										UserFields.EditCheckboxValueColor(id);

									}

								});
							}
						}
					} ]

				});

		var form = new Ext.form.FormPanel({
			id : 'UF.EditCheckbox',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			}, grid ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 650,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowCheckbox',
					height : 450,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',
														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
								store.baseParams.Id = Item;
								store.reload();
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowCheckbox')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	
	
	// Форма для редактирование типа radioboxgroup
	UserFields.EditRadiobox = function(Item) {
		var store = new Ext.data.Store({
			proxy : new Ext.data.HttpProxy({
				url : 'admincp.php',
				method : 'POST'

			}),
			baseParams : {
				xaction : "LoadUFValues",
				module : Module,
				Id : Item
			},

			reader : new Ext.data.JsonReader({
				root : 'results',
				totalProperty : 'total',
				id : 'Id'
			}, [ {
				name : 'Id'

			}, {
				name : 'UFId'
			}, {
				name : 'Sort'
			}, {
				name : 'Value'
			} ])

		});
		var pagingBar = new Ext.PagingToolbar({
			pageSize : 25,
			store : store,
			paramNames : {
				start : 'start',
				limit : 'limit'
			},
			displayInfo : true

		});

		var RowAction = new Ext.ux.grid.RowActions({

			actions : [ {
				iconCls : 'delete',
				qtip : 'Удалить'
			}, {
				iconCls : 'edit',
				qtip : 'Редактировать'
			} ],
			widthIntercept : Ext.isSafari ? 4 : 2,
			id : 'actions'
		});
		RowAction.on({
			action : function(grid, record, action, row, col) {
				if (action == 'delete') {
					Ext.MessageBox.confirm('',
							'Вы уверены что хотите удалить это значение',
							function(btn) {
								if (btn == "yes") {
									Ext.Ajax.request({
										url : 'admincp.php',
										params : {
											module : Module,
											task : 'DeleteUFValue',
											Id : record.data.Id
										},
										method : 'post',
										success : function() {
											store.reload();
										}
									});
								}
							})
				}

				else if (action == 'edit') {
					UserFields.EditRadioboxValue(record.data.Id, store)
				}
			}
		});

		var grid = new Ext.grid.EditorGridPanel(
				{
					store : store,
					enableColLock : false,
					clicksToEdit : 1,
					height : 200,
					title : 'Значения',
					frame : true,
					id : 'UF.GridRadioboxValues',

					loadMask : true,
					autoWidth : true,
					listeners : {
						"afteredit" : function(oGrid_event) {
							Ext.Ajax
									.request({
										waitMsg : 'Пожалуйста подождите...',
										url : 'admincp.php',
										params : {
											xaction : "SaveUFValue",
											Id : oGrid_event.record.data.Id,
											module : Module,
											Sort : oGrid_event.record.data.Sort
										},
										success : function(response) {
											var result = Ext
													.decode(response.responseText);
											if (result) {
												if (result.success) {
													store.commitChanges();
												}
											}

										},
										failure : function(response) {
											var result = response.responseText;
											Ext.MessageBox
													.alert('error',
															'could not connect to the database. retry later');
										}
									});
						}
					},
					columns : [ {

						header : "Id",
						width : 80,
						sortable : false,
						dataIndex : 'Id'

					}, {

						header : "Заголовок",
						width : 150,
						sortable : false,

						dataIndex : 'Value'
					}, {
						header : "Поз.",
						width : 50,

						sortable : true,
						dataIndex : 'Sort',
						editor : {
							xtype : 'numberfield'
						}
					}, RowAction ],

					sm : new Ext.grid.RowSelectionModel({
						singleRadiobox : true
					}),
					viewConfig : {
						forceFit : false
					},

					bbar : pagingBar,
					plugins : [ RowAction ],
					iconCls : 'icon-grid',
					split : true,
					tbar : [ {
						text : 'Добавить',
						iconCls : 'add',
						handler : function() {
							if (Item != 0) {
								Ext.Ajax.request({
									url : '/admincp.php',
									params : {
										module : Module,
										task : 'CreateUFValue',
										Id : Item

									},
									success : function(o) {
										store.reload();
										var id = Number(o.responseText);
										UserFields.EditValue(id, store);

									}

								});
							}
						}
					} ]

				});

		var form = new Ext.form.FormPanel({
			id : 'UF.EditRadiobox',
			frame : true,
			border : false,

			height : 550,
			plain : true,
			autoScroll : true,
			labelAlign : 'top',
			items : [ {
				xtype : 'hidden',
				name : 'Id',
				value : 0
			}, {
				xtype : 'textfield',
				fieldLabel : 'Название поля',
				name : 'Title',
				anchor : '90%',
				allowBlank : false
			}, grid ]

		});
		return new Ext.Window(
				{
					modal : true,
					border : false,
					width : 650,
					title : 'Создание/Редактирование поля',
					iconCls : 'add',
					id : 'UF.EditWindowRadiobox',
					height : 450,
					listeners : {
						'show' : function() {
							if (Item != 0) {
								form
										.getForm()
										.load(
												{
													url : '/admincp.php',
													params : {
														module : Module,
														task : 'LoadUFField',
														Id : Item
													},
													waitMsg : 'Подождите.. идёт загрузка данных'
												});
								store.baseParams.Id = Item;
								store.reload();
							}
						}
					},
					layout : 'fit',
					items : [ form ],
					buttons : [ {
						text : 'Сохранить',
						iconCls : 'apply',
						handler : function() {
							if (form.getForm().isValid()) {
								tinyMCE.triggerSave();
								form
										.getForm()
										.submit(
												{
													url : '/admincp.php',
													method : 'post',
													params : {
														module : Module,
														task : 'SaveUFField'
													},
													success : function(form,
															action) {
														if (action.result) {
															if (action.result.success) {
																UserFields.Store
																		.reload();
																Ext
																		.getCmp(
																				'UF.EditWindowRadiobox')
																		.close();
																App
																		.setAlert(
																				'',
																				'Категория успешно создана');
																UserFields.Tree.root
																		.reload();
															} else {
																App
																		.setAlert(
																				'',
																				'Во время обработки данных произошла ошибка');
															}
														} else {
															App
																	.setAlert(
																			'',
																			'Во время обработки данных произошла ошибка');
														}
													},
													failure : function() {
														App
																.setAlert('',
																		'Во время обработки данных произошла ошибка');
													}
												});
							} else {
								App
										.setAlert('',
												'Проверьте правильность заполнения формы');
							}
						}
					} ]
				}).show();
	}
	UserFields.render = false;
	UFModules[Module] = UserFields;
	return {
		tab : {
			title : 'Характеристики',
			listeners : {
				render : function() {
					UserFields.Store.load();
					UserFields.Tree.root.expand();
					UserFields.render = true;
				}
			},
			border : false,
			layout : 'border',
			items : [ UserFields.Tree, UserFields.Grid ]
		},
		init : function() {
			if (UserFields.render) {
				UserFields.Store.load();
				UserFields.Tree.root.expand();
			}
		},
		uf : UserFields
	}

}

/**
	* @class extswfuploadbtn
	* @extends Ext.Button
	* simple button extended with an swfupload multifile dialog and realtime progress bars
	* @param {Object} conf The configuration object
	* @cfg {String} text The text to place on the button
	* @cfg {Object} postparams The params object to be passed as POST var to backend
	* @cfg {Integer} minWidth The minimum width of the button
	* @cfg {String} tooltop The tooltip for the button (not working now...)
	* @cfg {String} id The Ext ID to asign to the button
	* @cfg {Integer} fileuploadlimit Maximum number of file to upload per session, default: 20
	* @cfg {Integer} filequeuelimit Maximum number of files to queue per session, default: 20
	* @cfg {Boolean} hidden Whether to render hidden or not, default: true
	* @cfg {Integer} statuswindowheight The height of the status window
	* @cfg {Boolean} disabled Whether to render disabled or not, default: true
	* @cfg {String} placeholder The DOM name to use for flash object, auto added to 
	* the button's div, default btnUploadHolder
	* @cfg {Boolean} jsonresponses Server returns json encoded stream, default: true
	* @cfg {String} jsonresponsefield Field name of json server response, default: msg
	* @cfg {String} uploadurl The url to use for backend processing of uploads, default: null
	* @cfg {String} flashurl The url to use for access to swfupload.swf, default: null
	* @cfg {String} filesizelimit The maximum file size allowed, default "2 MB"
	* @cfg {String} buttonimageurl The url to retrieve the FLASH button image, will overlay the default Ext button, default:  null
	* @cfg {Constant} buttonwindowmode The mode for the FLASH screen, default: SWFUpload.WINDOW_MODE.TRANSPARENT
	* @cfg {Function} uploadstarthandler Override local handler, default: myCustomUploadStartHandler
	* @cfg {Function} uploadsuccesshandler Override local handler, default: myCustomUploadSuccessHandler
	* @cfg {Function} uploadprogresshandler Override local handler, default: myCustomProgressHandler
	* @cfg {Function} filequeuedhandler Override local handler, default: myCustomFileQueuedHandler
	* @cfg {Function} filedialogcompletehandler Override local handler, default: myCustomDialogCompleteHandler
	* @cfg {Function} filedialogstarthandler Override local handler, default: myCustomFileDialogStartHandler
	* @cfg {Function} filequeueerrorhandler Override local handler, default: myCustomFileQueueErrorHandler
	* @cfg {Function} fileuploaderrorhandler Override local handler, default myCustomFileUploadErrorHandler
	* @cfg {Boolean} hideoncomplete Whether to hide the display window on completion or not, default: false
	* @cfg {Boolean} isSingle Whether or not to allow only single file upload or not, default: false
	* @cfg {String} iconpath Path to icon collection, default: '/fw-sales/wp-content/plugins/funeralworks_obituary_plugin/extjs/resources/images/icons/'
	*
	* needs following css for correct positioning of button overlay
	*	.swfupload {
	*	    position: absolute;
	*	    z-index: 1;
	*	    vertical-align: top;
	*	    text-align: center;
	*	}
	*/

	
	Ext.ux.swfbtn = function(cfg){
		this.postparams = {};
	    this.text = cfg.text || "Add Slides";
	    this.minWidth = cfg.minWidth || 84;
	    this.tooltip = cfg.tooltip ||  '';
	    this.id = cfg.id || 'addmemphotobtn';
	    this.hidden = cfg.hidden || false;
	    this.disabled = cfg.disabled || false;
	    this.statuswindowheight = cfg.statuswindowheight || 150;
	    this.icon = cfg.icon || '';
	    this.listeners = cfg.listeners || {};
	    
	   
	    var iconpath = cfg.iconpath || '/core/icons/';
	    var swfu, selectedrow, msg, numfiles=0, totalbytes=0, extrapostparams=new Array();
	    var custompostparams = cfg.customparams || '';
	    
    	var fileprogressbar = new Ext.ProgressBar({
    		text: 'Загрузка',
    		hidden: true,
    		height: 25,
    		autoWidth: true
    	});
    	
    	var o = new Ext.ProgressBar({
    		text: 'Загрузка',
    		height: 25,
    		hidden: true,
    		autoWidth: true
    	});
    	
	    var filestore = new Ext.data.JsonStore({
    		data: [],
    		fields: ['id', 'index', 'name', 'size', 'type', 'creationdate', 'modificationdate', 'filestatus']
    	});

		var rec = Ext.data.Record.create([
			{name: 'id', mapping: 'id'},
			{name: 'index', mapping: 'index'},
			{name: 'name', mapping: 'name'},
			{name: 'size', mapping: 'size'},
			{name: 'type', mapping: 'type'},
			{name: 'creationdate', mapping: 'creationdate'},
			{name: 'modificationdate', mapping: 'modificationdate'},
			{name: 'filestatus', mapping: 'filestatus'}
		]);
		
		function translateSwfStatus(val, meta, rec, row, col, store){
			switch(val){
				case -1 :
					text = "В очереди";
				break;
				case -2:
					text = "Загрузка";
				break;
				case -3:
					text = "Ошибка";
				break;
				case -4:
					text = "Загружен";
				break;
				default:
					text = val;
				break;
			}
			return(text);
		}
		
		var cm = new Ext.grid.ColumnModel({
			defaults: {
				sortable: true
			},
			columns: [
				{header: 'Имя', width: 150, dataIndex: 'name'},
				{header: 'Размер', width: 60, dataIndex: 'size'},
				{header: 'Создан', width: 60, dataIndex: 'creationdate', renderer:function(v){
					return new Date(v).format('d.m.Y');
				}},
				{id: 'filestatus', header: 'Статус', width: 100, dataIndex: 'filestatus', renderer: translateSwfStatus}
			]
		});
		
		var addfiles = new Ext.Button({
			text: cfg.isSingle ? 'Выбрать файл(ы)':'Выбрать файл(ы)',
			icon: iconpath + '/add.png',
			style: 'padding-right: 10px;',
			listeners: {
				'render': function(btn) {
		        	// create a div to render to
		    		Ext.get(this.id).child('em').insertFirst({tag: 'span', id: cfg.placeholder || 'btnUploadHolder'});
		    		/*if (Ext.getCmp('additemshop'))
					{
		    			cfg.postparams.ItemID = Ext.getCmp('Catalog.EditRecord').getForm().findField('id').getValue();
						

					}*/
					if(Ext.getCmp('Catalog.EditRecord'))
					{
						cfg.postparams.ItemID= Ext.getCmp('Catalog.EditRecord').getForm().findField('Id').getValue();
						
					}
		    		var settings = {
			    		button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
				    	button_placeholder_id: cfg.placeholder || 'btnUploadHolder',
				    	button_image_url: cfg.buttonimageurl || '',
				    	button_width: cfg.buttonwidth || Ext.get(this.id).getWidth(),
				    	button_height: cfg.buttonheight || 20,
						upload_url: cfg.uploadurl || '',
						flash_url: cfg.flashurl || '',
						post_params: cfg.postparams,	// actaully set dynamically in local handlers
						file_types: cfg.filetypes || "",
						file_types_description: cfg.filetypesdescription || "Изображения",
						file_size_limit: cfg.filesizelimit || "2 MB",
						file_post_name: cfg.filepostname || 'Filedata',
						file_upload_limit: cfg.fileuploadlimit || 0,
						file_queue_limit: cfg.filequeuelimit || 0,
						file_dialog_start_handler: cfg.filedialogstarthandler || myCustomFileDialogStartHandler,
						upload_start_handler: cfg.uploadstarthandler || myCustomUploadStartHandler,
						upload_success_handler: cfg.uploadsuccesshandler || myCustomUploadSuccessHandler,
						upload_progress_handler: cfg.progresshandler || myCustomProgressHandler,
						file_queued_handler: cfg.filequeuedhandler || myCustomFileQueuedHandler,
						file_dialog_complete_handler: cfg.dialogcompletehandler || myCustomDialogCompleteHandler,
						file_queue_error_handler: cfg.filequeueerror || myCustomFileQueueErrorHandler,
						upload_error_handler: cfg.uploaderrorhandler || myCustomUploadErrorHandler,
						upload_complete_handler: cfg.uploadcompletehandler || myCustomUploadCompleteHandler,
						custom_settings: cfg.customsettings || {},
						debug: cfg.debug || false
					};
		    		var serverMessages="";	    	

		    		function myCustomFileDialogStartHandler(){
		    			
				    }
			
				    function myCustomDialogCompleteHandler(numselected, numqueued, num){
				    	// display the progress bar window
				    	if(numselected) {
				    		if(cfg.isSingle && num > 1){
				    			myCustomFileQueueErrorHandler('', SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED, '1');
				    			clearqueue();
				    		} else {
						    	totalbytessent = 0;
						    	swffilessent = 0;
						    	lastbytecount = 0;
					    		upload.enable();
					    		clearq.enable();
				    		}
					    }
				    }
			
				    function myCustomFileQueuedHandler(f){
				    	var myrec = new rec({
				    		id: f.id,
				    		index: f.index,
				    		name: f.name,
				    		size: f.size,
				    		type: f.type,
				    		creationdate: f.creationdate,
				    		modificationdate: f.modificationdate,
				    		filestatus: f.filestatus
				    	});
				    	filestore.add(myrec);
				    	totalbytes += f.size;
				    	if(cfg.isSingle) {
				    		addfiles.disable();
				    	}
				    }
			
					function myCustomFileQueueErrorHandler(file, ec, m){
						try { // Handle this error separately because we don't want to create a FileProgress element for it.                 
							switch (ec) {
				                 case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
		                         	msg = ": You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file."));                         
		                         case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:                        
		                         	msg = " is too big.  Not queued for upload.";                         
		                         	this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);                         
		                         case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:                         
		                         	msg = " is empty.  Not queued for upload.  Please select another file.";                         
		                         	this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);                         
		                         case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:                         
		                         	msg = " is not an allowed file type. Not queued for upload.";                         
		                         	this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);                         
		                         default:                         
		                         	msg = ": An error occurred with the upload. Try again later.";                         
		                         	this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);                         
		                     }         
		                 } catch (e) {         } 	
		                Ext.MessageBox.show({
		                	title: 'Ошибка при выборе файлов',
		                	msg: (file ? 'File '+file.name+msg:cfg.isSingle ? 'Слишком много файлов выбрано.  Максимально '+m+', ':"Неизвестная ошибка"),
		                	buttons: Ext.MessageBox.OK,
		                	scope: this
		                });
						return;
					}
					
					function myCustomUploadErrorHandler(file, e, m){
						Ext.MessageBox.show({
							title: 'Ошибка при загрузке',
							msg: m,
							buttons: Ext.MessageBox.OK,
							fn: function() {
								clearq.disable();
								if(!cfg.isSingle) {
									o.updateProgress(0, 'Overall Progress');
									o.hide();
								}
								fileprogressbar.updateProgress(0);
								fileprogressbar.hide();
							}
						});
					}
					
					function myCustomUploadStartHandler(file){
						var pp = cfg.postparams;
						var parms = extrapostparams[swffilessent];
						Ext.each(parms, function(item, ind, all){
							eval("pp."+ item.id +" = item.val;");
						});
			    		numfiles = filestore.getCount();
			    		swfu.setPostParams(pp);
			    		if(!o.isVisible() && !cfg.isSingle) o.show();
			    		if(!fileprogressbar.isVisible()) fileprogressbar.show();
			    		cancelupload.enable();
			    		clearq.disable();
				    	var r = filestore.findExact('id', file.id);
				    	if(r != -1) {
				    		var rec = filestore.getAt(r);
				    		rec.set('filestatus', file.filestatus);
				    	}
			    		fileprogressbar.updateProgress(0, 'Отправка файла '+file.name+' (файл '+(swffilessent+1)+' из '+numfiles+')');
				    	return true;
				    }
			
				    function myCustomUploadSuccessHandler(file, server_data, receivedResponse){
				    	lastbytecount = 0;
				    	++swffilessent;
			    		if(cfg.jsonresponses) {
			    			var json = Ext.util.JSON.decode(server_data);
			       			serverMessages += json.msg;
			    		} else {
			    			serverMessages += server_data;
			    		}
			       		serverMessages += "<br><br>";
				    	if(swffilessent == filestore.getCount()) {
				    		
				    		filestore.removeAll();
			    			numfiles = 0;
			    			totalbytes = 0;
			    			fileprogressbar.hide();
			    			o.hide();
			    			clearq.disable();
			    			upload.disable();
			    			delfile.disable();
			    			extrapostparams = [];
							// reload store data if we have a store defined
				    		// note: must have global scope
				    		if(cfg.store){
					    		eval(cfg.store+'.reload();');
					    	}
					    	if(cfg.hideoncomplete) gridwin.hide();
				    	}
				    	if (Ext.getCmp('additemshop'))
						{
			    			Ext.getCmp('ImaAddItem').store.reload();
							

						}
						if(Ext.getCmp('Catalog.EditRecord'))
						{
							Catalog.ImagesStore.reload();
							
						}
			    		var settings
				    	cancelupload.disable();
				    }
				    
				    function myCustomProgressHandler(file, b, t){
				    	// update progress bar with new information
				    	totalbytessent += b - lastbytecount;
				    	fileprogressbar.updateProgress((b/t), 'Отправка файла '+file.name+' (файл '+(swffilessent+1)+' из '+numfiles+') ('+b+'/'+t+')');
				    	if(!cfg.isSingle) o.updateProgress((totalbytessent/totalbytes));
				    	lastbytecount = b;
				    }
				    
				    function myCustomUploadCompleteHandler(f){
				    	var r = filestore.findExact('id', f.id);
				    	if(r != -1) {
				    		var rec = filestore.getAt(r);
				    		rec.set('filestatus', f.filestatus);
				    	}
				    	if(cfg.isSingle){
				    		gridwin.hide();
				    	}
				    }
				    
			    	swfu = new SWFUpload(settings);
				}
			}
		});

		var delfile = new Ext.Button({
			text: cfg.isSingle ? 'Удалить файл(ы)':'Удалить файл(ы)',
			icon: iconpath+'/delete.png',
			style: 'padding-right: 10px;',
			disabled: true,
			handler: function(){
				var f = filestore.getAt(selectedrow);
				swfu.cancelUpload(f.data.id, false);
				filestore.removeAt(selectedrow);
				addfiles.enable();
				this.disable();
			}
		});
		
		var cancelupload = new Ext.Button({
			text: 'Отменить загрузку',
			icon: iconpath+'/cancel.png',
			style: 'padding-right: 10px;',
			disabled: true,
			handler: clearqueue
		})
		
		var upload = new Ext.Button({
			text: 'Загрузить',
			icon: iconpath+'/arrow_up.png',
			style: 'padding-right: 10px;',
			disabled: true,
			handler: function() {
	    		if(custompostparams){
		   			var formwindows = [];
	    			var results = {};
	    			var i=0;
	    			var numqueued = filestore.getCount();
	    			var filesprocessed = 0;
	    			// var tform = {}; 
	    			// while(i < numqueued){
	    			filestore.each(function(rec){
		    			eval("var formid = 'tform"+i+"';");
		    			eval("var winid = 'swfuploadwin"+rec.data.id+"';");
		    			eval("var buttonid = '"+formid+"button0';");
		    			var btn = new Ext.Button({
    						text: 'Save Data',
    						id: buttonid,
    						listeners: {
    							'click': function(){
	    							filesprocessed += 1;
	    							eval("var t = Ext.getCmp('tform"+i+"').getForm();");
	    							var tt = t.getValues();
	    							var record = [];
	    							Ext.each(t.items.items, function(item, ind, all){
	    								var id = item.name;
	    								var value = t.findField(id).getValue();
	    								record.push({id: id, val: value});
	    							});
									extrapostparams[i] = record;
									formwindows[i].close();
	    							if(filesprocessed == numqueued){
	    								swfu.startUpload();
	    							} else {
	    								if(i++ < numqueued) {
	    									formwindows[i].show();
	    								
	    								}
	    							}
    							}
    						}		    			
		    			});
		    			
	    				if(!cfg.form) {
		    				var tform = new Ext.form.FormPanel({
		    					id: formid,
		    					layout: 'form',
		    					bodyStyle: 'padding: 10px;',
		    					title: rec.data.name+': Additional Information Required',
		    					autoWidth: true,
		    					autoHeight: true,
		    					buttons: [btn],
		    					keys: [{
							    	key: [10,13],
							    	handler: function(t, e){
							    		this.fireEvent('click');
							    	},
							    	scope: btn
		    					}]
		    				});
		    				Ext.each(custompostparams, function(item, index, all){
		    					switch(item.fieldtype){
		    						case 'checkbox':
		    							var fld = new Ext.form.Checkbox({
		    								name: item.title,
				    						fieldLabel: item.label,
				    						allowBlank: item.allowBlank || true
		    							});
		    						break;
		    						default:
				    					var fld = new Ext.form.TextField({
				    						name: item.title,
				    						fieldLabel: item.label,
				    						maxLength: item.maxlength,
				    						allowBlank: item.allowBlank || true
				    					});
				    				break;
		    					}
		    					tform.add(fld);
			    			});
	    				} else {
	    					// preconfigured form passed in as argument
	    					var tform = cfg.form.cloneConfig({
	    						id: formid,
	    						bodyStyle: 'padding: 10px;',
	    						buttons: [btn],
	    						keys: [{
							    	key: [10,13],
							    	handler: function(t, e){
							    		this.fireEvent('click');
							    	},
							    	scope: btn
		    					}]
	    					});
	    				}
	    			
	    				tform.on({
	    					'afterrender': function() {
			    				var w = this.el.getWidth();
			    				var z = this.el.getHeight();
			    				this.ownerCt.setWidth((w+50));
			    				this.ownerCt.setHeight((z+30));
			    			}
	    				});
	    				
		    			var winoptions = {
		    				id: winid, 
		    				layout: 'fit',
		    				autoHeight: true, 
		    				autoWidth: false,
		    				width: 300,
		    				autoScroll: true,
		    				closeAction: 'close', 
		    				hidden: true, 
		    				modal: true, 
		    				items: [tform]
		    			}; 
		    			formwindows[i] = new Ext.Window(winoptions);
		    			formwindows[i].on({
		    				'show': function() {
			    				var form = Ext.getCmp(formid).getForm();
			    				var fld = form.items.items[0];
			    				fld.focus(false, 50);
			    			}
		    			});
		    			i++;
		    		});
		    		
		    		i = 0;
		    		formwindows[0].show();
	    		} else {
	    			swfu.startUpload();
	    		}
	    		
			}
			// scope: Ext.getCmp(cfg.id || 'addmemphotobtn')
		});
		
		function clearqueue(){
			filestore.each(function(rec){
				var id = rec.data.id;
				swfu.cancelUpload(id, false);
			});
			filestore.removeAll();
			clearq.disable();
			upload.disable();
		}
		
		var clearq = new Ext.Button({
			text: 'Очистить список',
			disabled: true,
			hidden: cfg.isSingle ? true:false,
			icon: iconpath+'/world_delete.png',
			handler: clearqueue
		});
		
		var grid = new Ext.grid.GridPanel({
			header: false,
			store: filestore,
			cm: cm,
			viewConfig: {
				forceFit: true
			},
			width: 686,
			height: cfg.isSingle ? 100:380,
			autoScroll: true,
			frame: true,
			stripeRows: true,
			bbar: [addfiles, delfile, clearq, cancelupload, upload],
			listeners: {
				rowclick: function(g, row, e){
					selectedrow = row;
					delfile.enable();
					
				}
			}
		});

		var errorpanel = new Ext.Panel({
			width: 600,
			autoHeight: true
		});

		var gridwin = new Ext.Window({
			title: 'Загрузка изображений',
			closeAction: 'hide',
			autoScroll: true,
			modal: true,
			width: 700,
			autoHeight: true,
			items: [o, errorpanel, grid, fileprogressbar]
		});
		
		gridwin.on('show', function(){
			addfiles.enable();
			fileprogressbar.hide();
			fileprogressbar.updateProgress(0);
			cancelupload.disable();
			upload.disable();
			if(cfg.isSingle) {
				filestore.removeAll();
			}
		});
		
	    Ext.ux.swfbtn.superclass.constructor.call(this);
	
	    this.addListener({
	    	'click': function() {
	    		gridwin.show();
	    	},
			scope: this
		}); 
	}
	
Ext.extend(Ext.ux.swfbtn, Ext.Button);
