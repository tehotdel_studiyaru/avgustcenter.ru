var contacts = {};

contacts.updateRecord = function(oGrid_event) {
	Ext.Ajax.request({
		waitMsg : 'Пожалуйста подождите...',
		url : 'admincp.php',
		params : {
			xaction : "Update",
			id : oGrid_event.record.data.id,
			active : oGrid_event.record.data.active,
			module : 'contacts',
			pos : oGrid_event.record.data.pos
		},
		success : function(response) {
			var result = eval(response.responseText);
			switch (result) {
			case 33:
				contacts.base.commitChanges(); // changes successful, get rid
				// of the red triangles
				contacts.base.reload(); // reload our datastore.
				break;
			default:
				Ext.MessageBox.alert('Ошибка',
						'Не возможно сохранить изменения...');
				break;
			}
		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
};
contacts.UpdateStatus = function(Id, Status) {
	Ext.Ajax.request({
		waitMsg : 'Пожалуйста подождите...',
		url : 'admincp.php',
		params : {
			xaction : "Update",
			id : Id,
			active : Status,
			module : 'contacts',
		},
		success : function(response) {
			var result = eval(response.responseText);
			switch (result) {
			case 33:
				contacts.base.getById(Id).set('active', Status)
				contacts.base.commitChanges(); // changes successful, get rid
				// of the red triangles

				break;
			default:
				Ext.MessageBox.alert('Ошибка',
						'Не возможно сохранить изменения...');
				break;
			}
		},
		failure : function(response) {
			var result = response.responseText;
			Ext.MessageBox.alert('error',
					'could not connect to the database. retry later');
		}
	});
}

contacts.addedit = function(id, copy) {

	function UploadPhoto() {
		var fotoupload2 = new Ext.FormPanel(
				{

					fileUpload : true,
					width : '100%',
					frame : true,
					// border:false,

					shim : true,
					bodyStyle : 'padding: 10px 10px 0 10px;',
					labelWidth : 50,
					items : [ {
						xtype : 'fileuploadfield',
						emptyText : 'Выберите файл для загрузки',
						fieldLabel : 'Файл',
						name : 'photo-path',
						width : '500',
						anchor : '95%',
						allowBlank : false,
						buttonCfg : {
							text : ' ',
							iconCls : 'upload-icon'
						}
					} ],
					buttonAlign : 'center',
					buttons : [ {
						text : 'Загрузить',
						handler : function() {
							if (fotoupload2.getForm().isValid()) {
								var idd = Ext.getCmp('contacts.form').getForm()
										.findField('id').getValue();

								fotoupload2
										.getForm()
										.submit(
												{
													url : 'admincp.php',
													method : 'POST',
													params : {
														id : idd,
														module : 'contacts',
														task : 'UploadPhoto'
													},
													waitTitle : 'Загрузка фотографии',
													waitMsg : 'Пожалуйста подождите, идёт загрузка фотографии...',
													success : function(
															fotoupload, o) {
														// msg('Success',
														// 'Processed file
														// "'+o.result.file+'"
														// on the server');
														// Ext.getCmp('contacts.form.tabimages.grid').store.reload();
														Ext
																.getCmp(
																		'contacts.UploadWindow')
																.close();

													},
													failure : function(
															fotoupload2, o) {
														Ext.MessageBox
																.alert(
																		'Ошибка',
																		'Не удалось загрузить фотографию');
													}
												});
							}
						}
					} ]
				});
		var shopupfileswin2 = new Ext.Window({
			// applyTo : 'hello-win',
			layout : 'fit',
			shim : false,
			modal : true,
			title : 'Загрузка фотографий',
			id : 'contacts.UploadWindow',
			width : 400,
			height : 200,
			autoScroll : true,
			closeAction : 'close',
			plain : true,
			listeners : {
				'close' : function() {
					Ext.getCmp('contacts.form.tabimages.grid').store.reload();
				}
			},
			items : [ fotoupload2 ],
			buttons : [ {
				text : 'Закрыть',
				handler : function() {
					Ext.getCmp('contacts.form.tabimages.grid').store.reload();
					Ext.getCmp('contacts.UploadWindow').close();
				}
			} ]
		}).show();
	}

	var base = new Ext.data.Store({
		proxy : new Ext.data.HttpProxy({
			url : 'admincp.php',
			method : 'POST'
		}),
		baseParams : {
			xaction : "Listing_Images",
			module : 'contacts'
		},

		reader : new Ext.data.JsonReader({
			root : 'results',
			totalProperty : 'total',
			id : 'id'
		}, [ {
			name : 'id',
			mapping : 'id'
		}, {
			name : 'image',
			mapping : 'image'
		}, {
			name : 'file',
			mapping : 'file'
		}, {
			name : 'osn',
			mapping : 'osn'
		}, {
			name : 'pos',
			mapping : 'pos'
		} ])

	});
	// End Base for ArticleGrid

	// PagingBar for articlegrid
	var pagingBar = new Ext.PagingToolbar({
		pageSize : 25,
		store : base,
		paramNames : {
			start : 'start',
			limit : 'limit'
		},
		displayInfo : true

	});
	// End
	// ArtclisRowAction

	var RowAction = new Ext.ux.grid.RowActions({

		actions : [ {
			iconCls : 'apply',
			qtip : 'Сделать основной'
		}, {
			iconCls : 'delete',
			qtip : 'Удалить'
		} ],
		widthIntercept : Ext.isSafari ? 4 : 2,
		id : 'actions'
	});
	RowAction.on({
		action : function(grid, record, action, row, col) {
			if (action == 'delete') {
				Ext.MessageBox.confirm('',
						'Вы уверены что хотите удалить эту фотографию',
						function(btn) {
							if (btn == "yes") {
								Ext.Ajax.request({
									url : 'admincp.php',
									params : {
										module : 'contacts',
										task : 'deleteImage',
										id : record.data.id
									},
									method : 'post',
									success : function() {
										base.reload();
									}
								});
							}
						})
			}
			if (action == "apply") {
				Ext.Ajax.request({
					url : 'admincp.php',
					params : {
						module : 'contacts',
						task : 'setOsnImage',
						id : record.data.id
					},
					method : 'post',
					success : function() {
						base.reload();
					}
				});
			}
		}
	});

	var grid = new Ext.grid.EditorGridPanel(
			{
				store : base,

				id : 'contacts.form.tabimages.grid',
				enableColLock : false,
				clicksToEdit : 1,
				height : '80%',
				frame : true,
				loadMask : true,
				autoWidth : true,
				listeners : {
					"afteredit" : function(oGrid_event) {
						Ext.Ajax
								.request({
									waitMsg : 'Пожалуйста подождите...',
									url : 'admincp.php',
									params : {
										xaction : "UpdateImagePos",
										id : oGrid_event.record.data.id,
										module : 'contacts',
										pos : oGrid_event.record.data.pos
									},
									success : function(response) {
										var result = eval(response.responseText);
										switch (result) {
										case 33:
											Ext
													.getCmp('contacts.form.tabimages.grid').store
													.commitChanges();

											break;
										default:
											Ext.MessageBox
													.alert('Ошибка',
															'Не возможно сохранить изменения...');
											break;
										}
									},
									failure : function(response) {
										var result = response.responseText;
										Ext.MessageBox
												.alert('error',
														'could not connect to the database. retry later');
									}
								});
					}
				},
				columns : [
						{
							id : 'image',
							header : "",
							width : 200,
							sortable : true,
							dataIndex : 'image',
							renderer : function(value) {
								return "<center><img src='/thumbs/80x80/files/contacts/"
										+ value + "' width='80'></center>";
							}
						}, {
							id : 'file',
							header : "Файл",
							width : 150,
							sortable : true,
							dataIndex : 'file'
						}, {
							header : "Поз.",
							width : 150,
							sortable : true,
							dataIndex : 'pos',
							editor : new Ext.form.NumberField()
						}, {
							id : 'osn',
							header : "",
							width : 150,
							sortable : true,
							dataIndex : 'osn',
							renderer : function(value) {
								if (value == 1) {
									return "<b>Основная</b>";
								}
								return "";
							}
						}, RowAction ],

				sm : new Ext.grid.RowSelectionModel({
					singleSelect : true
				}),
				viewConfig : {
					forceFit : false
				},
				height : 150,
				bbar : pagingBar,
				plugins : RowAction,
				iconCls : 'icon-grid',
				split : true,
				tbar : [ {
					text : 'Загрузить новую фотографию',
					handler : function() {
						UploadPhoto();

					},
					iconCls : 'add'
				} ]

			});

	var form = new Ext.FormPanel({
		id : 'contacts.form',
		width : 890,

		frame : true,
		labelAlign : 'top',
		items : [ {
			xtype : 'tabpanel',
			activeItem : 0,
			defaults : {
				frame : true,
				width : 800,
				height : 550,
				bodyStyle : 'padding:10px;'
			},
			items : [ {
				title : 'Описание',
				layout : 'form',
				autoScroll : true,
				iconCls : 'viewlist',
				items : [ {
					layout : 'table',
					layoutConfig : {
						columns : 4,
						tableAttrs : {
							style : {
								width : 880
							}
						}
					},
					defaults : {
						width : 250
					},
					items : [ {
						layout : 'form',
						width : 400,
						colspan : 2,
						items : [ {

							xtype : 'textfield',
							fieldLabel : 'Название',
							name : 'name',
							anchor : '95%'
						} ]
					} ]
				}, {
					xtype : 'textarea',
					name : 'address',
					fieldLabel : 'Адрес',
					height : 100,
					anchor : '95%'
				}, {
					xtype : 'textarea',
					name : 'phone',
					fieldLabel : 'Телефоны',
					height : 100,
					anchor : '95%'
				}, {
					xtype : 'textarea',
					name : 'email',
					fieldLabel : 'Email',
					height : 100,
					anchor : '95%'
				}, {
					xtype : 'textarea',
					name : 'desc',
					fieldLabel : 'Код карты',
					height : 100,
					anchor : '95%'
				}, {
					xtype : 'hidden',
					name : 'id'
				} ]
			} ]
		} ]
	});
	new Ext.Window({
		width : 900,
		height : 640,
		frame : true,
		constrainHeader : true,
		closeAction : 'close',
		modal : true,
		id : 'contacts.WindowAddEdit',
		items : [ form ],
		listeners : {
			"show" : function() {
				if (id == 0) {
					Ext.Ajax.request({
						url : 'admincp.php',
						waitMsg : 'Подождите пожалуйста',
						method : 'POST',
						params : {
							module : 'contacts',
							task : 'NewItem'
						},
						success : function(response) {

							var o = eval(response.responseText);
							Ext.getCmp('contacts.form').getForm().findField(
									'id').setValue(o);
							base.baseParams = {
								xaction : "Listing_Images",
								module : 'contacts',
								dd : o
							};
						}
					});
				} else if (copy) {
					Ext.Ajax.request({
						url : 'admincp.php',
						waitMsg : 'Подождите пожалуйста',
						method : 'POST',
						params : {
							copy : true,
							id : id,
							module : 'contacts',
							task : 'NewItem'
						},
						success : function(response) {

							var o = eval(response.responseText);
							Ext.getCmp('contacts.form').getForm().findField(
									'id').setValue(o);
							base.baseParams = {
								xaction : "Listing_Images",
								module : 'contacts',
								dd : o
							};
						}
					});
				} else {
					base.baseParams = {
						xaction : "Listing_Images",
						module : 'contacts',
						dd : id
					};

				}
			}
		},
		buttonAlign : 'right',
		buttons : [ {
			text : 'Сохранить',
			iconCls : 'accept',
			handler : function() {
				Ext.ux.TinyMCE.initTinyMCE();
				tinyMCE.triggerSave();
				form.getForm().submit({
					url : 'admincp.php',
					method : 'POST',
					params : {
						module : 'contacts',
						task : 'save'
					},
					waitMsg : 'Пожалуйста подождите',
					success : function() {
						Ext.getCmp('contacts.WindowAddEdit').close();
						contacts.base.reload();

					}
				});
			}
		} ]
	}).show();
}

contacts.base = new Ext.data.Store({

	proxy : new Ext.data.HttpProxy({
		url : 'admincp.php',
		method : 'POST'
	}),
	baseParams : {
		xaction : "Listing",
		module : 'contacts'
	},

	reader : new Ext.data.JsonReader({
		root : 'results',
		totalProperty : 'total',
		id : 'id'
	}, [ {
		name : 'id',
		mapping : 'id'
	}, {
		name : 'pos',
		mapping : 'pos'
	}, {
		name : 'name',
		mapping : 'name'
	}, {
		name : 'notice',
		mapping : 'notice'
	}, {
		name : 'desc',
		mapping : 'desc'
	}, {
		name : 'date2',
		mapping : 'date2'
	}, {
		name : 'date',
		mapping : 'date'
	}, {
		name : 'address',
		mapping : 'address'
	}, {
		name : 'phone',
		mapping : 'phone'
	}, {
		name : 'email',
		mapping : 'email'
	}, {
		name : 'KeysPage',
		mapping : 'KeysPage'
	}, {
		name : 'active',
		type : 'int',
		mapping : 'active'
	}, {
		name : 'UpdatedDate'
	}, {
		name : 'CreatedDate'
	}, {
		name : 'UpdatedDate2'
	}, {
		name : 'CreatedDate2'
	}, {
		name : 'H1'
	}, {
		name : 'Tags'
	}, {
		name : 'url'
	} ])

});

// PagingBar for articlegrid
contacts.pagingBar = new Ext.PagingToolbar({
	pageSize : 25,
	store : contacts.base,
	paramNames : {
		start : 'start',
		limit : 'limit'
	},
	displayInfo : true

});
// End
// ArtclisRowAction
// Dobavlenie k stolbcu redaktiorovanie/udalit zagolovka, t.e stolbec operacii
contacts.RowAction = new Ext.ux.grid.RowActions({

	actions : [ {
		iconCls : 'delete',
		qtip : 'Удалить'
	}, {
		iconCls : 'copy',
		qtip : 'Копировать'
	}, {
		iconCls : 'edit',
		qtip : 'Редактировать'
	} ],
	widthIntercept : Ext.isSafari ? 4 : 2,
	id : 'actions',
	header : '<center><b>Операции</b></center>'
});
contacts.RowAction.on({
	action : function(grid, record, action, row, col) {
		if (action == 'delete') {
			Ext.MessageBox.confirm('',
					'Вы уверены что хотите удалить эту запись', function(btn) {
						if (btn == "yes") {
							Ext.Ajax.request({
								url : 'admincp.php',
								params : {
									module : 'contacts',
									task : 'deleteItem',
									id : record.data.id
								},
								method : 'post',
								success : function() {
									contacts.base.reload();
								}
							});
						}
					})
		}
		if (action == 'copy') {
			contacts.addedit(record.data.id, true);

			Ext.getCmp('contacts.form').getForm().loadRecord(record)
		}
		if (action == 'edit') {

			contacts.addedit(record.data.id);
			Ext.getCmp('contacts.form').getForm().loadRecord(record)

		}
	}
});
// redactrivac zagolovki polei stranci
contacts.grid = new Ext.grid.EditorGridPanel(
		{
			store : contacts.base,
			title : '',
			frame : true,
			loadMask : true,
			id : 'contacts.grid',
			layout : 'fit',
			enableColLock : false,
			clicksToEdit : 1,
			autoWidth : true,
			columns : [
					{
						id : 'id',
						header : "<b>Id</b>",
						width : 40,
						sortable : false,
						dataIndex : 'id',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}

					},

					{
						id : 'name',
						header : "<b>Наименование</b>",
						width : 200,
						sortable : false,
						dataIndex : 'name',
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						}

					},

					{
						header : "<center><b>Создание</b></center>",
						width : 80,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 0) {
								style = "color:#999999";
							}
							return '<span style="' + style + '">' + v
									+ '</span>';
						},
						sortable : true,
						dataIndex : 'CreatedDate'
					},

					{
						id : 'Active',
						header : "<center><b></b></center>",
						sortable : true,
						dataIndex : 'active',
						width : 50,
						renderer : function(v, tag, rec) {
							var style = "";
							if (rec.data.active == 0) {
								style = "color:#999999";
							}
							return '<div style="height:16px;"><form action="'
									+ rec.get('link')
									+ '" id="contactsLink'
									+ rec.get('id')
									+ '" target="_blank"></form><img style="cursor:pointer;" ext:qtip="Предосмотр" onclick="document.getElementById(\'contactsLink'
									+ rec.get('id')
									+ '\').submit();" src="/core/images/icons/Internet.png" /></a> <span style="'
									+ style
									+ '"><img  onclick="contacts.UpdateStatus('
									+ rec.data.id
									+ ', '
									+ (rec.data.active == 0 ? 1 : 0)
									+ ');" style="cursor:pointer;" ext:qtip="'
									+ (rec.data.active == 0 ? 'Активировать'
											: 'Деактивировать')
									+ '" src="/core/images/icons/eye'
									+ (rec.data.active == 0 ? '2' : '')
									+ '.png" /></span></div>';
						}
					}, contacts.RowAction ],

			sm : new Ext.grid.RowSelectionModel({
				singleSelect : true
			}),
			viewConfig : {
				forceFit : false
			},
			height : 150,

			bbar : contacts.pagingBar,
			plugins : contacts.RowAction,
			iconCls : 'icon-grid',
			split : true,
			tbar : [ {
				text : 'Добавить новую запись',
				handler : function() {

					contacts.addedit(0);

				},
				iconCls : 'add'
			} ],
			region : 'center'

		});

contacts.grid.on('afteredit', contacts.updateRecord);

// End articleGrid

contacts.view = {
	id : 'contacts',
	title : 'Новости',
	layout : 'fit',

	items : [ contacts.grid ]
}
init_modules[init_modules.length] = contacts.view;
init_nav_modules[init_nav_modules.length] = {
	text : 'Новости',
	iconCls : 'pages',
	handler : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'contacts') {
			Ext.getCmp('Content').layout.setActiveItem('contacts');
			if (contacts.base.data.length < 1) {
				contacts.base.load({
					params : {
						start : 0,
						limit : 25
					}
				});

			}
			;

		}
	}
};

Ext.apply(actions, {
	'contacts' : function() {
		if (Ext.getCmp('Content').layout.activeItem.id != 'contacts') {
			Ext.getCmp('Content').layout.setActiveItem('contacts');
			if (contacts.base.data.length < 1) {
				contacts.base.load({
					params : {
						start : 0,
						limit : 25
					}
				});
			}
			;

		}
	}
});
ModulesRightMenu += '<li><img src="core/icons/news.png"/><a id="contacts" href="#">Контакты</a></li>';
