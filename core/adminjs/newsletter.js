
function windowNews(){
var NewsLetter = new Ext.Panel({

	title: 'Подписка',
	layout:'fit',
	id:'NewsLetterMenu',
	autoHeight: false,
	frame:true,
	
	
	items: [new Ext.Panel({
		
		autoHeight:false,
		autoWidth:false,
		width:640,
		layout:'fit',

		frame:true,
		//frame:false,
		//border:false,
		items:[new Ext.FormPanel({
			id:'Newsletter',
			labelAlign:'top',
			autoScroll:true,
			listeners: {
				'afterrender': function() {

				}
			},
			items:[{
				xtype:'textfield',
				name:'name',
				width:350,
				fieldLabel:'Имя отправителя'
			},{
				xtype:'textfield',
				name:'subject',
				width:350,
				fieldLabel:'Тема письма'
			},htmled({
				width:'100%',
				name:'message',
				height:350,
				label:'Текст письма'
			})

			]
			,
			bbar:[{
				text:'Сохранить',
				iconCls:'apply',
				handler: function() {
					tinyMCE.triggerSave();
					Ext.getCmp('Newsletter').getForm().submit({
						url:'admincp.php',
						method:'post',
						waitMsg:'Сохранение данных',
						params: {
							module:'catalog',
							task:'saveNewsletter'
						},
						success: function() {
							App.setAlert('','Настройки успешно сохраненны');
						}
					});
				}
			},{
				text:'<strong>Тестовая рассылка писем</strong>',
				iconCls:'mail_test',
				handler: function() {
					
					Ext.Ajax.request({
						url:'admincp.php',
						method:'post',
						params: {
							module:'catalog',
							
							task:'totalEmails'
						},
						success: function(o) {
							if (!Ext.getCmp('Newsletter').getForm().isValid()) {
								return false;
							}
							var res = Ext.decode(o.responseText);
							if (res) {
								var def1 = Ext.MessageBox.buttonText.yes;
								var def2 = Ext.MessageBox.buttonText.no;
								Ext.MessageBox.buttonText.yes = 'Отправить';
								Ext.MessageBox.buttonText.no = 'Отказаться';
								Ext.Msg.show({
									title:'Внимание',
									buttonText: {
										yes:'Отправить',
										no:'Отказаться'
									},
									msg: 'Общее кол-во получателей в рассылке: 1',
									buttons: Ext.Msg.YESNO,
									fn: function(btn) {
										if (btn=='yes') {
											tinyMCE.triggerSave();
											Ext.getCmp('Newsletter').getForm().submit({
												url:'admincp.php',
												method:'post',
												waitMsg:'Сохранение данных',
												params: {
													module:'catalog',
													task:'saveNewsletter'
												},
												success: function() {
													App.setAlert('','Настройки успешно сохраненны');
													tinyMCE.triggerSave();
													Ext.getCmp('Newsletter').getForm().submit({
														url:'admincp.php',
														method:'post',
														waitMsg:'Отправка писем',
														params: {
															module:'catalog',
															task:'sendNewsletterTest'
														},
														success: function() {
															App.setAlert('','Письмо успешно отправлено');
														}
													});
												}
											});
										}
									},
									icon: Ext.MessageBox.WARNING
								});
								Ext.MessageBox.buttonText.yes = def1;
								Ext.MessageBox.buttonText.no = def2;

							}
						}
					});

				}
			},'->',{
				text:'<strong>Разослать письма</strong>',
				iconCls:'mailed',
				handler: function() {
					if (!Ext.getCmp('Newsletter').getForm().isValid()) {
						return false;
					}
					
					tinyMCE.triggerSave();
					Ext.getCmp('Newsletter').getForm().submit({
						url:'admincp.php',
						method:'post',
						waitMsg:'Сохранение данных',
						params: {
							module:'catalog',
							
							task:'saveNewsletter'
						},
						success: function() {

							Ext.Ajax.request({
								url:'admincp.php',
								method:'post',
								params: {
									module:'catalog',
									
									task:'totalEmails'
								},
								success: function(o) {
									var res = Ext.decode(o.responseText);
									if (res) {
										var def1 = Ext.MessageBox.buttonText.yes;
										var def2 = Ext.MessageBox.buttonText.no;
										Ext.MessageBox.buttonText.yes = 'Отправить';
										Ext.MessageBox.buttonText.no = 'Отказаться';
										Ext.Msg.show({
											title:'Внимание',
											buttonText: {
												yes:'Отправить',
												no:'Отказаться'
											},
											msg: 'Общее кол-во получателей в рассылке: '+res.total,
											buttons: Ext.Msg.YESNO,
											fn: function(btn) {
												if (btn=='yes') {
													App.setAlert('','Настройки успешно сохраненны');
													tinyMCE.triggerSave();
													Ext.getCmp('Newsletter').getForm().submit({
														url:'admincp.php',
														method:'post',
														waitMsg:'Отправка писем',
														params: {
															module:'catalog',
															task:'sendNewsletter'
														},
														success: function() {
															App.setAlert('','Письма успешно отправлены');
														}
													});
												}
											},
											icon: Ext.MessageBox.WARNING
										});
										Ext.MessageBox.buttonText.yes = def1;
										Ext.MessageBox.buttonText.no = def2;
									}
								}
							});

						}
					});

				}
			}]
		})]
	})]
});
   new Ext.Window({
	   width:900,
	   height:600,
	   modal:true,
	   layout:'fit',
	   items:[NewsLetter],
	   listeners:{'show':function(){
		   Ext.getCmp('Newsletter').getForm().load({
				url:'admincp.php',
				method:'post',
				waitMsg:'Загрузка данных',
				params: {
					module:'catalog',
					task:'loadNewsletter'
				}

			});
		
	   }}
   }).show();
}

Ext.apply(actions, {
	'NewsLetterMenu': function() {
		
			
		    windowNews();
				
		
	}
});
ModulesRightMenu+='<li><i><a id="NewsLetterMenu" href="#">Подписка</a></i></li>';
