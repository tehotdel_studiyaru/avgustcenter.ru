var redirects = {};

redirects.updateRecord = function(oGrid_event){
    Ext.Ajax.request({
        waitMsg: 'Пожалуйста подождите...',
        url: 'admincp.php',
        params: {
            xaction: "Update",
            id: oGrid_event.record.data.Id,
            active: oGrid_event.record.data.Active,
            module: 'redirects'
        },
        success: function(response){
            var result = eval(response.responseText);
            switch (result) {
                case 33:
                    redirects.base.commitChanges(); // changes successful, get rid of the red triangles
                    redirects.base.reload(); // reload our datastore.
                    break;
                default:
                    Ext.MessageBox.alert('Ошибка', 'Не возможно сохранить изменения...');
                    break;
            }
        },
        failure: function(response){
            var result = response.responseText;
            Ext.MessageBox.alert('error', 'could not connect to the database. retry later');
        }
    });
};



redirects.addedit = function(id){
    var form = new Ext.FormPanel({
        id: 'redirects.form',
        width: 450,
        height:250,
        frame: true,
        labelAlign: 'top',
        items: [{xtype:'hidden', 'name':'Id'},{
        	xtype:'combo',
            typeAhead: true,
            name:'Type',
            hiddenName:'Type',
            fieldLabel:'Тип редиректа',
            triggerAction: 'all',
            store: new Ext.data.SimpleStore({
                fields: ['partyValue', 'partyName'],
                data: [['301', '301 Moved Permanently («перемещено навсегда»'], ['300', '300 Multiple Choices («множество выборов»)'],['302','302 Moved Temporarily («перемещено временно»)'],['303','303 See Other (смотреть другое)'],['307','307 Temporary Redirect («временное перенаправление»)']]
            }),
            editable:false,
            mode: 'local',
            anchor:'90%',
            value:301,
            displayField: 'partyName',
            valueField: 'partyValue',
            lazyRender: true,
            listClass: 'x-combo-list-small'
        },{
            layout: 'table',
            layoutConfig: {
                columns:2,
                tableAttrs: {
                    style: {
                        width: 880
                    }
                }
            },
            defaults: {
                width: 250
            },
            items: [{
                layout: 'form',
                width: 400,
                colspan: 2,
                items: [{
                
                    xtype: 'textfield',
                    fieldLabel: 'Откуда',
                    name: 'From',
                    anchor: '95%'
                }]
            },{
                layout: 'form',
                width: 400,
                colspan: 2,
                items: [{
                
                    xtype: 'textfield',
                    fieldLabel: 'Куда',
                    name: 'To',
                    anchor: '95%'
                }]
            }]
        }]
    });
    new Ext.Window({
        width: 460,
        height: 250,
        frame: true,
        layout:'fit',
        constrainHeader: true,
        closeAction: 'close',
        modal: true,
        id: 'redirects.WindowAddEdit',
        items: [form],
        buttonAlign: 'right',
        buttons: [{
            text: 'Сохранить',
            iconCls: 'accept',
            handler: function(){
                Ext.ux.TinyMCE.initTinyMCE();
                tinyMCE.triggerSave();
                form.getForm().submit({
                    url: 'admincp.php',
					method:'POST',
                    params: {
                        module: 'redirects',
                        task: 'save'
                    },
                    waitMsg: 'Пожалуйста подождите',
                    success: function(){
                        Ext.getCmp('redirects.WindowAddEdit').close();
                        redirects.base.reload();
                        
                    }
                });
            }
        }]
    }).show();
}


redirects.base = new Ext.data.Store({

    proxy: new Ext.data.HttpProxy({
        url: 'admincp.php',
        method: 'POST'
    }),
    baseParams: {
        xaction: "Listing",
        module: 'redirects'
    },
    remoteSort:true,
    
    reader: new Ext.data.JsonReader({
        root: 'results',
        totalProperty: 'total',
        id: 'Id'
    }, ['Id','Type','From','To','Active', 'Test'])

});





// PagingBar for articlegrid
redirects.pagingBar = new Ext.PagingToolbar({
    pageSize: 500,
    store: redirects.base,
    paramNames: {
        start: 'start',
        limit: 'limit'
    },
    displayInfo: true


});
// End
// ArtclisRowAction

redirects.RowAction = new Ext.ux.grid.RowActions({

    actions: [{
        iconCls: 'delete',
        qtip: 'Удалить'
    }, {
        iconCls: 'edit',
        qtip: 'Редактировать'
    }],
    widthIntercept: Ext.isSafari ? 4 : 2,
    id: 'actions',
    header:'<center><b>Операции</b></center>'
});
redirects.RowAction.on({
    action: function(grid, record, action, row, col){
        if (action == 'delete') {
            Ext.MessageBox.confirm('', 'Вы уверены что хотите удалить эту запись', function(btn){
                if (btn == "yes") {
                    Ext.Ajax.request({
                        url: 'admincp.php',
                        params: {
                            module: 'redirects',
                            task: 'deleteItem',
                            id: record.data.Id
                        },
                        method: 'post',
                        success: function(){
                            redirects.base.reload();
                        }
                    });
                }
            })
        }
        if (action == 'edit') {
        
            redirects.addedit(record.data.id);
            Ext.getCmp('redirects.form').getForm().loadRecord(record)
            
        }
    }
});
redirects.sm = new  Ext.grid.CheckboxSelectionModel();
redirects.grid = new Ext.grid.EditorGridPanel({
    store: redirects.base,
    title: '',
    frame: true,
    loadMask: true,
    id: 'redirects.grid',
    layout: 'fit',
    enableColLock: false,
    clicksToEdit: 1,
    autoWidth: true,
    columnLines: true,
    columns: [redirects.sm,{
    	header:'<b>Id</b>',
    	width:40,
    	dataIndex:'Id',
    	sortable:false,
    	renderer:function(v){
    		return '<span style="color:#ccc">'+v+'</span>';
    	}
    },{
        id: 'Type',
        header: "<b>Тип</b>",
        width: 40,
        sortable: false,
        dataIndex: 'Type'
    }, {
        
        header: "<b>Откуда</b>",
        width: 200,
        sortable: true,
        dataIndex: 'From'
    }, {
        
        header: "<b>Куда</b>",
        width: 200,
        sortable: true,
        dataIndex: 'To'
    }, {
       
        header: '<b>Статус</b>',
		sortable:false,
        dataIndex: 'Active',
        width: 100, 
        editor: new Ext.form.ComboBox({
            typeAhead: true,
            triggerAction: 'all',
            store: new Ext.data.SimpleStore({
                fields: ['partyValue', 'partyName'],
                data: [['1', 'Активная'], ['0', 'Не активная']]
            }),
            mode: 'local',
            displayField: 'partyName',
            valueField: 'partyValue',
            lazyRender: true,
            listClass: 'x-combo-list-small'
        }),
        renderer: function(value){
            if (value == 1) {
                return "Активная";
            }
            return "Не активная";
        }
        
    }, redirects.RowAction,{
    	width:450,
    	dataIndex:'Test',
        header:'<b>Проверка</b>'
    }],
    
    sm:redirects.sm,
    viewConfig: {
        forceFit: false
    },
    height: 150,
    bbar: redirects.pagingBar,
    plugins: redirects.RowAction,
    iconCls: 'icon-grid',
    split: true,
    tbar: [{
        text: 'Добавить новую запись',
        handler: function(){
        
            redirects.addedit(0);
            
            
        },
        iconCls: 'add'
    },'-',{
        text:'Проверка',
        iconCls:'test',
        handler:function(){
        	Ext.MessageBox.alert('','Проверка может занять какое то время. Подождите');
        	Ext.Ajax.request({
        		timeout:3600000,
        		url:'/admincp.php',
        		params:{module:'redirects', 'xaction':'testing'},
        		success:function(res){
        			Ext.MessageBox.alert('','Проверка завершилась успешно');
        			redirects.base.reload();
        			
        		},
        		failure:function(){
        			redirects.base.reload();
        		}
        	});
        }
    },'-',{
    	text:'Удалить выбранные',
    	iconCls:'delete',
    	handler:function(){
    		var data = new Array();
    		var sel = Ext.getCmp('redirects.grid').getSelectionModel().getSelections();
    		if (sel.length==0){
    			return new Ext.MessageBox.alert('','Выберите записи');
    		}
    		 Ext.MessageBox.confirm('', 'Вы уверены что хотите удалить эти записи', function(btn){
                 if (btn == "yes") {
                	 $.each(Ext.getCmp('redirects.grid').getSelectionModel().getSelections(), function(el){
             		    data.push(this.id);
             		});
             		Ext.Ajax.request({
             			url:'/admincp.php',
             			params:{module:'redirects', xaction:'deleteSelected', ids:data.join(',')},
             			success:function(){
             				
             			}
             		});
                 }
             })
    		
    	}
    },{
    	text:'Выгрузка Excel',
    	handler:function(){
    		redirects.importExcel++;
    		Ext.DomHelper.append(document.body, {
    			tag:'iframe',
    			src:'/exportRedirects.php',
    			style:'width:0px; height:0px;'
    		})
    	}
    },{
    	text:'Загрузка Excel',
    	handler:function(){
var fotoupload2 = new Ext.FormPanel({
		        
	            fileUpload: true,
	            width: '100%',
	            frame: true,
	            //border:false,
	            
	            shim: true,
	            bodyStyle: 'padding: 10px 10px 0 10px;',
	            labelWidth: 50,
	            items: [{
	                xtype: 'fileuploadfield',
	                emptyText: 'Выберите файл для загрузки',
	                fieldLabel: 'Файл',
	                name: 'photo-path',
	                width: '500',
	                anchor: '95%',
	                allowBlank: false,
	                buttonCfg: {
	                    text: ' ',
	                    iconCls: 'upload-icon'
	                }
	            }],
	            buttonAlign: 'center',
	            buttons: [{
	                text: 'Загрузить',
	                handler: function(){
	                    if (fotoupload2.getForm().isValid()) {
	                       
	                        
	                        fotoupload2.getForm().submit({
	                            url: '/importRedirects.php',
	                            method: 'POST',
	                           
	                            waitTitle: 'Обработка данных',
	                            waitMsg: 'Пожалуйста подождите, идёт загрузка фотографии...',
	                            success: function(fotoupload, o){
	                            	if (o.result.msg){
	                            		return Ext.MessageBox.alert('', o.result.msg);
	                            	}
	                            	 Ext.MessageBox.alert('Ошибка', 'Импорт редиректов завершен успешно');
	                                //msg('Success', 'Processed file "'+o.result.file+'" on the server');
	                                //Ext.getCmp('slider.form.tabimages.grid').store.reload();
	                                Ext.getCmp('slider5.UploadWindow').close();
	                                redirects.reload();
	                            },
	                            failure: function(fotoupload2, o){
	                                Ext.MessageBox.alert('Ошибка', 'Не удалось загрузить файл');
	                            }
	                        });
	                    }
	                }
	            }]
	        });
	        var shopupfileswin2 = new Ext.Window({
	            //applyTo     : 'hello-win',
	            layout: 'fit',
	            shim: false,
	            modal: true,
	            title: 'Импорт редиректов',
	            id: 'slider5.UploadWindow',
	            width: 400,
	            height: 200,
	            autoScroll: true,
	            closeAction: 'close',
	            plain: true,
	           
	            items: [fotoupload2],
	            buttons: [{
	                text: 'Закрыть',
	                handler: function(){
	                    
	                    Ext.getCmp('slider5.UploadWindow').close();
	                }
	            }]
		});
	        shopupfileswin2.show();
    	}
    }, '-', new Ext.ux.form.SearchField({
		hideTrigger1 : false,
		store : redirects.base,
		emptyText : 'Поиск',
		paramName : 'search',
		onTrigger1Click : function() {
			if (this.hasSearch) {
				this.el.dom.value = '';
				var o = {
					start : 0
				};
				this.store.baseParams = this.store.baseParams || {};
				this.store.baseParams[this.paramName] = '';
				this.store.reload({
					params : o
				});
				this.hasSearch = false;
			}
		},
		onTrigger2Click : function() {
			var v = this.getRawValue();
			if (v.length < 1) {
				this.onTrigger1Click();
				return;
			}
			var o = {
				start : 0
			};
			this.store.baseParams = this.store.baseParams || {};
			this.store.baseParams[this.paramName] = v;
			this.store.reload({
				params : o
			});
			this.hasSearch = true;
		}
	}) ],
    region: 'center'

});
redirects.ShowWin=function(id){
	Ext.MessageBox.alert('Информация',$('#'+id).html());
}
function showLinks(el){var html = $('div',$(el).parent()).html(); new Ext.Window({width:500, autoScroll:true, bodyStyle:'background:#fff; padding:15px', height:500, modal:true, html:html}).show();}
redirects.grid.on('afteredit', redirects.updateRecord);


// End articleGrid
redirects.importExcel = 0;
redirects.view = {
    id: 'redirects',
    title: 'Редиректы',
    layout: 'fit',
    
    items: [redirects.grid]
}
init_modules[init_modules.length] = redirects.view;
init_nav_modules[init_nav_modules.length] = {
    text: 'Слайдер',
    iconCls: 'pages',
    handler: function(){
        if (Ext.getCmp('Content').layout.activeItem.id != 'redirects') {
            Ext.getCmp('Content').layout.setActiveItem('redirects');
            if (redirects.base.data.length < 1) {
                redirects.base.load({
                    params: {
                        start: 0,
                        limit: 500
                    }
                });
                
                
            };
            
                    }
    }
};

Ext.apply(actions, {
    'redirects': function(){
        if (Ext.getCmp('Content').layout.activeItem.id != 'redirects') {
            Ext.getCmp('Content').layout.setActiveItem('redirects');
            if (redirects.base.data.length < 1) {
                redirects.base.load({
                    params: {
                        start: 0,
                        limit: 500
                    }
                });
            };
            
                    }
    }
});
ModulesRightMenu2 += '<li><img src="core/icons/redirects.png"/><a id="redirects" href="#">Редиректы</a></li>';
