var seo=  {};

seo.all = new Ext.form.FormPanel({
	frame:true,
	
	items:[{
		xtype:'textarea',
		anchor:'95%',
		name:'General_Robots',
		fieldLabel:'Robots.txt',
        height:300
	},{
		xtype:'button',
		handler:function(){
			seo.all.getForm().submit({
				waitMsg:'Подождите пожалуйста, идёт отправка данных',
				url:'/admincp.php',
				method:'post',
				params:{module:'seo', task:'save'}
			});
		},
		text:'Сохранить',
		iconCls:'apply'
	}]
});



seo.viewRobots = {
		id:'robots',
		title:'СЕО',
		items:seo.all
		
}

seo.viewLinks= {
		id:'seo_links',
		title:'Перелинковка'
		
	
}


try {

	init_modules[init_modules.length] = seo.viewRobots;
	
	Ext.apply(actions, {
	'robots': function()
	{
		
		if (Ext.getCmp('Content').layout.activeItem.id != 'robots')
		{
			
			Ext.getCmp('Content').layout.setActiveItem('robots');
			seo.all.load({
				url:'admincp.php',
				waitMsg:'Подождите пожалуйста, идёт загрузка данных',
				params:{module:'seo', task:'loadGeneralData'}
				,method:'post'
			});
		}
	}
	});
	ModulesRightMenu2+='<li><img src="core/icons/robots.png" /><a id="robots" href="#">Robots</a></li>';
	
	
	

	init_modules[init_modules.length] = seo.viewLinks;
	
	Ext.apply(actions, {
	'seo_links': function()
	{
		
		if (Ext.getCmp('Content').layout.activeItem.id != 'seo_links')
		{
			Ext.getCmp('Content').layout.setActiveItem('seo_links');
		}
	}
	});
	ModulesRightMenu2+='<li><img src="core/icons/seo_link.png" /><a id="seo_links" href="#">Перелинковка</a></li>';
}
catch (e){
	console.log(e);
}
