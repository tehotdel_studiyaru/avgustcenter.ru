$(document).ready(function() {
	$('#dcallback').click(function() {
		var x = $('body').height();
		$('#fade').height(x + 20);
		$('#container2').fadeIn('slow');
		$('#fade').fadeTo('slow', 0.5);
		return false;
	});
	
	$('.dcallback_order').click(function() {
		var x = $('body').height();
		$('#fade').height(x + 20);
		$('#container3').fadeIn('slow');
		$('#fade').fadeTo('slow', 0.5);
		return false;
	});
	
	$('button.orange').click(function() {
		var x = $('body').height();
		$('#fade').height(x + 20);
		$('#container3').fadeIn('slow');
		$('#fade').fadeTo('slow', 0.5);
		return false;
	});

	$('.closer21').click(function() {
		$('#fade, #container2, #container3').fadeOut('slow');
	});
});

function dialogError(text){
	
	alert(text);
}
function showForm(){
	$("#call").dialog('open');
}

function callbackpostForm44() {

	Ext.Ajax.request({
		url : '/form.php',
		form : 'callback',
		method : 'post',
		params : {
			zakaz : true
		},
		success : function(o) {

			var res = Ext.decode(o.responseText);
			if (res.success) {
				$('#fade, #container2').css('display', 'none');
				$('html,body').removeClass('hd');
				$('#callback').trigger('reset');
				dialogError('Спасибо. Ваша заявка успешно отправлена');

			} else {
				if (res.msg) {
					dialogError(res.msg);
					// alert(res.msg);
				} else {
					alert(o.responseText);
				}
			}

		},
		failure : function() {

			dialogError('Во время отправки данных произошла ошибка.');
		}
	});

}

function callbackpostForm45() {

	Ext.Ajax.request({
		url : '/form3.php',
		form : 'callback_order',
		method : 'post',
		
		success : function(o) {

			var res = Ext.decode(o.responseText);
			if (res.success) {
				$('#fade, #container3').css('display', 'none');
				$('html,body').removeClass('hd');
				$('#callback').trigger('reset');
				dialogError('Спасибо. Ваша заявка успешно отправлена');

			} else {
				if (res.msg) {
					dialogError(res.msg);
					// alert(res.msg);
				} else {
					alert(o.responseText);
				}
			}

		},
		failure : function() {

			dialogError('Во время отправки данных произошла ошибка.');
		}
	});

}

function callbackpostForm(){
	
	Ext.Ajax.request({
		url:'/form.php',
		form:'callback',
		method:'post',
		success:function(o){
		
		var res = Ext.decode(o.responseText);
		if (res.success){
			$('#fade, #container2').css('display','none');
			$('html,body').removeClass('hd'); 
			dialogError('Спасибо. Ждите звонка в указанное время');
			
		}
		else {
			if (res.msg){
				dialogError(res.msg);
				//alert(res.msg);
			}
			else {
				alert(o.responseText);
			}
		}
		
	},
	failure:function(){
		
		 dialogError('Во время отправки данных произошла ошибка.');
	}
	});
	
}


function orderclick(){
	
	Ext.Ajax.request({
		url:'/form.php',
		form:'oneclick',
		method:'post',
		success:function(o){
		
		var res = Ext.decode(o.responseText);
		if (res.success){
		dialogError('Спасибо.Ваш заказ принят.');
			
		}
		else {
			if (res.msg){
				dialogError(res.msg);
				//alert(res.msg);
			}
			else {
				alert(o.responseText);
			}
		}
		
	},
	failure:function(){
		
		 dialogError('Во время отправки данных произошла ошибка.');
	}
	});
	
}
