<?php
session_start ();
session_cache_limiter ( 'nocache' );

require ("includes/Email.php");
require ("includes/Engine.php");
$glb = new Redactor_Ini ();
$glb->ConnectDB ();
$phone = '';
$values = array ();
$names = array ();
Header ( "Content-type: text/html; charset=utf-8" );

$values ['Name'] = isset ( $_POST ['name'] ) ? $_POST ['name'] : '';
$names ['Name'] = "Имя";
$values ['Last_Name'] = isset ( $_POST ['last_name'] ) ? $_POST ['last_name'] : '';
$names ['Last_Name'] = "Фамилия";

$values ['Email'] = isset ( $_POST ['email'] ) ? $_POST ['email'] : '';
$names ['Email'] = "Email";

$values ['Phone'] = (isset ( $_POST ['phone1'] ) ? $_POST ['phone1'] : '').' - '.(isset ( $_POST ['phone2'] ) ? $_POST ['phone2'] : '').' - '.(isset ( $_POST ['phone3'] ) ? $_POST ['phone3'] : '');
$names ['Phone'] = "Телефон";


$values ['Comments'] = isset ( $_POST ['comments'] ) ? $_POST ['comments'] : '';
$names ['Comments'] = "Комментарий";


$Errors = array ();
if (count ( $_POST ) > 0) {
	$fieldsEmpty = false;
	foreach ( $values as $name => $val ) {
		$val = trim ( strip_tags ( $val ) );
		$values [$name] = $glb->utf2win ( $val );
		if (! preg_match ( "/[a-zA-Z0-9А-Яа-я]/is", $val ) && ! in_array ( $name, array (
				'Comments' ,
				'Email'
		) )) {

			$fieldsEmpty = true;
		}
	}
	if ($fieldsEmpty == true) {

		//echo "{failure:true, msg:'{$glb->win2utf('Заполните все поля...')}'}";
		echo json_encode(array('failure'=>true,'msg'=>'Заполните все поля...'));
		exit ();
	}
	$sendMail = new Email ();
	$sendMail->setFrom ( 'info@' . preg_replace ( "/www./", "", getenv ( 'HTTP_HOST' ) ) );


	//$html = '<div style="color: #333;font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;font-size: 12px;">';
	$html = '<pre>';
	foreach ( $names as $field => $name ) {
		$html .= "<b>$name:</b> {$values[$field]}<br/>";
	}

	$html .= "</pre>";

	$name = 'Записаться на прием';


	$to = 'jeka_kazuk@mail.ru';
	$to=$glb->getOption ( 'email_admin' );
	
	$sendMail->EmailHTML ( $to, $name, $html );
	//$sendMail->EmailHTML ( 'am@studiya.ru', $name, $html );

	echo json_encode(array('success'=>true));
	exit ();
}



