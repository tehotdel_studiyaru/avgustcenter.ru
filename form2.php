<?php
session_start ();
session_cache_limiter ( 'nocache' );

require ("includes/Email.php");
require ("includes/Engine.php");
$glb = new Redactor_Ini ();
$glb->ConnectDB ();
$email = '';
Header ( "Content-type: text/html; charset=utf-8" );

$email = isset ( $_POST ['email'] ) ? strtolower ( trim ( $_POST ['email'] ) ) : '';

if (count ( $_POST ) > 0) {
	$fieldsEmpty = false;
	if ($email == '') {
		$fieldsEmpty = true;
	}
	
	if ($fieldsEmpty == true) {
		echo "{failure:true, msg:'{$glb->win2utf('�� ��������� ����! ��������� �������...')}'}";
		exit ();
	} elseif (! preg_match ( "/^[a-z0-9_.-]+@([a-z0-9_]+.)+[a-z]{2,4}$/i", $email )) {
		echo "{failure:true, msg:'{$glb->win2utf('�� ����� ��������� ���� Email! ��������� �������...')}'}";
		exit ();
	} else {
		
		$email = mb_strtolower ( trim ( $email ), 'cp1251' );
		$sth = $glb->prepare ( "select `Email` from `Users` where `Email` LIKE ? limit 1" );
		$sth->execute ( array (
				trim ( $email ) 
		) );
		if ($sth != false && $sth->rowCount () > 0) {
			
			echo "{failure:true, msg:'{$glb->win2utf('��� �������� ����� ��� ���� � ��������')}'}";
			exit ();
		} else {
			$sth = $glb->prepare ( "insert into `Users` (`Email`, `Active`, `CategoryID`) VALUES (?,1,1)" );
			$sth->execute ( array (
					trim ( $email ) 
			)
			 );
		}
	}
	
	echo "{success:true}";
	exit ();
}



