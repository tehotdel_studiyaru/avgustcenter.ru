<?php
session_start ();
session_cache_limiter ( 'nocache' );

require ("includes/Email.php");
require ("includes/Engine.php");
$glb = new Redactor_Ini ();
$glb->ConnectDB ();
$phone = '';
$values = array ();
$names = array ();
Header ( "Content-type: text/html; charset=utf-8" );
if (isset ( $_POST ['FormCall'] )) {
	$values ['Name'] = isset ( $_POST ['Name'] ) ? $_POST ['Name'] : '';
	$names ['Name'] = "���";
	
	$values ['Phone'] = isset ( $_POST ['Phone'] ) ? $_POST ['Phone'] : '';
	$names ['Phone'] = "�������";
	
	$values ['Comments'] = isset ( $_POST ['Comments'] ) ? $_POST ['Comments'] : '';
	$names ['Comments'] = "�����������";
} elseif (isset ( $_POST ['FormOrder'] )) {
	$values ['Name'] = isset ( $_POST ['Name'] ) ? $_POST ['Name'] : '';
	$names ['Name'] = "���";
	$values ['Phone'] = isset ( $_POST ['Phone'] ) ? $_POST ['Phone'] : '';
	$names ['Phone'] = "�������";
	$values ['Email'] = isset ( $_POST ['Email'] ) ? $_POST ['Email'] : '';
	$names ['Email'] = "E-mail";
	
	$values ['Comments'] = isset ( $_POST ['Comments'] ) ? $_POST ['Comments'] : '';
	$names ['Comments'] = "�����������";
	$values ['Url'] = isset ( $_POST ['Url'] ) ? $_POST ['Url'] : '';
	$names ['Url'] = "������ �� ��������";
}

$Errors = array ();
if (count ( $_POST ) > 0) {
	$fieldsEmpty = false;
	foreach ( $values as $name => $val ) {
		$val = trim ( strip_tags ( $val ) );
		$values [$name] = $glb->utf2win ( $val );
		if (! preg_match ( "/[a-zA-Z0-9�-��-�]/is", $val ) && ! in_array ( $name, array (
				'Comments' ,
				'Email'
		) )) {
			
			$fieldsEmpty = true;
		}
	}
	if ($fieldsEmpty == true) {
		
		echo "{failure:true, msg:'{$glb->win2utf('��������� ��� ����...')}'}";
		exit ();
	}	
		$sendMail = new Email ();
		$sendMail->setFrom ( 'info@' . preg_replace ( "/www./", "", getenv ( 'HTTP_HOST' ) ) );
		
		$html = '<div style="color: #333;font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;font-size: 12px;">';
		foreach ( $names as $field => $name ) {
			$html .= "<b>$name:</b> {$values[$field]}<br/>";
		}
		
		$html .= "</div>";
		if (isset ( $_POST ['FormCall'] )) {
			$name = '������ �� ��������';
		} elseif (isset ( $_POST ['FormOrder'] )) {
			$name = '�����';
		}
		
		$sendMail->EmailHTML ( $glb->getOption ( 'email_admin' ), $name, $html );
		echo "{success:true}";
		exit ();
	}



