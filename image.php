<?php
$_GET ['image'] = isset ( $_GET ['image'] ) ? urldecode ( $_GET ['image'] ) : null;
if (isset ( $_GET ['catalog'] )) {
	$path = pathinfo ( $_GET ['array'], PATHINFO_FILENAME );
	$ext = pathinfo ( $_GET ['array'], PATHINFO_EXTENSION );
	require ("includes/Email.php");
	require ("includes/Engine.php");
	$glb = new Redactor_Ini ();
	$glb->ConnectDB ();
	
	$a = explode ( "_", base64_decode ( $path ) );
	$pathname = dirname ( __FILE__ ) . '/thumbs/catalog/';
	if (file_exists ( "files/catalog/o_{$a[2]}.{$ext}" )) {
		$sales = false;
	
		$sth = $glb->prepare ( "select count(1) from `Catalog_Items` WHERE `Id`=(select `ItemID` from `Catalog_Items_Files` WHERE `Id`=? LIMIT 1) AND `uf_20`=21" );
		$sth->execute ( array (
				$a [2] 
		) );
		if ($sth->rowCount () > 0 && $sth->fetchColumn () > 0) {
			$sales = true;
		}
		
		$image = new Imagick ();
		$width = $a [0];
		$height = $a [1];
		$image->readIMage ( getenv ( 'DOCUMENT_ROOT' ) . '/' . "files/catalog/o_{$a[2]}.{$ext}" );
		$iWidth = $oldWidth = $image->getImageWidth ();
		$iHeight = $oldHeight = $image->getImageHeight ();
		if ($width != 0 && $height != 0) {
			
			$bg = new Imagick ();
			if (isset ( $_GET ['grey'] )) {
				
				$pixel = new ImagickPixel ( '#ededed' );
				$pixel->setcolor ( '#ededed' );
				$bg->setbackgroundcolor ( '#ededed' );
			} else {
				$pixel = new ImagickPixel ( '#ffffff' );
				$pixel->setcolor ( '#ffffff' );
				$bg->setbackgroundcolor ( '#ffffff' );
			}
			$bg->newImage ( $width, $height, $pixel );
			$bg->setImageFormat ( 'jpeg' );
			
			$geo = $image->getImageGeometry ();
			if ($width < $image->getimagewidth () && $height < $image->getImageheight ()) {
				$image->thumbnailimage ( $width, $height, true );
			}
			
			
			
			
			$iWidth = $image->getImageWidth ();
			$iHeight = $image->getImageHeight ();
			
			if ($sales == true && file_exists ( "sales.png" )) {
			
				$watermark = new Imagick ();
				$watermark->readImage ( "sales.png" );
				// how big are the images?
			
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
			
				$del = 3;
			
				$watermark->thumbnailimage ( $iWidth / $del, $iHeight / $del, true );
				// get new size
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
			
				// echo "{$iHeight} -- {$wHeight}";
			
				$x = ($iWidth - $wWidth);
			
				if ($width == 0) {
					$x = ($iWidth - $wWidth);
				}
			
				$y = $iHeight - $wHeight;
			
				$image->compositeImage ( $watermark, imagick::COMPOSITE_OVER, $x, $y );
			
				$watermark->destroy ();
			}
			if (file_exists ( "water.png" ) && $width>150) {
			
				$watermark = new Imagick ();
				$watermark->readImage ( "water.png" );
				// how big are the images?
			
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
			
				if ($iHeight < $wHeight || $iWidth < $wWidth) {
					// resize the watermark
						
					$watermark->thumbnailimage ( $iWidth, $iHeight, true );
					// get new size
					$wWidth = $watermark->getImageWidth ();
					$wHeight = $watermark->getImageHeight ();
				}
				// echo "{$iHeight} -- {$wHeight}";
			
				$x = ($iWidth - $wWidth) / 2;
				
					$y = ($iHeight - ($wHeight * 2));
				
				
			
				for($x = 0; $x <= $iWidth; $x ++) {
						
					$image->compositeImage ( $watermark, imagick::COMPOSITE_OVER, $x, $y );
					$x += $wWidth - 1;
				}
			
				$watermark->destroy ();
			}
			
			
			$x = 0;
			$y = 0;
			if ($iWidth > $width) {
				$x = ($iWidth - $width) / 2;
			} else {
				$x = ($width - $iWidth) / 2;
			}
			if ($height < $iHeight) {
				$y = ($iHeight - $height) / 2;
			} else {
				$y = ($height - $iHeight) / 2;
			}
			
			$x = ceil ( $x );
			$y = ceil ( $y );
			if (($iWidth + $x) > $width) {
				$x = 0;
			}
			if (($iHeight + $y) > $height) {
				$y = 0;
			}
			
			$bg->setImageColorspace ( $image->getImageColorspace () );
			$bg->compositeImage ( $image, $image->getImageCompose (), $x, $y );
			$image = $bg;
		} 

		else {
			
			if ($sales == true && file_exists ( "sales.png" )) {
				
				$watermark = new Imagick ();
				$watermark->readImage ( "sales.png" );
				// how big are the images?
				
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
				
				$del = 3;
				
				$watermark->thumbnailimage ( $iWidth / $del, $iHeight / $del, true );
				// get new size
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
				
				// echo "{$iHeight} -- {$wHeight}";
				
				$x = ($iWidth);
				
				if ($width == 0) {
					$x = ($iWidth - $wWidth);
				}
				
				$y = $iHeight - $wHeight;
				
				$image->compositeImage ( $watermark, imagick::COMPOSITE_OVER, $x, $y );
				
				$watermark->destroy ();
			}
			if (file_exists ( "water.png" )) {
				
				$watermark = new Imagick ();
				$watermark->readImage ( "water.png" );
				// how big are the images?
				
				$wWidth = $watermark->getImageWidth ();
				$wHeight = $watermark->getImageHeight ();
				
				if ($iHeight < $wHeight || $iWidth < $wWidth) {
					// resize the watermark
					
					$watermark->thumbnailimage ( $iWidth, $iHeight, true );
					// get new size
					$wWidth = $watermark->getImageWidth ();
					$wHeight = $watermark->getImageHeight ();
				}
				// echo "{$iHeight} -- {$wHeight}";
				
				$x = ($iWidth - $wWidth*5) / 2;
			
					$y = ($iHeight - ($wHeight * 4.7));
				
				if ($y < 0) {
					$y = ($iHeight - ($wHeight));
				}
				if ($y == 0) {
					$y = $iHeight - 20;
				}
				
				for($x = 0; $x <= $iWidth; $x ++) {
					
					$image->compositeImage ( $watermark, imagick::COMPOSITE_OVER, $x, $y );
					$x += $wWidth - 1;
				}
				
				$watermark->destroy ();
			}
		}
		
		header ( "Content-Length: " . strlen ( $image ) );
		header ( "Content-Type: image/jpeg" );
		
		echo $image;
		
		@mkdir ( $pathname, 0777, true );
		// $image->writeimage ( $pathname . $_GET ['array'] );
		// chmod ( $pathname . $_GET ['array'], 0666 );
		$bg->destroy ();
		$image->destroy ();
	}
} elseif (isset ( $_GET ['image'] ) && ! empty ( $_GET ['image'] ) && file_exists ( getenv ( 'DOCUMENT_ROOT' ) . '/' . $_GET ['image'] )) {
	$width = isset ( $_GET ['width'] ) ? ( int ) $_GET ['width'] : 0;
	$height = isset ( $_GET ['height'] ) ? ( int ) $_GET ['height'] : 0;
	$water = isset ( $_GET ['water'] ) ? true : false;
	
	$image = new Imagick ();
	$image->readIMage ( getenv ( 'DOCUMENT_ROOT' ) . '/' . $_GET ['image'] );
	$pathname = dirname ( __FILE__ ) . '/thumbs/';
	if ($width != 0 && $height != 0) {
		$pathname .= "{$width}x{$height}/";
	}
	
	$iWidth = $image->getImageWidth ();
	$iHeight = $image->getImageHeight ();
	if ($width != 0 && $height != 0) {
		
		$bg = new Imagick ();
		if (isset ( $_GET ['grey'] )) {
			
			$pixel = new ImagickPixel ( '#ededed' );
			$pixel->setcolor ( '#ededed' );
			$bg->setbackgroundcolor ( '#ededed' );
		} else {
			$pixel = new ImagickPixel ( '#ffffff' );
			$pixel->setcolor ( '#ffffff' );
			$bg->setbackgroundcolor ( '#ffffff' );
		}
		$bg->newImage ( $width, $height, $pixel );
		$bg->setImageFormat ( 'jpeg' );
		
		$geo = $image->getImageGeometry ();
		if ($width < $image->getimagewidth () && $height < $image->getImageheight ()) {
			$image->thumbnailimage ( $width, $height, true );
		}
		
		$iWidth = $image->getImageWidth ();
		$iHeight = $image->getImageHeight ();
		$x = 0;
		$y = 0;
		if ($iWidth > $width) {
			$x = ($iWidth - $width) / 2;
		} else {
			$x = ($width - $iWidth) / 2;
		}
		if ($height < $iHeight) {
			$y = ($iHeight - $height) / 2;
		} else {
			$y = ($height - $iHeight) / 2;
		}
		
		$x = ceil ( $x );
		$y = ceil ( $y );
		if (($iWidth + $x) > $width) {
			$x = 0;
		}
		if (($iHeight + $y) > $height) {
			$y = 0;
		}
		
		$bg->setImageColorspace ( $image->getImageColorspace () );
		$bg->compositeImage ( $image, $image->getImageCompose (), $x, $y );
		$image = $bg;
	}
	
	if (file_exists ( "water.png" )) {
		$watermark = new Imagick ();
		$watermark->readImage ( "water.png" );
		// how big are the images?
		
		$wWidth = $watermark->getImageWidth ();
		$wHeight = $watermark->getImageHeight ();
		
		if ($iHeight < $wHeight || $iWidth < $wWidth) {
			// resize the watermark
			$watermark->scaleImage ( $iWidth, $iHeight );
			// get new size
			$wWidth = $watermark->getImageWidth ();
			$wHeight = $watermark->getImageHeight ();
		}
		
		$x = 0;
		$y = ($iHeight - $wHeight);
		
		$image->compositeImage ( $watermark, imagick::COMPOSITE_OVER, $x, $y );
		$watermark->destroy ();
	}
	header ( "Content-Length: " . strlen ( $image ) );
	header ( "Content-Type: image/jpeg" );
	
	echo $image;
	
	@mkdir ( $pathname . pathinfo ( $_GET ['image'], PATHINFO_DIRNAME ), 0777, true );
	$image->writeimage ( $pathname . $_GET ['image'] );
	chmod ( $pathname . $_GET ['image'], 0666 );
	$bg->destroy ();
	$image->destroy ();
} else {
}