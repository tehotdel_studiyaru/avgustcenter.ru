<?php
putenv ( 'TMPDIR=' . dirname ( dirname ( __FILE__ ) ) . "/tmp" );
define ( 'DEBUG', true );
define ( 'APPLICATION_PATH', dirname ( dirname ( __FILE__ ) ) );
define ( 'PEARPATH', dirname ( __FILE__ ) . '/extra' );
define ( 'Redactor_Engine', true );
$ModulesPaths = array ();

$ModulesPaths [] = realpath ( APPLICATION_PATH . '/includes' );
$ModulesPaths [] = realpath ( APPLICATION_PATH . '/includes/extra' );
$ModulesPaths [] = realpath ( APPLICATION_PATH );
$ModulesPaths [] = get_include_path ();

set_include_path ( implode ( PATH_SEPARATOR, $ModulesPaths ) );
function num2str($num) {
	$nul = '����';
	$ten = array (
			array (
					'',
					'����',
					'���',
					'���',
					'������',
					'����',
					'�����',
					'����',
					'������',
					'������' 
			),
			array (
					'',
					'����',
					'���',
					'���',
					'������',
					'����',
					'�����',
					'����',
					'������',
					'������' 
			) 
	);
	$a20 = array (
			'������',
			'�����������',
			'����������',
			'����������',
			'������������',
			'����������',
			'�����������',
			'����������',
			'������������',
			'������������' 
	);
	$tens = array (
			2 => '��������',
			'��������',
			'�����',
			'���������',
			'����������',
			'���������',
			'�����������',
			'���������' 
	);
	$hundred = array (
			'',
			'���',
			'������',
			'������',
			'���������',
			'�������',
			'��������',
			'�������',
			'���������',
			'���������' 
	);
	$unit = array ( // Units
			array (
					'�������',
					'�������',
					'������',
					1 
			),
			array (
					'�����',
					'�����',
					'������',
					0 
			),
			array (
					'������',
					'������',
					'�����',
					1 
			),
			array (
					'�������',
					'��������',
					'���������',
					0 
			),
			array (
					'��������',
					'��������',
					'����������',
					0 
			) 
	);
	//
	list ( $rub, $kop ) = explode ( '.', sprintf ( "%015.2f", floatval ( $num ) ) );
	$out = array ();
	if (intval ( $rub ) > 0) {
		foreach ( str_split ( $rub, 3 ) as $uk => $v ) { // by 3 symbols
			if (! intval ( $v ))
				continue;
			$uk = sizeof ( $unit ) - $uk - 1; // unit key
			$gender = $unit [$uk] [3];
			list ( $i1, $i2, $i3 ) = array_map ( 'intval', str_split ( $v, 1 ) );
			// mega-logic
			$out [] = $hundred [$i1]; // 1xx-9xx
			if ($i2 > 1)
				$out [] = $tens [$i2] . ' ' . $ten [$gender] [$i3]; // 20-99
			else
				$out [] = $i2 > 0 ? $a20 [$i3] : $ten [$gender] [$i3]; // 10-19 | 1-9
					                                                       // units without rub & kop
			if ($uk > 1)
				$out [] = morph ( $v, $unit [$uk] [0], $unit [$uk] [1], $unit [$uk] [2] );
		} // foreach
	} else
		$out [] = $nul;
	$out [] = morph ( intval ( $rub ), $unit [1] [0], $unit [1] [1], $unit [1] [2] ); // rub
	$out [] = $kop . ' ' . morph ( $kop, $unit [0] [0], $unit [0] [1], $unit [0] [2] ); // kop
	return trim ( preg_replace ( '/ {2,}/', ' ', join ( ' ', $out ) ) );
}

/**
 * �������� ����������
 * @ author runcore
 */
function morph($n, $f1, $f2, $f5) {
	$n = abs ( intval ( $n ) ) % 100;
	if ($n > 10 && $n < 20)
		return $f5;
	$n = $n % 10;
	if ($n > 1 && $n < 5)
		return $f2;
	if ($n == 1)
		return $f1;
	return $f5;
}
require_once 'Net/URL/Mapper.php';
require_once 'Headers.php';
require_once 'Breadcrumbs.php';
require_once 'Email.php';
require_once 'Metas.php';
require_once '404.php';
require_once 'SystemUsers.php';
class Redactor_Route {
}
class rewriteUrls extends Redactor_Ini {
	static $cache = array ();
	static $item = false;
	static $cat = false;
	static $urls = array (
			array (
					'basket/order',
					array (
							'module' => 'basket',
							'order' => true 
					) 
			),
			array (
					'basket',
					array (
							'module' => 'basket' 
					) 
			),
			array (
					'favorite',
					array (
							'module' => 'catalog',
							'favorite' => true 
					) 
			),
			array (
					'catalog/looked',
					array (
							'module' => 'catalog',
							'looked' => 1 
					) 
			),
			
			array (
					'brands/:brands',
					array (
							'module' => 'brands' 
					) 
			),
			
			array (
					'brands',
					array (
							'module' => 'brands' 
					) 
			),
			array (
					'account',
					array (
							'module' => 'account' 
					) 
			),
			array (
					'search',
					array (
							'module' => 'search' 
					) 
			),
			array (
					'account/logout',
					array (
							'module' => 'account',
							'logout' => true 
					) 
			),
			
			array (
					'account/orders/:oredrsItem',
					
					array (
							'module' => 'account',
							'orders' => true 
					) 
			),
			array (
					'account/log',
					array (
							'module' => 'account',
							'log' => true 
					) 
			),
			array (
					'account/news',
					array (
							'module' => 'account',
							'news' => true 
					) 
			),
			
			array (
					'account/warranty',
					array (
							'module' => 'account',
							'warranty' => true 
					) 
			),
			
			array (
					'account/terms',
					array (
							'module' => 'account',
							'terms' => true 
					) 
			),
			array (
					'account/subscribe',
					array (
							'module' => 'account',
							'subscribe' => true 
					) 
			),
			array (
					'account/looked',
					array (
							'module' => 'account',
							'looked' => true 
					) 
			),
			array (
					'account/callback',
					array (
							'module' => 'account',
							'callback' => true 
					) 
			),
			array (
					"account/registration",
					array (
							"module" => "account",
							"reg" => true 
					) 
			),
			array (
					"account/recover",
					array (
							"module" => "account",
							"recover" => true 
					) 
			),
			array (
					'sitemap',
					array (
							'module' => 'sitemap' 
					) 
			) 
	);
	static $noIsModule = array (
			'articles',
			'shop',
			'news',
			'clients',
			'partners',
			'gallery',
			'faq',
			'pages',
			'catalog',
			'brands',
			'account' 
	);
	static function getUrlCustom(&$params) {
		$matches = array ();
		foreach ( self::$urls as $url ) {
			if (isset ( $url [1] ) && count ( $url [1] ) > 0) {
				$p = $url [1];
			} else {
				$p = array ();
			}
			
			if (preg_match ( "#/:#", $url [0] )) {
				
				$s = explode ( "/", $url [0] );
				foreach ( $s as $o ) {
					$o = trim ( $o );
					if (substr ( $o, 0, 1 ) == ':') {
						$o = substr ( $o, 1, strlen ( $o ) );
						if (! isset ( $p [$o] )) {
							$p [$o] = '';
						}
					}
				}
			}
			
			if (isset ( $p ['module'] ) && isset ( $params ['module'] ) && $p ['module'] == $params ['module']) {
				
				$i = 0;
				$isNot = false;
				foreach ( $params as $paramName => $paramValue ) {
					if (isset ( $p [$paramName] )) {
						$i ++;
					} else {
						$isNot = true;
					}
				}
				if ($isNot == false && count ( $p ) == count ( $params )) {
					$totalParams = $i;
					if ($totalParams > 0) {
						if (! isset ( $matches [$totalParams] )) {
							$matches [$totalParams] = array ();
						}
						$matches [$totalParams] [] = $url;
					}
				}
			}
		}
		
		if (count ( $matches ) > 0) {
			krsort ( $matches );
			$match = current ( current ( $matches ) );
			
			$url = $match [0];
			foreach ( $match [1] as $paramName => $paramValue ) {
				
				if (is_string ( $paramValue ) or is_numeric ( $paramValue ) or is_bool ( $paramValue )) {
					if (is_bool ( $paramValue )) {
						if ($paramValue == true) {
							$paramValue = 1;
						} else {
							$paramValue = 0;
						}
					}
					
					$url = preg_replace ( "#:{$paramName}#", strtolower ( $paramValue ), $url );
					if (isset ( $params [$paramName] )) {
						unset ( $params [$paramName] );
					}
				}
			}
			
			foreach ( $params as $paramName => $paramValue ) {
				if (is_string ( $paramValue ) or is_numeric ( $paramValue ) or is_bool ( $paramValue )) {
					if (is_bool ( $paramValue )) {
						if ($paramValue == true) {
							$paramValue = 1;
						} else {
							$paramValue = 0;
						}
					}
					
					$url = preg_replace ( "#:{$paramName}#", strtolower ( $paramValue ), $url );
					
					if (isset ( $params [$paramName] )) {
						unset ( $params [$paramName] );
					}
				}
			}
			return $url;
		}
		return false;
	}
	static function matchCustom($path) {
		$path = parse_url ( $path, PHP_URL_PATH );
		
		foreach ( self::$urls as $url ) {
			
			$preg = self::getPreg ( $url [0] );
			
			if (preg_match ( "#^{$preg}$#", $path, $m )) {
				$params = array ();
				foreach ( $m as $num => $val ) {
					if (is_numeric ( $num )) {
						continue;
					}
					$params [$num] = $val;
				}
				return array_merge ( $url [1], $params );
				break;
			}
		}
		return false;
	}
	static function getPreg($url) {
		$url = explode ( "/", $url );
		$new = array ();
		foreach ( $url as $u ) {
			
			$u = trim ( $u );
			if (empty ( $u )) {
				continue;
			}
			if (substr ( $u, 0, 1 ) == ":") {
				$new [] = "(?P<" . preg_replace ( "#^:(.*)#is", "$1", $u ) . ">.*?)";
			} else {
				$new [] = $u;
			}
		}
		return "/" . implode ( "/", $new ) . "";
	}
	static $isBe = array ();
	private static $Instance = null;
	static function getInstance() {
		if (self::$Instance == null) {
			self::$Instance = new self ();
		}
		
		return self::$Instance;
	}
	// ���������� ����� ������� ������ � ����������� URL
	static function tryIt($array) {
		$array = array_filter ( explode ( '/', $array ) );
		$match = array ();
		
		$module = isset ( $array ['module'] ) ? $array ['module'] : (isset ( $array [0] ) ? $array [0] : '');
		unset ( $array [0] );
		if (count ( $array ) == 1 && is_numeric ( $array [1] )) {
			$match ['module'] = $module;
			$match [$module] = $array [1];
		} elseif (count ( $array ) == 2 && ! is_numeric ( $array [1] ) && $array [1] == 'catid' && is_numeric ( $array [2] )) {
			$match ['module'] = $module;
			$match ['catid'] = $array [2];
		} elseif (count ( $array ) == 0) {
			$match ['module'] = $module;
		}
		
		return $match;
	}
	// �������� �� ���������� �������� URL �� ������� � �������
	static function match($url) {
		$path = parse_url ( $url, PHP_URL_PATH );
		$oldUrl = $url;
		if (($custom = self::$mapper->match ( $path )) != false) {
			
			return $custom;
		} elseif (($custom = self::matchCustom ( $path )) != false) {
			
			return $custom;
		}
		
		$path = explode ( "/", $path );
		
		$new = array ();
		$i = 0;
		$isCat = 0;
		foreach ( $path as $num => $p ) {
			$p = trim ( urldecode ( $p ) );
			if (empty ( $p )) {
				continue;
			}
			$i ++;
			
			$new [] = $p;
		}
		
		$isNew = false;
		
		$last = 0;
		$match = array ();
		$module = (count ( $p ) == 1 && is_numeric ( $p ) ? 'pages' : false);
		foreach ( $new as $num => $p ) {
			
			if (($el = self::find ( $p, $last, $module, $isCat )) != false) {
				
				$last = $el ['objId'];
				$match = array (
						'module' => $el ['module'] 
				);
				
				$module = $el ['module'];
				
				if ($el ['objId'] == 0 && in_array ( $el ['module'], self::$noIsModule )) {
					
					continue;
				}
				
				if ($el ['isCat'] == 1) {
					$match ['catid'] = $el ['objId'];
				} else {
					$match [$el ['module']] = $el ['objId'];
				}
			} elseif (in_array ( $p, self::$noIsModule )) {
				
				$match ['module'] = $p;
				$module = $p;
			} elseif (is_numeric ( $p )) {
				
				if (isset ( $match ['module'] )) {
					$m = $match;
					$m [$match ['module']] = $p;
					$m2 = $m;
					$m2 ['module'] = self::getModuleName ( $m ['module'] );
				} else {
					
					$m = $m2 = array (
							'module' => 'pages',
							'pages' => $p 
					);
					unset ( $m2 ['module'] );
				}
				
				if (($u = self::getUrl ( $m )) != "/" . implode ( '/', $m2 ) && $u != implode ( '/', $m2 )) {
					self::getInstance ()->saveHit ( true );
					// exit ( "{$u} === " . implode ( '/', $m2 ) );
					// header ( "Location: {$u}", true, 301 );
					
					// exit ();
					return self::tryIt ( $oldUrl );
				} else {
					
					return self::tryIt ( $oldUrl );
				}
			} else {
				
				// start edit
				$sth = self::getInstance ()->query ( "select `module` from `rewriteUrls` where `module`='brands' and `isCat`=0 and ( `name`='" . end ( $new ) . "' or `custom`='" . end ( $new ) . "') limit 1" );
				if ($sth != false && $sth->rowCount () > 0) {
					$module = $sth->fetchColumn ();
				}
				
				if (($el = self::find ( $p, $last, $module, $isCat )) != false) {
					$match ['brands'] = $el ['objId'];
				} else {
					
					// end edit
					if (($moduleName = self::getModuleByName ( $new [0] ))) {
						$new [0] = $moduleName;
					}
					
					return self::tryIt ( $oldUrl );
				}
			}
		}
		
		return $match;
	}
	// ����� ����� URL �� ������� � �������
	static function find($name, $parent = 0, $module = false, $isCat = 0) {
		$dop = '';
		
		if ($module) {
			$dop = " and `module`='" . addslashes ( $module ) . "'";
		}
		if ($isCat == 1) {
			 $dop = ' and `isCat`=1';
		}
		// echo "select `module`, `objId`, `isCat`, `parentId`, `custom` from `rewriteUrls` where `name`='" . addslashes ( $name ) . "' and CHAR_LENGTH(TRIM(`custom`))=0 {$dop} or `custom`='" . addslashes ( $name ) . "' {$dop} limit 1" ;
		$sql = self::getInstance ()->query ( "select `module`, `objId`, `isCat`, `parentId`, `custom` , `name` from `rewriteUrls` where `name`='" . addslashes ( $name ) . "'  and CHAR_LENGTH(TRIM(`custom`))=0 {$dop} or `custom`='" . addslashes ( $name ) . "'  {$dop} limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			
			return $sql->fetch ( PDO::FETCH_ASSOC );
		}
		return false;
	}
	// static function getRow($module, $parentId, $current){
	// $sth = self::getStatement()->Stm("select `objId`, `parentId`, `isCat` from `rewriteUrls` where `module`=? and `objId`=?");
	// }
	// �������� ������������ ����� URL
	protected function getParent($id, $module, $isCat = 2, &$urls, $params = array()) {
		$dop = '';
		
		  if ($isCat != 2) { if ($isCat == 1) { $dop = ' and `isCat`="1"'; } else { $dop = ' and `isCat`="0"'; } }
		 
		
		if ($params ['isCat'] == 0 && $params ['parentId'] != 0) {
			$dop = ' and `isCat`="1"';
		} elseif ($params ['isCat'] == 1 && $params ['parentId'] != 0) {
			$dop = ' and `isCat`="1"';
		} else {
			$dop = ' and `isCat`="0"';
		}
		
		if ($module == 'pages') {
			$dop = ' and `isCat`="0"';
		}
// 		echo "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `objId`='{$id}' {$dop}  limit 1";
		
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `objId`='{$id}' {$dop}  limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			if (empty ( $row ['name'] )) {
				$row ['name'] = $row ['objId'];
			}
			if (! empty ( $row ['custom'] )) {
				$row ['name'] = $row ['custom'];
			}
			if (in_array ( $row ['module'], self::$noIsModule ) && $row ['parentId'] == 0 && $row ['objId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 2;
				} else {
					$isCat = 1;
				}
				self::getParent ( 0, $row ['module'], $isCat, $urls, $row );
			} elseif ($row ['parentId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 2;
				} else {
					$isCat = 1;
				}
				self::getParent ( $row ['parentId'], $row ['module'], $isCat, $urls, $row );
			}
			$urls [] = urlencode ( $row ['name'] );
		} elseif (in_array ( $module, self::$noIsModule )) {
			
			$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `objId`='0' limit 1" );
			if ($sql != false && $sql->rowCount () > 0) {
				$row = $sql->fetch ( PDO::FETCH_ASSOC );
				
				if (! empty ( $row ['custom'] )) {
					$row ['name'] = $row ['custom'];
				}
				
				$urls [] = urlencode ( $row ['name'] );
			}
		}
	}
	// �������� ������� �������� ������ �� ����� URL
	static function getModuleByName($module) {
		$objId = 0;
		$dop = '';
		
		$module = urldecode ( $module );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `module` from `rewriteUrls` where `name`='" . addslashes ( $module ) . "' and `parentId`='0' and `isCat`='0' and `objId`='0' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$urls [] = $row ['module'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� ������� �������� ������
	static function getModuleName($module) {
		$objId = 0;
		$dop = '';
		
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `name` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' and `parentId`='0' and `isCat`='0' and `objId`='0' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			$urls [] = $row ['name'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� URL ������ ������� ���� ������������ ��������������
	static function getSingleUrl($params) {
		$objId = 0;
		$dop = '';
		
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`=0";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$urls [] = $row ['name'];
		}
		
		return implode ( '/', $urls );
	}
	// �������� URL ������ ������� ���� ������������ � ������
	static function getSingleCustomUrl($params) {
		$objId = 0;
		$dop = '';
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`=0";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`custom`, `objId` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$urls [] = $row ['custom'];
		}
		
		return implode ( '/', $urls );
	}
	// ��������� URL ������
	static function saveUrl($params) {
		$old = $params;
		$objId = 0;
		$parentId = 0;
		$isCat = 0;
		if (isset ( $params ['catid'] )) {
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			$objId = ( int ) $params ['catid'];
			
			$isCat = 1;
			
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] )) {
			
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`='0'";
			$objId = ( int ) $params [$params ['module']];
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		if (! isset ( $params ['custom'] )) {
			$params ['custom'] = self::getSingleCustomUrl ( $old );
		}
		if (! isset ( $params ['name'] )) {
			$params ['name'] = self::getSingleUrl ( $old );
		}
		if (isset ( $params ['parentId'] )) {
			$parentId = ( int ) $params ['parentId'];
			$dop2 = " and `parentId`='{$parentId}'";
		}
		$count = 0;
		$stm = self::getInstance ()->query ( "select count(1) from `rewriteUrls` where `module`='" . addslashes ( $params ['module'] ) . "' {$dop}" );
		if ($stm != false && $stm->rowCount () > 0) {
			$count = $stm->fetchColumn ();
		}
		if ($count == 0) {
			self::getInstance ()->exec ( "insert into `rewriteUrls` (`name`, `module`, `parentId`, `objId`, `isCat`, `custom`) values ('" . addslashes ( $params ['name'] ) . "','" . addslashes ( $params ['module'] ) . "','{$parentId}','{$objId}', '{$isCat}', '" . addslashes ( $params ['custom'] ) . "')" );
		} else {
			self::getInstance ()->exec ( "update `rewriteUrls` set `name`='" . addslashes ( $params ['name'] ) . "', `custom`='" . addslashes ( $params ['custom'] ) . "', `parentId`='{$parentId}', `isCat`='{$isCat}' where `module`='" . addslashes ( $params ['module'] ) . "' {$dop}" );
		}
	}
	static $mapper = null;
	
	// �������� ������ URL ������
	static function getUrl($params, $additionalParams = array()) {
		$objId = 0;
		$dop = '';
		$catId = 0;
		$isCat = 2;
		self::$cat = false;
		self::$item = false;
		
		$ss = $params;
		if (isset ( $params [$params ['module']] ) && ! is_numeric ( $params [$params ['module']] )) {
			
			unset ( $params [$params ['module']] );
		}
		$trueCat = 0;
		
		if (isset ( $params ['catid'] )) {
			
			self::$cat = true;
			$dop = " and `objId`='" . ( int ) $params ['catid'] . "' and `isCat`='1'";
			$catId = ( int ) $params ['catid'];
			$isCat = 1;
			$trueCat = 1;
			unset ( $params ['catid'] );
		} elseif (isset ( $params [$params ['module']] ) && is_numeric ( $params [$params ['module']] )) {
			$objId = $params [$params ['module']];
			$isCat = 0;
			self::$item = true;
			
			$dop = " and `objId`='" . ( int ) $params [$params ['module']] . "' and `isCat`='0'";
			unset ( $params [$params ['module']] );
		} else {
			$dop = " and `parentId`='0' and `objId`='0'";
		}
		
		$module = $params ['module'];
		unset ( $params ['module'] );
		$urls = array ();
		$sql = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='" . addslashes ( $module ) . "' {$dop} limit 1" );
		
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			
			if (empty ( $row ['name'] )) {
				$row ['name'] = $row ['objId'];
			}
			if (! empty ( $row ['custom'] )) {
				$row ['name'] = $row ['custom'];
			}
			
			if (in_array ( $row ['module'], self::$noIsModule ) && $row ['parentId'] == 0 && $row ['objId'] != 0) {
				
				self::getParent ( 0, $row ['module'], $isCat, $urls, $row );
				
				$urls [] = urlencode ( $row ['name'] );
			} elseif ($row ['parentId'] == 0 && $row ['objId'] == 0) {
				$module = $row ['name'];
			} elseif ($row ['parentId'] != 0) {
				if ($row ['isCat'] == 0) {
					$isCat = 1;
				} elseif (self::$cat == true) {
					$isCat = 1;
				} else {
					$isCat = 2;
				}
				
				self::getParent ( $row ['parentId'], $row ['module'], $isCat, $urls, $row );
				$urls [] = urlencode ( $row ['name'] );
			}
		} elseif (($m = self::getModuleName ( $module ))) {
			if (! empty ( $m )) {
				$module = urlencode ( $m );
			} else {
				unset ( $module );
			}
		} else {
			unset ( $module );
		}
		if (! isset ( $module )) {
			$module = $ss ['module'];
		}
		if (count ( $urls ) == 0) {
			if (isset ( $module )) {
				$urls [] = $module;
			}
			if ($objId != 0) {
				$urls [] = $objId;
			} 

			elseif ($catId != 0) {
				$urls [] = 'catid';
				$urls [] = $catId;
			}
		}
		// / dop
		if (isset ( $params ['brands'] )) {
			$sth = self::getInstance ()->query ( "select `parentId`, `module`,`name`, `objId`, `isCat`, `custom` from `rewriteUrls` where `module`='brands' and `isCat`=0 and `objId`='{$params['brands']}' limit 1" );
			if ($sth != false && $sth->rowCount () > 0) {
				$rr = $sth->fetch ();
				$flag = 0;
				if (! empty ( $rr->custom )) {
					$flag = 1;
					$urls [] = $rr->custom;
				} elseif (! empty ( $rr->name )) {
					$flag = 1;
					$urls [] = $rr->name;
				}
				if ($flag == 1) {
					unset ( $params ['brands'] );
				}
			}
		}
		
		/*
		 * if (isset ( $additionalParams ['limit'] )) {
		 * $urls [] = 'limit';
		 * $urls [] = $additionalParams ['limit'];
		 * unset ( $additionalParams ['limit'] );
		 * }
		 */
		// end dop
		$params = array_merge ( $params, $additionalParams );
		$query = http_build_query ( $params );
		if (! empty ( $query )) {
			$query = '?' . $query;
		}
		
		return '/' . implode ( '/', $urls ) . '' . $query;
	}
}
class Redactor_Ini {
	private $DBName = null;
	private $DBUser = null;
	private $DBPassword = null;
	private $DBHost = null;
	private $DBPort = 3306;
	private $DBCharset = 'cp1251';
	protected static $Stm = array ();
	protected static $Adapter = null;
	var $CurrentModule = '';
	var $CurrentObjID = 0;
	var $CurrentCatID = 0;
	
	/* ���������������� ���� */
	var $UserFieldsGroupsTable = 'UserFields_Groups';
	var $UserFieldsTable = 'UserFields';
	var $UserFieldsValues = 'UserFields_Values';
	var $UserFieldsTypes = array (
			0 => '',
			1 => '',
			2 => '',
			3 => '',
			4 => '' 
	);
	
	// TODO: ������� ������� ������� ���������� ������
	public function update($table, $fields, $where = array()) {
		$query = "UPDATE `{$table}` SET ";
		$f = array ();
		$v = array ();
		$params = array ();
		foreach ( $fields as $name => $value ) {
		}
	}
	// TODO: ������� ������� ������� ������� ������
	public function insert($table, $fields) {
	}
	function addUserField($params = array('table'=>'', 'Module'=>null, 'isCategory'=>0, 'Group'=>0), $fields = array()) {
		if (! is_array ( $params ) or ! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Module' => null,
				'isCategory' => 0,
				'Group' => 0 
		), $params );
		if (empty ( $params ['table'] ) or $params ['Module'] == null or ! isset ( $fields ['Type'] ) or $fields ['Type'] > 4 or $fields ['Type'] < 0) {
			return false;
		}
		$f = array ();
		$values = array ();
		foreach ( $fields as $name => $value ) {
			if ($name != 'Id') {
				$f ["`{$name}`"] = "?";
				$values [] = $value;
			}
		}
		$f ['`Group`'] = "?";
		$values [] = ( int ) $params ['Group'];
		$f ['`isCategory`'] = "?";
		$values [] = ( int ) $params ['isCategory'];
		$f ['`Module`'] = "?";
		$values [] = $params ['Module'];
		$sth = $this->prepare ( "INSERT INTO `{$this->UserFieldsTable}` (" . implode ( ", ", array_keys ( $f ) ) . ") VALUES (" . implode ( ", ", $f ) . ");" );
		if ($sth != false && ($sth->execute ( $values )) != false) {
			return $this->addFieldToTable ( array (
					'type' => $fields ['Type'],
					'table' => $params ['table'],
					'Id' => $this->lastId (),
					'default' => (isset ( $fields ['default'] ) ? $fields ['default'] : '') 
			) );
		}
		return false;
	}
	function addUserFieldGroup($params) {
		return $this->UpdateUserFieldsGroup ( $params );
	}
	function UpdateUserFieldsGroup($params = array('Module'=>null, 'isCategory'=>0, 'Title'=>'', 'Id'=>0)) {
		if (! is_array ( $params )) {
			return false;
		}
		$params = array_merge ( array (
				'Module' => null,
				'isCategory' => 0,
				'Title' => '',
				'Id' => 0 
		), $params );
		if ($params ['Module'] == null) {
			return false;
		}
		if ($params ['Id'] != 0) {
			$sth = $this->prepare ( "UPDATE `{$this->UserFieldsGroupsTable}` SET `Title`=?, `Module`=?, `isCategory`=? Where `Id`=?" );
			if ($sth != false && ($sth->execute ( array (
					$params ['Title'],
					$params ['Module'],
					$params ['isCategory'],
					$params ['Id'] 
			) )) != false) {
				return true;
			} else {
				return false;
			}
		} else {
			$sth = $this->prepare ( "INSERT INTO `{$this->UserFieldsGroupsTable}` (`Title`, `Module`, `isCategory`) VALUES (?, ?, ?)" );
			if ($sth != false && ($sth->execute ( array (
					$params ['Title'],
					$params ['Module'],
					$params ['isCategory'] 
			) )) != false) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}
	function updateUserField($params = array('table'=>'', 'Module'=>null, 'isCategory'=>0), $fields = array()) {
		if (! is_array ( $params ) or ! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Module' => null,
				'isCategory' => 0 
		), $params );
		if (empty ( $params ['table'] ) or $params ['Module'] == null or ! isset ( $fields ['Id'] ) or $fields ['Id'] == 0 or empty ( $fields ['Id'] ) or ! is_numeric ( $fields ['Id'] )) {
			return false;
		}
		$f = array ();
		$values = array ();
		foreach ( $fields as $name => $value ) {
			if ($name != 'Id') {
				$f [] = "`{$name}`=?";
				$values [] = $value;
			}
		}
		$values [] = ( int ) $params ['Id'];
		$values [] = $params ['Module'];
		$sth = $this->prepare ( "UPDATE `{$this->UserFieldsTable}` SET " . implode ( ", ", $f ) . " WHERE `Id`=? AND `Module`=?" );
		if ($sth != false && ($sth->execute ( $values )) != false) {
			return true;
		}
		return false;
	}
	
	/* END ���������������� ���� */
	function saveHit($r301 = false) {
		if ($r301) {
			$r301 = 1;
		} else {
			$r301 = 0;
		}
		$headers = array ();
		if (function_exists ( 'getallheaders' )) {
			$headers = getallheaders ();
		}
		$headers = array_merge ( $headers, headers_list () );
		$headers = serialize ( $headers );
		
		$stm = $this->Stm ( "insert into `Statistic` (`Module`, `Url`, `Headers`, `ObjID`, `Method`, `IP`, `USER_AGENT`, `REFERER`, `r301`, `responseCode`) VALUES (?, ?, ?, ?,?,?,?,?,?,?)" );
		if ($stm != false && ($stm->execute ( array (
				$this->CurrentModule,
				getenv ( 'REQUEST_URI' ),
				$headers,
				$this->CurrentObjID,
				getenv ( 'REQUEST_METHOD' ),
				getenv ( 'SERVER_ADDR' ),
				getenv ( 'HTTP_USER_AGENT' ),
				getenv ( 'HTTP_REFERER' ),
				$r301,
				http_response_code () 
		) ))) {
			return true;
		}
		return false;
	}
	
	// ���������� ���� �� �������������� �������
	// TODO: �������� ����������� �������� �������� ����, �������
	function formatDate($format, $date = null) {
		if ($date != null) {
			$date = strtotime ( $date );
		}
		$monthsFull = array (
				"01" => "������",
				"02" => "�������",
				"03" => "�����",
				"04" => "������",
				"05" => "���",
				"06" => "����",
				"07" => "����",
				"08" => "�������",
				"09" => "��������",
				"10" => "�������",
				"11" => "������",
				"12" => "�������" 
		);
		$monthsShort = array (
				"01" => "������",
				"02" => "�������",
				"03" => "����",
				"04" => "������",
				"05" => "���",
				"06" => "����",
				"07" => "����",
				"08" => "������",
				"09" => "��������",
				"10" => "�������",
				"11" => "������",
				"12" => "�������" 
		);
		$monthsMicro = array (
				"01" => "���",
				"02" => "���",
				"03" => "���",
				"04" => "���",
				"05" => "���",
				"06" => "���",
				"07" => "���",
				"08" => "���",
				"09" => "���",
				"10" => "���",
				"11" => "���",
				"12" => "���" 
		);
		
		if (preg_match ( "#FullMonth#is", $format )) {
			$format = str_replace ( "FullMonth", $this->ucfirst ( $monthsFull [date ( 'm', $date )] ), $format );
			$format = str_replace ( "fullmonth", $monthsFull [date ( 'm', $date )], $format );
		}
		if (preg_match ( "#ShortMonth#is", $format )) {
			$format = str_replace ( "ShortMonth", $this->ucfirst ( $monthsShort [date ( 'm', $date )] ), $format );
			$format = str_replace ( "shortmonth", $monthsShort [date ( 'm', $date )], $format );
		}
		
		if (preg_match ( "#micromonth#is", $format )) {
			$format = preg_replace ( "#micromonth#", $monthsMicro [date ( 'm', $date )], $format );
		}
		
		return date ( $format, $date );
	}
	function ucfirst($str, $enc = 'windows-1251') {
		return mb_strtoupper ( mb_substr ( $str, 0, 1, $enc ), $enc ) . mb_substr ( $str, 1, mb_strlen ( $str, $enc ), $enc );
	}
	public function getStm($Query) {
		return isset ( self::$Stm [$Query] ) ? self::$Stm [$Query] : false;
	}
	/**
	 *
	 * @param field_type $DBName        	
	 */
	private function setDBName($DBName) {
		$this->DBName = $DBName;
	}
	
	/**
	 *
	 * @param field_type $DBUser        	
	 */
	private function setDBUser($DBUser) {
		$this->DBUser = $DBUser;
	}
	
	/**
	 *
	 * @param field_type $DBPassword        	
	 */
	private function setDBPassword($DBPassword) {
		$this->DBPassword = $DBPassword;
	}
	
	/**
	 *
	 * @param field_type $DbHost        	
	 */
	private function setDBHost($DbHost) {
		$this->DBHost = $DbHost;
	}
	
	/**
	 *
	 * @param number $DbPort        	
	 */
	private function setDBPort($DbPort) {
		$this->DBPort = $DbPort;
	}
	
	/**
	 *
	 * @return the $Adapter
	 */
	public static function getAdapter() {
		return self::$Adapter;
	}
	public function Stm($Query) {
		if (isset ( self::$Stm [$Query] )) {
			return self::$Stm [$Query];
		}
		self::$Stm [$Query] = self::getAdapter ()->prepare ( $Query );
		return self::$Stm [$Query];
	}
	public function lastId($name = null) {
		return self::getAdapter ()->lastInsertId ( $name );
	}
	public function lastInsertId($name = null) {
		return self::getAdapter ()->lastInsertId ( $name );
	}
	public function prepare($Query) {
		if (isset ( self::$Stm [$Query] )) {
			return self::$Stm [$Query];
		}
		self::$Stm [$Query] = self::getAdapter ()->prepare ( $Query );
		return self::$Stm [$Query];
	}
	
	/**
	 *
	 * @param string $DbCharset        	
	 */
	private function setDBCharset($DbCharset) {
		$this->DBCharset = $DbCharset;
	}
	public function setAdapter($Adapter) {
		self::$Adapter = $Adapter;
	}
	function query($Query) {
		return self::getAdapter ()->query ( $Query );
	}
	function exec($Query) {
		return self::getAdapter ()->exec ( $Query );
	}
	function get($param) {
		$sql = $this->query ( "select `value` from `site_setting` where `option` LIKE '" . addslashes ( $param ) . "' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			return $sql->fetchColumn ();
		}
		return false;
	}
	function getOption($param) {
		$sql = $this->query ( "select `value` from `site_setting` where `option` LIKE '" . addslashes ( $param ) . "' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			return $sql->fetchColumn ();
		}
		return false;
	}
	// ����������� � ���� ������
	public function ConnectDB() {
		include ("includes/Config.php");
		$dsn = "mysql:dbname=" . $this->DBName . ";host=" . $this->DBHost . ";port={$this->DBPort}";
		try {
			self::setAdapter ( new PDO ( $dsn, $this->DBUser, $this->DBPassword, array (
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'' . $this->DBCharset . '\'',
					PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ 
			) ) );
		} catch ( PDOException $e ) {
			echo $e->getMessage ();
		}
	}
	// ������ ������� require_once � ���������
	public function includeModule($module) {
		if (file_exists ( 'modules/' . $module . ".cls.php" )) {
			require_once ('modules/' . $module . ".cls.php");
		}
	}
	
	// ��������������� �������, ������� � Json
	function JEncode($arr, $en = 1) {
		require_once ("JSON.php"); // if php<5.2 need JSON class
		$json = new Services_JSON (); // instantiate new json object
		if ($en == 1) {
			$data = $json->encode ( $arr ); // encode the data in json format
		} else {
			$data = $json->decode ( $arr );
		}
		
		return $data;
	}
	// �� Utf-8 � Windows-1251
	public function utf2win($str) {
		$text = @iconv ( "UTF-8", "windows-1251//TRANSLIT//IGNORE", $str );
		
		return $text;
	}
	// �� Windows-1251 � Utf-8
	public function win2utf($str) {
		$text = @iconv ( "windows-1251", "UTF-8//IGNORE", $str );
		
		return $text;
	}
	
	// ��������
	function translit($str) {
		$str = $this->strtolower ( $str );
		$new = "~`!@#$%^&*()+=\\|�<>{}[]:;'\",.?/";
		$p = str_split ( $new );
		
		foreach ( $p as $n => $v ) {
			$str = trim ( str_replace ( $v, "", $str ) );
		}
		
		$new = array (
				"/�/" => "y",
				"/�/" => "c",
				"/�/" => "u",
				"/�/" => "k",
				"/�/" => "e",
				"/�/" => "e",
				"/�/" => "n",
				"/�/" => "g",
				"/�/" => "sh",
				"/�/" => "sh",
				"/�/" => "z",
				"/�/" => "h",
				"/�/" => "",
				"/�/" => "f",
				"/�/" => "i",
				"/�/" => "v",
				"/�/" => "a",
				"/�/" => "p",
				"/�/" => "r",
				"/�/" => "o",
				"/�/" => "l",
				"/�/" => "d",
				"/�/" => "j",
				"/�/" => "e",
				"/�/" => "ya",
				"/�/" => "ch",
				"/�/" => "s",
				"/�/" => "m",
				"/�/" => "i",
				"/�/" => "t",
				"/�/" => "",
				"/�/" => "b",
				"/�/" => "u" 
		);
		
		$str = preg_replace ( array_keys ( $new ), array_values ( $new ), $str );
		$str = trim ( $str );
		$str = str_replace ( "       ", "-", $str );
		$str = str_replace ( "      ", "-", $str );
		$str = str_replace ( "     ", "-", $str );
		$str = str_replace ( "    ", "-", $str );
		$str = str_replace ( "   ", "-", $str );
		$str = str_replace ( "  ", "-", $str );
		$str = str_replace ( " ", "-", $str );
		
		for($i = 15; $i >= 2; $i --) {
			$str = str_replace ( str_repeat ( '-', $i ), "-", $str );
		}
		
		return $str;
	}
	// ������ ������� strtolower
	function strtolower($str) {
		$big = "��������������������������������QWERTYUIOPASDFGHJKLZXCVBNM";
		$min = "��������������������������������qwertyuiopasdfghjklzxcvbnm";
		$split = str_split ( $big );
		$split2 = str_split ( $min );
		$ar = array ();
		foreach ( $split as $n => $t ) {
			$ar ["/$t/is"] = $split2 [$n];
		}
		return preg_replace ( array_keys ( $ar ), array_values ( $ar ), $str );
	}
}
class Redactor_Admin extends Redactor_Ini {
	var $login = false;
	static $Collector = array ();
	function getModule($module) {
		$file = ucfirst ( strtolower ( $module ) );
		$moduleClass = 'Redactor_Module_' . $file;
		if (isset ( self::$Collector [$moduleClass] )) {
			return self::$Collector [$moduleClass];
		}
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		$this->includeModule ( $file );
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		return false;
	}
	// �������� ������ ���������� Ajax
	public function includeModule($module) {
		if (file_exists ( 'modules/' . $module . ".cls.php" )) {
			require_once ('modules/' . $module . ".cls.php");
		}
	}
	function clean($string) {
		return $string;
		$string = str_replace ( ' ', '-', $string ); // Replaces all spaces with hyphens.
		$string = preg_replace ( '/[^A-Za-z�-��-�0-9\-]/', '', $string ); // Removes special chars.
		
		return preg_replace ( '/-+/', '-', $string ); // Replaces multiple hyphens with single one.
	}
	// ������ ������� urlencode
	function urlencode($url) {
		if (substr ( $url, 0, 1 ) == '/') {
			$url = substr ( $url, 1, strlen ( $url ) );
		}
		$newUrl = $url;
		$ext = pathinfo ( $newUrl, PATHINFO_EXTENSION );
		
		if (strlen ( $ext ) > 0) {
			$ext = "." . $ext;
			$newUrl = substr ( $url, 0, strlen ( $url ) - strlen ( $ext ) );
		}
		$exp = explode ( '/', $newUrl );
		foreach ( $exp as $s => $u ) {
			$exp [$s] = urlencode ( $u );
		}
		return implode ( "/", $exp ) . $ext;
	}
	function __construct($exec = true) {
		if (isset ( $_GET ['xaction'] ) && $_GET ['xaction'] == 'exit') {
			unset ( $_SESSION ['admin'] );
			session_unset ();
			
			setcookie ( 'user', '', time (), '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );
			setcookie ( 'passwd', '', time (), '/', preg_replace ( "/www./", "", "." . getenv ( 'HTTP_HOST' ) ) );
			
			header ( "Location: /admincp.php" );
			exit ();
		}
		
		if ($exec == true) {
			
			$this->ConnectDB ();
			
			rewriteUrls::$mapper = Net_URL_Mapper::getInstance ();
			$modules = array (
					// 'gallery' => 'gallery',
					'news' => 'news',
					'faq' => 'faq',
					
					'articles' => 'articles',
					'pages' => 'pages',
					'search' => 'search',
					'photos' => 'photos',
					'brands' => 'brands',
					'shop' => 'shop',
					'cart' => 'cart',
					'reviews' => 'reviews',
					'gallery' => 'gallery',
					'help' => 'help',
					"catalog" => "catalog",
					
					'sitemap' => 'mapsite' 
			);
			rewriteUrls::$mapper->connect ( 'search.html', array (
					'module' => 'search' 
			) );
			rewriteUrls::$mapper->connect ( 'cart.html', array (
					'module' => 'cart' 
			) );
			rewriteUrls::$mapper->connect ( 'shop-search.html', array (
					'module' => 'shop',
					'shop' => 'search',
					'search' => true 
			) );
			foreach ( $modules as $name => $module ) {
				rewriteUrls::$mapper->connect ( $name, array (
						'module' => $module 
				) );
			}
			
			$sth = $this->prepare ( "select `module`, `isCat`, `custom`, `objId` from `rewriteUrls` where LENGTH(`custom`)>0" );
			$sth->execute ();
			foreach ( $sth->fetchAll () as $row ) {
				if ($row->isCat != 0) {
					rewriteUrls::$mapper->connect ( $this->clean ( $row->custom ), array (
							'module' => $row->module,
							'catid' => $row->objId 
					) );
				} else {
					rewriteUrls::$mapper->connect ( $this->clean ( $row->custom ), array (
							'module' => $row->module,
							$row->module => $row->objId 
					) );
				}
			}
			$modules = array (
					// 'gallery' => 'gallery',
					'news' => 'news',
					'faq' => 'faq',
					'brands' => 'brands',
					'articles' => 'articles',
					'pages' => 'pages',
					'search' => 'search',
					'photos' => 'photos',
					'shop' => 'shop',
					'cart' => 'cart',
					'reviews' => 'reviews',
					'gallery' => 'gallery',
					'help' => 'help',
					"catalog" => "catalog",
					
					'sitemap' => 'mapsite' 
			);
			rewriteUrls::$mapper->connect ( 'search.html', array (
					'module' => 'search' 
			) );
			rewriteUrls::$mapper->connect ( 'cart.html', array (
					'module' => 'cart' 
			) );
			rewriteUrls::$mapper->connect ( 'shop-search.html', array (
					'module' => 'shop',
					'shop' => 'search',
					'search' => true 
			) );
			foreach ( $modules as $name => $module ) {
				rewriteUrls::$mapper->connect ( $name, array (
						'module' => $module 
				) );
				rewriteUrls::$mapper->connect ( $name . '/:' . $module, array (
						'module' => $module 
				), array (
						"{$module}" => "([0-9]+)" 
				) );
				rewriteUrls::$mapper->connect ( $name . '/catid/:catid', array (
						'module' => $module 
				), array (
						"catid" => "([0-9]+)" 
				) );
				/*
				 * //
				 * rewriteUrls::$mapper->connect ( $name . '/catid/:catid/brands', array (
				 * 'module' => $module
				 * ), array (
				 * "catid" => "([0-9]+)",
				 * "brands" => "([0-9]+)"
				 * ) );
				 * //
				 */
				rewriteUrls::$mapper->connect ( $name . '/catid/:catid/page/:page', array (
						'module' => $module 
				), array (
						"catid" => "([0-9]+)",
						"page" => "([0-9]+)" 
				) );
				rewriteUrls::$mapper->connect ( $name . '/page/:page', array (
						'module' => $module 
				), array (
						"page" => "([0-9]+)" 
				) );
			}
			
			foreach ( rewriteUrls::$urls as $url ) {
				rewriteUrls::$mapper->connect ( $url [0], $url [1] );
			}
			
			if (! SystemUsers::iUser ()) {
				$this->login = false;
				return false;
			}
			if (isset ( $_GET ['help'] )) {
				
				exit ();
			}
			if (isset ( $_POST ['xaction'] )) {
				$task = $_POST ['xaction'];
			} elseif (isset ( $_POST ['task'] )) {
				$task = $_POST ['task'];
			} elseif (isset ( $_GET ['xaction'] )) {
				$task = $_GET ['xaction'];
			} elseif (isset ( $_GET ['task'] )) {
				$task = $_GET ['task'];
			}
			if (isset ( $_POST ['module'] )) {
				$module = $_POST ['module'];
				$extclass = $module . "_admin";
			} elseif (isset ( $_GET ['module'] )) {
				$module = $_GET ['module'];
				$extclass = $module . "_admin";
			}
			
			if (! isset ( $module )) {
				$module = "";
			}
			if (! isset ( $task )) {
				$task = "";
			}
			if (! isset ( $extclass )) {
				$extclass = "";
			}
			
			if (isset ( $task ) && $task != "") {
				if (! class_exists ( $extclass )) {
					$this->includeModule ( ucfirst ( $module ) );
					if (class_exists ( $extclass )) {
						
						$mod = new $extclass ();
						$mod->$task ();
					}
				} else {
					$mod = new $extclass ();
					$mod->$task ();
				}
				exit ();
			}
		} else {
			$this->ConnectDB ();
			if (! SystemUsers::iUser ()) {
				$this->login = false;
				return false;
			}
		}
	}
	var $UserFieldsGroupsTable = 'UserFields_Groups';
	var $UserFieldsTable = 'UserFields';
	var $UserFieldsValuesTable = 'UserFields_Values';
	var $UserFieldsTypes = array (
			0 => '',
			1 => '',
			2 => '',
			3 => '',
			4 => '' 
	);
	public function getColumns($table) {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `{$table}`" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
	final public function LoadUFGroup() {
		$Data = array ();
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		
		$table = $this->UserFieldsGroupsTable;
		
		$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ( PDO::FETCH_ASSOC );
			foreach ( $row as $name => $value ) {
				$Data [$name] = $this->win2utf ( $value );
			}
		}
		echo json_encode ( array (
				"success" => true,
				"data" => $Data 
		) );
	}
	final function addFieldToTable($params = array('table'=>null, 'Type'=>0, 'default'=>'', 'Id'=>0)) {
		if (! is_array ( $params )) {
			return false;
		}
		$params = array_merge ( array (
				'table' => '',
				'Type' => 0,
				'default' => '',
				'Id' => 0 
		), $params );
		if (empty ( $params ['table'] ) or empty ( $params ['Id'] ) or ! is_numeric ( $params ['Id'] )) {
			return false;
		}
		$params ['Id'] = ( int ) $params ['Id'];
		if ($params ['Type'] == 1) {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` TEXT NOT NULL ;" );
		} elseif ($params ['Type'] == 3) {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` TEXT NOT NULL ;" );
		} elseif ($params ['Type'] == 7) {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` DATE NOT NULL ;" );
		} elseif ($params ['Type'] == 2) {
		} elseif ($params ['Type'] == 4 or $params ['Type'] == 5 or $params ['Type'] == 6) {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` INT(11) NOT NULL DEFAULT  '0';" );
		} else {
			$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$params['Id']}` VARCHAR( 100 ) NOT NULL ;" );
		}
	}
	final public function addUFField() {
		$params ['Title'] = isset ( $_POST ['Title'] ) ? $this->utf2win ( $_POST ['Title'] ) : '��� ��������';
		$params ['Type'] = isset ( $_POST ['Type'] ) ? ( int ) $_POST ['Type'] : 0;
		$params ['Group'] = isset ( $_POST ['Group'] ) ? ( int ) $_POST ['Group'] : 0;
		$params ['Module'] = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$params ['isCategory'] = isset ( $_POST ['isCategory'] ) ? strtolower ( $_POST ['isCategory'] ) : '';
		$sth = $this->prepare ( "INSERT INTO `UserFields` (`Module`,`Group`, `Type`, `isCategory`, `Title`) VALUES (?, ?,?,?,?)" );
		if ($sth != false && ($sth->execute ( array (
				$params ['Module'],
				$params ['Group'],
				$params ['Type'],
				$params ['isCategory'],
				$params ['Title'] 
		) ))) {
			if ($params ['isCategory'] == 1) {
				$params ['table'] = $this->CategoriesTable;
			} else {
				$params ['table'] = $this->ItemsTable;
			}
			$params ['Id'] = $this->lastId ();
			$this->addFieldToTable ( $params );
			echo json_encode ( array (
					"success" => true,
					'Id' => $params ['Id'] 
			) );
			return true;
		}
		echo json_encode ( array (
				"failure" => true 
		) );
	}
	// TODO: �������� � ������� ������� ����� �� ��������� � ����� ����
	final public function DeleteUFCategory() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if ($Id != 0) {
			$this->exec ( "DELETE FROM {$this->UserFieldsGroupsTable} WHERE `Id`='{$Id}' LIMIT 1" );
			$this->exec ( "UPDATE `{$this->UserFieldsTable}` SET `Group`='0' WHERE `Group`='{$Id}'" );
		}
		echo json_encode ( array (
				"success" => true 
		) );
	}
	final public function SaveUFGroup() {
		$params ['Module'] = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$params ['Id'] = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$params ['Title'] = isset ( $_POST ['Title'] ) ? $this->utf2win ( $_POST ['Title'] ) : '';
		$params ['isCategory'] = isset ( $_POST ['isCategory'] ) ? ( int ) $_POST ['isCategory'] : 0;
		if ($params ['isCategory'] != 0 && $params ['isCategory'] != 1) {
			$params ['isCategory'] = 0;
		}
		if ($this->UpdateUserFieldsGroup ( $params )) {
			echo json_encode ( array (
					'success' => true 
			) );
		} else {
			echo json_encode ( array (
					'failure' => true,
					'error' => $this->getAdapter ()->errorInfo () 
			) );
		}
	}
	final public function DeleteUF() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$Module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$this->exec ( "ALTER TABLE  `{$this->ItemsTable}` DROP  `uf_{$Id}`" );
		$this->exec ( "DELETE FROM `{$this->UserFieldsTable}` WHERE `Id`='{$Id}'" );
		$sth = $this->prepare ( "SELECT `Id`, `UFId` FROM  `{$this->UserFieldsValuesTable}` WHERE `UFId`='{$Id}'" );
		$sth->execute ();
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$this->exec ( "ALTER TABLE  `{$this->ItemsTable}` DROP  `uf_{$Id}_{$row->Id}`" );
			}
		}
		$this->exec ( "DELETE FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`='{$Id}'" );
		echo json_encode ( array (
				'success' => true 
		) );
	}
	final public function DeleteUFValue() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$sth = $this->prepare ( "SELECT `Id`, `UFId` FROM  `{$this->UserFieldsValuesTable}` WHERE `Id`='{$Id}'" );
		$sth->execute ();
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$this->exec ( "ALTER TABLE  `{$this->ItemsTable}` DROP  `uf_{$row->UFId}_{$row->Id}`" );
			}
		}
		$this->exec ( "DELETE FROM `{$this->UserFieldsValuesTable}` WHERE `Id`='{$Id}'" );
	}
	final public function LoadUFValues() {
		$results = array ();
		$rows = 0;
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->UserFieldsValuesTable}` WHERE `{$this->UserFieldsValuesTable}`.`UFId`=?" );
		
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		if ($count != false && ($count->execute ( array (
				$Id 
		) )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `{$this->UserFieldsValuesTable}`.`Id`,`{$this->UserFieldsValuesTable}`.`UFId`, `{$this->UserFieldsValuesTable}`.`Value`, `{$this->UserFieldsValuesTable}`.`Sort`
			FROM `{$this->UserFieldsValuesTable}` WHERE `{$this->UserFieldsValuesTable}`.`UFId`=? ORDER BY `Sort` LIMIT {$start}, {$limit}" );
			
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				
				foreach ( $sth->fetchAll () as $row ) {
					
					$results [] = array (
							"Id" => $row->Id,
							"UFId" => $row->UFId,
							"Sort" => $row->Sort,
							"Value" => $this->win2utf ( $row->Value ) 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	final public function SaveUFValue() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		$columns = $this->getColumns ( $this->UserFieldsValuesTable );
		$fields = array ();
		$cols = array ();
		foreach ( $_POST as $name => $value ) {
			if (in_array ( $name, $columns )) {
				$fields [] = '`' . $name . '`=?';
				$cols [] = $this->utf2win ( $value );
			}
		}
		$cols [] = $Id;
		
		$sth = $this->prepare ( "UPDATE `{$this->UserFieldsValuesTable}` SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( $cols )) != false) {
			echo json_encode ( array (
					'success' => true 
			) );
			return true;
		}
		echo json_encode ( array (
				'failure' => true,
				'error' => $this->getAdapter ()->errorInfo () 
		) );
	}
	final public function CreateUFCheckboxValue() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		$params = array ();
		if (isset ( $_POST ['isCategory'] ) && $_POST ['isCategory'] == 1) {
			$params ['table'] = $this->CategoriesTable;
		} else {
			$params ['table'] = $this->ItemsTable;
		}
		$this->exec ( "INSERT INTO `{$this->UserFieldsValuesTable}` (`UFId`) VALUES ('{$Id}')" );
		$new = $this->lastId ();
		$this->exec ( "ALTER TABLE  `{$params['table']}` ADD  `uf_{$Id}_{$new}` INT( 1) NOT NULL DEFAULT  '0';" );
		echo $new;
	}
	final public function CreateUFValue() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		$params = array ();
		
		$this->exec ( "INSERT INTO `{$this->UserFieldsValuesTable}` (`UFId`) VALUES ('{$Id}')" );
		$new = $this->lastId ();
		echo $new;
	}
	final public function SaveUFField() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		$columns = $this->getColumns ( $this->UserFieldsTable );
		$fields = array ();
		$cols = array ();
		foreach ( $_POST as $name => $value ) {
			if (in_array ( $name, $columns )) {
				$fields [] = '`' . $name . '`=?';
				$cols [] = $this->utf2win ( $value );
			}
		}
		$cols [] = $Id;
		
		$sth = $this->prepare ( "UPDATE {$this->UserFieldsTable} SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( $cols )) != false) {
			echo json_encode ( array (
					'success' => true 
			) );
			return true;
		}
		echo json_encode ( array (
				'failure' => true,
				'error' => $this->getAdapter ()->errorInfo () 
		) );
	}
	final public function LoadUFRecords() {
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		
		$results = array ();
		$rows = 0;
		$Group = isset ( $_POST ['Group'] ) ? ( int ) $_POST ['Group'] : 0;
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->UserFieldsTable}` WHERE `{$this->UserFieldsTable}`.`Group`=? AND `Module`=?" );
		
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		if ($count != false && ($count->execute ( array (
				$Group,
				strtolower ( $_POST ['module'] ) 
		) )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `{$this->UserFieldsTable}`.`Id`, `{$this->UserFieldsTable}`.`Title`, `{$this->UserFieldsTable}`.`Sort`, `{$this->UserFieldsTable}`.`Type`
		FROM `{$this->UserFieldsTable}` WHERE `{$this->UserFieldsTable}`.`Group`=? AND `Module`=? ORDER BY `Sort` LIMIT {$start}, {$limit}" );
			
			if ($sth != false && ($sth->execute ( array (
					$Group,
					strtolower ( $_POST ['module'] ) 
			) )) != false && $sth->rowCount () > 0) {
				
				foreach ( $sth->fetchAll () as $row ) {
					
					$results [] = array (
							"Id" => $row->Id,
							"Sort" => $row->Sort,
							"Title" => $this->win2utf ( $row->Title ),
							"Type" => $row->Type 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	final private function getField($row, $Item = null) {
		$field = array (
				'style' => array (
						'paddingLeft' => '0px' 
				) 
		);
		if ($row->Type == 1) {
			$field ['xtype'] = 'textarea';
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			$field ['anchor'] = '90%';
			
			$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
		} elseif ($row->Type == 2) {
			$sth = $this->prepare ( "SELECT `Id`, `UFId`, `Value` FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`=? ORDER BY `Sort` " );
			$sth->execute ( array (
					$row->Id 
			) );
			if ($sth != false && $sth->rowCount () > 0) {
				$field ['xtype'] = 'checkboxgroup';
				$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
				$field ['items'] = array ();
				$field ['name'] = 'uf_' . $row->Id;
				$field ['columns'] = 5;
				$hidden = array ();
				foreach ( $sth->fetchAll () as $row ) {
					
					$field ['items'] [] = array (
							'boxLabel' => $this->win2utf ( $row->Value ),
							'name' => 'uf_' . $row->UFId . '_' . $row->Id,
							'value' => '1',
							'inputValue' => '1',
							'uncheckedValue' => '0',
							'checked' => (($Item != null && isset ( $Item ['uf_' . $row->UFId . '_' . $row->Id] ) && $Item ['uf_' . $row->UFId . '_' . $row->Id] == 1) ? true : false) 
					);
				}
			} else {
				$field ['xtype'] = 'hidden';
			}
		} elseif ($row->Type == 8) {
			$sth = $this->prepare ( "SELECT `Id`, `UFId`, `Value` FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`=? ORDER BY `Sort` " );
			$sth->execute ( array (
					$row->Id 
			) );
			if ($sth != false && $sth->rowCount () > 0) {
				$field ['xtype'] = 'checkboxgroup';
				$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
				$field ['items'] = array ();
				$field ['name'] = 'uf_' . $row->Id;
				$field ['columns'] = 5;
				$hidden = array ();
				foreach ( $sth->fetchAll () as $row ) {
					
					$field ['items'] [] = array (
							'boxLabel' => $this->win2utf ( $row->Value ),
							'name' => 'uf_' . $row->UFId . '_' . $row->Id,
							'value' => '1',
							'inputValue' => '1',
							'uncheckedValue' => '0',
							'checked' => (($Item != null && isset ( $Item ['uf_' . $row->UFId . '_' . $row->Id] ) && $Item ['uf_' . $row->UFId . '_' . $row->Id] == 1) ? true : false) 
					);
				}
			} else {
				$field ['xtype'] = 'hidden';
			}
		} elseif ($row->Type == 5) {
			$sth = $this->prepare ( "SELECT `Id`, `UFId`, `Value` FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`=? ORDER BY `Sort` " );
			$sth->execute ( array (
					$row->Id 
			) );
			if ($sth != false && $sth->rowCount () > 0) {
				$field ['xtype'] = 'radiogroup';
				$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
				$field ['items'] = array ();
				$field ['name'] = 'uf_' . $row->Id;
				$field ['columns'] = 5;
				$hidden = array ();
				$i = 0;
				foreach ( $sth->fetchAll () as $row ) {
					$i ++;
					if ($i == 1) {
						$field ['items'] [] = array (
								'boxLabel' => $this->win2utf ( '��� ������' ),
								'name' => 'uf_' . $row->UFId,
								'value' => '0',
								'inputValue' => '0',
								
								'checked' => (($Item != null && isset ( $Item ['uf_' . $row->UFId] ) && $Item ['uf_' . $row->UFId] == 0) ? true : false) 
						);
					}
					$field ['items'] [] = array (
							'boxLabel' => $this->win2utf ( $row->Value ),
							'name' => 'uf_' . $row->UFId,
							'value' => $row->Id,
							'inputValue' => $row->Id,
							
							'checked' => (($Item != null && isset ( $Item ['uf_' . $row->UFId] ) && $Item ['uf_' . $row->UFId] == $row->Id) ? true : false) 
					);
				}
			} else {
				$field ['xtype'] = 'hidden';
			}
		} elseif ($row->Type == 3) {
			$field = array (
					'style' => array (
							'paddingLeft' => '0px' 
					),
					'name' => 'uf_' . $row->Id,
					'xtype' => 'tinymce',
					'width' => 840,
					'fieldLabel' => '<b>' . $this->win2utf ( $row->Title ) . '</b>',
					'tinymceSettings' => array (
							'convert_urls' => 0,
							'relative_urls' => 0,
							'height' => 150,
							'width' => 750,
							
							'forced_root_block' => false,
							'force_br_newlines' => true,
							'force_p_newlines' => false,
							'extended_valid_elements' => "a[charset|coords|href|hreflang|name|rel|rev|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],abbr[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],acronym[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],address[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],applet[code|object|align|alt|archive|codebase|height|hspace|name|vspace|width|class|id|style|title],area[alt|coords|href|nohref|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],b[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],base[href|target],basefont[color|face|size|class|dir|id|lang|style|title],bdo[dir|class|id|lang|style|title],big[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],blockquote[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],body[alink|background|bgcolor|link|text|vlink|class|dir|id|lang|style|title|onclick|ondblclick|onload|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onunload],br[class|id|style|title],button[disabled|name|type|value|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],caption[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],center[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],cite[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],code[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],col[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],colgroup[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],del[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dfn[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dir[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],div[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dl[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],em[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],fieldset[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],font[color|face|size|class|dir|id|lang|style|title],form[action|accept|accept-charset|enctype|method|name|target|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onreset|onsubmit],frame[frameborder|longdesc|marginheight|marginwidth|name|noresize|scrolling|src|class|id|style|title],frameset[cols|rows|class|id|style|title|onload|onunload],head[profile|dir|lang],h1[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h2[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h3[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h4[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h5[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h6[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],hr[align|noshade|size|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],html[xmlns|dir|lang],i[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],iframe[align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width|class|id|style|title],img[alt|src|align|border|height|hspace|ismap|longdesc|usemap|vspace|width|class|dir|id|lang|style|title|onabort|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],input[],ins[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],kbd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],label[for|accesskey|class|dir|id|lang|style|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],legend[align|accesskey|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],li[type|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],link[charset|href|hreflang|media|rel|rev|target|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],map[name|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],menu[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],meta[content|http-equiv|name|scheme|dir|lang],noframes[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],noscript[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],object[align|archive|border|classid|codebase|codetype|data|declare|height|hspace|name|standby|type|usemap|vspace|width|class|dir|id|lang|style|tabindex|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ol[compact|start|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],optgroup[label|disabled|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],option[disabled|label|selected|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],p[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],param[name|type|value|valuetype|id],pre[width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],q[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],s[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],samp[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],script[type|charset|defer|src|xml:space],select[disabled|multiple|name|size|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],small[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],span[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strike[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strong[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|media|dir|lang|title],sub[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],sup[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],table[align|bgcolor|border|cellpadding|cellspacing|frame|rules|summary|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tbody[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],td[abbr|align|axis|bgcolor|char|charoff|colspan|headers|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],textarea[cols|rows|disabled|name|readonly|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onselect],tfoot[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],th[abbr|align|axis|bgcolor|char|charoff|colspan|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],thead[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],title[dir|lang],tr[align|bgcolor|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],u[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ul[compact|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],var[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup]",
							'theme' => "advanced",
							'plugins' => "safari,pagebreak,filemanager, imagemanager,fullscreen,style,layer,table,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
							'theme_advanced_buttons1' => "insertfile,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,pasteword,bullist,numlist,outdent,indent,blockquote,forecolor,backcolor,sub,|,fullscreen",
							'theme_advanced_buttons2' => "filemanager,imagemanager, undo,redo,link,unlink,anchor,image,cleanup,code,preview,tablecontrols,hr,removeformat,visualaid,advhr,print,insertlayer,moveforward,movebackward,absolute,cite,visualchars",
							'theme_advanced_statusbar_location' => "bottom",
							'theme_advanced_resizing' => false,
							'extended_valid_elements' => "a[charset|coords|href|hreflang|name|rel|rev|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],abbr[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],acronym[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],address[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],applet[code|object|align|alt|archive|codebase|height|hspace|name|vspace|width|class|id|style|title],area[alt|coords|href|nohref|shape|target|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],b[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],base[href|target],basefont[color|face|size|class|dir|id|lang|style|title],bdo[dir|class|id|lang|style|title],big[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],blockquote[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],body[alink|background|bgcolor|link|text|vlink|class|dir|id|lang|style|title|onclick|ondblclick|onload|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onunload],br[class|id|style|title],button[disabled|name|type|value|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],caption[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],center[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],cite[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],code[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],col[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],colgroup[align|char|charoff|span|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],del[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dfn[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dir[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],div[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dl[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],dt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],em[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],fieldset[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],font[color|face|size|class|dir|id|lang|style|title],form[action|accept|accept-charset|enctype|method|name|target|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onreset|onsubmit],frame[frameborder|longdesc|marginheight|marginwidth|name|noresize|scrolling|src|class|id|style|title],frameset[cols|rows|class|id|style|title|onload|onunload],head[profile|dir|lang],h1[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h2[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h3[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h4[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h5[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],h6[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],hr[align|noshade|size|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],html[xmlns|dir|lang],i[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],iframe[align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width|class|id|style|title],img[alt|src|align|border|height|hspace|ismap|longdesc|usemap|vspace|width|class|dir|id|lang|style|title|onabort|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],input[],ins[cite|datetime|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],kbd[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],label[for|accesskey|class|dir|id|lang|style|title|onblur|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],legend[align|accesskey|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],li[type|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],link[charset|href|hreflang|media|rel|rev|target|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],map[name|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],menu[compact|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],meta[content|http-equiv|name|scheme|dir|lang],noframes[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],noscript[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],object[align|archive|border|classid|codebase|codetype|data|declare|height|hspace|name|standby|type|usemap|vspace|width|class|dir|id|lang|style|tabindex|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ol[compact|start|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],optgroup[label|disabled|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],option[disabled|label|selected|value|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],p[align|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],param[name|type|value|valuetype|id],pre[width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],q[cite|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],s[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],samp[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],script[type|charset|defer|src|xml:space],select[disabled|multiple|name|size|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],small[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],span[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strike[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],strong[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],style[type|media|dir|lang|title],sub[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],sup[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],table[align|bgcolor|border|cellpadding|cellspacing|frame|rules|summary|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tbody[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],td[abbr|align|axis|bgcolor|char|charoff|colspan|headers|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],textarea[cols|rows|disabled|name|readonly|accesskey|class|dir|id|lang|style|tabindex|title|onblur|onchange|onclick|ondblclick|onfocus|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup|onselect],tfoot[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],th[abbr|align|axis|bgcolor|char|charoff|colspan|height|nowrap|rowspan|scope|valign|width|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],thead[align|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],title[dir|lang],tr[align|bgcolor|char|charoff|valign|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],tt[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],u[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],ul[compact|type|class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup],var[class|dir|id|lang|style|title|onclick|ondblclick|onmousedown|onmousemove|onmouseout|onmouseover|onmouseup|onkeydown|onkeypress|onkeyup]",
							
							'template_external_list_url' => "example_template_list.js",
							'theme_advanced_buttons3' => "",
							'theme_advanced_toolbar_location' => "top",
							'theme_advanced_toolbar_align' => "left",
							'theme_advanced_path' => false,
							'theme_advanced_resize_horizontal' => false,
							'theme_advanced_resizing' => true 
					) 
			);
		} elseif ($row->Type == 4) {
			
			$sth = $this->prepare ( "SELECT `Id`, `Value` FROM `{$this->UserFieldsValuesTable}` WHERE `UFId`=? ORDER BY `Sort`" );
			if ($sth != false && $sth->execute ( array (
					$row->Id 
			) ) && $sth->rowCount () > 0) {
				$field = array (
						'xtype' => 'combo',
						'style' => array (
								'paddingLeft' => '0px' 
						),
						'typeAhead' => true,
						'triggerAction' => 'all',
						'fieldLabel' => '������',
						'store' => array (
								'xtype' => 'arraystore',
								'autoDestroy' => true,
								'idIndex' => 0,
								'fields' => array (
										'value',
										'display' 
								),
								'data' => array (
										array (
												0,
												$this->win2utf ( '�� �������' ) 
										) 
								) 
						),
						'editable' => false,
						'mode' => 'local',
						'displayField' => 'display',
						'valueField' => 'value',
						'fieldLabel' => '<b>' . $this->win2utf ( $row->Title ) . '</b>',
						'name' => 'uf_' . $row->Id,
						'hiddenName' => 'uf_' . $row->Id,
						'lazyRender' => true,
						'listClass' => 'x-combo-list-small' 
				);
				foreach ( $sth->fetchAll () as $row ) {
					$field ['store'] ['data'] [] = array (
							$row->Id,
							$this->win2utf ( $row->Value ) 
					);
				}
			} else {
				$field ['xtype'] = 'hidden';
			}
		} elseif ($row->Type == 7) {
			$field ['xtype'] = 'datefield';
			$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
			$field ['value'] = date ( 'Y-m-d' );
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			$field ['format'] = 'Y-m-d';
		} elseif ($row->Type == 6) {
			
			$field ['xtype'] = 'numberfield';
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
		} else {
			$field ['xtype'] = 'textfield';
			$field ['name'] = 'uf_' . $row->Id;
			$field ['id'] = 'uf_' . $row->Id;
			$field ['fieldLabel'] = '<b>' . $this->win2utf ( $row->Title ) . '</b>';
		}
		return $field;
	}
	final public function getUFGroupName($id) {
		$sth = $this->prepare ( "SELECT `Title` FROM `{$this->UserFieldsGroupsTable}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$id 
		) )) != false && $sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return '��� ��������';
	}
	final public function getUFJS($module, $Group = 0, $isCategory = 0, $Item = null) {
		$module = strtolower ( $module );
		$fields = array ();
		$fields2 = array ();
		$sth = $this->prepare ( "SELECT * FROM {$this->UserFieldsTable} WHERE `Module` LIKE ? AND `isCategory`=? AND `Group`=? ORDER BY `Sort` ASC" );
		if ($sth != false && ($sth->execute ( array (
				$module,
				$isCategory,
				0 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				
				$fields2 [] = $this->getField ( $row, $Item );
			}
			$fields [] = array (
					'xtype' => 'fieldset',
					'title' => $this->win2utf ( '�����' ),
					'autoHeight' => true,
					'collapsible' => true,
					'defaults' => array (
							'xtype' => 'textfield' 
					),
					'layout' => 'form',
					'items' => $fields2 
			);
		}
		
		if ($Group != 0) {
			$fields2 = array ();
			$sth = $this->prepare ( "SELECT * FROM {$this->UserFieldsTable} WHERE `Module` LIKE ? AND `isCategory`=? AND `Group`=? ORDER BY `Sort` ASC" );
			if ($sth != false && ($sth->execute ( array (
					$module,
					$isCategory,
					$Group 
			) )) != false && $sth->rowCount () > 0) {
				
				foreach ( $sth->fetchAll () as $row ) {
					
					$fields2 [] = $this->getField ( $row, $Item );
				}
				$fields [] = array (
						'xtype' => 'fieldset',
						'defaults' => array (
								'xtype' => 'textfield' 
						),
						'title' => $this->win2utf ( $this->getUFGroupName ( $Group ) ),
						'autoHeight' => true,
						'collapsible' => true,
						'layout' => 'form',
						'items' => $fields2 
				);
			}
		}
		$fields [] = array (
				'xtype' => 'hidden',
				'name' => 'uffields',
				'value' => 1 
		);
		return $fields;
	}
	final public function LoadUFCategories() {
		$Data = array (
				array (
						"Title" => $this->win2utf ( '�����' ),
						"Id" => 0,
						'text' => $this->win2utf ( '�����' ),
						"id" => 0,
						'icon' => '/core/icons/folder.png',
						'draggable' => false,
						"leaf" => true 
				) 
		);
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$sth = $this->Stm ( "Select `Id`, `Title` from `{$this->UserFieldsGroupsTable}` WHERE `Module`=? ORDER BY `Title`" );
		if ($sth != false && ($sth->execute ( array (
				$module 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				$item = array (
						"Title" => $this->win2utf ( $row->Title ),
						"Id" => $row->Id,
						'text' => $this->win2utf ( $row->Title ),
						"id" => $row->Id,
						'icon' => '/core/icons/folder.png',
						'draggable' => false,
						"leaf" => true 
				);
				
				$Data [] = $item;
			}
		}
		echo json_encode ( $Data );
	}
	final public function getUFGroups() {
		$Data = array (
				array (
						
						0,
						$this->win2utf ( '�� �������' ) 
				) 
		);
		$module = isset ( $_POST ['module'] ) ? strtolower ( $_POST ['module'] ) : '';
		$sth = $this->Stm ( "Select `Id`, `Title` from `{$this->UserFieldsGroupsTable}` WHERE `Module`=? ORDER BY `Title`" );
		if ($sth != false && ($sth->execute ( array (
				$module 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				$Data [] = array (
						$row->Id,
						$this->win2utf ( $row->Title ) 
				);
			}
		}
		return $Data;
	}
	final public function LoadUFField() {
		$Data = array ();
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		
		$table = $this->UserFieldsTable;
		
		$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ( PDO::FETCH_ASSOC );
			foreach ( $row as $name => $value ) {
				$Data [$name] = $this->win2utf ( $value );
			}
		}
		echo json_encode ( array (
				"success" => true,
				"data" => $Data 
		) );
	}
	final public function LoadUFValue() {
		$Data = array ();
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		
		$table = $this->UserFieldsValuesTable;
		
		$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ( PDO::FETCH_ASSOC );
			foreach ( $row as $name => $value ) {
				$Data [$name] = $this->win2utf ( $value );
			}
		}
		echo json_encode ( array (
				"success" => true,
				"data" => $Data 
		) );
	}
}
class Redactor_Action extends Redactor_Ini {
	private $module = "pages";
	private static $FileTemplate = 'index';
	private static $Template = 'default';
	static $defaultUrls = array ();
	static $brands = array ();
	protected static $Collector = array ();
	var $content = '';
	static $noView = false;
	static $noTemplate = false;
	function setDisableView($s = true) {
		if (is_bool ( $s )) {
			self::$noView = $s;
		}
	}
	function setDisableTemplate($s = true) {
		if (is_bool ( $s )) {
			self::$noTemplate = $s;
		}
	}
	
	/**
	 *
	 * @return the $Template
	 */
	public static function getTemplate() {
		return Redactor_Action::$Template;
	}
	function getParentCategoryId($id) {
		$sth = $this->prepare ( "select `CategoryID` from `Catalog_Items` where `Id`=? limit 1" );
		$sth->execute ( array (
				$id 
		) );
		if ($sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return 0;
	}
	/**
	 *
	 * @param string $Template        	
	 */
	public static function setTemplate($Template) {
		Redactor_Action::$Template = $Template;
	}
	
	// ������������� ������� ���� �������
	function setFileTemplate($filename) {
		self::$FileTemplate = $filename;
	}
	// ���������� ������� ���� �������
	function getFileTemplate() {
		return self::$FileTemplate;
	}
	// ��������� � ������ URL ������� �� ���������
	function addDefaultUrl($module, $string, $params = array()) {
		self::$defaultUrls [$module] [$string] = $params;
	}
	// ������������� ��� ������� URL �� ���������
	// TODO: ������� �������������� ��������� URL � ����������� �� ������������� �������
	function defineDefaultUrls() {
		self::addDefaultUrl ( "pages", "pages-(?P<pages>[0-9]+?).html" );
		self::addDefaultUrl ( "news", "news.html" );
		self::addDefaultUrl ( "news", "news-page-(?P<page>[0-9]+?).html" );
		self::addDefaultUrl ( "news", "news-(?P<news>[0-9]+?).html" );
		self::addDefaultUrl ( "articles", "articles.html" );
		self::addDefaultUrl ( "articles", "articles-page-(?P<page>[0-9]+?).html" );
		self::addDefaultUrl ( "articles", "articles-(?P<articles>[0-9]+?).html" );
		
		self::addDefaultUrl ( "faq", "faq.html" );
		self::addDefaultUrl ( "faq", "faq-page-(?P<page>[0-9]+?).html" );
		
		self::addDefaultUrl ( "reviews", "reviews.html" );
		self::addDefaultUrl ( "reviews", "reviews-page-(?P<page>[0-9]+?).html" );
	}
	function clean($string) {
		return $string;
		$string = str_replace ( ' ', '-', $string ); // Replaces all spaces with hyphens.
		$string = preg_replace ( '/[^A-Za-z�-��-�0-9\-]/', '', $string ); // Removes special chars.
		
		return preg_replace ( '/-+/', '-', $string ); // Replaces multiple hyphens with single one.
	}
	// ������ ������� urlencode
	function urlencode($url) {
		if (substr ( $url, 0, 1 ) == '/') {
			$url = substr ( $url, 1, strlen ( $url ) );
		}
		$newUrl = $url;
		$ext = pathinfo ( $newUrl, PATHINFO_EXTENSION );
		
		if (strlen ( $ext ) > 0) {
			$ext = "." . $ext;
			$newUrl = substr ( $url, 0, strlen ( $url ) - strlen ( $ext ) );
		}
		$exp = explode ( '/', $newUrl );
		foreach ( $exp as $s => $u ) {
			$exp [$s] = urlencode ( $u );
		}
		return implode ( "/", $exp ) . $ext;
	}
	// ���������� false ��� ��������� ������ ������
	function getModule($module) {
		$file = ucfirst ( strtolower ( $module ) );
		$moduleClass = 'Redactor_Module_' . $file;
		if (isset ( self::$Collector [$moduleClass] )) {
			return self::$Collector [$moduleClass];
		}
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		$this->includeModule ( $file );
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ();
			return self::$Collector [$moduleClass];
		}
		return false;
	}
	// ���������� ����� ����� �� ������ �����
	function getBlock($name) {
		$sql = $this->Stm ( "select `text` from `blocks` where `name` LIKE ? limit 1" );
		$sql->execute ( array (
				$name 
		) );
		if ($sql != false && $sql->rowCount () != false) {
			return $sql->fetchColumn ();
		}
		return '';
	}
	// ���������� ������ URL ��� ������
	function getUrl($params, $additionalParams = array()) {
		return rewriteUrls::getUrl ( $params, $additionalParams );
	}
	// ���������� false ��� ��������� ������ ��������� ����
	function getHelper($helperName) {
		$file = ucfirst ( strtolower ( $helperName ) );
		
		$moduleClass = 'Redactor_Helper_' . $file;
		if (isset ( self::$Collector [$moduleClass] )) {
			return self::$Collector [$moduleClass];
		}
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ( $moduleClass );
			return self::$Collector [$moduleClass];
		}
		if (file_exists ( "Helpers/{$file}.php" )) {
			require_once "Helpers/{$file}.php";
		}
		
		if (class_exists ( $moduleClass )) {
			self::$Collector [$moduleClass] = new $moduleClass ( $moduleClass );
			return self::$Collector [$moduleClass];
		}
		return false;
	}
	
	// ���������� �������� ������ ��� ������� � MySQL ������
	function changeFields($value) {
		return "`{$value}`";
	}
	function LayoutPath() {
		return "/template/default/";
	}
	static $VirualPageRow = false;
	static function isVirtualPage() {
		if (self::$VirualPageRow === false) {
			return false;
		}
		return true;
	}
	static function getVirtualPage() {
		return self::$VirualPageRow;
	}
	function virtualPage() {
		$path = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_PATH );
		$query = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_QUERY );
		$sth = $this->prepare ( "select * from `Virtpages` WHERE `To`=? LIMIT 1" );
		$sth->execute ( array (
				$path 
		) );
		
		if ($sth->rowCount () > 0) {
			$row = $sth->fetch ();
			
			self::$VirualPageRow = $row;
			$oldPath = parse_url ( $row->From, PHP_URL_PATH );
			$oldQuery = parse_url ( $row->From, PHP_URL_QUERY );
			$a = array ();
			parse_str ( $oldQuery, $a );
			$get = $this->matchUrl ( $oldPath );
			$a2 = array ();
			parse_str ( $query, $a2 );
			$get = array_merge ( $_GET, $a, $a2 );
			return $get;
		}
		return false;
	}
	// ���������� ������� �������� ������
	function getContent() {
		return $this->content;
	}
	function testRedirect() {
		$sth = $this->prepare ( "select * FROM `Redirects` WHERE `From`=? AND `Active`=1 LIMIT 1" );
		$sth->execute ( array (
				getenv ( 'REQUEST_URI' ) 
		) );
		
		if ($sth->rowCount () > 0) {
			$row = $sth->fetch ();
			
			header ( "Location: {$row->To}", true, $row->Type );
			exit ();
		}
	}
	function view() {
		$this->ConnectDB ();
		
		rewriteUrls::$mapper = Net_URL_Mapper::getInstance ();
		$modules = array (
				// 'gallery' => 'gallery',
				'news' => 'news',
				'faq' => 'faq',
				'brands' => 'brands',
				'articles' => 'articles',
				'pages' => 'pages',
				'search' => 'search',
				'photos' => 'photos',
				'shop' => 'shop',
				'cart' => 'cart',
				'reviews' => 'reviews',
				'gallery' => 'gallery',
				'help' => 'help',
				"catalog" => "catalog",
				
				'sitemap' => 'mapsite' 
		);
		rewriteUrls::$mapper->connect ( 'search.html', array (
				'module' => 'search' 
		) );
		rewriteUrls::$mapper->connect ( 'cart.html', array (
				'module' => 'cart' 
		) );
		rewriteUrls::$mapper->connect ( 'shop-search.html', array (
				'module' => 'shop',
				'shop' => 'search',
				'search' => true 
		) );
		foreach ( $modules as $name => $module ) {
			rewriteUrls::$mapper->connect ( $name, array (
					'module' => $module 
			) );
		}
		/*
		 * $sth = $this->prepare ( "select `module`, `isCat`, `custom`, `objId` from `rewriteUrls` where LENGTH(`custom`)>0" ); $sth->execute (); foreach ( $sth->fetchAll () as $row ) { if ($row->isCat != 0) { rewriteUrls::$mapper->connect ( $this->clean ( $row->custom ), array ( 'module' => $row->module, 'catid' => $row->objId ) ); } else { rewriteUrls::$mapper->connect ( $this->clean ( $row->custom ), array ( 'module' => $row->module, $row->module => $row->objId ) ); } } $sth = $this->prepare ( "select `module`, `isCat`, `name` , `objId` from `rewriteUrls` where `module`='articles' and LENGTH(`name`)>0" ); $sth->execute (); foreach ( $sth->fetchAll () as $row ) { if ($row->isCat != 0) { rewriteUrls::$mapper->connect ( $this->clean ( $row->name ), array ( 'module' => $row->module, 'catid' => $row->objId ) ); } else { rewriteUrls::$mapper->connect ( $this->clean ( $row->name), array ( 'module' => $row->module, $row->module => $row->objId ) ); } }
		 */
		$modules = array (
				// 'gallery' => 'gallery',
				'news' => 'news',
				'faq' => 'faq',
				'brands' => 'brands',
				'articles' => 'articles',
				'pages' => 'pages',
				'search' => 'search',
				'photos' => 'photos',
				'shop' => 'shop',
				'cart' => 'cart',
				'reviews' => 'reviews',
				'gallery' => 'gallery',
				'help' => 'help',
				"catalog" => "catalog",
				
				'sitemap' => 'mapsite' 
		);
		rewriteUrls::$mapper->connect ( 'search.html', array (
				'module' => 'search' 
		) );
		rewriteUrls::$mapper->connect ( 'cart.html', array (
				'module' => 'cart' 
		) );
		rewriteUrls::$mapper->connect ( 'shop-search.html', array (
				'module' => 'shop',
				'shop' => 'search',
				'search' => true 
		) );
		foreach ( $modules as $name => $module ) {
			rewriteUrls::$mapper->connect ( $name, array (
					'module' => $module 
			) );
			rewriteUrls::$mapper->connect ( $name . '/:' . $module, array (
					'module' => $module 
			), array (
					"{$module}" => "([0-9]+)" 
			) );
			rewriteUrls::$mapper->connect ( $name . '/catid/:catid', array (
					'module' => $module 
			), array (
					"catid" => "([0-9]+)" 
			) );
			
			rewriteUrls::$mapper->connect ( $name . '/catid/:catid/page/:page', array (
					'module' => $module 
			), array (
					"catid" => "([0-9]+)",
					"page" => "([0-9]+)" 
			) );
			rewriteUrls::$mapper->connect ( $name . '/page/:page', array (
					'module' => $module 
			), array (
					"page" => "([0-9]+)" 
			) );
		}
		
		foreach ( rewriteUrls::$urls as $url ) {
			rewriteUrls::$mapper->connect ( $url [0], $url [1] );
		}
		
		$path = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_PATH );
		$query = parse_url ( getenv ( 'REQUEST_URI' ), PHP_URL_QUERY );
		$ext = pathinfo ( $path, PATHINFO_EXTENSION );
		if ($path != '/' && empty ( $ext ) && substr ( $path, - 1 ) == '/' && ! preg_match ( "#//#", $path )) {
			$new = substr ( $path, 0, strlen ( $path ) - 1 );
			if (! empty ( $query )) {
				$new .= '?' . $query;
			}
			$this->saveHit ( true );
			header ( "Location: {$new}", true, 301 );
			
			exit ();
		}
		
		$redirect = include ('redirect.php');
		if (isset ( $redirect [getenv ( 'REQUEST_URI' )] )) {
			$this->saveHit ( true );
			header ( "HTTP/1.1 301 Moved Permanently" );
			header ( "Location: {$redirect[getenv('REQUEST_URI')]}" );
			exit ();
		}
		
		self::defineDefaultUrls ();
		
		/*
		 * SEO Module
		 */
		
		$this->testRedirect ();
		
		if (($match = $this->virtualPage ()) != false) {
			$_GET = $match;
		} else {
			$this->matchUrl ();
		}
		
		$this->initMetaTags ();
		
		$moduleClass = '';
		if (isset ( $_GET ['module'] )) {
			$moduleClass = "Redactor_Module_" . ucfirst ( $_GET ['module'] );
			if (! class_exists ( $moduleClass )) {
				$this->includeModule ( ucfirst ( $_GET ['module'] ) );
			}
			if (class_exists ( $moduleClass )) {
				
				self::$Collector [$moduleClass] = new $moduleClass ();
				if (method_exists ( self::$Collector [$moduleClass], '_init' )) {
					self::$Collector [$moduleClass]->_init ();
				}
				
				if (method_exists ( self::$Collector [$moduleClass], 'getView' ) && self::$noView == false) {
					self::$Collector [$moduleClass]->getView ();
				}
			} else {
				header ( "HTTP/1.x 404 Not Found" );
				$mod = new not_found ();
				if (method_exists ( $mod, 'getView' ) && self::$noView == false) {
					$mod->getView ();
				}
				
				self::$Collector [$moduleClass] = $mod;
			}
		} elseif (! isset ( $_GET ['module'] ) && $_SERVER ['REQUEST_URI'] != "/") {
			header ( "HTTP/1.x 404 Not Found" );
			$mod = new not_found ();
			
			if (method_exists ( $mod, 'getView' )) {
				$mod->getView ();
			}
			self::$Collector ['not_found'] = $mod;
		} else {
			$moduleClass = "Redactor_Module_" . ucfirst ( $this->module );
			if (! class_exists ( $moduleClass )) {
				$this->includeModule ( ucfirst ( $this->module ) );
			}
			if (class_exists ( $moduleClass )) {
				
				self::$Collector [$moduleClass] = new $moduleClass ();
				if (method_exists ( self::$Collector [$moduleClass], '_init' )) {
					self::$Collector [$moduleClass]->_init ();
				}
				if (method_exists ( self::$Collector [$moduleClass], 'getView' ) && self::$noView == false) {
					self::$Collector [$moduleClass]->getView ();
				}
			} else {
				header ( "HTTP/1.x 404 Not Found" );
				$mod = new not_found ();
				
				if (method_exists ( $mod, 'getView' ) && self::$noView == false) {
					$mod->getView ();
				}
				self::$Collector ['not_found'] = $mod;
			}
		}
		$this->saveHit ();
		
		if (! empty ( $moduleClass ) && isset ( self::$Collector [$moduleClass] )) {
			
			$this->content = self::$Collector [$moduleClass]->over;
		}
		
		$this->show ();
	}
	// ������������� ��������� ������
	// TODO: �������� �������������� ������ ����� ��� 500 � �.�.
	function setError($errnum = 404) {
		switch ($errnum) {
			case 404 :
				header ( "HTTP/1.x 404 Not Found" );
				break;
		}
	}
	// ������������� �������� � Title �������� �� ���������
	function initMetaTags() {
		$rows = $this->getAdapter ()->query ( "select * from `site_setting`" );
		
		if ($rows->rowCount () > 0) {
			foreach ( $rows as $row ) {
				switch ($row->option) {
					case "title" :
						BreadcrumbsTitle::add ( $row->value );
						break;
					case "desc" :
						Metas::setDescription ( $row->value );
						break;
					case "keys" :
						Metas::setKeywords ( $row->value );
						break;
				}
			}
		}
	}
	
	// �������� �������� URL
	private function matchUrl($url = null) {
		if ($url === null) {
			$url = getenv ( 'REQUEST_URI' );
		}
		$path = parse_url ( $url, PHP_URL_PATH );
		$ext = pathinfo ( $path, PATHINFO_EXTENSION );
		
		if (! empty ( $ext )) {
			$pos = (strpos ( $path, $ext ));
			
			$t = substr ( $path, ($pos + strlen ( $ext )), strlen ( $path ) );
			
			if (! empty ( $t )) {
				return false;
			}
		}
		
		$path = $url;
		$clearPath = parse_url ( $path, PHP_URL_PATH );
		if (preg_match ( "#//#", $clearPath )) {
			return false;
		}
		
		$params = array (
				'module' => false 
		);
		$route = false;
		
		if ($path == "/") {
			$_GET ['module'] = 'pages';
			$_GET ['isIndex'] = true;
			return true;
		}
		
		if (($match = rewriteUrls::match ( $path )) != false) {
			$_GET = array_merge ( $_GET, $match );
			return $_GET;
		}
		
		foreach ( self::$defaultUrls as $module => $urls ) {
			if ($route == true) {
				break;
			}
			foreach ( $urls as $url => $addparams ) {
				$params ['module'] = $module;
				$url2 = str_replace ( "/", "\/", $url );
				if (preg_match ( "/$url2/", $path, $p )) {
					
					unset ( $p [0] );
					foreach ( array_keys ( $p ) as $key ) {
						if (! is_string ( $key )) {
							unset ( $p [$key] );
						}
					}
					$totalParams = count ( $p );
					
					foreach ( $p as $pNum => $value ) {
						if (empty ( $value )) {
							unset ( $p [$pNum] );
						}
					}
					
					if ($totalParams != count ( $p )) {
						$params ['module'] = false;
						break;
					}
					$params = array_merge ( $params, $p );
					$params = array_merge ( $params, $addparams );
					foreach ( array_keys ( $params ) as $key ) {
						
						if (! is_string ( $key )) {
							
							unset ( $params [$key] );
						}
					}
					
					$route = true;
					break;
				} else {
					$params ['module'] = false;
				}
			}
		}
		if ($params ['module'] != false) {
			$_GET = array_merge ( $_GET, $params );
			return true;
		}
		
		$_GET = array_merge ( $_GET, $params );
		
		return true;
	}
	// ����� �������
	private function show() {
		if (self::$noTemplate == false) {
			$file = 'template/' . self::getTemplate () . '/' . $this->getFileTemplate () . '.phtml';
			
			if (file_exists ( $file )) {
				include ($file);
			} else {
				echo '������! ���� ������� �� ������';
			}
		} else {
			echo $this->content;
		}
	}
	
	// ������ ������ �� ���������� ��� �������� ��������� ��� ������ �������
	function pagginator($config = array('rows'=>0, 'limit'=>0, 'pageParam'=>'page')) {
		if (! is_array ( $config )) {
			return false;
		}
		if (! isset ( $config ['rows'] ) or ! isset ( $config ['limit'] )) {
			return false;
		}
		$config ['limit'] = ( int ) $config ['limit'];
		$config ['rows'] = ( int ) $config ['rows'];
		
		if ($config ['limit'] <= 0 or $config ['rows'] <= 0) {
			return false;
		}
		if (! isset ( $config ['pageParam'] )) {
			$config ['pageParam'] = 'page';
		}
		
		$pages = array (
				'prev' => false,
				'current' => 1,
				'next' => false,
				'pages' => array (),
				'lastpage' => 0,
				'coupleLast' => 3 
		);
		
		$adjacents = 1;
		
		$page = isset ( $_GET [$config ['pageParam']] ) ? ( int ) $_GET [$config ['pageParam']] : 1;
		
		if ($page < 1) {
			$page = 1;
		}
		
		if ($page == 0) {
			$page = 1;
		}
		$add = '';
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil ( $config ['rows'] / $config ['limit'] );
		$pages ['lastpage'] = $lastpage;
		$lpm1 = $lastpage - 1;
		
		if ($lastpage > 1) {
			
			if ($prev < 1) {
			} else {
				$pages ['prev'] = $prev;
			}
			
			$now = $lastpage - (2 + ($adjacents * 2));
			/*
			 * if (($page - 2) >= 1 or ($page - 2) == 1) {
			 * //$now = $page - 2;
			 *
			 * } elseif (($page - 2) < 1) {
			 * $now = 1;
			 * }
			 *
			 * elseif ($lastpage - (2 + ($adjacents * 2)) > $page) {
			 * $now = $page;
			 * }
			 */
			if ($config ['rows'] > 3) {
				$now = $page;
			}
			$end = $lastpage;
			if (($now + 4) < $lastpage) {
				$end = $now + 4;
			}
			$tot = $end - $now + 1;
			
			if ($tot < 4 && $lastpage >= 4) {
				$now = $now - 1;
				if ($now < 1) {
					$now = 1;
				}
				
				$end = $end + 2;
				if ($end > $lastpage) {
					$end = $lastpage;
				}
			}
			for($counter = $now; $counter <= $end; $counter ++) {
				if ($counter == $page) {
					$pages ['current'] = $counter;
					$pages ['pages'] [] = $counter;
				} else {
					$pages ['pages'] [] = $counter;
				}
			}
			// next button
			if ($page < $counter - 1) {
				$pages ['next'] = $next;
			}
		}
		return $pages;
	}
}
class Redactor_Helper extends Redactor_Action {
	function __toString() {
		if (isset ( $this->over )) {
			return $this->over;
		}
	}
}
class Redactor_Helper_Title extends Redactor_Helper {
	var $over = "";
	function __construct() {
		$this->over = BreadcrumbsTitle::getClear ( ' / ' );
		if (Redactor_Action::isVirtualPage ()) {
			if (! empty ( Redactor_Action::getVirtualPage ()->Title )) {
				$this->over = Redactor_Action::getVirtualPage ()->Title;
			}
		}
	}
}
class Redactor_Helper_Desc extends Redactor_Helper {
	var $over = "";
	function __construct() {
		if (Redactor_Action::isVirtualPage ()) {
			if (! empty ( Redactor_Action::getVirtualPage ()->Title )) {
				Metas::$Desc = Redactor_Action::getVirtualPage ()->Title;
			}
		}
		$this->over = htmlspecialchars ( Metas::$Desc, ENT_COMPAT, 'cp1251' );
	}
}
class Redactor_Helper_Keys extends Redactor_Helper {
	var $over = "";
	function __construct() {
		if (Redactor_Action::isVirtualPage ()) {
			if (! empty ( Redactor_Action::getVirtualPage ()->Title )) {
				Metas::$Keys = Redactor_Action::getVirtualPage ()->Title;
			}
		}
		$this->over = htmlspecialchars ( Metas::$Keys, ENT_COMPAT, 'cp1251' );
		
	}
}
class StringPlural {
	/**
	 * ���������� ���������� ����
	 */
	const PLURAL_DEFAULT_LANG = 'ru';
	
	/**
	 * �������� ���������� ����� ����� ��� �����������(!) ����� � ������������ �
	 * ������ ������������ ����������.
	 * ���������� ���� ������������ ���������� PLURAL_DEFAULT_LANG (�� �������
	 * "ru") � ������ ������.
	 *
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::Plural(1, array('����', '�����', '�������')); //����
	 * @example self::Plural(2, '����', '�����', '�������'); //�����
	 * @example self::Plural(5, '����', '�����', '�������'); //�������
	 *         
	 * @return string
	 */
	static function Plural($amount, $_) {
		$argv = func_get_args ();
		$arr = array ();
		
		if (is_array ( $_ )) {
			$arr = $_;
		} else {
			for($i = 1, $x = count ( $argv ); $i < $x; $i ++)
				$arr [] = $argv [$i];
		}
		
		return self::PluralLang ( self::PLURAL_DEFAULT_LANG, $amount, $arr );
	}
	
	/**
	 * �������� ���������� ����� ����� ��� ����������� ����� � ������������ �
	 * ������ ������������ ����������
	 *
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::PluralEn(1, array('window', 'windows')); //window
	 * @example self::PluralEn(2, 'window', 'windows'); //windows
	 *         
	 * @return string
	 */
	static function PluralEn($amount, $_) {
		$argv = func_get_args ();
		$arr = array ();
		
		if (is_array ( $_ )) {
			$arr = $_;
		} else {
			for($i = 1, $x = count ( $argv ); $i < $x; $i ++)
				$arr [] = $argv [$i];
		}
		
		return self::PluralLang ( 'en', $amount, $arr );
	}
	
	/**
	 * �������� ���������� ����� ����� ��� ������� ����� � ������������ � ������
	 * ������������ ����������
	 *
	 * @param string $lang
	 *        	������������� ����� ��� �������� ����� ���������� ����� �����
	 * @param integer $amount
	 *        	����� ������������ ���������� "���������" �����
	 * @param mixed $_        	
	 *
	 * @example self::PluralLang('en', 1, array('window', 'windows')); //window
	 * @example self::PluralLang('en', 2, 'window', 'windows'); //windows
	 *         
	 * @return string
	 */
	static function PluralLang($lang, $amount, $_) {
		$argv = func_get_args ();
		
		if (count ( $argv ) < 3) {
			trigger_error ( __METHOD__ . ': missing required arguments', E_USER_WARNING );
			return null;
		}
		
		$amount = ( int ) $amount;
		
		$form = self::PluralLangGetForm ( $lang, $amount );
		if (is_array ( $_ )) {
			if (array_key_exists ( $form, $_ )) {
				return $_ [$form];
			} elseif (count ( $_ > 0 )) {
				return $_ [0];
			} else {
				trigger_error ( __METHOD__ . ': missing required arguments', E_USER_WARNING );
				return null;
			}
		} else {
			if (array_key_exists ( ($form + 2), $argv )) {
				return $argv [$form + 2];
			} else {
				return $argv [2];
			}
		}
	}
	
	/**
	 * �������� ���������� ��������� �������������� ����� ��� ������� �����
	 *
	 * @param string $lang
	 *        	������������� �����
	 * @return integer ���������� ���������
	 */
	static function PluralLangGetCount($lang) {
		switch ($lang) {
			case 'ach' :
			case 'af' :
			case 'ak' :
			case 'am' :
			case 'an' :
			case 'arn' :
			case 'ast' :
			case 'az' :
			case 'bg' :
			case 'bn' :
			case 'br' :
			case 'ca' :
			case 'da' :
			case 'de' :
			case 'el' :
			case 'en' :
			case 'eo' :
			case 'es' :
			case 'et' :
			case 'eu' :
			case 'fi' :
			case 'fil' :
			case 'fo' :
			case 'fr' :
			case 'fur' :
			case 'fy' :
			case 'gl' :
			case 'gu' :
			case 'ha' :
			case 'he' :
			case 'hi' :
			case 'hu' :
			case 'ia' :
			case 'is' :
			case 'it' :
			case 'jv' :
			case 'kn' :
			case 'ku' :
			case 'lb' :
			case 'ln' :
			case 'mai' :
			case 'mfe' :
			case 'mg' :
			case 'mi' :
			case 'mk' :
			case 'ml' :
			case 'mn' :
			case 'mr' :
			case 'nah' :
			case 'nap' :
			case 'nb' :
			case 'ne' :
			case 'nl' :
			case 'se' :
			case 'nn' :
			case 'no' :
			case 'nso' :
			case 'oc' :
			case 'or' :
			case 'ps' :
			case 'pa' :
			case 'pap' :
			case 'pms' :
			case 'pt' :
			case 'rm' :
			case 'sco' :
			case 'si' :
			case 'so' :
			case 'son' :
			case 'sq' :
			case 'sw' :
			case 'sv' :
			case 'ta' :
			case 'te' :
			case 'ti' :
			case 'tk' :
			case 'tr' :
			case 'ur' :
			case 'wa' :
			case 'yo' :
				return 2;
			case 'ar' :
				return 6;
			case 'ay' :
			case 'bo' :
			case 'cgg' :
			case 'dz' :
			case 'fa' :
			case 'hy' :
			case 'id' :
			case 'ja' :
			case 'jbo' :
			case 'ka' :
			case 'kk' :
			case 'km' :
			case 'ko' :
			case 'ky' :
			case 'lo' :
			case 'ms' :
			case 'sah' :
			case 'su' :
			case 'tg' :
			case 'th' :
			case 'tt' :
			case 'ug' :
			case 'uz' :
			case 'vi' :
			case 'wo' :
			case 'zh' :
				return 1;
			case 'be' :
			case 'bs' :
			case 'cs' :
			case 'hr' :
			case 'lt' :
			case 'lv' :
			case 'mnk' :
			case 'pl' :
			case 'ro' :
			case 'ru' :
			case 'sk' :
			case 'sr' :
			case 'uk' :
				return 3;
			case 'cy' :
			case 'gd' :
			case 'kw' :
			case 'mt' :
			case 'sl' :
				return 4;
			case 'ga' :
				return 5;
			default :
				return 1;
		}
	}
	
	/**
	 * �������� ������������� ����� �������������� �����
	 *
	 * @param string $lang
	 *        	������������� �����
	 * @param integer $n
	 *        	����� �� ������� ������������ ����� �����
	 * @return integer ������������� ����� �����
	 */
	private static function PluralLangGetForm($lang, $n) {
		switch ($lang) {
			case 'ach' :
			case 'ak' :
			case 'am' :
			case 'arn' :
			case 'br' :
			case 'fil' :
			case 'fr' :
			case 'ln' :
			case 'mfe' :
			case 'mg' :
			case 'mi' :
			case 'oc' :
			case 'ti' :
			case 'tr' :
			case 'wa' :
				return ( int ) ($n > 1);
			case 'af' :
			case 'an' :
			case 'ast' :
			case 'az' :
			case 'bg' :
			case 'bn' :
			case 'ca' :
			case 'da' :
			case 'de' :
			case 'el' :
			case 'en' :
			case 'eo' :
			case 'es' :
			case 'et' :
			case 'eu' :
			case 'fi' :
			case 'fo' :
			case 'fur' :
			case 'fy' :
			case 'gl' :
			case 'gu' :
			case 'ha' :
			case 'he' :
			case 'hi' :
			case 'hu' :
			case 'ia' :
			case 'it' :
			case 'kn' :
			case 'ku' :
			case 'lb' :
			case 'mai' :
			case 'ml' :
			case 'mn' :
			case 'mr' :
			case 'nah' :
			case 'nap' :
			case 'nb' :
			case 'ne' :
			case 'nl' :
			case 'se' :
			case 'nn' :
			case 'no' :
			case 'nso' :
			case 'or' :
			case 'ps' :
			case 'pa' :
			case 'pap' :
			case 'pms' :
			case 'pt' :
			case 'rm' :
			case 'sco' :
			case 'si' :
			case 'so' :
			case 'son' :
			case 'sq' :
			case 'sw' :
			case 'sv' :
			case 'ta' :
			case 'te' :
			case 'tk' :
			case 'ur' :
			case 'yo' :
				return ( int ) ($n != 1);
			case 'jv' :
				return ( int ) ($n != 0);
			case 'ay' :
			case 'bo' :
			case 'cgg' :
			case 'dz' :
			case 'fa' :
			case 'hy' :
			case 'id' :
			case 'ja' :
			case 'jbo' :
			case 'ka' :
			case 'kk' :
			case 'km' :
			case 'ko' :
			case 'ky' :
			case 'lo' :
			case 'ms' :
			case 'sah' :
			case 'su' :
			case 'tg' :
			case 'th' :
			case 'tt' :
			case 'ug' :
			case 'uz' :
			case 'vi' :
			case 'wo' :
			case 'zh' :
				return ( int ) (0);
			
			case 'be' :
			case 'bs' :
			case 'hr' :
			case 'ru' :
			case 'sr' :
			case 'uk' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2));
			case 'lt' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n % 10 >= 2 && ($n % 100 < 10 or $n % 100 >= 20) ? 1 : 2));
			case 'lv' :
				return ( int ) ($n % 10 == 1 && $n % 100 != 11 ? 0 : ($n != 0 ? 1 : 2));
			case 'mt' :
				return ( int ) ($n == 1 ? 0 : ($n == 0 || ($n % 100 > 1 && $n % 100 < 11) ? 1 : (($n % 100 > 10 && $n % 100 < 20) ? 2 : 3)));
			case 'pl' :
				return ( int ) ($n == 1 ? 0 : ($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100 < 10 || $n % 100 >= 20) ? 1 : 2));
			case 'ar' :
				return ( int ) ($n == 0 ? 0 : ($n == 1 ? 1 : ($n == 2 ? 2 : ($n % 100 >= 3 && $n % 100 <= 10 ? 3 : ($n % 100 >= 11 ? 4 : 5)))));
			case 'gd' :
				return ( int ) (($n == 1 || $n == 11) ? 0 : (($n == 2 || $n == 12) ? 1 : (($n > 2 && $n < 20) ? 2 : 3)));
			case 'is' :
				return ( int ) ($n % 10 != 1 || $n % 100 == 11);
			
			case 'cs' :
			case 'sk' :
				return ( int ) (($n == 1) ? 0 : (($n >= 2 && $n <= 4) ? 1 : 2));
			case 'cy' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : (($n != 8 && $n != 11) ? 2 : 3)));
			case 'ga' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : ($n < 7 ? 2 : ($n < 11 ? 3 : 4))));
			case 'kw' :
				return ( int ) (($n == 1) ? 0 : (($n == 2) ? 1 : (($n == 3) ? 2 : 3)));
			
			case 'mk' :
				return ( int ) (($n == 1 || $n % 10 == 1) ? 0 : 1);
			case 'mnk' :
				return ( int ) ($n == 0 ? 0 : ($n == 1 ? 1 : 2));
			
			case 'ro' :
				return ( int ) ($n == 1 ? 0 : (($n == 0 || ($n % 100 > 0 && $n % 100 < 20)) ? 1 : 2));
			case 'sl' :
				return ( int ) ($n % 100 == 1 ? 1 : ($n % 100 == 2 ? 2 : ($n % 100 == 3 || $n % 100 == 4 ? 3 : 0)));
			default :
				return 0;
		}
	}
}
