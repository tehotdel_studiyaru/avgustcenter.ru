<?php
class Redactor_Module_Account extends Redactor_Action {
	var $over = '';
	var $View = 'index';
	var $UserRow = null;
	var $iUser = false;
	var $pathView = 'account';
	function __construct() {
		if ($this->getModule ( 'users' )->iUser () == false) {
			header ( "Location: /catalog?login=true" );
		}
		if (isset ( $_GET ['logout'] )) {
			unset ( $_SESSION ['User'] );
			header ( "Location: /", true );
			exit ();
		}
		
		if (isset ( $_GET ['copy'] )) {
			$this->UserRow = $this->getModule ( 'users' )->getUserRow ();
			$sth = $this->prepare ( "select * from `Orders` where `UserID`=? AND `Id`=?" );
			$sth->execute ( array (
					$this->UserRow->Id,
					( int ) $_GET ['copy'] 
			) );
			if ($sth->rowCount () > 0) {
				
				
				$items = $this->prepare ( "select * from `Orders_Items` WHERE `OrderID`=?" );
				$items->execute ( array (
						( int ) $_GET ['copy'] 
				) );
				if ($items->rowCount () > 0) {
					
					foreach ( $items->fetchAll () as $row ) {
						$_SESSION ['Catalog'] [$row->ItemID] = $row->Qty;
					}
					$this->getModule ('basket')->countPrice ();
				}
			}
			
			header ( "Location: /basket", true );
		}
		elseif (isset($_GET['pdf'])){
			$this->UserRow = $this->getModule ( 'users' )->getUserRow ();
			$catalog = $this->getModule ( 'Basket' )->getPDF2 ( (int)$_GET['pdf'], $this->UserRow->Id );
			exit();
		}
		elseif (isset($_GET['cancle'])){
			$this->UserRow = $this->getModule ( 'users' )->getUserRow ();
			$this->query("update `Orders` Set `Status`='2' WHERE `Id`='".(int)$_GET['cancle']."' and `UserID`='{$this->UserRow->Id}' LIMIT 1");
			header ( "Location: /account/orders", true );
		}
	}
	function _init() {
		if (file_exists ( "Views/{$this->pathView}/Config.php" )) {
			include ("Views/{$this->pathView}/Config.php");
		}
	}
	private function includeView($name) {
		if (file_exists ( 'Views/' . $this->pathView . '/' . $name . '.phtml' )) {
			include ('Views/' . $this->pathView . '/' . $name . '.phtml');
		}
		return '';
	}
	function getView() {
		ob_start ();
		
		Breadcrumbs::add ( '<a href="' . $this->getUrl ( array (
				'module' => 'account' 
		) ) . '">������ �������</a>' );
		BreadcrumbsTitle::add ( '������ �������' );
		include ('Views/account/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
	}
}