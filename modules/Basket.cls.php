<?php
require_once 'modules/Catalog.cls.php';
include ('MPDF/mpdf.php');
class Redactor_Module_Basket extends Redactor_Action {
	var $View = 'index';
	var $TotalQty = 0;
	var $Field = 'Price';
	public function getView() {
		$this->setFileTemplate ( 'cart' );
		Breadcrumbs::add ( '�������' );
		BreadcrumbsTitle::set ( '�������' );
		ob_start ();
		include ('Views/basket/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
	}
	function getPDF($Id) {
		$this->orderId = ( int ) $Id;
		$this->View = 'clear';
		$this->getView ();
		$pdf = new mPDF ( 'utf-8' );
		$pdf->charset_in = 'cp1251';
		
		$pdf->SetDisplayMode ( 'fullpage' );
		$pdf->writeHTML ( $this->over );
		$pdf->Output ( "order{$this->orderId}.pdf", 'D' );
		exit ();
	}
	function getPDF2($Id, $User) {
		$this->orderId = ( int ) $Id;
		$this->UserId = $User;
		$this->View = 'clear';
		$this->getView ();
		$pdf = new mPDF ( 'utf-8' );
		$pdf->charset_in = 'cp1251';
		
		$pdf->SetDisplayMode ( 'fullpage' );
		$pdf->writeHTML ( $this->over );
		$pdf->Output ( "order{$this->orderId}.pdf", 'D' );
		exit ();
	}
	function __construct() {
		if (! isset ( $_SESSION ['Catalog'] )) {
			$_SESSION ['Catalog'] = array ();
		}
		if (! isset ( $_SESSION ['Total'] )) {
			$_SESSION ['Total'] = 0;
		}
		if (isset ( $_POST ['deleteItem'] )) {
			if (isset ( $_SESSION ['Catalog'] [( int ) $_POST ['deleteItem']] )) {
				unset ( $_SESSION ['Catalog'] [( int ) $_POST ['deleteItem']] );
				$this->countPrice ();
			}
			exit ( "{total:" . count ( $_SESSION ['Catalog'] ) . ", totalPrice:'" . number_format ( $this->getTotal (), 0, '', ' ' ) . "', field:'" . $this->Field . "', clear:'" . $this->getTotal () . "'}" );
		}
		
		if (isset ( $_POST ['add2Basket'] )) {
			if (isset ( $_POST ['ItemID'] )) {
				if (isset ( $_POST ['sizes'] )) {
					
					$_SESSION ['Catalog'] [( int ) $_POST ['ItemID']] = array (
							'Qty' => 1,
							'Sizes' => $_POST ['sizes'] 
					);
				} else {
					$_SESSION ['Catalog'] [( int ) $_POST ['ItemID']] = array (
							'Qty' => 1 
					);
				}
			}
		} elseif (isset ( $_POST ['recount'] ) && is_array ( $_POST ['recount'] )) {
			foreach ( $_POST ['recount'] as $id => $qty ) {
				if (is_string ( $qty ) or is_numeric ( $qty )) {
					
					if ($qty == 0 && isset ( $_SESSION ['Catalog'] [( int ) $id] )) {
						unset ( $_SESSION ['Catalog'] [( int ) $id] );
					} elseif (isset ( $_SESSION ['Catalog'] [( int ) $id] )) {
						$_SESSION ['Catalog'] [( int ) $id] = 1;
					}
				}
			}
		}
		$this->countPrice ();
		
		if (isset ( $_POST ['ajax'] )) {
			exit ( json_encode ( array (
					"success" => true,
					'html' => $this->win2utf ( '	<p>� ����� �������</p>
					<div>
						<a href="' . $this->getUrl ( array (
							'module' => 'basket' 
					) ) . '">' . $this->totalQty () . '</a>' . StringPlural::Plural ( $this->totalQty (), array (
							'�����',
							'������',
							'�������' 
					) ) . '
					</div>
					<span>' . $this->totalQty () . '</span>' ) 
			) ) );
		}
	}
	function getItems() {
		$Ids = array ();
		foreach ( $_SESSION ['Catalog'] as $Id => $Qty ) {
			$Ids [] = ( int ) $Id;
		}
		$total = $this->getTotal ();
		
		if (count ( $Ids ) > 0) {
			$sth = $this->prepare ( "SELECT `Id`, `CategoryID`, `Brand`, `Title`,`Price`, `Article`, `uf_1` FROM `Catalog_Items` WHERE `Active`='1' AND `CategoryID`!=0 AND `Id` IN (" . implode ( ", ", $Ids ) . ")" );
			if ($sth != false && $sth->execute () && $sth->rowCount () > 0) {
				$items = array ();
				foreach ( $sth->fetchAll () as $row ) {
					$row = new Redactor_Module_Catalog_Item ( $row );
					$row->Qty = $_SESSION ['Catalog'] [$row->Id] ['Qty'];
					if (! empty ( $row->uf_1 )) {
						$row->Total = $row->uf_1 * $row->Qty;
					} else {
						$row->Total = $row->Price * $row->Qty;
					}
					
					$items [] = $row;
				}
				return $items;
			}
		}
		return false;
	}
	function getTotal() {
		return isset ( $_SESSION ['Total'] ) ? $_SESSION ['Total'] : 0;
	}
	function totalQty() {
		$total = 0;
		if (isset ( $_SESSION ['Catalog'] ) && count ( $_SESSION ['Total'] ) > 0) {
			foreach ( $_SESSION ['Catalog'] as $Id => $Qty ) {
				if (is_numeric ( $Qty ['Qty'] )) {
					$total += $Qty ['Qty'];
				}
			}
		}
		return $total;
	}
	function saveOrder() {
		$User = $this->getModule ( 'users' )->getUserRow ();
		$adress = '';
		if (isset ( $_POST ['City'] ) && $_POST ['City'] == 1) {
			$metro = isset ( $_POST ['Metro'] ) && ! empty ( $_POST ['Metro'] ) ? '����� ' . $_POST ['Metro'] . ', ' : '';
			$street = isset ( $_POST ['Street'] ) && ! empty ( $_POST ['Street'] ) ? '����� ' . $_POST ['Street'] . ', ' : '';
			$korpus = isset ( $_POST ['Korpus'] ) && ! empty ( $_POST ['Korpus'] ) ? '������ ' . $_POST ['Korpus'] . ', ' : '';
			$podezd = isset ( $_POST ['Podezd'] ) && ! empty ( $_POST ['Podezd'] ) ? '������� ' . $_POST ['Podezd'] . ', ' : '';
			$homephone = isset ( $_POST ['HomePhone'] ) && ! empty ( $_POST ['HomePhone'] ) ? '������� ' . $_POST ['HomePhone'] . ', ' : '';
			$apartament = isset ( $_POST ['Apartament'] ) && ! empty ( $_POST ['Apartament'] ) ? '�������� ' . $_POST ['Apartament'] . ', ' : '';
			$level = isset ( $_POST ['Level'] ) && ! empty ( $_POST ['Level'] ) ? '���� ' . $_POST ['Level'] . ', ' : '';
			
			$adress = "������, {$metro}{$street}{$korpus}{$podezd}{$homephone}{$apartament}{$level}";
		} else {
			$metro = isset ( $_POST ['Index'] ) && ! empty ( $_POST ['Index'] ) ? '������ ' . $_POST ['Index'] . ', ' : '';
			$street = isset ( $_POST ['CityNoMsk'] ) && ! empty ( $_POST ['CityNoMsk'] ) ? '' . $_POST ['CityNoMsk'] . ', ' : '';
			$korpus = isset ( $_POST ['AddressNoMsk'] ) && ! empty ( $_POST ['AddressNoMsk'] ) ? ' ' . $_POST ['AddressNoMsk'] . ', ' : '';
			
			$adress = "{$metro}{$street}{$korpus}";
		}
		
		$dop = 0;
		$dev = $this->query ( "select * from `Oplata` where `Active`=1 and `Choose`=1 " );
		if ($dev != false && $dev->rowCount () > 0) {
			foreach ( $dev->fetchAll () as $row ) {
				if (isset ( $_POST ['Delivery'] ) && $_POST ['Delivery'] == $row->name) {
					
					if (! empty ( $row->pageLink )) {
						
						$dop = $row->pageLink;
					}
				}
			}
		}
		
		$values = array (
				"`Name`" => $_POST ['Name'],
				"`Phone`" => $_POST ['Phone'],
				"`Email`" => $_POST ['Email'],
				"`Comment`" => $_POST ['Comments'],
				"`Address`" => $adress,
				"`Delivery`" => (isset ( $_POST ['Delivery'] ) ? $_POST ['Delivery'] : ''),
				"`Payment`" => (isset ( $_POST ['Payment'] ) ? $_POST ['Payment'] : ''),
				'`Sum`' => $this->getTotal () + $dop,
				'`DeliveryPrice`' => $dop 
		);
		
		$cols = array ();
		foreach ( $values as $s ) {
			$cols [] = '?';
		}
		
		$Items = array ();
		
		$sth = $this->prepare ( "INSERT INTO `Orders` (" . implode ( ", ", array_keys ( $values ) ) . ") VALUES (" . implode ( ", ", $cols ) . ")" );
		$sth->execute ( array_values ( $values ) );
		$Id = $this->lastId ();
		$sth = $this->prepare ( "INSERT INTO  `Orders_Items` (`OrderID`, `ItemID`, `Article`, `Name`, `Price`, `Qty`) VALUES (?,?,?, ?, ?, ?)" );
		$user = ($this->getModule ( 'users' )->iUser () ? true : false);
		$count = count ( $this->getItems () );
		foreach ( $this->getItems () as $row ) {
			
			$Qty = $row->Qty;
			
			$row->Price = ( int ) $row->{$this->Field};
			$Items [] = '<tr>
					<td>' . $row->Article . '</td>
					<td>' . $row->Title . '</td>
					<td>' . number_format ( $row->Price, 0, '', ' ' ) . '</td>
					
					</tr>';
			$sth->execute ( array (
					$Id,
					$row->Id,
					$row->Article,
					$row->Title,
					$row->Price,
					$row->Qty 
			) );
		}
		
		$this->orderId = $Id;
		
		$text = '<p>���������� ��� �� ����� �� ����� "anmag.ru"!<br/>
				��� ������������� �������� � ���� � ��������� �����,<br/>
				����� �������� ������ ������ � �������� ��������� ���� ���������.
				</p>
				
				
				<table width="640" border="1">';
		$text .= "<tr><td><strong>�������</strong></td>
				<td><strong>������������</strong></td>
				<td><strong>����</strong></td>
				
				
				<tr>
				";
		
		$text .= implode ( "", $Items );
		$text .= '<tr><td colspan="2"><strong>�����:</strong>' . ($dop > 0 ? '<span style="float:right">(� ������ �������� +' . $dop . ' ���.)</span>' : '') . '</td><td>' . ($this->getTotal () + $dop) . '</td></tr></table>';
		if (isset ( $_POST ['order'] )) {
			$text .= '<p><strong>����������:</strong></p><table>
				
				<tr>
				
				<td width="200"><strong>�.�.�</strong></td>
				<td>' . $_POST ['Name'] . '</td>
				</tr>
				
				<tr>
				<td><strong>�������:</strong></td>
				<td>' . $_POST ['Phone'] . '</td>
				</tr>
						<tr>
				<td><strong>Email:</strong></td>
				<td>' . $_POST ['Email'] . '</td>
				</tr>
				<tr>
				<td><strong>�����:</strong></td>
				<td>' . $adress . '</td>
				</tr>
				<tr>
				<td><strong>������ ��������:</strong></td>
				<td>' . $_POST ['Delivery'] . '</td>
				</tr><tr>
				<td><strong>������ ������:</strong></td>
				<td>' . $_POST ['Payment'] . '</td>
				</tr><tr>
				<td><strong>�����������:</strong></td>
				<td>' . $_POST ['Comments'] . '</td>
				</tr>
						</table>
				';
			$mail = new Email ();
			$mail->EmailHTML ( $this->getOption ( 'email_admin' ), "����� #{$this->orderId}", "<div style='width:640px'>" . $text . '</div>' );
			$mail = new Email ();
			$mail->EmailHTML ( $_POST ['Email'], "����� #{$this->orderId}", "<div style='width:640px'>" . $text . '</div>' );
		}
		
		// $_SESSION ['Catalog'] = array ();
		
		// $_SESSION ['Total'] = 0;
		
		return $Id;
	}
	function totalPositions() {
		return isset ( $_SESSION ['Catalog'] ) ? count ( $_SESSION ['Catalog'] ) : 0;
	}
	function countPrice() {
		$total = 0;
		
		if (isset ( $_SESSION ['Catalog'] ) && count ( $_SESSION ['Total'] ) > 0) {
			foreach ( $_SESSION ['Catalog'] as $Id => $Qty ) {
				
				$sth = $this->prepare ( "SELECT `Price`, `uf_1` FROM `Catalog_Items` WHERE `Active`=? AND `CategoryID`!=? AND `Id`=? LIMIT 1" );
				if ($sth != false && $sth->execute ( array (
						1,
						0,
						$Id 
				) ) != false && $sth->rowCount () > 0) {
					$row = $sth->fetch ();
					if (! empty ( $row->uf_1 )) {
						$total += ( int ) $row->uf_1 * $Qty ['Qty'];
					} else {
						$total += ( int ) $row->Price * $Qty ['Qty'];
					}
				}
			}
		}
		
		$_SESSION ['Total'] = $total;
	}
	function addItem($id, $qty = 1) {
	}
}