<?php

class Redactor_Module_Catalog extends Redactor_Action
{

    var $over = '';

    private $ItemsTable = "Catalog_Items";

    private $CategoriesTable = "Catalog_Categories";

    private $ImagesTable = "Catalog_Items_Files";

    private $VirtualItemsTable = 'Catalog_Items_Categories';

    var $CurrentCategory = 0;

    var $CurrentBrand = 0;

    var $CurrentID = 0;

    var $Total = 0;

    var $ChoosedBrands = array();

    var $ChoosedMinPrice = 0;

    var $ChoosedMaxPrice = 0;

    var $MinPrice = 0;

    var $MaxPrice = 0;

    var $Brands = array();

    var $LimitRecords = 10;

    var $ImagesPath = "files/catalog";

    var $MethodFilter = '_GET';

    var $paramCategoryID = 'catid';

    var $paramItemId = 'catalog';

    var $paramBrandId = 'brand';

    var $paramPage = 'page';

    var $CurrentPage = 1;

    var $Start = false;
    // TODO: Сделать функция для заполнения переменной CurrentItemRow и получение данных из неё
    var $CurrentItemRow = null;

    const FILTER_GET = 1;

    const FILTER_POST = 2;

    const FILTER_PARAMS_LARGER = 1;

    const FILTER_PARAMS_LESS = 2;

    const FILTER_PARAMS_EQUAL = 3;

    const FILTER_PARAMS_LARGER_OR_EQUAL = 4;

    const FILTER_PARAMS_LESS_OR_EQUAL = 5;

    const FILTER_PARAMS_TYPE_INT = 1;

    const FILTER_PARAMS_TYPE_STRING = 2;

    const FILTER_PARAMS_TYPE_ARRAY = 3;

    var $totalCompare = 0;

    var $userSort = array();

    var $selectedUserSort = false;

    var $View = 'index';

    var $CatName = 'Каталог';

    var $orderBy = array(
        array(
            '`Sort`',
            'ASC'
        )
    );

    var $currentSort = array();

    var $Plugins = array(
        'Catalog' => 'Items',
        'Brands' => 'Brands',
        'Catalog_Categories' => 'Categories'
    );

    var $FilterName = 'Filter';

    var $FilterParams = array();

    var $Columns = array();

    var $ListItemsFields = array(
        'Id',
        'Sort',
        'Title',
        'CategoryID',
        'Brand',
        'Price',
        'Article',
        'Notice'
    );

    var $data = array();

    /**
     *
     * @return the $currentSort
     */
    public function getCurrentSort()
    {
        return $this->currentSort;
    }

    /**
     *
     * @param multitype: $currentSort            
     */
    public function setCurrentSort(array $currentSort)
    {
        $this->currentSort = $currentSort;
    }

    function getFirstCat($id)
    {
        $sth = $this->prepare("select `Id`, `parentId` from `Catalog_Categories` where `Id`=?");
        $sth->execute(array(
            $id
        ));
        if ($sth->rowCount() > 0) {
            $row = $sth->fetch();
            if ($row->parentId == 0) {
                return $row->Id;
            } else {
                return $this->getFirstCat($row->parentId);
            }
        }
        return $id;
    }

    public function getUFValuesByUFID($UF)
    {
        $sth = $this->prepare("SELECT `Value`, `Id`, `Code` FROM `{$this->UserFieldsValues}` WHERE `UFId`=? ORDER BY `Sort`");
        $sth->execute(array(
            $UF
        ));
        if ($sth->rowCount() > 0) {
            $row1 = array();
            foreach ($sth->fetchAll() as $row) {
                $rows = array();
                $rows['id'] = $row->Id;
                $rows['value'] = $row->Value;
                $rows['code'] = $row->Code;
                $row1[] = $rows;
            }
            return $row1;
        }
        return false;
    }

    public function getUFValue($value)
    {
        $sth = $this->prepare("SELECT `Value` FROM `{$this->UserFieldsValues}` WHERE `Id`=? ORDER BY `Sort` LIMIT 1");
        $sth->execute(array(
            $value
        ));
        if ($sth->rowCount() > 0) {
            return $sth->fetchColumn();
        }
        return false;
    }

    /**
     *
     * @return the $Start
     */
    public function getStart()
    {
        return $this->Start;
    }

    /**
     *
     * @param number $Start            
     */
    public function setStart($Start)
    {
        $this->Start = $Start;
    }

    /**
     *
     * @return the $paramPage
     */
    public function getParamPage()
    {
        return $this->paramPage;
    }

    /**
     *
     * @return the $CurrentPage
     */
    public function getCurrentPage()
    {
        return $this->CurrentPage;
    }

    /**
     *
     * @param string $paramPage            
     */
    public function setParamPage($paramPage)
    {
        $this->paramPage = $paramPage;
    }

    /**
     *
     * @param number $CurrentPage            
     */
    public function setCurrentPage($CurrentPage)
    {
        $this->CurrentPage = $CurrentPage;
    }

    /**
     *
     * @return the $ListItemsFields
     */
    public function getListItemsFields()
    {
        return $this->ListItemsFields;
    }

    /**
     *
     * @param multitype:string $ListItemsFields            
     */
    public function setListItemsFields($ListItemsFields)
    {
        $this->ListItemsFields = $ListItemsFields;
    }

    public function set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : false;
    }

    /**
     *
     * @return the $orderBy
     */
    function getBrand($id)
    {
        $sth = $this->prepare("select `Title` from `Brands_Items` where `Id`=? limit 1");
        $sth->execute(array(
            $id
        ));
        if ($sth->rowCount() > 0) {
            return $sth->fetchColumn();
        }
        
        return '';
    }

    public function getOrderBy()
    {
        $orderBy = "";
        
        if (is_array($this->orderBy) && count($this->orderBy) > 0) {
            $orderBy = "ORDER BY ";
            $array = array();
            foreach ($this->orderBy as $p) {
                
                if (is_array($p) && count($p) == 2) {
                    $array[] = "{$p[0]} {$p[1]}";
                } elseif (is_array($p) && count($p) == 1) {
                    $array[] = "{$p[0]} ASC";
                } else {
                    $array[] = "{$this->orderBy[0]} {$this->orderBy[1]}";
                    break;
                }
            }
            $orderBy .= implode(", ", $array);
        }
        return $orderBy;
    }

    /**
     *
     * @param string $orderBy            
     */
    public function setOrderBy($orderBy, $on = 'ASC')
    {
        if (is_array($orderBy)) {
            $new = array();
            
            foreach ($orderBy as $params) {
                if (is_array($params)) {
                    if (in_array($params[0], $this->Columns)) {
                        if (preg_match("/ASC/is", $params[1])) {
                            $new[] = array(
                                "`{$params [0]}`",
                                'ASC'
                            );
                        } elseif (preg_match("/DESC/is", $params[1])) {
                            $new[] = array(
                                "`{$params [0]}`",
                                'DESC'
                            );
                        } else {
                            $new[] = array(
                                "`{$params [0]}`",
                                'ASC'
                            );
                        }
                    }
                } else {
                    
                    if (count($orderBy) == 2 && in_array($orderBy[0], $this->Columns)) {
                        
                        $new[] = array(
                            "`{$orderBy[0]}`",
                            $orderBy[1]
                        );
                        
                        break;
                    } elseif (count($orderBy) == 1 && in_array($orderBy[0], $this->Columns)) {
                        
                        $new[] = array(
                            "`{$orderBy[0]}`",
                            'ASC'
                        );
                        
                        break;
                    }
                    
                    break;
                }
            }
            if (count($new) > 0) {
                $this->orderBy = $new;
            }
        } elseif (is_string($orderBy)) {
            if (in_array($orderBy, $this->Columns)) {
                $new[] = array(
                    $orderBy,
                    $on
                );
            }
            if (count($new) > 0) {
                $this->orderBy = $new;
            }
        }
    }

    /**
     *
     * @return the $paramBrandId
     */
    public function getParamBrandId()
    {
        return $this->paramBrandId;
    }

    function getImageCatCountries($id, $type = 0, $limit = false)
    {
        if ($limit != false && is_numeric($limit)) {
            $limit = ' limit ' . $limit;
        } else {
            $limit = '';
        }
        
        $sth = $this->Stm('SELECT * FROM `Catalog_Categories_Files` where `ItemID`=? ORDER BY `MainImage` DESC, `Sort` ASC ' . $limit); // запрос
        $sth->execute(array(
            $id            
        ));
        
        if ($sth->rowCount() > 0) {
            $images = array();
            foreach ($sth->fetchAll() as $row) {
                
                $images[] = $row;
            }
            return $images;
        }
        return false;
    }

    /**
     *
     * @param string $paramBrandId            
     */
    public function setParamBrandId($paramBrandId)
    {
        $this->paramBrandId = $paramBrandId;
    }

    /**
     *
     * @return the $paramCategoryID
     */
    public function getParamCategoryID()
    {
        return $this->paramCategoryID;
    }

    /**
     *
     * @return the $paramItemId
     */
    public function getParamItemId()
    {
        return $this->paramItemId;
    }

    /**
     *
     * @param string $paramCategoryID            
     */
    public function setParamCategoryID($paramCategoryID)
    {
        $this->paramCategoryID = $paramCategoryID;
    }

    /**
     *
     * @param string $paramItemId            
     */
    public function setParamItemId($paramItemId)
    {
        $this->paramItemId = $paramItemId;
    }

    /**
     *
     * @return the $View
     */
    public function getView()
    {
        if ($this->getCurrentId() != 0) {
            $this->View = 'card';
        }
        if (isset($_GET['onlyView'])) {
            include ('Views/catalog/list.phtml');
            exit();
        } else {
            ob_start();
            include ('Views/catalog/' . $this->View . '.phtml');
            $this->over = ob_get_clean();
        }
    }

    /**
     *
     * @param string $View            
     */
    public function setView($View)
    {
        $this->View = $View;
    }

    /**
     *
     * @return the $FilterParams
     */
    private function getFilterParams()
    {
        return $this->FilterParams;
    }

    /**
     *
     * @param multitype: $FilterParams            
     */
    private function setFilterParams($FilterParams)
    {
        if (! is_array($FilterParams)) {
            return false;
        }
        $fp = array();
        foreach ($FilterParams as $FilterName => $params) {}
        
        $this->FilterParams = $FilterParams;
    }

    public function getColumns()
    {
        if (count($this->Columns) == 0) {
            $sth = $this->Query("SHOW COLUMNS FROM `{$this->ItemsTable}`");
            if ($sth != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $this->Columns[] = $row->Field;
                }
                $this->Columns[] = 'prior';
            }
        }
        return $this->Columns;
    }

    function setUserSort(array $sort = array())
    {
        $new = array();
        
        foreach ($sort as $field => $s) {
            if (is_array($s)) {
                
                $new[$field] = $s;
            }
        }
        
        $this->userSort = $new;
    }

    function getCategoryName($CategoryID)
    {
        $sth = $this->Stm("SELECT `Title`, `H1` FROM `{$this->CategoriesTable}` WHERE `Id`=?");
        if ($sth != false && ($sth->execute(array(
            $CategoryID
        ))) != false && $sth->rowCount() > 0) {
            $row = $sth->fetch();
            $this->CategoryName = $row->Title;
            if ($row->H1 != '') {
                $this->CategoryName = $row->H1;
            }
            
            return $this->CategoryName;
        }
        
        return '';
    }

    function _init()
    {
        if (isset($_POST['getimage'])) {
            
            $sth = $this->prepare("select * from `Catalog_Items_Files` where `Id`=? ");
            $sth->execute(array(
                (int) $_POST['id']
            ));
            
            if ($sth->rowCount() > 0) {
                $row = $sth->fetch();
                echo json_encode(array(
                    'success' => 'true',
                    'original' => '/files/catalog/o_' . $row->Id . '.' . $row->Extension,
                    'main' => "/thumbs/catalog/" . base64_encode("406_406_{$row->Id}") . ".{$row->Extension}"
                ));
            }
            echo '';
            exit();
        }
        
        if (! isset($_SESSION['Catalog'])) {
            $_SESSION['Catalog'] = array();
        }
        
        $this->getColumns();
        
        $catId = isset($_GET['catid']) ? (int) $_GET['catid'] : 0;
        $this->CatName = $this->getCategoryName($catId);
        if (file_exists("Views/catalog/Config.php")) {
            
            include ("Views/catalog/Config.php");
        }
        
        $this->setCurrentID(isset($_GET[$this->getParamItemId()]) ? $_GET[$this->getParamItemId()] : 0);
        $this->setCurrentCategory(isset($_GET[$this->getParamCategoryID()]) ? $_GET[$this->getParamCategoryID()] : 0);
        $this->setCurrentBrand(isset($_GET[$this->getParamBrandId()]) ? $_GET[$this->getParamBrandId()] : 0);
        if (file_exists("Views/catalog/AfterExec.php")) {
            include ("Views/catalog/AfterExec.php");
        }
        if (is_numeric($this->getLimitRecords())) {
            $this->setStart(abs($this->getLimitRecords() * ($this->getCurrentPage() - 1)));
        } else {
            $this->setStart(false);
        }
        $this->setCurrentPage(isset($_GET[$this->getParamPage()]) ? (int) $_GET[$this->getParamPage()] : 1);
        if ($this->getCurrentPage() <= 0) {
            $this->setCurrentPage(1);
        }
        if (is_numeric($this->getLimitRecords())) {
            $this->setStart(abs($this->getLimitRecords() * ($this->getCurrentPage() - 1)));
        } else {
            $this->setStart(false);
        }
        $this->setCurrentPage(isset($_GET[$this->getParamPage()]) ? (int) $_GET[$this->getParamPage()] : 1);
        if ($this->getCurrentPage() <= 0) {
            $this->setCurrentPage(1);
        }
        
        // TODO: Сделать возможность изменять названия ключа для сортировки
        
        if (isset($_GET['Sort'])) {
            $_SESSION['Catalog_Sort'] = urldecode($_GET['Sort']);
            if (count($this->userSort) == 0) {
                if (in_array($_SESSION['Catalog_Sort'], $this->getColumns())) {
                    $this->setOrderBy(array(
                        $_SESSION['Catalog_Sort'],
                        'ASC'
                    ));
                }
            } else {
                if (isset($this->userSort[$_SESSION['Catalog_Sort']])) {
                    $this->selectedUserSort = $_SESSION['Catalog_Sort'];
                    $this->setOrderBy($this->userSort[$_SESSION['Catalog_Sort']]);
                } elseif (in_array($_SESSION['Catalog_Sort'], $this->getColumns())) {
                    
                    $this->setOrderBy(array(
                        $_SESSION['Catalog_Sort'],
                        'ASC'
                    ));
                }
            }
        } elseif (isset($_SESSION['Catalog_Sort'])) {
            
            if (count($this->userSort) == 0) {
                if (in_array($_SESSION['Catalog_Sort'], $this->getColumns())) {
                    
                    $this->setOrderBy(array(
                        $_SESSION['Catalog_Sort'],
                        'ASC'
                    ));
                }
            } else {
                
                if (isset($this->userSort[$_SESSION['Catalog_Sort']])) {
                    
                    $this->setOrderBy($this->userSort[$_SESSION['Catalog_Sort']]);
                    $this->selectedUserSort = $_SESSION['Catalog_Sort'];
                } elseif (in_array($_SESSION['Catalog_Sort'], $this->getColumns())) {
                    $this->setOrderBy(array(
                        $_SESSION['Catalog_Sort'],
                        'ASC'
                    ));
                }
            }
        } else {
            if ($this->orderBy) {
                if (is_array(current($this->orderBy))) {
                    $this->setOrderBy(current($this->orderBy));
                    $this->selectedUserSort = current(array_keys($this->orderBy));
                } else {
                    $this->setOrderBy(current($this->orderBy));
                    $this->selectedUserSort = current(array_keys($this->orderBy));
                }
            } elseif (count($this->userSort) != 0) {
                
                if (is_array(current($this->userSort))) {
                    $this->setOrderBy(current($this->userSort));
                    $this->selectedUserSort = current(array_keys($this->userSort));
                } else {
                    $this->setOrderBy(current($this->userSort));
                    $this->selectedUserSort = current(array_keys($this->userSort));
                }
            }
        }
        
        foreach ($this->orderBy as $sort) {
            if (is_array($sort)) {
                $this->currentSort[] = str_replace("`", "", $sort[0]);
            } else {
                $this->currentSort[] = str_replace("`", "", $sort);
            }
        }
    }

    /**
     *
     * @return the $MethodFilter
     */
    public function getMethodFilter()
    {
        return $this->MethodFilter;
    }

    /**
     *
     * @param string $MethodFilter            
     */
    public function setMethodFilter($MethodFilter = Redactor_Module_Catalog::FILTER_GET)
    {
        if ($MethodFilter == self::FILTER_GET) {
            $MethodFilter = '_GET';
        } else 
            if ($MethodFilter == self::FILTER_POST) {
                $MethodFilter = '_POST';
            } else {
                $MethodFilter = '_GET';
            }
    }

    /**
     *
     * @return the $CurrentCategory
     */
    public function getCurrentCategory()
    {
        return $this->CurrentCategory;
    }

    /**
     *
     * @return the $CurrentBrand
     */
    public function getCurrentBrand()
    {
        return $this->CurrentBrand;
    }

    /**
     *
     * @return the $CurrentID
     */
    public function getCurrentID()
    {
        return $this->CurrentID;
    }

    /**
     *
     * @return the $Total
     */
    public function getTotal()
    {
        return $this->Total;
    }

    /**
     *
     * @return the $ChoosedBrands
     */
    public function getChoosedBrands()
    {
        return $this->ChoosedBrands;
    }

    /**
     *
     * @return the $ChoosedMinPrice
     */
    public function getChoosedMinPrice()
    {
        return $this->ChoosedMinPrice;
    }

    /**
     *
     * @return the $ChoosedMaxPrice
     */
    public function getChoosedMaxPrice()
    {
        return $this->ChoosedMaxPrice;
    }

    /**
     *
     * @return the $MinPrice
     */
    public function getMinPrice()
    {
        return $this->MinPrice;
    }

    /**
     *
     * @return the $MaxPrice
     */
    public function getMaxPrice()
    {
        return $this->MaxPrice;
    }

    /**
     *
     * @return the $LimitRecords
     */
    public function getLimitRecords()
    {
        return $this->LimitRecords;
    }

    /**
     *
     * @return the $ImagesPath
     */
    public function getImagesPath()
    {
        return $this->ImagesPath;
    }

    /**
     *
     * @param number $CurrentCategory            
     */
    public function setCurrentCategory($CurrentCategory)
    {
        $this->CurrentCategory = $CurrentCategory;
    }

    /**
     *
     * @param number $CurrentBrand            
     */
    public function setCurrentBrand($CurrentBrand)
    {
        $this->CurrentBrand = $CurrentBrand;
    }

    /**
     *
     * @param number $CurrentID            
     */
    public function setCurrentID($CurrentID)
    {
        $this->CurrentID = $CurrentID;
    }

    /**
     *
     * @param number $Total            
     */
    public function setTotal($Total)
    {
        $this->Total = $Total;
    }

    /**
     *
     * @param multitype: $ChoosedBrands            
     */
    public function setChoosedBrands($ChoosedBrands)
    {
        $this->ChoosedBrands = $ChoosedBrands;
    }

    /**
     *
     * @param number $ChoosedMinPrice            
     */
    public function setChoosedMinPrice($ChoosedMinPrice)
    {
        $this->ChoosedMinPrice = $ChoosedMinPrice;
    }

    /**
     *
     * @param number $ChoosedMaxPrice            
     */
    public function setChoosedMaxPrice($ChoosedMaxPrice)
    {
        $this->ChoosedMaxPrice = $ChoosedMaxPrice;
    }

    /**
     *
     * @param number $MinPrice            
     */
    public function setMinPrice($MinPrice)
    {
        $this->MinPrice = $MinPrice;
    }

    /**
     *
     * @param number $MaxPrice            
     */
    public function setMaxPrice($MaxPrice)
    {
        $this->MaxPrice = $MaxPrice;
    }

    /**
     *
     * @param number $LimitRecords            
     */
    public function setLimitRecords($LimitRecords)
    {
        $this->LimitRecords = $LimitRecords;
    }

    /**
     *
     * @param string $ImagesPath            
     */
    public function setImagesPath($ImagesPath)
    {
        $this->ImagesPath = $ImagesPath;
    }

    function getBreadcrumbsCategory($id = null)
    {
        if ($id == null) {
            $id = $this->getCurrentCategory();
        }
        if ($id == 0) {
            return false;
        }
        $sth = $this->prepare("SELECT `Id`, `Title`, `parentId`,`H1` FROM `{$this->CategoriesTable}` WHERE `Id`=? LIMIT 1");
        if ($sth != false && $sth->execute(array(
            $id
        )) != false && $sth->rowCount() > 0) {
            $row = $sth->fetch();
            if ($row->parentId != 0) {
                $this->getBreadcrumbsCategory($row->parentId);
            }
           
				if(!empty($row->H1))
				{
					$row->Title=$row->H1;
				}
				
                Breadcrumbs::add('<a href="' . $this->getUrl(array(
                    "module" => 'catalog',
                    'catid' => $row->Id
                )) . '">' . $row->Title . '</a>');
           
        }
    }

    function getRootCategoryId($id)
    {
        $sth = $this->prepare("select `Id`, `parentId` from `Catalog_Categories` where `Id`=(select `parentId` from `Catalog_Categories` where `Id`=?) limit 1");
        $sth->execute(array(
            $id
        ));
        if ($sth->rowCount() > 0) {
            $row = $sth->fetch();
            if ($row->parentId != 0) {
                return $this->getRootCategoryId($row->Id);
            }
            return $row->Id;
        }
        return $id;
    }

    function setBreadcrumbs()
    {
        if ($this->getCurrentID() != 0) {
            $getCategory = false;
            
            if ($this->getCurrentCategory() != 0) {
                $getCategory = true;
                $this->getBreadcrumbsCategory($this->getCurrentCategory());
            }
            if ($this->CurrentItemRow != null) {
                Breadcrumbs::add('<a href="' . $this->getUrl(array(
                    "module" => 'catalog',
                    'catalog' => $this->CurrentItemRow->Id
                )) . '">' . $this->CurrentItemRow->Title . '</a>');
            } else {
                $sth = $this->prepare("SELECT `Id`, `Title`,`CategoryID` FROM `{$this->ItemsTable}` WHERE `Id`=? LIMIT 1");
                if ($sth != false && $sth->execute(array(
                    $this->getCurrentId()
                )) != false && $sth->rowCount() > 0) {
                    $row = $sth->fetch();
                    if ($getCategory == false) {
                        $this->getBreadcrumbsCategory($row->CategoryID);
                    }
                    Breadcrumbs::add('<a href="' . $this->getUrl(array(
                        "module" => 'catalog',
                        'catalog' => $row->Id
                    )) . '">' . $row->Title . '</a>');
                }
            }
        } elseif ($this->getCurrentCategory() != 0) {
            $this->getBreadcrumbsCategory($this->getCurrentCategory());
        }
    }

    function setLastModified()
    {
        $sth = $this->prepare("SELECT `UpdatedDate` FROM `{$this->ItemsTable}` WHERE `Id`=? LIMIT 1");
        if ($sth != false && $sth->execute(array(
            $this->getCurrentId()
        )) != false && $sth->rowCount() > 0) {
            
            $LastModified_unix = strtotime($sth->fetchColumn()); // время последнего изменения страницы
            $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
            $IfModifiedSince = false;
            if (isset($_ENV['HTTP_IF_MODIFIED_SINCE'])) {
                $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
            }
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
            }
            if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                exit();
            }
            header('Last-Modified: ' . $LastModified);
        }
    }

    function getItem($Id = null)
    {
        if ($Id === null) {
            $Id = $this->getCurrentID();
        }
        
        $sth = $this->prepare("SELECT * FROM `{$this->ItemsTable}` WHERE `Id`=? AND `Active`=1 LIMIT 1");
        if ($sth != false && $sth->execute(array(
            $Id
        )) != false) {
            $row = $sth->fetch();
            if (! $row) {
                return false;
            }
            $this->CurrentItemRow = $row;
            $this->setCurrentID($row->Id);
            $this->setCurrentCategory($row->CategoryID);
            if (empty($row->H1)) {
                $row->H1 = $row->Title;
            }
            
            
            if (! empty($row->TitlePage)) {
                BreadcrumbsTitle::set($row->TitlePage);
            } else {
                BreadcrumbsTitle::set($row->Title);
            }
            
            if (! empty($row->DescPage)) {
                Metas::setDescription($row->DescPage);
            }
            if (! empty($row->KeysPage)) {
                Metas::setKeywords($row->KeysPage);
            }
            return new Redactor_Module_Catalog_Item($row);
        }
        return false;
    }

    function getItemsByCategoryId($limit = false) {
	}

    public function getQueryLimit()
    {
        if (! is_bool($this->getStart())) {
            return "limit {$this->getStart()}, {$this->getLimitRecords()}";
        }
        return '';
    }

    function getLooked()
    {
        if (! isset($_SESSION['CatalogLooked']) or isset($_SESSION['CatalogLooked']) && ! is_array($_SESSION['CatalogLooked'])) {
            $_SESSION['CatalogLooked'] = array();
        }
        return $_SESSION['CatalogLooked'];
    }

    function addLooked($id)
    {
        if (! isset($_SESSION['CatalogLooked']) or isset($_SESSION['CatalogLooked']) && ! is_array($_SESSION['CatalogLooked'])) {
            $_SESSION['CatalogLooked'] = array();
        }
        $_SESSION['CatalogLooked'][] = (int) $id;
    }

    function getLookedItems()
    {
        $fields = array(
            "`Id`"
        );
        if (($lf = $this->getListItemsFields()) && is_array($lf)) {
            foreach ($lf as $field) {
                if (is_array($field)) {
                    $fields[] = current($field);
                } elseif (is_string($field) && ! in_array("`{$field}`", $fields) && in_array($field, $this->getColumns())) {
                    $fields[] = "`{$field}`";
                }
            }
        }
        $query = array();
        $queryOr = array();
        $params = array();
        
        if (count($this->getLooked()) == 0) {
            return false;
        }
        
        if (count($query) > 0) {
            $query = " AND " . implode(" AND ", $query);
        } else {
            $query = '';
        }
        if (count($queryOr) > 0) {
            $queryOr = " OR " . implode(" OR ", $queryOr);
        } else {
            $queryOr = '';
        }
        
        $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0 AND `Id` IN (" . implode(", ", $this->getLooked()) . "){$query}{$queryOr}");
        if ($count != false && ($count->execute($params)) != false && ($count->rowCount() > 0)) {
            $this->setTotal($count->fetchColumn());
        }
        
        if ($this->getTotal() > 0) {
            $sth = $this->Stm("SELECT " . implode(", ", $fields) . " FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0 AND `Id` IN (" . implode(", ", $this->getLooked()) . "){$query}{$queryOr} {$this->getOrderBy()} {$this->getQueryLimit()}");
            if ($sth != false && ($sth->execute($params)) != false && ($sth->rowCount() > 0)) {
                $rows = array();
                foreach ($sth->fetchAll() as $row) {
                    $rows[] = new Redactor_Module_Catalog_Item($row);
                }
                return $rows;
            }
        }
    }

    function getCatField($field = '*', $cat = 0)
    {
        //if (! $this->isCategory()) {
            //return false;
        //}
        if ($cat == 0) {
            $cat = $this->getCurrentCategory();
        }        
        
        $sth = $this->prepare("select {$field} from `{$this->CategoriesTable}` where `Id`=?");
        $sth->execute(array(
            $cat
        ));
        
        if ($sth->rowCount() > 0) {
            return $sth->fetch();
        }
        return false;
    }
    // TODO: Добавить возможность добавлять собственные проверки к поля в запросе
    function getAllItems($id = 0)
    {
        $fields = array(
            "*",
            "IF(`Price`<=0, `Price`>0, 1) as `prior`"
        );
        
        $addF = false;
        if (($lf = $this->getListItemsFields()) && is_array($lf)) {
            foreach ($lf as $field) {
                if (is_array($field)) {
                    $fields[] = current($field);
                } elseif (is_string($field) && ! in_array("`{$field}`", $fields) && in_array($field, $this->getColumns())) {
                    $fields[] = "`{$field}`";
                }
            }
        }
        $query = array();
        $queryOr = array();
        $params = array();
        $a = array();
        $q = array();
        
        $filter = "";
        
        if (($CategoryID = $this->getCurrentCategory()) != 0) {			
			
            if ($id != 0) {
                $CategoryID = $id;
            }
            if ($this->hasCategoryChildren($CategoryID)) {
                
                $cats = array();
                
                $this->getCategoriesChildrens($CategoryID, $cats);
                
                $query[] = "(`CategoryID` in (" . implode(", ", $cats) . ") OR (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=`Catalog_Items`.`Id` AND `CategoryID` in (" . implode(", ", $cats) . "))>0)";
            } else {
                
                $query[] = "(`CategoryID`=:CategoryID OR (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=`Catalog_Items`.`Id` AND `CategoryID`=:CategoryID)>0)";
                $params[':CategoryID'] = $CategoryID;
            }
        }
        
        if (isset($_GET['brands'])) {
            $a[] = "`Brand`='{$_GET['brands']}'";
        }
        
        if (count($a) > 0) {
            $q[] = "(" . implode(" OR ", $a) . ")";
        }
        
        if (count($q) > 0) {
            $filter = " AND (" . implode(" AND ", $q) . ")";
        }
        
        if (count($query) > 0) {
            $query = " AND " . implode(" AND ", $query);
        } else {
            $query = '';
        }
        if (count($queryOr) > 0) {
            $queryOr = " OR " . implode(" OR ", $queryOr);
        } else {
            $queryOr = '';
        }
        if (isset($_GET['search'])) {
            $qs = urldecode($_GET['search']);
            $m = array();
            $sth = $this->query("SHOW COLUMNS FROM `{$this->ItemsTable}` WHERE `Type` LIKE '%text%' OR `Type` LIKE '%char%'");
            if ($sth && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $b) {
                    $m[] = "`{$b->Field}` COLLATE utf8_general_ci LIKE :SEARCH";
                }
                $params[':SEARCH'] = "%{$qs}%";
                $q[] = "(" . implode(" OR ", $m) . ")";
            }
        }
        
        if (count($q) > 0) {
            $filter = " AND (" . implode(" AND ", $q) . ")";
        }
        
        $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0{$filter}{$query}{$queryOr}");
        
        if ($count != false && ($count->execute($params)) != false && ($count->rowCount() > 0)) {
            
            $this->setTotal($count->fetchColumn());
        }
        
        $count = $this->Stm("SELECT Min(`Price`) as `min`, Max(`Price`) as `max` FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0{$query}{$queryOr}");
        
        if ($count != false && ($count->execute($params)) != false && ($count->rowCount() > 0)) {
            $r = $count->fetch();
            $this->setMinPrice($r->min);
            $this->setMaxPrice($r->max);
        }
        
        $count = $this->Stm("SELECT `Brand` FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0{$query}{$queryOr} group by `Brand`");
        if ($count != false && ($count->execute($params)) != false && ($count->rowCount() > 0)) {
            $ids = array();
            foreach ($count->fetchAll() as $row) {
                $ids[] = $row->Brand;
            }
            $sth = $this->prepare("select `Title`, `Id` from `Brands_Items` WHERE `Id` in (" . implode(", ", $ids) . ") ORDER BY `Sort`");
            $sth->execute();
            if ($sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $r) {
                    $this->Brands[$r->Id] = (object) array(
                        'Title' => $r->Title,
                        'Id' => $r->Id
                    );
                }
            }
        }
        
        $this->Add = array();
        $sth = $this->prepare("SELECT `Value`, `UFId`, `Id` FROM `UserFields_Values`   ORDER BY `Sort`");
        $sth->execute();
        if ($sth->rowCount() > 0) {
            foreach ($sth->fetchAll() as $row) {
                $count = $this->Stm("SELECT count(1) FROM `{$this->ItemsTable}` WHERE `Active`=1 and `uf_{$row->UFId}`={$row->Id} AND `CategoryID`!=0{$query}{$queryOr}");
                if ($count != false && ($count->execute($params)) != false && ($count->rowCount() > 0) && $count->fetchColumn() > 0) {
                    
                    $this->Add[$row->UFId][$row->Id] = (object) array(
                        'Title' => $row->Value,
                        'Field' => "uf_{$row->UFId}",
                        "Value" => $row->Id
                    );
                }
            }
        }
        
        if ($this->getTotal() > 0) {
            
            $sth = $this->Stm("SELECT " . implode(", ", $fields) . " FROM `{$this->ItemsTable}` WHERE `Active`=1 AND `CategoryID`!=0{$filter}{$query}{$queryOr} {$this->getOrderBy()} {$this->getQueryLimit()}");
            
            if ($sth != false && ($sth->execute($params)) != false && ($sth->rowCount() > 0)) {
                $rows = array();
                foreach ($sth->fetchAll() as $row) {
                    $rows[] = new Redactor_Module_Catalog_Item($row);
                }
                return $rows;
            }
        }
        
        return false;
    }

    function isCategory()
    {
        if (isset($_GET['catid'])) {
            return true;
        }
        return false;
    }

    function hasCategoryChildren($id = null)
    {
        if ($id == null) {
            $id = $this->getCurrentCategory();
        }
        $sth = $this->Stm("SELECT COUNT(1) FROM `{$this->CategoriesTable}` WHERE `parentId`=?");
        if ($sth != false && ($sth->execute(array(
            $id
        ))) != false && $sth->rowCount() > 0) {
            return $sth->fetchColumn();
        }
        return 0;
    }

    function getCategoriesChildrens($id, &$CategoiesIDs)
    {
        if ($id != 0) {
            $CategoiesIDs[] = $id;
        }
        
        $sth = $this->Stm("SELECT `Id` FROM `{$this->CategoriesTable}` WHERE `parentId`=?");
        
        if ($sth != false && ($sth->execute(array(
            $id
        ))) != false && $sth->rowCount() > 0) {
            foreach ($sth->fetchAll() as $row) {
                
                $this->getCategoriesChildrens($row->Id, $CategoiesIDs);
            }
        }
    }

    function getCategoryById($Id)
    {
        $sth = $this->prepare("select * FROM `Catalog_Categories` WHERE `Id`=? LIMIT 1");
        $sth->execute(array(
            $Id
        ));
        if ($sth->rowCount() > 0) {
            return $sth->fetch();
        }
        return false;
    }

    function getCategories($parentId = 0)
    {
        $sth = $this->Stm("SELECT `Id`, `Title`, `Icon` FROM `{$this->CategoriesTable}` WHERE `parentId`=? ORDER BY `Sort`");
        
        if ($sth != false && ($sth->execute(array(
            $parentId
        ))) != false && $sth->rowCount() > 0) {
            return $sth->fetchAll();
        }
        return false;
    }

    function getItemsWhere($params)
    {}
}

class Redactor_Module_Catalog_Item_Image extends Redactor_Ini
{

    private $ImagesPath = "files/catalog";

    private $ImageWidth = 0;

    private $ImageHeight = 0;

    function __construct($data)
    {
        if ($data) {
            foreach ($data as $n => $r) {
                if (is_array($r)) {
                    foreach ($r as $name => $value) {
                        $this->{$name} = $value;
                    }
                } else {
                    $this->{$n} = $r;
                }
            }
        }
    }

    function thumb($width = 100, $height = 100)
    {
        return $this->getThumbUrl($width, $height);
    }

    function getThumb($width = 100, $height = 100)
    {
        return $this->getThumbUrl($width, $height);
    }

    function getPath()
    {
        return $this->ImagesPath;
    }

    function getThumbUrl($width = 100, $height = 100)
    {
        $a = base64_encode(serialize(array(
            "width" => $width,
            "height" => $height,
            "image" => $this->getFileName()
        )));
        return "/thumbs/catalog/" . base64_encode("{$width}_{$height}_{$this->id()}") . ".{$this->getExtension()}";
    }

    function isMain()
    {
        if ($this->get('isMain') > 0) {
            return true;
        }
        return false;
    }

    function getFileName()
    {
        return "o_{$this->id()}.{$this->getExtension()}";
    }

    function getExtension()
    {
        return $this->get('Extension');
    }

    function getSize()
    {
        if ($this->ImageWidth > 0 && $this->ImageHeight > 0) {
            return array(
                'width' => $this->ImageWidth,
                'height' => $this->ImageHeight
            );
        }
        $size = getimagesize($this->getPath() . '/' . $this->getFileName());
        if ($size) {
            $this->ImageWidth = $size[0];
            $this->ImageHeight = $size[1];
        }
    }

    function getWidth()
    {
        $this->getSize();
        return $this->ImageWidth;
    }

    function getHeight()
    {
        $this->getSize();
        return $this->ImageHeight;
    }

    function isVertical()
    {
        if ($this->getWidth() >= $this->getHeight()) {
            return true;
        }
        return false;
    }

    function isHorizontal()
    {
        if ($this->getWidth() <= $this->getHeight()) {
            return true;
        }
        return false;
    }

    function getFileSize()
    {
        return filesize($this->getPath() . '/' . $this->getFileName());
    }

    function getUrl()
    {
        return '/' . $this->getPath() . '/' . $this->getFileName();
    }

    function id()
    {
        return $this->get('Id') ? $this->get('Id') : false;
    }

    function get($name)
    {
        return isset($this->{$name}) ? $this->{$name} : '';
    }

    function __get($name)
    {
        return isset($this->{$name}) ? $this->{$name} : false;
    }

    function __set($name, $value)
    {
        $this->{$name} = $value;
    }
}

class Redactor_Module_Catalog_Item extends Redactor_Ini
{

    private $BrandsTable = "Brands_Items";

    private $ItemsTable = "Catalog_Items";

    private $CategoriesTable = "Catalog_Categories";

    private $ImagesTable = "Catalog_Items_Files";

    private $VirtualItemsTable = 'Catalog_Items_Categories';

    var $Columns = array();

    public function getUFValues($value)
    {
        $sth = $this->prepare("SELECT `Value`, `Id` FROM `{$this->UserFieldsValues}` WHERE `UFId`=? ORDER BY `Sort`");
        $sth->execute(array(
            $value
        ));
        if ($sth->rowCount() > 0) {
            $rows = array();
            foreach ($sth->fetchAll() as $row) {
                $rows[] = (object) array(
                    'Title' => $row->Value,
                    "Value" => $this->{"uf_" . $value . "_{$row->Id}"}
                );
            }
            return $rows;
        }
        return false;
    }

    public function getUFValue($value)
    {
        $sth = $this->prepare("SELECT `Value` FROM `{$this->UserFieldsValues}` WHERE `Id`=? ORDER BY `Sort` LIMIT 1");
        $sth->execute(array(
            $value
        ));
        if ($sth->rowCount() > 0) {
            return $sth->fetchColumn();
        }
        return false;
    }

    public function getUFGroup()
    {
        if ($this->getField('TypeUF') != false or $this->getField('TypeUF') != 0) {
            $sth = $this->prepare("SELECT `Id`, `Title` FROM `{$this->UserFieldsTable}` WHERE `Group`=? OR `Id` IN (15,16,17,18) ORDER BY `Sort`");
            if ($sth != false && $sth->execute(array(
                $this->getField('TypeUF')
            )) != false && $sth->rowCount() > 0) {
                $fields = array();
                
                foreach ($sth->fetchAll() as $row) {
                    if ($this->getField('uf_' . $row->Id)) {
                        $fields[] = (object) array(
                            'Id' => $row->Id,
                            'Title' => $row->Title,
                            'Value' => $this->{'uf_' . $row->Id}
                        );
                    }
                }
                
                return $fields;
            }
        }
        return false;
    }

    public function getColumns()
    {
        if (count($this->Columns) == 0) {
            $sth = $this->Query("SHOW COLUMNS FROM `{$this->ItemsTable}`");
            if ($sth != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $this->Columns[] = $row->Field;
                }
            }
        }
        return $this->Columns;
    }

    function __construct($data)
    {
        foreach ($data as $n => $r) {
            if (is_array($r)) {
                foreach ($r as $name => $value) {
                    $this->{$name} = $value;
                }
            } else {
                $this->{$n} = $r;
            }
        }
    }

    function getUrl()
    {
        return rewriteUrls::getUrl(array(
            'module' => 'catalog',
            'catalog' => $this->id()
        ));
    }

    function id()
    {
        return $this->get('Id') ? $this->get('Id') : false;
    }

    function getVirtualCategories()
    {}

    function getBrand()
    {}

    function getField($Field)
    {
        if ($this->get($Field) != '') {
            return $this->get($Field);
        }
        if (in_array($Field, $this->getColumns())) {
            $sth = $this->Stm("SELECT `{$Field}` FROM `{$this->ItemsTable}` WHERE `Id`=?");
            if ($sth != false && ($sth->execute(array(
                $this->id()
            ))) != false && $sth->rowCount() > 0) {
                $this->{$Field} = $sth->fetchColumn();
                return $this->{$Field};
            }
        }
        return false;
    }

    function getBrandName()
    {
        if (($BrandName = $this->get('BrandName')) != '') {
            return $BrandName;
        }
        if (($BrandId = $this->getField('Brand')) != false) {
            
            $sth = $this->Stm("SELECT `Title` FROM `{$this->BrandsTable}` WHERE `Active`=1 AND `Id`=? LIMIT 1");
            if ($sth != false && ($sth->execute(array(
                $BrandId
            ))) != false && $sth->rowCount() > 0) {
                $this->BrandName = $sth->fetchColumn();
                return $this->BrandName;
            }
        }
        return false;
    }

    function getCategoryId()
    {
        if (($CategoryID = $this->get('CategoryID')) != '') {
            return $CategoryID;
        }
        $sth = $this->Stm("SELECT `CategoryID` FROM `{$this->ItemsTable}` WHERE `Id`=?");
        if ($sth != false && ($sth->execute(array(
            $this->id()
        ))) != false && $sth->rowCount() > 0) {
            $this->CategoryID = $sth->fetchColumn();
            return $this->CategoryID;
        }
        return false;
    }

    function getCategoryName()
    {
        if (($CategoryName = $this->get('CategoryName')) != '') {
            return $CategoryName;
        }
        if (($CategoryID = $this->getCategoryID()) != false) {
            $sth = $this->Stm("SELECT `Title` FROM `{$this->CategoriesTable}` WHERE `Id`=?");
            if ($sth != false && ($sth->execute(array(
                $CategoryID
            ))) != false && $sth->rowCount() > 0) {
                $this->CategoryaName = $sth->fetchColumn();
                return $this->CategoryName;
            }
        }
        return '';
    }

    function getImage()
    {
        $sth = $this->Stm('SELECT * FROM `' . $this->ImagesTable . '` where `ItemID`=? ORDER BY `MainImage` DESC, `Sort` ASC LIMIT 1'); // запрос
        if ($sth != false && ($sth->execute(array(
            $this->id()
        ))) != false && $sth->rowCount() > 0) {
            return new Redactor_Module_Catalog_Item_Image($sth->fetch());
        }
        return false;
    }
    
    function getSecondaryImage()
    {
        $sth = $this->Stm('SELECT * FROM `' . $this->ImagesTable . '` where `ItemID`=? AND `MainImage`=0 ORDER BY  `Sort` ASC LIMIT 1'); // запрос
        if ($sth != false && ($sth->execute(array(
            $this->id()
        ))) != false && $sth->rowCount() > 0) {
            return new Redactor_Module_Catalog_Item_Image($sth->fetch());
        }
        return false;
    }
    
    
    
    function getImages($limit = false)
    {
        if ($limit != false && is_numeric($limit)) {
            $limit = ' limit ' . $limit;
        } else {
            $limit = '';
        }
        
        $sth = $this->Stm('SELECT * FROM `' . $this->ImagesTable . '` where `ItemID`=? ORDER BY `MainImage` DESC, `Sort` ASC ' . $limit); // запрос
        if ($sth != false && ($sth->execute(array(
            $this->id()
        ))) != false && $sth->rowCount() > 0) {
            $images = array();
            foreach ($sth->fetchAll() as $row) {
                $images[] = new Redactor_Module_Catalog_Item_Image($row);
            }
            return $images;
        }
        return false;
    }

    function __get($name)
    {
        return isset($this->{$name}) ? $this->{$name} : false;
    }

    function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    function get($name)
    {
        return isset($this->{$name}) ? $this->{$name} : '';
    }
}

class Catalog_admin extends Redactor_Admin
{

    var $ItemsTable = "Catalog_Items";

    var $CategoriesTable = "Catalog_Categories";

    var $ImagesTable = "Catalog_Items_Files";

    var $VirtualItemsTable = 'Catalog_Items_Categories';

    var $LimitRecords = 25;

    var $ImagesPath = "files/catalog";

    function __construct()
    {}

    public function DeleteImageTree($Id)
    {
        if ($Id == null) {
            $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        }
        $sth = $this->Stm("SELECT * FROM `Catalog_Categories_Files` WHERE `Id`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            $row = $sth->fetch();
            $this->exec("DELETE FROM `Catalog_Categories_Files` WHERE `Id`='{$row->Id}'");
            $file = APPLICATION_PATH . "/files/countries/o_{$row->Id}.{$row->Extension}";
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    public function SaveFileTree()
    {
        $Id = 0;
        if (isset($_POST['Id'])) {
            $Id = (int) $_POST['Id'];
            unset($_POST['Id']);
        }
        
        $fields = array();
        $params = array();
        $table = '';
        $cols = array();
        
        $table = 'Catalog_Categories_Files';
        $cols = $this->getColumns($table);
        
        foreach ($_POST as $name => $value) {
            if (in_array($name, $cols)) {
                $fields[] = '`' . $name . '`=?';
                $params[] = $value;
            }
        }
        $this->ImagesPath = APPLICATION_PATH . '/files/countries';
        
        if (isset($_FILES['photo-path']) && isset($_FILES['photo-path']['name']) && ! empty($_FILES['photo-path']['name'])) {
            $ext = strtolower(pathinfo($_FILES['photo-path']['name'], PATHINFO_EXTENSION));
            if (in_array($ext, array(
                'jpg',
                'jpeg',
                'png',
                'gif'
            ))) {
                
                $sth = $this->Stm("INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)");
                if ($sth != false && ($sth->execute(array(
                    $_POST['ItemID'],
                    $ext,
                    1
                ))) != false) {
                    $Id = $this->getAdapter()->lastInsertId();
                    if ($Id) {
                        if (is_writeable($this->ImagesPath)) {
                            if (move_uploaded_file($_FILES['photo-path']['tmp_name'], $this->ImagesPath . "/o_{$Id}.{$ext}")) {
                                chmod($this->ImagesPath . "/o_{$Id}.{$ext}", 0666);
                            } else {
                                $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                                echo json_encode(array(
                                    "failure" => true,
                                    'error' => 'error move file'
                                ));
                                return false;
                            }
                        } else {
                            $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                            echo json_encode(array(
                                "failure" => true,
                                'error' => 'path not writable'
                            ));
                            return false;
                        }
                    } else {
                        echo json_encode(array(
                            "failure" => true,
                            'error' => 'no ID'
                        ));
                        return false;
                    }
                } else {
                    echo json_encode(array(
                        "failure" => true,
                        'error' => 'insert image error',
                        'pdo' => $this->getAdapter()->errorInfo()
                    ));
                    return false;
                }
            }
        }
        
        if ($Id == 0) {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
            exit();
        }
        
        if (isset($_POST['MainImage']) && isset($_POST['ItemID'])) {
            $this->exec("UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . (int) $_POST['ItemID'] . "'");
        }
        $fields[] = '`UpdatedDate`=CURRENT_TIMESTAMP';
        
        $sth = $this->Stm("UPDATE `{$table}` SET " . implode(", ", $fields) . " WHERE `Id`=?");
        $params[] = $Id;
        
        if ($sth != false && ($sth->execute($params)) != false) {
            echo json_encode(array(
                "success" => true
            ));
        } else {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
    }

    public function LoadImagesTree()
    {
        $results = array();
        $rows = 0;
        
        $count = $this->Stm("SELECT COUNT(1) FROM `Catalog_Categories_Files` WHERE `ItemID`=? order by `isImage` desc ");
        $start = isset($_POST['start']) ? (int) $_POST['start'] : 0;
        $limit = isset($_POST['limit']) ? (int) $_POST['limit'] : $this->LimitRecords;
        if ($count != false && ($count->execute(array(
            (int) $_POST['Id']
        ))) != false && $count->rowCount() > 0) {
            $rows = $count->fetchColumn();
        }
        if ($rows > 0) {
            
            $sth = $this->Stm("SELECT `Id`,`Extension`, `Title`, `Sort`, `Type`, `MainImage`, `ItemID` FROM `Catalog_Categories_Files` WHERE  `ItemID`=? order by `isImage` desc LIMIT {$start}, {$limit}");
            if ($sth != false && ($sth->execute(array(
                (int) $_POST['Id']
            ))) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $results[] = array(
                        "Id" => $row->Id,
                        'ItemID' => $row->ItemID,
                        "Sort" => $row->Sort,
                        'Type' => $row->Type,
                        'MainImage' => $row->MainImage,
                        "Title" => $this->win2utf($row->Title),
                        "image" => 'o_' . $row->Id . '.' . $row->Extension
                    );
                }
            }
        }
        
        echo json_encode(array(
            "total" => $rows,
            "results" => $results
        ));
    }

    function arrayEncode($ar)
    {
        $return = array();
        if (is_array($ar)) {
            foreach ($ar as $name => $value) {
                $return[$name] = $this->arrayEncode($value);
            }
        } else {
            return $this->win2utf($ar);
        }
        
        return $return;
    }

    function arrayDecode($ar)
    {
        $return = array();
        if (is_array($ar)) {
            foreach ($ar as $name => $value) {
                $return[$this->utf2win($name)] = $this->arrayEncode($value);
            }
        } else {
            return $this->utf2win($str);
        }
        
        return $return;
    }

    function totalEmails()
    {
        $total = 0;
        $sql = $this->query("select count(*) from `Users` where `Active`='1'");
        if ($sql && $sql->rowCount() > 0) {
            $total = $sql->fetchColumn();
        }
        echo json_encode(array(
            "total" => $total
        ));
    }
    
    //
    function sendNewsletterTest()
    {
        $sql = $this->query("select * from `Newsletter`");
        if ($sql && $sql->rowCount() > 0) {
            $mail = $sql->fetch(PDO::FETCH_ASSOC);
            $mail['message'] = "<div style='color: #333;font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;font-size: 11px;'>{$mail['message']}</div>";
            
            $send = new Email();
            $send->setFrom('info@' . preg_replace("/www./is", "", getenv("HTTP_HOST")), $mail['name']);
            $to = $this->get('email_admin');
            
            $send->EmailHTML($to, $mail['subject'], $mail['message']);
        }
        echo "{success:true}";
    }

    function saveNewsletter()
    {
        $subject = isset($_POST['subject']) ? addslashes($this->utf2win($_POST['subject'])) : '';
        $message = isset($_POST['message']) ? addslashes($this->utf2win($_POST['message'])) : '';
        $name = isset($_POST['name']) ? addslashes($this->utf2win($_POST['name'])) : '';
        
        $sql = $this->query("select * from `Newsletter`");
        if ($sql && $sql->rowCount() > 0) {
            $this->query("update `Newsletter` set `subject`='{$subject}', `name`='{$name}', `message`='{$message}'");
        } else {
            $this->query("insert into `Newsletter` values ('$subject','$message','{$name}')");
        }
        echo "{success:true}";
    }

    function sendNewsletter()
    {
        set_time_limit(3600);
        
        $sql = $this->query("select * from `Newsletter` ");
        if ($sql && $sql->rowCount() > 0) {
            $mail = $sql->fetch(PDO::FETCH_ASSOC);
            $mail['message'] = "<div style='color: #333;font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;font-size: 11px;'>{$mail['message']}</div>";
            
            $sql = $this->query("select * from `Users` where `Active`='1'");
            if ($sql && $sql->rowCount() > 0) {
                foreach ($sql->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    $send = new Email();
                    
                    $send->setFrom('info@' . preg_replace("/www./is", "", getenv("HTTP_HOST")), $mail['name']);
                    $send->EmailHTML($row['Email'], $mail['subject'], $mail['message']);
                    usleep(30000);
                }
            }
        }
        echo "{success:true}";
    }

    function loadNewsletter()
    {
        $sql = $this->query("select * from `Newsletter`");
        
        if ($sql && $sql->rowCount() > 0) {
            $row = $sql->fetch(PDO::FETCH_ASSOC);
            $row['to'] = 'redactor_all';
            $row = $this->arrayEncode($row);
            
            echo json_encode(array(
                "success" => true,
                "data" => $row
            ));
            return true;
        }
        echo "{failure:true}";
    }

    public function ChangeCategoryIDItems()
    {
        $items = json_decode($_POST['items']);
        $new = array();
        $CategoryID = (int) $_POST['CategoryID'];
        if (is_array($items) && count($items) > 0) {
            $new = array();
            foreach ($items as $id) {
                $new[] = (int) $id;
            }
            unset($items);
            $this->exec("UPDATE `{$this->ItemsTable}` SET `CategoryID`='{$CategoryID}' WHERE `Id` IN (" . implode(", ", $new) . ")");
        }
    }

    private function getParentsId($Id, &$items)
    {
        $items[] = $Id;
        $sth = $this->Stm("SELECT `parentId` from `{$this->CategoriesTable}` WHERE `Id`=? limit 1");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            $parentId = $sth->fetchColumn();
            if ($parentId != 0) {
                $this->getParentsId($parentId, $items);
            }
        }
    }

    private function getChildrensId($Id, &$items)
    {
        $items[] = $Id;
        $sth = $this->Stm("SELECT `Id` from  `{$this->CategoriesTable}` WHERE `parentId`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            foreach ($sth->fetchAll() as $row) {
                
                $this->getParentsId($row->Id, $items);
            }
        }
    }

    private function isMainCategoryItem($Id, $Category)
    {
        $sth = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}` WHERE `Id`=? AND `CategoryID`=?");
        if ($sth != false && ($sth->execute(array(
            $Id,
            $Category
        ))) != false && $sth->fetchColumn() > 0) {
            return true;
        }
        return false;
    }

    private function isChecked($Id, $Category)
    {
        $sth = $this->Stm("SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=? AND `CategoryID`=?");
        if ($sth != false && ($sth->execute(array(
            $Id,
            $Category
        ))) != false && $sth->fetchColumn() > 0) {
            return true;
        }
        return false;
    }

    public function LoadCategories($parentId = 0, $notIn = array())
    {
        if ($parentId == 0 && ! isset($_POST['checked']) && isset($_POST['Id']) && $_POST['Id'] != 0) {
            
            $this->getChildrensId((int) $_POST['Id'], $notIn);
        }
        $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        $checkedTest = isset($_POST['checked']) ? true : false;
        if (count($notIn) > 0) {
            $notIn2 = " AND `Id` NOT IN (" . implode(", ", $notIn) . ")";
        } else {
            $notIn2 = '';
        }
        $Data = array();
        
        $sth = $this->Stm("Select `Id`, `Title` from `{$this->CategoriesTable}` where `parentId`=? {$notIn2} ORDER BY `Sort` asc");
        
        if ($sth != false && ($sth->execute(array(
            $parentId
        ))) != false && $sth->rowCount() > 0) {
            
            foreach ($sth->fetchAll() as $row) {
                
                $link = rewriteUrls::getUrl(array(
                    'module' => 'catalog',
                    'catid' => $row->Id
                ));
                
                $item = array(
                    "Title" => $this->win2utf($row->Title),
                    "Id" => $row->Id,
                    'text' => $this->win2utf($row->Title),
                    "id" => $row->Id,
                    "Link" => $link,
                    'qtip' => $link,
                    'draggable' => true,
                    "leaf" => false,
                    "children" => $this->LoadCategories($row->Id, $notIn)
                );
                
                if ($checkedTest && $this->isMainCategoryItem($Id, $row->Id) == false) {
                    $item['checked'] = $this->isChecked($Id, $row->Id);
                } elseif (isset($_POST['checked']) && isset($_POST['Id']) && $_POST['Id'] != 0) {
                    $item['checked'] = false;
                }
                
                $Data[] = $item;
            }
        }
        
        if ($parentId == 0) {
            echo json_encode($Data);
            return false;
        }
        return $Data;
    }

    public function DeleteCategory($Id = null)
    {
        if ($Id == null) {
            $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        }
        $sth = $this->Stm("SELECT `Id` FROM `{$this->CategoriesTable}` WHERE `parentId`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            foreach ($sth->fetchAll() as $row) {
                $this->DeleteRecords($row->Id);
                $this->DeleteCategory($row->Id);
            }
        }
        
        $sth = $this->Stm("DELETE FROM `{$this->CategoriesTable}` WHERE `Id`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false) {}
        $this->exec("delete from `rewriteUrls` WHERE `module`='catalog' and `isCat`='1' and `objId`='{$Id}'");
    }

    private function getCategoryName($Id)
    {
        if ($Id == 0) {
            return 'Корень';
        }
        $sth = $this->Stm("SELECT `Title` FROM `{$this->CategoriesTable}` WHERE `Id`=? LIMIT 1");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            return $sth->fetchColumn();
        }
        return 'Корень';
    }

    public function DeleteImage($Id)
    {
        if ($Id == null) {
            $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        }
        $sth = $this->Stm("SELECT * FROM `{$this->ImagesTable}` WHERE `Id`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false && $sth->rowCount() > 0) {
            $row = $sth->fetch();
            $this->exec("DELETE FROM `{$this->ImagesTable}` WHERE `Id`='{$row->Id}'");
            $file = $this->ImagesPath . "/o_{$row->Id}.{$row->Extension}";
            if (file_exists($file)) {
                unlink($file);
            }
        }
    }

    public function DeleteRecord($Id = null)
    {
        if ($Id == null) {
            $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        }
        if (isset($_POST['virtual'])) {
            $this->DeleteVirtualCategory($Id, (isset($_POST['CategoryID']) ? (int) $_POST['CategoryID'] : 0));
        } else {
            $this->DeleteVirtualCategories($Id);
            $this->DeleteImages($Id);
            $this->exec("DELETE FROM `{$this->ItemsTable}` WHERE `Id`='{$Id}'");
            $this->exec("delete from `rewriteUrls` WHERE `module`='catalog' and `isCat`='0' and `objId`='{$Id}'");
        }
    }

    private function DeleteImages($Id)
    {
        if ($Id != 0) {
            $sth = $this->Stm("SELECT `Id` FROM `{$this->ImagesTable}` WHERE `ItemID`=?");
            if ($sth != false && ($sth->execute(array(
                $Id
            ))) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $this->DeleteImage($row->Id);
                }
            }
        }
    }

    public function DeleteRecords()
    {
        $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
        if ($Id != 0) {
            $sth = $this->Stm("SELECT `Id` FROM `{$this->ItemsTable}` WHERE `CategoryID`=?");
            if ($sth != false && ($sth->execute(array(
                $Id
            ))) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $this->DeleteRecord($row->Id);
                }
            }
        }
    }

    public function LoadRecord()
    {
        $to = isset($_POST['to']) ? $_POST['to'] : '';
        if ($to == 'Category' || $to == 'Record' || $to == "File") {
            $Data = array();
            $Id = isset($_POST['Id']) ? (int) $_POST['Id'] : 0;
            
            if ($to == 'Category') {
                $table = $this->CategoriesTable;
                $fieldCategory = 'parentId';
            } elseif ($to == 'File') {
                $table = $this->ImagesTable;
            } else {
                $table = $this->ItemsTable;
                $fieldCategory = 'CategoryID';
                if ($Id == 0) {
                    $sth = $this->query("INSERT INTO `{$table}` (`Id`,`UpdatedDate`, `Active`) VALUES (NULL, CURRENT_TIMESTAMP,1)");
                    if ($sth != false) {
                        $Id = $this->getAdapter()->lastInsertId();
                    } else {
                        echo json_encode(array(
                            'failure' => true,
                            'data' => array()
                        ));
                        return false;
                    }
                }
            }
            $sth = $this->Stm("SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1");
            if ($sth != false && ($sth->execute(array(
                $Id
            ))) != false && $sth->rowCount() > 0) {
                $row = $sth->fetch(PDO::FETCH_ASSOC);
                if (isset($_POST['TypeUF'])) {
                    $row['TypeUF'] = (int) $_POST['TypeUF'];
                }
                foreach ($row as $name => $value) {
                    if (isset($_POST['TypeUF'])) {
                        if (preg_match("/^uf_/", $name)) {
                            $Data[$name] = $this->win2utf($value);
                        }
                    } else {
                        $Data[$name] = $this->win2utf($value);
                    }
                }
                if (isset($fieldCategory)) {
                    $Data['categoryName'] = $this->win2utf($this->getCategoryName($row[$fieldCategory]));
                }
                if ($to == 'Category') {
                    $Data['url'] = $this->win2utf(rewriteUrls::getSingleCustomUrl(array(
                        'module' => 'catalog',
                        'catid' => $row['Id']
                    )));
                } elseif ($to == 'Record') {
                    
                    $Data['UFData'] = $this->getUFJS('catalog', $row['TypeUF'], 0, $row);
                    if (! isset($_POST['TypeUF'])) {
                        $Data['UFGroups'] = array(
                            "results" => $this->getUFGroups()
                        );
                        $Data['VirtualCategoriesName'] = $this->win2utf(implode(", ", $this->getVirtualCategoryNames($row['Id'])));
                        $Data['url'] = $this->win2utf(rewriteUrls::getUrl(array(
                            'module' => 'catalog',
                            'catalog' => $row['Id']
                        )));
                    }
                }
                $Data["UpdatedDate2"] = $this->win2utf($this->formatDate("d micromonth Y H:i:s", $row['UpdatedDate']));
                $Data["CreatedDate2"] = $this->win2utf($this->formatDate("d micromonth Y H:i:s", $row['CreatedDate']));
            }
            echo json_encode(array(
                "success" => true,
                "data" => $Data
            ));
        } else {
            echo json_encode(array(
                'failure' => true,
                'data' => array()
            ));
        }
    }

    function getColumns($table)
    {
        $cols = array();
        $sth = $this->Query("SHOW COLUMNS FROM `{$table}`");
        if ($sth != false && $sth->rowCount() > 0) {
            foreach ($sth->fetchAll() as $row) {
                $cols[] = $row->Field;
            }
        }
        
        return $cols;
    }

    private function countFiles($Id)
    {
        $sth = $this->Stm("SELECT COUNT(1) FROM `Catalog_Items_Files` WHERE `ItemID`=?");
        if ($sth != false && ($sth->execute(array(
            $Id
        ))) != false) {
            return $sth->fetchColumn();
        }
        return 0;
    }

    public function LoadRecords()
    {
        $columns = $this->getColumns($this->ItemsTable);
        $orderBy = "ORDER BY ";
        if (isset($_POST['sort']) && in_array($_POST['sort'], $columns)) {
            $orderBy .= "`{$this->ItemsTable}`.`{$_POST['sort']}`";
            if (isset($_POST['dir']) && in_array($_POST['dir'], array(
                "ASC",
                "DESC"
            ))) {
                $orderBy .= ' ' . $_POST['dir'];
            }
        } else {
            $orderBy .= "`{$this->ItemsTable}`.`Sort` ASC";
        }
        $results = array();
        $rows = 0;
        $dop = array();
        
        $CategoryID = isset($_POST['CategoryID']) ? (int) $_POST['CategoryID'] : 0;
        $params = array();
        if (isset($_POST['search'])) {
            $s = $this->utf2win($_POST['search']);
            
            $sth = $this->query("SHOW COLUMNS FROM `{$this->ItemsTable}` WHERE `Type` LIKE '%char%' or `Type` LIKE 'text'");
            if ($sth && $sth->rowCount() > 0) {
                $f = array();
                $params[':QUERY'] = "%{$s}%";
                foreach ($sth->fetchAll() as $row) {
                    
                    $dop[] = "`{$row->Field}` COLLATE utf8_general_ci LIKE :QUERY";
                }
            }
            
            if (count($dop) == 0) {
                echo json_encode(array(
                    "total" => 0,
                    "results" => array()
                ));
                return false;
            }
        }
        if (is_array($dop) && count($dop) > 0) {
            $dop = " WHERE (" . implode(" OR ", $dop) . ")";
        } elseif (! is_string($dop)) {
            $dop = '';
        }
        if (! isset($_POST['search'])) {
            $params = array(
                ':CategoryID' => $CategoryID
            );
            $dop = "";
            if ($CategoryID == 22) {
                $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}` ");
            } else {
                $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}` WHERE `{$this->ItemsTable}`.`CategoryID`=:CategoryID  {$dop}  OR (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=`Catalog_Items`.`Id` AND `CategoryID`=:CategoryID)>0");
            }
        } else {
            
            $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ItemsTable}`{$dop}");
        }
        
        $start = isset($_POST['start']) ? (int) $_POST['start'] : 0;
        $limit = isset($_POST['limit']) ? (int) $_POST['limit'] : $this->LimitRecords;
        if ($count != false && ($count->execute($params)) != false && $count->rowCount() > 0) {
            $rows = $count->fetchColumn();
        }
        
        if ($rows > 0) {
            if (! isset($_POST['search'])) {
                if ($CategoryID == 22) {
                    $sth = $this->Stm("SELECT *,CASE WHEN (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `{$this->VirtualItemsTable}`.`CategoryID`!=`{$this->ItemsTable}`.`CategoryID` AND `{$this->VirtualItemsTable}`.`CategoryID`=:CategoryID)>0 THEN TRUE ELSE FALSE END AS `isVirtual` FROM `{$this->ItemsTable}`  LIMIT {$start}, {$limit}");
                } else {
                    $sth = $this->Stm("SELECT CASE WHEN (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `{$this->VirtualItemsTable}`.`CategoryID`!=`{$this->ItemsTable}`.`CategoryID` AND `{$this->VirtualItemsTable}`.`CategoryID`=:CategoryID)>0 THEN TRUE ELSE FALSE END AS `isVirtual` ,`{$this->ItemsTable}`.`Id`, `{$this->ItemsTable}`.`Title`, `{$this->ItemsTable}`.`Sort`, `{$this->ItemsTable}`.`Active`, `{$this->ItemsTable}`.`UpdatedDate`, `{$this->ItemsTable}`.`CreatedDate`, `{$this->ItemsTable}`.`CategoryID`, `{$this->ItemsTable}`.`Price`, `{$this->ItemsTable}`.`Article`
			 FROM `{$this->ItemsTable}` WHERE `{$this->ItemsTable}`.`CategoryID`=:CategoryID {$dop} OR (SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=`Catalog_Items`.`Id` AND `CategoryID`=:CategoryID)>0  {$dop} {$orderBy} LIMIT {$start}, {$limit}");
                }
            } else {
                $sth = $this->Stm("SELECT FALSE AS `isVirtual` ,`{$this->ItemsTable}`.`Id`, `{$this->ItemsTable}`.`Title`, `{$this->ItemsTable}`.`Sort`, `{$this->ItemsTable}`.`Active`, `{$this->ItemsTable}`.`UpdatedDate`, `{$this->ItemsTable}`.`CreatedDate`, `{$this->ItemsTable}`.`CategoryID`, `{$this->ItemsTable}`.`Price`, `{$this->ItemsTable}`.`Article`
				FROM `{$this->ItemsTable}` {$dop} {$orderBy} LIMIT {$start}, {$limit}");
            }
            
            if ($sth != false && ($sth->execute($params)) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $results[] = array(
                        "Id" => $row->Id,
                        'Link' => rewriteUrls::getUrl(array(
                            'module' => 'catalog',
                            'id' => $row->Id
                        )),
                        "Sort" => $row->Sort,
                        'Link' => rewriteUrls::getUrl(array(
                            'module' => 'catalog',
                            'catalog' => $row->Id
                        )),
                        "isVirtual" => $row->isVirtual,
                        "Active" => $row->Active,
                        "UpdatedDate" => $this->win2utf($this->formatDate("d micromonth Y", $row->UpdatedDate)),
                        "CreatedDate" => $this->win2utf($this->formatDate("d micromonth Y", $row->CreatedDate)),
                        "Title" => $this->win2utf($row->Title),
                        "Article" => $this->win2utf($row->Article),
                        "Price" => $row->Price,
                        'CurrentCategoryID' => $CategoryID,
                        'categoryName' => $this->win2utf($this->getCategoryName($row->CategoryID))
                    );
                }
            }
        }
        
        echo json_encode(array(
            "total" => $rows,
            "results" => $results
        ));
    }

    function SaveDataOrder()
    {
        $sth = $this->prepare("update `Orders` set `Status`=? WHERE `Id`=?");
        $sth->execute(array(
            $_POST['Status'],
            $_POST['Id']
        ));
        echo '{success:true}';
    }

    function getOrderInfo($row)
    {
        $msg = '';
        
        /*
         * $company = $this->getModule ( 'company' )->getCompanyById ( $row->CompanyID ); $msg = '<div style="font-weight: bold; padding-left: 2px; width:200px">ИНН ' . $company->uf_71->Value . ',<br/> КПП ' . $company->uf_72->Value . ',' . $company->Title . ',<br/> ' . $company->uf_59->Value . ',' . $company->uf_75->Value . ',<br/> тел.:' . $company->uf_96->Value . '</div>';
         */
        return $msg;
    }

    public function LoadOrders()
    {
        $columns = $this->getColumns($this->ItemsTable);
        
        $results = array();
        $rows = 0;
        
        $dop = '';
        if (isset($_POST['search'])) {
            $s = $this->utf2win($_POST['search']);
            $sth = $this->prepare("select `Id` from `Users` Where `Email` LIKE ?");
            $sth->execute(array(
                "%{$s}%"
            ));
            if ($sth->rowCount() > 0) {
                $ids = array();
                foreach ($sth->fetchAll() as $row) {
                    $ids[] = $row->Id;
                }
                if (count($ids) > 0) {
                    $dop .= "`UserID` in (" . implode(",", $ids) . ")";
                } else {
                    echo json_encode(array(
                        "total" => 0,
                        "results" => array()
                    ));
                    return false;
                }
            } else {
                echo json_encode(array(
                    "total" => 0,
                    "results" => array()
                ));
                return false;
            }
        }
        $count = $this->Stm("SELECT COUNT(1) FROM `Orders`" . (! empty($dop) ? ' WHERE ' . $dop : ''));
        
        $start = isset($_POST['start']) ? (int) $_POST['start'] : 0;
        $limit = isset($_POST['limit']) ? (int) $_POST['limit'] : $this->LimitRecords;
        if ($count != false && ($count->execute()) != false && $count->rowCount() > 0) {
            $rows = $count->fetchColumn();
        }
        
        if ($rows > 0) {
            $sth = $this->Stm("SELECT *
				FROM `Orders`" . (! empty($dop) ? ' WHERE ' . $dop : '') . " Order by `Id` desc LIMIT {$start}, {$limit}");
            
            if ($sth != false && ($sth->execute()) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $info = "";
                    
                    $item = array(
                        "Id" => $row->Id,
                        'Status' => $row->Status,
                        "Date" => $row->Date,
                        "Address" => $this->win2utf($row->Address),
                        
                        "Payment" => $this->win2utf($row->Payment),
                        "Delivery" => $this->win2utf($row->Delivery),
                        "Sum" => $this->win2utf($row->Sum . " P")
                    );
                    
                    $item['total'] = $this->query("select count(1) from `Orders_Items` where `OrderID`='{$row->Id}'")->fetchColumn();
                    $item['email'] = $row->Email;
                    $item['fio'] = $this->win2utf($row->Name);
                    
                    $results[] = $item;
                }
            }
        }
        
        echo json_encode(array(
            "total" => $rows,
            "results" => $results
        ));
    }

    public function LoadImages()
    {
        $columns = $this->getColumns($this->ItemsTable);
        $orderBy = "ORDER BY ";
        if (isset($_POST['sort']) && in_array($_POST['sort'], $columns)) {
            $orderBy .= "`{$_POST['sort']}`";
            if (isset($_POST['dir']) && in_array($_POST['dir'], array(
                "ASC",
                "DESC"
            ))) {
                $orderBy .= ' ' . $_POST['dir'];
            }
        } else {
            $orderBy .= '`Sort` ASC';
        }
        $results = array();
        $rows = 0;
        $CategoryID = isset($_POST['CategoryID']) ? (int) $_POST['CategoryID'] : 0;
        $count = $this->Stm("SELECT COUNT(1) FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=?");
        $start = isset($_POST['start']) ? (int) $_POST['start'] : 0;
        $limit = isset($_POST['limit']) ? (int) $_POST['limit'] : $this->LimitRecords;
        if ($count != false && ($count->execute(array(
            (int) $_POST['ItemID']
        ))) != false && $count->rowCount() > 0) {
            $rows = $count->fetchColumn();
        }
        if ($rows > 0) {
            $sth = $this->Stm("SELECT `Id`,`Extension`, `Title`, `Sort`, `MainImage`, `ItemID` FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=? {$orderBy} LIMIT {$start}, {$limit}");
            if ($sth != false && ($sth->execute(array(
                (int) $_POST['ItemID']
            ))) != false && $sth->rowCount() > 0) {
                foreach ($sth->fetchAll() as $row) {
                    $results[] = array(
                        "Id" => $row->Id,
                        'ItemID' => $row->ItemID,
                        "Sort" => $row->Sort,
                        'MainImage' => $row->MainImage,
                        "Title" => $this->win2utf($row->Title),
                        "image" => 'o_' . $row->Id . '.' . $row->Extension
                    );
                }
            }
        }
        
        echo json_encode(array(
            "total" => $rows,
            "results" => $results
        ));
    }

    public function UpdateSortCategories($nodes = null)
    {
        if ($nodes == null) {
            $nodes = json_decode($_POST['nodes']);
        }
        if (is_array($nodes)) {
            $sth = $this->Stm("UPDATE `Catalog_Categories` SET `Sort`=? WHERE `Id`=?");
            if ($sth == false) {
                return false;
            }
            foreach ($nodes as $node) {
                
                $sth->execute(array(
                    $node->Sort,
                    $node->Id
                ));
                if ($node->childs) {
                    $this->UpdateSortCategories($node->childs);
                }
            }
        }
    }

    function cleanDir()
    {
        $files = glob("site/*");
        $c = count($files);
        if (count($files) > 0) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    public function SaveArchive()
    {
        $Id = 0;
        if (isset($_POST['Id'])) {
            $Id = (int) $_POST['Id'];
            unset($_POST['Id']);
        }
        
        $fields = array();
        $params = array();
        $table = '';
        $cols = array();
        
        $table = $this->ImagesTable;
        $cols = $this->getColumns($table);
        
        foreach ($_POST as $name => $value) {
            if (in_array($name, $cols)) {
                $fields[] = '`' . $name . '`=?';
                $params[] = $value;
            }
        }
        
        if (isset($_FILES['archive-path']) && isset($_FILES['archive-path']['name']) && ! empty($_FILES['archive-path']['name'])) {
            $ext = strtolower(pathinfo($_FILES['archive-path']['name'], PATHINFO_EXTENSION));
            if (in_array($ext, array(
                'zip'
            ))) {
                
                $zip = new ZipArchive();
                if ($zip->open("/{$_FILES ['archive-path'] ['tmp_name']}") === true) {
                    $zip->extractTo("site/");
                    $zip->close();
                } else {
                    echo json_encode(array(
                        "failure" => true,
                        'error' => 'no extract'
                    ));
                    return false;
                }
                
                foreach (glob("site/*") as $file) 

                {
                    $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
                    if (! in_array($ext, array(
                        'jpg',
                        'png',
                        'jpeg',
                        'gif'
                    ))) {
                        
                        continue;
                    }
                    
                    $sth = $this->Stm("INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)");
                    if ($sth != false && ($sth->execute(array(
                        $_POST['ItemID'],
                        $ext,
                        1
                    ))) != false) {
                        $Id = $this->getAdapter()->lastInsertId();
                        if ($Id) {
                            if (is_writeable($this->ImagesPath)) {
                                
                                if (copy($file, $this->ImagesPath . "/o_{$Id}.{$ext}")) {
                                    chmod($this->ImagesPath . "/o_{$Id}.{$ext}", 0666);
                                } else {
                                    $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                                    continue;
                                }
                            } else {
                                $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }
                $this->cleanDir();
            }
        }
        
        if ($Id == 0) {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
        
        if (isset($_POST['MainImage']) && isset($_POST['ItemID'])) {
            $this->exec("UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . (int) $_POST['ItemID'] . "'");
        }
        $fields[] = '`UpdatedDate`=CURRENT_TIMESTAMP';
        
        $sth = $this->Stm("UPDATE `{$table}` SET " . implode(", ", $fields) . " WHERE `Id`=?");
        $params[] = $Id;
        
        if ($sth != false && ($sth->execute($params)) != false) {
            echo json_encode(array(
                "success" => true
            ));
        } else {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
    }

    public function SaveFile()
    {
        $Id = 0;
        if (isset($_POST['Id'])) {
            $Id = (int) $_POST['Id'];
            unset($_POST['Id']);
        }
        
        $fields = array();
        $params = array();
        $table = '';
        $cols = array();
        
        $table = $this->ImagesTable;
        $cols = $this->getColumns($table);
        
        foreach ($_POST as $name => $value) {
            if (in_array($name, $cols)) {
                $fields[] = '`' . $name . '`=?';
                $params[] = $value;
            }
        }
        
        if (isset($_FILES['photo-path']) && isset($_FILES['photo-path']['name']) && ! empty($_FILES['photo-path']['name'])) {
            $ext = strtolower(pathinfo($_FILES['photo-path']['name'], PATHINFO_EXTENSION));
            if (in_array($ext, array(
                'jpg',
                'jpeg',
                'png',
                'gif'
            ))) {
                
                $sth = $this->Stm("INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)");
                if ($sth != false && ($sth->execute(array(
                    $_POST['ItemID'],
                    $ext,
                    1
                ))) != false) {
                    $Id = $this->getAdapter()->lastInsertId();
                    if ($Id) {
                        if (is_writeable($this->ImagesPath)) {
                            if (move_uploaded_file($_FILES['photo-path']['tmp_name'], $this->ImagesPath . "/o_{$Id}.{$ext}")) {
                                chmod($this->ImagesPath . "/o_{$Id}.{$ext}", 0666);
                            } else {
                                $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                                echo json_encode(array(
                                    "failure" => true,
                                    'error' => 'error move file'
                                ));
                                return false;
                            }
                        } else {
                            $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                            echo json_encode(array(
                                "failure" => true,
                                'error' => 'path not writable'
                            ));
                            return false;
                        }
                    } else {
                        echo json_encode(array(
                            "failure" => true,
                            'error' => 'no ID'
                        ));
                        return false;
                    }
                } else {
                    echo json_encode(array(
                        "failure" => true,
                        'error' => 'insert image error',
                        'pdo' => $this->getAdapter()->errorInfo()
                    ));
                    return false;
                }
            }
        }
        
        if ($Id == 0) {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
        
        if (isset($_POST['MainImage']) && isset($_POST['ItemID'])) {
            $this->exec("UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . (int) $_POST['ItemID'] . "'");
        }
        $fields[] = '`UpdatedDate`=CURRENT_TIMESTAMP';
        
        $sth = $this->Stm("UPDATE `{$table}` SET " . implode(", ", $fields) . " WHERE `Id`=?");
        $params[] = $Id;
        
        if ($sth != false && ($sth->execute($params)) != false) {
            echo json_encode(array(
                "success" => true
            ));
        } else {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
    }

    private function AddVirtualCategory($Id, $CategoryID)
    {
        if ($this->isChecked($Id, $CategoryID) == false) {
            $sth = $this->Stm("INSERT INTO `{$this->VirtualItemsTable}` (`ItemID`, `CategoryID`) VALUES (?, ?)");
            if ($sth != false && ($sth->execute(array(
                $Id,
                $CategoryID
            )))) {
                return true;
            }
        }
        return false;
    }

    private function getVirtualCategoryNames($Id)
    {
        $Names = array();
        
        $sth = $this->Stm("SELECT {$this->CategoriesTable}.`Title` FROM `{$this->CategoriesTable}`, `{$this->VirtualItemsTable}` WHERE `{$this->VirtualItemsTable}`.`ItemID`=? AND `{$this->CategoriesTable}`.`Id`=`{$this->VirtualItemsTable}`.`CategoryID` GROUP BY `{$this->VirtualItemsTable}`.`CategoryID`");
        if ($sth != false && ($sth->execute(array(
            $Id
        )))) {
            foreach ($sth->fetchAll() as $row) {
                $Names[] = $row->Title;
            }
        }
        return $Names;
    }

    private function DeleteVirtualCategories($Id)
    {
        $sth = $this->Stm("DELETE FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=?");
        
        if ($sth != false && ($sth->execute(array(
            $Id
        )))) {
            return true;
        }
        return false;
    }

    private function DeleteVirtualCategory($Id, $CategoryID)
    {
        $sth = $this->Stm("DELETE FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=? AND `CategoryID`=?");
        if ($sth != false && ($sth->execute(array(
            $Id,
            $CategoryID
        )))) {
            return true;
        }
        return false;
    }

    public function SaveData()
    {
        $to = isset($_POST['to']) ? $_POST['to'] : false;
        if ($to == false) {
            echo json_encode(array(
                "failure" => true
            ));
            return false;
        }
        $Id = 0;
        if (isset($_POST['Id'])) {
            $Id = (int) $_POST['Id'];
            unset($_POST['Id']);
        }
        $cols2 = array();
        $fields = array();
        $params = array();
        $table = '';
        $cols = array();
        if ($to == "Category") {
            $table = $this->CategoriesTable;
            $cols = $this->getColumns($table);
        } elseif ($to == "Item") {
            $table = $this->ItemsTable;
            $cols = $this->getColumns($table);
        }
        if (isset($_POST['uffields'])) {
            foreach ($cols as $col) {
                if (preg_match("#uf_([0-9]+?)_([0-9]+)#", $col) && ! isset($_POST[$col])) {
                    $_POST[$col] = 0;
                }
            }
        }
        foreach ($_POST as $name => $value) {
            
            if (in_array($name, $cols)) {
                
                if ($Id == 0) {
                    $fields[] = '`' . $name . '`';
                    $cols2[] = "?";
                } else {
                    $fields[] = '`' . $name . '`=?';
                }
                if ($to != "Category") {
                    $params[] = $this->utf2win($value);
                } else {
                    $params[] = $value;
                }
            }
        }
        
        if ($Id == 0) {
            $cols2[] = 'CURRENT_TIMESTAMP';
            $fields[] = '`UpdatedDate`';
        } else {
            $fields[] = '`UpdatedDate`=CURRENT_TIMESTAMP';
        }
        if ($Id == 0) {
            $sth = $this->Stm("INSERT INTO `{$table}` (" . implode(", ", $fields) . ") VALUES (" . implode(", ", $cols2) . ")");
        } else {
            $sth = $this->Stm("UPDATE `{$table}` SET " . implode(", ", $fields) . " WHERE `Id`=?");
            $params[] = $Id;
        }
        if ($sth != false && ($sth->execute($params)) != false) {
            if ($Id == 0) {
                $Id = $this->getAdapter()->lastInsertId();
            }
            if ($to == "Item") {
                if (isset($_POST['VirtualCategoriesId']) && ! empty($_POST['VirtualCategoriesId'])) {
                    $this->DeleteVirtualCategories($Id);
                    $checked = @json_decode($_POST['VirtualCategoriesId']);
                    if ($checked && is_array($checked) && count($checked) > 0) {
                        foreach ($checked as $CategoryID) {
                            $this->AddVirtualCategory($Id, $CategoryID);
                        }
                    }
                } elseif (isset($_POST['VirtualCategoriesName']) && empty($_POST['VirtualCategoriesName'])) {
                    $this->DeleteVirtualCategories($Id);
                }
            }
            if ($to == 'Category') {
                if (isset($_FILES['photo-path']) && isset($_FILES['photo-path']['name']) && ! empty($_FILES['photo-path']['name']) && ($ext = strtolower(pathinfo($_FILES['photo-path']['name'], PATHINFO_EXTENSION))) != false && in_array($ext, array(
                    "jpeg",
                    "jpg",
                    "png",
                    "gif"
                ))) {
                    if (move_uploaded_file($_FILES['photo-path']['tmp_name'], "files/catalog/categoryIcon_{$Id}.{$ext}")) {
                        $this->exec("update `Catalog_Categories` SET `Icon`='categoryIcon_{$Id}.{$ext}' WHERE `Id`='{$Id}'");
                    }
                }
                $sql = $this->query("select  `Title`,`parentId` from `{$table}` where `Id`='{$Id}' limit 1");
                if ($sql != false && $sql->rowCount() > 0) {
                    $row = $sql->fetch(PDO::FETCH_ASSOC);
                    if (isset($_POST['url']) && ! empty($_POST['url'])) {
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id,
                            'parentId' => $row['parentId'],
                            'custom' => $_POST['url']
                        ));
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id
                        ));
                        
                        if ($this->get('Catalog_TypeURL') == 2) {
                            rewriteUrls::saveUrl(array(
                                'module' => 'catalog',
                                'catid' => $Id,
                                'parentId' => $row['parentId']
                            ));
                            // 'name' => $this->translit ( $row ['Title'] )
                        } else {
                            rewriteUrls::saveUrl(array(
                                'module' => 'catalog',
                                'catid' => $Id,
                                'parentId' => $row['parentId'],
                                'name' => ''
                            ));
                        }
                    } elseif (isset($_POST['url']) && empty($_POST['url'])) {
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id,
                            'parentId' => $row['parentId'],
                            'custom' => ''
                        ));
                        
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id
                        ));
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id,
                            'parentId' => $row['parentId']
                        ));
                        // 'name' => $this->translit ( $row ['Title'] )
                    } else {
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id
                        ));
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catid' => $Id,
                            'parentId' => $row['parentId']
                        ));
                        // 'name' => $this->translit ( $row ['Title'] )
                    }
                }
                $url = rewriteUrls::getSingleUrl(array(
                    'module' => 'catalog',
                    'catid' => $Id
                ));
                $url2 = rewriteUrls::getSingleCustomUrl(array(
                    'module' => 'catalog',
                    'catid' => $Id
                ));
                if (empty($url) && empty($url2)) {
                    $this->exec("DELETE FROM `rewriteUrls` where `module`='catalog' and `objId`='{$Id}' and `isCat`='1' and `parentId`='" . (int) $row['parentId'] . "'");
                }
            } else {
                $sql = $this->query("select  `Title`,`CategoryID` from `{$table}` where `Id`='{$Id}' limit 1");
                if ($sql != false && $sql->rowCount() > 0) {
                    $row = $sql->fetch(PDO::FETCH_ASSOC);
                    if (isset($_POST['url']) && ! empty($_POST['url'])) {
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id,
                            'parentId' => $row['CategoryID'],
                            'custom' => $_POST['url']
                        ));
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id
                        ));
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id,
                            'parentId' => $row['CategoryID']
                        ));
                        // 'name' => $this->translit ( $row ['Title'] )
                    } elseif (isset($_POST['url']) && empty($_POST['url'])) {
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id,
                            'parentId' => $row['CategoryID'],
                            'custom' => ''
                        ));
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id
                        ));
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id,
                            'parentId' => $row['CategoryID']
                        ));
                        // 'name' => $this->translit ( $row ['Title'] )
                    } else {
                        $url = rewriteUrls::getSingleUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id
                        ));
                        
                        rewriteUrls::saveUrl(array(
                            'module' => 'catalog',
                            'catalog' => $Id,
                            'parentId' => $row['CategoryID']
                        ));
                        // 'name' => $this->translit ( $row ['Title'] )
                    }
                }
                $url = rewriteUrls::getSingleUrl(array(
                    'module' => 'catalog',
                    'catalog' => $Id
                ));
                $url2 = rewriteUrls::getSingleCustomUrl(array(
                    'module' => 'catalog',
                    'catalog' => $Id
                ));
                if (empty($url) && empty($url2)) {
                    $this->exec("DELETE FROM `rewriteUrls` where `module`='catalog' and `objId`='{$Id}' and `isCat`='0' and `parentId`='" . (int) $row['CategoryID'] . "'");
                }
            }
            echo json_encode(array(
                "success" => true
            ));
        } else {
            
            echo json_encode(array(
                "failure" => true
            ));
        }
        return false;
    }

    function CopyRecord()
    {
        $id = 0;
        if (isset($_POST['Id'])) {
            $id = (int) $_POST['Id'];
        }
        if ($id == 0) {
            return false;
        }
        $arr = array(
            "`TypeUF`",
            "`TitlePage`",
            "`Title`",
            "`Tags`",
            "`Sort`",
            "`Price`",
            "`Notice`",
            "`Noindex`",
            "`KeysPage`",
            "`H1`",
            "`Description`",
            "`DescPage`",
            "`CategoryID`",
            "`Brand`",
            "`Available`",
            "`Article`"
        );
        $sth = $this->prepare("INSERT INTO `Catalog_Items` (" . implode(',', $arr) . ") 
  SELECT " . implode(',', $arr) . "  FROM `Catalog_Items` 
    WHERE `Id` = ?");
        $sth->execute(array(
            $id
        ));
        if ($sth->rowCount() > 0) {
            $uid = $this->lastId();
            
            $sth1 = $this->prepare("
  SELECT  * FROM `Catalog_Items_Files`   WHERE `ItemID` = ?");
            $sth1->execute(array(
                $id
            ));
            if ($sth1->rowCount() > 0) {
                foreach ($sth1->fetchAll() as $row) {
                    $st = $this->query("INSERT INTO `Catalog_Items_Files` (`ItemID`, `Extension`,`isImage`) values ('{$uid}','{$row->Extension}', '1')");
                    
                    $imId = $this->lastId();
                    
                    copy("files/catalog/o_{$row->Id}.{$row->Extension}", "files/catalog/o_{$imId}.{$row->Extension}");
                }
            }
        }
    }

    public function multiplyUpload()
    {
        $Id = 0;
        if (isset($_POST['Id'])) {
            $Id = (int) $_POST['Id'];
            unset($_POST['Id']);
        }
        
        $fields = array();
        $params = array();
        $table = '';
        $cols = array();
        
        $table = $this->ImagesTable;
        $cols = $this->getColumns($table);
        
        foreach ($_POST as $name => $value) {
            if (in_array($name, $cols)) {
                $fields[] = '`' . $name . '`=?';
                $params[] = $value;
            }
        }
        
        if (isset($_FILES['Filedata']) && isset($_FILES['Filedata']['name']) && ! empty($_FILES['Filedata']['name'])) {
            $ext = strtolower(pathinfo($_FILES['Filedata']['name'], PATHINFO_EXTENSION));
            if (in_array($ext, array(
                'jpg',
                'jpeg',
                'png',
                'gif'
            ))) {
                
                $sth = $this->Stm("INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)");
                if ($sth != false && ($sth->execute(array(
                    $_POST['ItemID'],
                    $ext,
                    1
                ))) != false) {
                    $Id = $this->getAdapter()->lastInsertId();
                    if ($Id) {
                        if (is_writeable($this->ImagesPath)) {
                            if (move_uploaded_file($_FILES['Filedata']['tmp_name'], $this->ImagesPath . "/o_{$Id}.{$ext}")) {
                                chmod($this->ImagesPath . "/o_{$Id}.{$ext}", 0666);
                            } else {
                                $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                                echo json_encode(array(
                                    "failure" => true,
                                    'error' => 'error move file'
                                ));
                                return false;
                            }
                        } else {
                            $this->exec("DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1");
                            echo json_encode(array(
                                "failure" => true,
                                'error' => 'path not writable'
                            ));
                            return false;
                        }
                    } else {
                        echo json_encode(array(
                            "failure" => true,
                            'error' => 'no ID'
                        ));
                        return false;
                    }
                } else {
                    echo json_encode(array(
                        "failure" => true,
                        'error' => 'insert image error',
                        'pdo' => $this->getAdapter()->errorInfo()
                    ));
                    return false;
                }
            }
        }
        
        if ($Id == 0) {
            echo json_encode(array(
                "failure" => true,
                'id not found' => true
            ));
        }
        
        if (isset($_POST['MainImage']) && isset($_POST['ItemID'])) {
            $this->exec("UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . (int) $_POST['ItemID'] . "'");
        }
        $fields[] = '`UpdatedDate`=CURRENT_TIMESTAMP';
        
        $sth = $this->Stm("UPDATE `{$table}` SET " . implode(", ", $fields) . " WHERE `Id`=?");
        $params[] = $Id;
        
        if ($sth != false && ($sth->execute($params)) != false) {
            echo json_encode(array(
                "success" => true
            ));
        } else {
            echo json_encode(array(
                "failure" => true,
                'pdo_error' => true
            ));
        }
    }
}
