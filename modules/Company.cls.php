<?php
class Redactor_Module_Company extends Redactor_Action {
	var $over = '';
	function __construct() {
	}
	function getCompaniesByUserID($UserID = 0, $fields = array('Id', 'Title')) {
		if (! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			$fields = array (
					"*" 
			);
		} else {
			$fields = array_map ( 'self::changeFields', $fields );
		}
		$sth = $this->prepare ( "SELECT " . implode ( ", ", $fields ) . " FROM `Company` WHERE `UserID`=?" );
		$sth->execute ( array (
				$UserID 
		) );
		if ($sth->rowCount () > 0) {
			$companies = array ();
			foreach ( $sth->fetchAll () as $row ) {
				$companies [] = $row;
			}
			return $companies;
		}
		return false;
	}
	function updateCompany($info, $Id, $UserID) {
		$cols = $this->getColumns ();
		$params = array ();
		$fields = array ();
		foreach ( $info as $name => $value ) {
			if ($name == 'Id') {
				continue;
			}
			if ($name == 'UserID') {
				continue;
			}
			if (in_array ( $name, $cols )) {
				$fields [] = "`{$name}`=?";
				$params [] = $value;
			}
		}
		$params [] = $Id;
		$params [] = $UserID;
		$sth = $this->prepare ( "UPDATE `Company` SET " . implode ( ", ", $fields ) . " WHERE `Id`=? AND `UserID`=?" );
		$sth->execute ( $params );
		
		return true;
	}
	function getColumns() {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `Company`" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
	function addCompany($info, $UserID) {
		$cols = $this->getColumns ();
		$params = array ();
		$fields = array ();
		foreach ( $info as $name => $value ) {
			if ($name == 'Id') {
				continue;
			}
			if ($name == 'UserID') {
				continue;
			}
			if (in_array ( $name, $cols )) {
				
				$fields ["`{$name}`"] = "?";
				$params [] = $value;
			}
		}
		
		$fields ["`UserID`"] = "?";
		$params [] = $UserID;
		$sth = $this->prepare ( "INSERT INTO `Company` (" . implode ( ", ", array_keys ( $fields ) ) . ", `UpdatedDate`) VALUES (" . implode ( ",", array_values ( $fields ) ) . ", CURRENT_TIMESTAMP)" );
		$sth->execute ( $params );
		
		return true;
	}
	function getCompanyById($Id, $fields = array(), $withUF = true, $UserID = 0) {
		if (! is_array ( $fields ) or is_array ( $fields ) && count ( $fields ) == 0) {
			$fields = array (
					"*" 
			);
		} else {
			if ($withUF) {
				$cols = $this->getUFColumns ();
				$fields = array_merge ( $fields, $cols );
			}
			$fields = array_map ( 'self::changeFields', $fields );
		}
		$params = array (
				$Id 
		);
		if ($UserID != 0) {
			$params [] = $UserID;
		}
		$sth = $this->prepare ( "SELECT " . implode ( ", ", $fields ) . " FROM `Company` WHERE `Id`=? " . ($UserID != 0 ? "AND `UserID`=?" : '') . " LIMIT 1" );
		$sth->execute ( $params );
		if ($sth->rowCount () > 0) {
			$row = $sth->fetch ();
			if ($withUF) {
				$uf = array ();
				foreach ( $row as $name => $value ) {
					if (substr ( $name, 0, 3 ) == 'uf_') {
						if (preg_match ( "#uf_([0-9]+?)_([0-9]+)#", $name, $match )) {
							$UFID = ( int ) $match [1];
							if (! isset ( $uf [$UFID] )) {
								$uf [$UFID] = array ();
							}
							$uf [$UFID] [] = array (
									$value,
									( int ) $match [2] 
							);
							
							unset ( $row->$name );
						}
						if (preg_match ( "#uf_([0-9]+)#", $name, $match )) {
							$UFID = ( int ) $match [1];
							$uf [$UFID] = $value;
							
							unset ( $row->$name );
						}
					}
				}
				$sth = $this->prepare ( "SELECT `Title`, `Group`, `Id` FROM `UserFields` WHERE `Id` IN (" . implode ( ", ", array_keys ( $uf ) ) . ") ORDER BY `Group` desc, `Sort`" );
				$sth->execute ();
				$new = array ();
				if ($sth->rowCount () > 0) {
					$i = 0;
					foreach ( $sth->fetchAll () as $o ) {
						if (isset ( $uf [$o->Id] ) && is_array ( $uf [$o->Id] )) {
							foreach ( $uf [$o->Id] as $u ) {
								$row->{'uf_' . $o->Id . '_' . $u [1]} = ( object ) array (
										'Title' => $o->Title,
										'Value' => $u [0],
										'Group' => $o->Group 
								);
							}
						} elseif (isset ( $uf [$o->Id] )) {
							$row->{'uf_' . $o->Id} = ( object ) array (
									'Title' => $o->Title,
									'Value' => $uf [$o->Id],
									'Group' => $o->Group 
							);
						}
					}
				}
			}
			return $row;
		}
		return false;
	}
	function getUFValue($Id) {
		if ($Id == 0) {
			return '';
		}
		$sth = $this->prepare ( "SELECT `Value` FROM `UserFields_Values` WHERE `Id`=? LIMIT 1" );
		$sth->execute ( array (
				$Id 
		) );
		if ($sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return '';
	}
	function getUFTitle($Id) {
		$sth = $this->prepare ( "SELECT `Title` FROM `UserFields` WHERE `Id`=? LIMIT 1" );
		$sth->execute ( array (
				$Id 
		) );
		if ($sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return '';
	}
	function getUFColumns() {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `Company` WHERE `Field` LIKE 'uf_%'" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
}
class Company_admin extends Redactor_Admin {
	var $ItemsTable = "Company";
	var $CategogetCompanyIdriesTable = "Company_Groups";
	var $ImagesTable = "Company_Files";
	var $LimitRecords = 25;
	var $ImagesPath = "files/company";
	function __construct() {
	}
	public function ChangeCategoryIDItems() {
		$items = json_decode ( $_POST ['items'] );
		$new = array ();
		$CategoryID = ( int ) $_POST ['CategoryID'];
		if (is_array ( $items ) && count ( $items ) > 0) {
			$new = array ();
			foreach ( $items as $id ) {
				$new [] = ( int ) $id;
			}
			unset ( $items );
			$this->exec ( "UPDATE `{$this->ItemsTable}` SET `CategoryID`='{$CategoryID}' WHERE `Id` IN (" . implode ( ", ", $new ) . ")" );
		}
	}
	private function getParentsId($Id, &$items) {
		$items [] = $Id;
		$sth = $this->Stm ( "SELECT `parentId` from `{$this->CategoriesTable}` WHERE `Id`=? limit 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$parentId = $sth->fetchColumn ();
			if ($parentId != 0) {
				$this->getParentsId ( $parentId, $items );
			}
		}
	}
	private function getChildrensId($Id, &$items) {
		$items [] = $Id;
		$sth = $this->Stm ( "SELECT `Id` from  `{$this->CategoriesTable}` WHERE `parentId`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				
				$this->getParentsId ( $row->Id, $items );
			}
		}
	}
	private function isMainCategoryItem($Id, $Category) {
		$sth = $this->Stm ( "SELECT COUNT(1) FROM `{$this->ItemsTable}` WHERE `Id`=? AND `CategoryID`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id,
				$Category 
		) )) != false && $sth->fetchColumn () > 0) {
			return true;
		}
		return false;
	}
	private function isChecked($Id, $Category) {
		$sth = $this->Stm ( "SELECT COUNT(1) FROM `{$this->VirtualItemsTable}` WHERE `ItemID`=? AND `CategoryID`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id,
				$Category 
		) )) != false && $sth->fetchColumn () > 0) {
			return true;
		}
		return false;
	}
	public function LoadCategories($parentId = 0, $notIn = array()) {
		if ($parentId == 0 && ! isset ( $_POST ['checked'] ) && isset ( $_POST ['Id'] ) && $_POST ['Id'] != 0) {
			
			$this->getChildrensId ( ( int ) $_POST ['Id'], $notIn );
		}
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		$checkedTest = isset ( $_POST ['checked'] ) ? true : false;
		if (count ( $notIn ) > 0) {
			$notIn2 = " AND `Id` NOT IN (" . implode ( ", ", $notIn ) . ")";
		} else {
			$notIn2 = '';
		}
		$Data = array ();
		
		$sth = $this->Stm ( "Select `Id`, `Title` from `{$this->CategoriesTable}` where `parentId`=? {$notIn2} ORDER BY `Sort` asc" );
		
		if ($sth != false && ($sth->execute ( array (
				$parentId 
		) )) != false && $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll () as $row ) {
				
				$item = array (
						"Title" => $this->win2utf ( $row->Title ),
						"Id" => $row->Id,
						'text' => $this->win2utf ( $row->Title ),
						"id" => $row->Id,
						
						'draggable' => true,
						"leaf" => false,
						"children" => $this->LoadCategories ( $row->Id, $notIn ) 
				);
				
				if ($checkedTest && $this->isMainCategoryItem ( $Id, $row->Id ) == false) {
					$item ['checked'] = $this->isChecked ( $Id, $row->Id );
				}
				
				$Data [] = $item;
			}
		}
		
		if ($parentId == 0) {
			echo json_encode ( $Data );
			return false;
		}
		return $Data;
	}
	public function DeleteCategory($Id = null) {
		if ($Id == null) {
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		}
		$sth = $this->Stm ( "SELECT `Id` FROM `{$this->CategoriesTable}` WHERE `parentId`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$this->DeleteRecords ( $row->Id );
				$this->DeleteCategory ( $row->Id );
			}
		}
		$sth = $this->Stm ( "DELETE FROM `{$this->CategoriesTable}` WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false) {
		}
	}
	private function getCategoryName($Id) {
		if ($Id == 0) {
			return '������';
		}
		$sth = $this->Stm ( "SELECT `Title` FROM `{$this->CategoriesTable}` WHERE `Id`=? LIMIT 1" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			return $sth->fetchColumn ();
		}
		return '������';
	}
	public function DeleteImage($Id) {
		if ($Id == null) {
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		}
		$sth = $this->Stm ( "SELECT * FROM `{$this->ImagesTable}` WHERE `Id`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false && $sth->rowCount () > 0) {
			$row = $sth->fetch ();
			$this->exec ( "DELETE FROM `{$this->ImagesTable}` WHERE `Id`='{$row->Id}'" );
			$file = $this->ImagesPath . "/o_{$row->Id}.{$row->Extension}";
			if (file_exists ( $file )) {
				unlink ( $file );
			}
		}
	}
	public function DeleteRecord($Id = null) {
		if ($Id == null) {
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		}
		
		$this->DeleteImages ( $Id );
		$this->exec ( "DELETE FROM `{$this->ItemsTable}` WHERE `Id`='{$Id}'" );
	}
	private function DeleteImages($Id) {
		if ($Id != 0) {
			$sth = $this->Stm ( "SELECT `Id` FROM `{$this->ImagesTable}` WHERE `ItemID`=?" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$this->DeleteImage ( $row->Id );
				}
			}
		}
	}
	public function DeleteRecords() {
		$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if ($Id != 0) {
			$sth = $this->Stm ( "SELECT `Id` FROM `{$this->ItemsTable}` WHERE `CategoryID`=?" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$this->DeleteRecord ( $row->Id );
				}
			}
		}
	}
	public function LoadRecord() {
		$to = isset ( $_POST ['to'] ) ? $_POST ['to'] : '';
		if ($to == 'Category' || $to == 'Record' || $to == "File") {
			$Data = array ();
			$Id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
			
			if ($to == 'Category') {
				$table = $this->CategoriesTable;
				$fieldCategory = 'parentId';
			} elseif ($to == 'File') {
				$table = $this->ImagesTable;
			} else {
				$table = $this->ItemsTable;
				$fieldCategory = 'CategoryID';
				if ($Id == 0) {
					$UserID = isset ( $_POST ['UserID'] ) ? ( int ) $_POST ['UserID'] : 0;
					if ($UserID != 0) {
						$f = ', `UserID`';
						$v = ", '{$UserID}'";
					} else {
						$f = '';
						$v = '';
					}
					$sth = $this->query ( "INSERT INTO `{$table}` (`Id`,`UpdatedDate`, `Active`{$f}) VALUES (NULL, CURRENT_TIMESTAMP,1{$v})" );
					if ($sth != false) {
						$Id = $this->getAdapter ()->lastInsertId ();
					} else {
						echo json_encode ( array (
								'failure' => true,
								'data' => array () 
						) );
						return false;
					}
				}
			}
			$sth = $this->Stm ( "SELECT * FROM `{$table}` WHERE `Id`=? LIMIT 1" );
			if ($sth != false && ($sth->execute ( array (
					$Id 
			) )) != false && $sth->rowCount () > 0) {
				$row = $sth->fetch ( PDO::FETCH_ASSOC );
				if (isset ( $_POST ['TypeUF'] )) {
					$row ['TypeUF'] = ( int ) $_POST ['TypeUF'];
				}
				foreach ( $row as $name => $value ) {
					if (isset ( $_POST ['TypeUF'] )) {
						if (preg_match ( "/^uf_/", $name )) {
							$Data [$name] = $this->win2utf ( $value );
						}
					} else {
						$Data [$name] = $this->win2utf ( $value );
					}
				}
				
				if ($to == 'Category') {
					$Data ['url'] = $this->win2utf ( rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'Company',
							'catid' => $row ['Id'] 
					) ) );
				} elseif ($to == 'Record') {
					
					$Data ['UFData'] = $this->getUFJS ( 'Company', $row ['TypeUF'], 0, $row );
					if (! isset ( $_POST ['TypeUF'] )) {
						$Data ['UFGroups'] = array (
								"results" => $this->getUFGroups () 
						);
					}
				}
				$Data ["UpdatedDate2"] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $row ['UpdatedDate'] ) );
				$Data ["CreatedDate2"] = $this->win2utf ( $this->formatDate ( "d micromonth Y H:i:s", $row ['CreatedDate'] ) );
			}
			echo json_encode ( array (
					"success" => true,
					"data" => $Data 
			) );
		} else {
			echo json_encode ( array (
					'failure' => true,
					'data' => array () 
			) );
		}
	}
	function getColumns($table) {
		$cols = array ();
		$sth = $this->Query ( "SHOW COLUMNS FROM `{$table}`" );
		if ($sth != false && $sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$cols [] = $row->Field;
			}
		}
		
		return $cols;
	}
	private function countFiles($Id) {
		$sth = $this->Stm ( "SELECT COUNT(1) FROM `Company_Items_Files` WHERE `ItemID`=?" );
		if ($sth != false && ($sth->execute ( array (
				$Id 
		) )) != false) {
			return $sth->fetchColumn ();
		}
		return 0;
	}
	public function LoadRecords() {
		$columns = $this->getColumns ( $this->ItemsTable );
		$orderBy = "ORDER BY ";
		if (isset ( $_POST ['sort'] ) && in_array ( $_POST ['sort'], $columns )) {
			$orderBy .= "`{$this->ItemsTable}`.`{$_POST['sort']}`";
			if (isset ( $_POST ['dir'] ) && in_array ( $_POST ['dir'], array (
					"ASC",
					"DESC" 
			) )) {
				$orderBy .= ' ' . $_POST ['dir'];
			}
		} else {
			$orderBy .= "`{$this->ItemsTable}`.`Sort` ASC";
		}
		
		$dop = array();
		$params = array ();
		if (isset ( $_POST ['search'] )) {
			$s = $this->utf2win ( $_POST ['search'] );
			$sth = $this->prepare ( "select `Id` from `Users` Where `Email` LIKE ?" );
			$sth->execute ( array (
					"%{$s}%" 
			) );
			if ($sth->rowCount () > 0) {
				$ids = array ();
				foreach ( $sth->fetchAll () as $row ) {
					$ids [] = $row->Id;
				}
				if (count ( $ids ) > 0) {
					$dop[]= "`UserID` in (:UserIds)";
					$params['UserIds'] = implode ( ",", $ids );
				}
			}
			
			$sth = $this->query("SHOW COLUMNS FROM `Company` WHERE `Type` LIKE '%char%' or `Type` LIKE 'text'");
			if ($sth && $sth->rowCount()>0){
				$f=  array();
				$params[':QUERY'] = "%{$s}%";
				foreach ($sth->fetchAll() as $row){
					
					$dop[]="`{$row->Field}` COLLATE utf8_general_ci LIKE :QUERY";
				}
				
			}
			
			
			if (count($dop)==0){
				echo json_encode ( array (
						"total" => 0,
						"results" => array ()
				) );
				return false;
			}
		}
		if (count($dop)>0){
		   $dop = "(".implode(" OR ", $dop).")";
		}
		else {
			$dop = '';
		}
		
		$results = array ();
		$rows = 0;
		
		
		$UserID = isset ( $_POST ['UserID'] ) ? ( int ) $_POST ['UserID'] : 0;
		if ($UserID != 0) {
			$params [] = $UserID;
		}
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->ItemsTable}` " . ($UserID != 0 ? "WHERE `{$this->ItemsTable}`.`UserID`=? " : '') . "".(!empty($dop)?' WHERE '.$dop:'')."
		" );
		
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : $this->LimitRecords;
		if ($count != false && ($count->execute ( $params )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `{$this->ItemsTable}`.`Id`, `{$this->ItemsTable}`.`UserID`, `{$this->ItemsTable}`.`Title`, `{$this->ItemsTable}`.`Sort`, `{$this->ItemsTable}`.`Active`, `{$this->ItemsTable}`.`UpdatedDate`, `{$this->ItemsTable}`.`CreatedDate`
			 FROM `{$this->ItemsTable}` " . ($UserID != 0 ? "WHERE `{$this->ItemsTable}`.`UserID`=? " : '') . "".(!empty($dop)?' WHERE '.$dop:'')."  {$orderBy} LIMIT {$start}, {$limit}" );
			
			if ($sth != false && ($sth->execute ( $params )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$user = $this->getModule ( 'users' )->getUserById ( $row->UserID );
					$item = array (
							"Id" => $row->Id,
							
							"Sort" => $row->Sort,
							
							"Active" => $row->Active,
							"UpdatedDate" => $this->win2utf ( $this->formatDate ( "d micromonth Y", $row->UpdatedDate ) ),
							"CreatedDate" => $this->win2utf ( $this->formatDate ( "d micromonth Y", $row->CreatedDate ) ),
							"Title" => $this->win2utf ( $row->Title ) 
					);
					if ($user) {
						$item ['Email'] = $user->Email;
					}
					$results [] = $item;
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	public function changePassword() {
		$password = $this->utf2win ( $_POST ['Password'] );
		$Id = ( int ) $_POST ['Id'];
		$sth = $this->prepare ( "UPDATE `{$this->ItemsTable}` SET `Password`=MD5(?), `UpdatedDate`=CURRENT_TIMESTAMP WHERE `Id`=?" );
		if ($sth != false && $sth->execute ( array (
				$password,
				$Id 
		) )) {
			echo json_encode ( array (
					'success' => true 
			) );
			return true;
		}
		echo json_encode ( array (
				'failure' => true 
		) );
		return true;
	}
	public function LoadImages() {
		$columns = $this->getColumns ( $this->ItemsTable );
		$orderBy = "ORDER BY ";
		if (isset ( $_POST ['sort'] ) && in_array ( $_POST ['sort'], $columns )) {
			$orderBy .= "`{$_POST['sort']}`";
			if (isset ( $_POST ['dir'] ) && in_array ( $_POST ['dir'], array (
					"ASC",
					"DESC" 
			) )) {
				$orderBy .= ' ' . $_POST ['dir'];
			}
		} else {
			$orderBy .= '`Sort` ASC';
		}
		$results = array ();
		$rows = 0;
		$CategoryID = isset ( $_POST ['CategoryID'] ) ? ( int ) $_POST ['CategoryID'] : 0;
		$count = $this->Stm ( "SELECT COUNT(1) FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=?" );
		$start = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$limit = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : $this->LimitRecords;
		if ($count != false && ($count->execute ( array (
				( int ) $_POST ['ItemID'] 
		) )) != false && $count->rowCount () > 0) {
			$rows = $count->fetchColumn ();
		}
		if ($rows > 0) {
			$sth = $this->Stm ( "SELECT `Id`,`Extension`, `Title`, `Sort`, `MainImage`, `ItemID` FROM `{$this->ImagesTable}` WHERE `isImage`=1 AND `ItemID`=? {$orderBy} LIMIT {$start}, {$limit}" );
			if ($sth != false && ($sth->execute ( array (
					( int ) $_POST ['ItemID'] 
			) )) != false && $sth->rowCount () > 0) {
				foreach ( $sth->fetchAll () as $row ) {
					$results [] = array (
							"Id" => $row->Id,
							'ItemID' => $row->ItemID,
							"Sort" => $row->Sort,
							'MainImage' => $row->MainImage,
							"Title" => $this->win2utf ( $row->Title ),
							"image" => 'o_' . $row->Id . '.' . $row->Extension 
					);
				}
			}
		}
		
		echo json_encode ( array (
				"total" => $rows,
				"results" => $results 
		) );
	}
	public function UpdateSortCategories($nodes = null) {
		if ($nodes == null) {
			$nodes = json_decode ( $_POST ['nodes'] );
		}
		if (is_array ( $nodes )) {
			$sth = $this->Stm ( "UPDATE `Company_Categories` SET `Sort`=? WHERE `Id`=?" );
			if ($sth == false) {
				return false;
			}
			foreach ( $nodes as $node ) {
				
				$sth->execute ( array (
						$node->Sort,
						$node->Id 
				) );
				if ($node->childs) {
					$this->UpdateSortCategories ( $node->childs );
				}
			}
		}
	}
	public function SaveFile() {
		$Id = 0;
		if (isset ( $_POST ['Id'] )) {
			$Id = ( int ) $_POST ['Id'];
			unset ( $_POST ['Id'] );
		}
		
		$fields = array ();
		$params = array ();
		$table = '';
		$cols = array ();
		
		$table = $this->ImagesTable;
		$cols = $this->getColumns ( $table );
		
		foreach ( $_POST as $name => $value ) {
			if (in_array ( $name, $cols )) {
				$fields [] = '`' . $name . '`=?';
				$params [] = $value;
			}
		}
		
		if (isset ( $_FILES ['photo-path'] ) && isset ( $_FILES ['photo-path'] ['name'] ) && ! empty ( $_FILES ['photo-path'] ['name'] )) {
			$ext = strtolower ( pathinfo ( $_FILES ['photo-path'] ['name'], PATHINFO_EXTENSION ) );
			if (in_array ( $ext, array (
					'jpg',
					'jpeg',
					'png',
					'gif' 
			) )) {
				
				$sth = $this->Stm ( "INSERT INTO `{$table}` (`ItemID`, `Extension`, `isImage`) VALUES (?,?,?)" );
				if ($sth != false && ($sth->execute ( array (
						$_POST ['ItemID'],
						$ext,
						1 
				) )) != false) {
					$Id = $this->getAdapter ()->lastInsertId ();
					if ($Id) {
						if (is_writeable ( $this->ImagesPath )) {
							if (move_uploaded_file ( $_FILES ['photo-path'] ['tmp_name'], $this->ImagesPath . "/o_{$Id}.{$ext}" )) {
								chmod ( $this->ImagesPath . "/o_{$Id}.{$ext}", 0666 );
							} else {
								$this->exec ( "DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1" );
								echo json_encode ( array (
										"failure" => true,
										'error' => 'error move file' 
								) );
								return false;
							}
						} else {
							$this->exec ( "DELETE FROM {$this->ImagesTable} WHERE `Id`='{$Id}' LIMIT 1" );
							echo json_encode ( array (
									"failure" => true,
									'error' => 'path not writable' 
							) );
							return false;
						}
					} else {
						echo json_encode ( array (
								"failure" => true,
								'error' => 'no ID' 
						) );
						return false;
					}
				} else {
					echo json_encode ( array (
							"failure" => true,
							'error' => 'insert image error',
							'pdo' => $this->getAdapter ()->errorInfo () 
					) );
					return false;
				}
			}
		}
		
		if ($Id == 0) {
			echo json_encode ( array (
					"failure" => true,
					'pdo_error' => true 
			) );
		}
		
		if (isset ( $_POST ['MainImage'] ) && isset ( $_POST ['ItemID'] )) {
			$this->exec ( "UPDATE `{$table}` set `MainImage`='0' where `ItemID`='" . ( int ) $_POST ['ItemID'] . "'" );
		}
		$fields [] = '`UpdatedDate`=CURRENT_TIMESTAMP';
		
		$sth = $this->Stm ( "UPDATE `{$table}` SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
		$params [] = $Id;
		
		if ($sth != false && ($sth->execute ( $params )) != false) {
			echo json_encode ( array (
					"success" => true 
			) );
		} else {
			echo json_encode ( array (
					"failure" => true,
					'pdo_error' => true 
			) );
		}
	}
	public function SaveData() {
		$to = isset ( $_POST ['to'] ) ? $_POST ['to'] : false;
		if ($to == false) {
			echo json_encode ( array (
					"failure" => true 
			) );
			return false;
		}
		$Id = 0;
		if (isset ( $_POST ['Id'] )) {
			$Id = ( int ) $_POST ['Id'];
			unset ( $_POST ['Id'] );
		}
		$cols2 = array ();
		$fields = array ();
		$params = array ();
		$table = '';
		$cols = array ();
		if ($to == "Category") {
			$table = $this->CategoriesTable;
			$cols = $this->getColumns ( $table );
		} elseif ($to == "Item") {
			$table = $this->ItemsTable;
			$cols = $this->getColumns ( $table );
		}
		if (isset ( $_POST ['uffields'] )) {
			foreach ( $cols as $col ) {
				if (preg_match ( "#uf_([0-9]+?)_([0-9]+)#", $col ) && ! isset ( $_POST [$col] )) {
					$_POST [$col] = 0;
				}
			}
		}
		foreach ( $_POST as $name => $value ) {
			
			if (in_array ( $name, $cols )) {
				
				if ($Id == 0) {
					$fields [] = '`' . $name . '`';
					$cols2 [] = "?";
				} else {
					$fields [] = '`' . $name . '`=?';
				}
				$params [] = $this->utf2win ( $value );
			}
		}
		
		if ($Id == 0) {
			$cols2 [] = 'CURRENT_TIMESTAMP';
			$fields [] = '`UpdatedDate`';
		} else {
			$fields [] = '`UpdatedDate`=CURRENT_TIMESTAMP';
		}
		if ($Id == 0) {
			$sth = $this->Stm ( "INSERT INTO `{$table}` (" . implode ( ", ", $fields ) . ") VALUES (" . implode ( ", ", $cols2 ) . ")" );
		} else {
			$sth = $this->Stm ( "UPDATE `{$table}` SET " . implode ( ", ", $fields ) . " WHERE `Id`=?" );
			$params [] = $Id;
		}
		if ($sth != false && ($sth->execute ( $params )) != false) {
			if ($Id == 0) {
				$Id = $this->getAdapter ()->lastInsertId ();
			}
			
			echo json_encode ( array (
					"success" => true 
			) );
		} else {
			
			echo json_encode ( array (
					"failure" => true 
			) );
		}
		return false;
	}
}