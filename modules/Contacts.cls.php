<?php
class Redactor_Module_Contacts extends Redactor_Action {
	var $over = "";
	var $View = 'list';
	var $limit = 12;
	var $currentId = 0;
	/**
	 * Устанавливает лимит вывода новостей на страницу
	 *
	 * @param number $limit        	
	 */
	public function setLimit($limit = 12) {
		$this->limit = $limit;
	}
	public function getLimit() {
		return $this->limit;
	}
	function getView() {
		$this->setFileTemplate ( 'contacts' );
		$this->currentId = isset ( $_GET ['contacts'] ) && is_numeric ( $_GET ['contacts'] ) ? ( int ) $_GET ['contacts'] : 0;
		ob_start ();
		if ($this->currentId != 0) {
			$this->View = 'card';
		}
		Breadcrumbs::add ( '<a href="' . $this->getUrl ( array (
				'module' => 'contacts' 
		) ) . '">Новости</a>' );
		BreadcrumbsTitle::add ( 'Контакты' );
		include ('Views/contacts/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
	}
	function getItemImage($id) {
		
		/* Выполнение запроса с передачей ему массива параметров */
		$sth = $this->Stm ( 'select * from `contacts_img` where `iditem`=? order by `osn` desc, `pos` asc limit 1' ); // запрос
		$sth->execute ( array (
				$id 
		) );
		
		if ($sth != false and $sth->rowCount () > 0) { // проверка существует ли $sth и есть ли строки в запросе
			$row = $sth->fetch ( PDO::FETCH_ASSOC ); // получение ассоциативного массива данных $row;
			
			$image = ( object ) array (
					"path" => "files/contacts",
					"file" => "o_{$row['id']}.{$row['ext']}" 
			);
			return $image;
		}
		
		return false;
	}
	function getItemImages($id, $limit = 6) {
		if (is_string ( $limit )) {
			$limit = '';
		} elseif (is_numeric ( $limit ) && $limit > 0) {
			$limit = ' limit ' . $limit;
		} else {
			return false;
		}
		
		$sth = $this->Stm ( 'select * from `contacts_img` where `iditem`= ? and `osn`=0 order by `osn` desc, `pos` asc  ' . $limit ); // запрос
		$sth->execute ( array (
				$id 
		) ); // внесение в запрос параметра
		
		if ($sth != false and $sth->rowCount () > 0) {
			
			foreach ( $sth->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$images [] = ( object ) array (
						"path" => "files/contacts",
						"file" => "o_{$row['id']}.{$row['ext']}" 
				);
			}
			return $images;
		}
		
		return false;
	}
	function getContactsItem($Fields = "*") {
		if ($this->currentId == 0) {
			return false;
		}
		if (is_string ( $Fields )) {
			$Fields = trim ( $Fields );
			if ($Fields != "*") {
				return false;
			}
		} elseif (is_array ( $Fields ) && count ( $Fields ) > 0) {
			$Fields = implode ( ",", array_map ( array (
					$this,
					'changeFields' 
			), $Fields ) );
		} else {
			return false;
		}
		
		$sql = $this->query ( "select  {$Fields} from `contacts` where `id`='{$this->currentId}' and `active`='1' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ();
			Breadcrumbs::add ( '<a href="?contacts=' . $row->id . '">' . $row->name . '</a>' );
			BreadcrumbsTitle::set ( $row->name );
			
			if (! empty ( $row->TitlePage )) {
				BreadcrumbsTitle::set ( $row->TitlePage );
			}
			if (! empty ( $row->DescPage )) {
				Metas::setDescription ( $row->DescPage );
			}
			if (! empty ( $row->KeysPage )) {
				Metas::setKeywords ( $row->KeysPage );
			}
			
			$sth = $this->query ( "SELECT ROW 
FROM (

SELECT @rownum := @rownum +1
ROW , a.*
FROM  `contacts` a, (

SELECT @rownum :=0
)r
ORDER BY  `date` desc
) AS article_with_rows
WHERE id ={$row->id};" );
			$row->prevId = 0;
			$row->prevDate = 0;
			$row->nextId = 0;
			$row->nextDate = 0;
			if ($sth->rowCount () > 0) {
				$rowNum = $sth->fetchColumn ();
				
				if ($rowNum >= 1) {
					
					$sth = $this->query ( "select * from `contacts` where `active`='1' order by `date` desc limit " . ($rowNum - 2) . ", 1" );
					if ($sth && $sth->rowCount () > 0) {
						$r = $sth->fetch ();
						$row->nextId = $r->id;
						$row->nextDate = $r->date;
					}
					$sth = $this->query ( "select * from `contacts` where `active`='1' order by `date` desc limit " . ($rowNum) . ", 1" );
					if ($sth && $sth->rowCount () > 0) {
						$r = $sth->fetch ();
						$row->prevId = $r->id;
						$row->prevDate = $r->date;
					}
				}
			}
			
			return $row;
		}
		return false;
	}
	function getContacts($Fields = array('id', 'name', 'notice', 'date')) {
		if (! is_array ( $Fields ) or is_array ( $Fields ) && count ( $Fields ) == 0) {
			return false;
		}
		
		$limit = '';
		$Fields = array_map ( array (
				$this,
				'changeFields' 
		), $Fields );
		if (is_numeric ( $this->limit )) {
			$nowPage = isset ( $_GET ['page'] ) ? ( int ) $_GET ['page'] : 1;
			if ($nowPage < 1) {
				$nowPage = 1;
			}
			$page = $nowPage - 1;
			if ($page < 0) {
				$page = 0;
			}
			$start = abs ( $this->limit * $page );
			$limit = "limit $start, {$this->limit}";
		}
		
		$year = isset ( $_GET ['year'] ) ? ( int ) $_GET ['year'] : null;
		
		if ($year != 0) {
			
			$dopUslovie = 'YEAR( `date` )="' . $year . '" and ';
		} else {
			$dopUslovie = 'YEAR(  `date` ) = YEAR( NOW( ) ) and ';
		}
		
		$sth = $this->prepare ( "select * from `contacts` where `active`=1  order by `date` desc  {$limit}" );
		$sth->execute ();
		
		if ($sth != false && $sth->rowCount () > 0) {
			return $sth->fetchAll ();
		}
		
		return false;
	}
	function countContacts() {
		$sql = $this->query ( "select count(1) from `contacts` where `active`='1'" );
		if ($sql != false && $sql->rowCount () > 0) {
			return $sql->fetchColumn ();
		}
		return 0;
	}
}
class contacts_admin extends Redactor_Admin {
	var $over = "";
	function __construct() {
		ini_set ( "memory_limit", "78M" );
		if (! isset ( $_SESSION ['admin'] )) {
			exit ();
		}
	}
	function Update() {
		if (isset ( $_POST ['pos'] )) {
			$pos = $_POST ['pos'];
			$pos = "pos='$pos',";
		} else {
			$pos = "";
		}
		if (isset ( $_POST ['active'] )) {
			$Active = $_POST ['active'];
			$Active = ", active='$Active' ";
		} else {
			$Active = "";
		}
		$this->exec ( "update contacts set {$pos} `UpdatedDate`=CURRENT_TIMESTAMP{$Active} where id='$_POST[id]'" );
		echo "33";
	}
	function UpdateImagePos() {
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		$pos = isset ( $_POST ['pos'] ) ? ( int ) $_POST ['pos'] : 0;
		$this->exec ( "update `contacts_img` set `pos`='{$pos}' where `id`='{$id}' limit 1" );
		echo "33";
	}
	function UploadPhoto() {
		if (isset ( $_POST ['id'] ) && ! empty ( $_POST ['id'] )) {
			$this->exec ( "insert into `contacts_img` values ('', '$_POST[id]','', '', '0')" );
			$id = $this->getAdapter ()->lastInsertId ();
			$uploaddir = $_SERVER ['DOCUMENT_ROOT'] . "/files/contacts/";
			$p = pathinfo ( basename ( $_FILES ['photo-path'] ['name'] ) );
			if (isset ( $p ['extension'] )) {
				$ext = strtolower ( $p ['extension'] );
				
				if (in_array ( $ext, array (
						"jpg",
						"jpeg",
						"png",
						"gif" 
				) )) {
					
					$uploadfile3 = $uploaddir . "o_$id.$ext";
					if (move_uploaded_file ( $_FILES ['photo-path'] ['tmp_name'], $uploadfile3 )) {
						
						$this->exec ( "update `contacts_img` set `ext`='$ext' where `id`='$id' limit 1" );
					} else {
						$this->exec ( "delete from `contacts_img` where `id`='$id' limit 1" );
						echo "{failure:true}";
						exit ();
					}
				} else {
					$this->exec ( "delete from `contacts_img` where `id`='$id' limit 1" );
					echo "{failure:true}";
					exit ();
				}
			} else {
				$this->exec ( "delete from `contacts_img` where `id`='$id' limit 1" );
				echo "{failure:true}";
				exit ();
			}
		}
		echo "{success:true}";
	}
	function A2Up($fields, $values) {
		$string = "";
		$i = 0;
		foreach ( $fields as $name => $value ) {
			$i ++;
			if ($i > 1) {
				$string .= ",";
			}
			
			$value = addslashes ( $value );
			$vv = isset ( $values [$name] ) ? $values [$name] : '';
			$string .= "`{$value}`='$vv'";
		}
		return $string;
	}
	function A2S($Array, $Sep = ",", $Closer = "", $Slashes = false) {
		if (is_array ( $Array )) {
			$string = "";
			$i = 0;
			foreach ( $Array as $name => $value ) {
				$i ++;
				if ($i > 1) {
					$string .= "{$Sep}";
				}
				if (is_array ( $value )) {
					$this->A2S ( $value, $Sep, $Closer, $Slashes );
				} else {
					if ($Slashes == true) {
						$value = addslashes ( $value );
					}
					$string .= "{$Closer}{$value}{$Closer}";
				}
			}
			return $string;
		}
		return $Array;
	}
	function getColumns() {
		$cols = array ();
		$sql = $this->query ( "SHOW COLUMNS FROM `contacts`" );
		if ($sql != false && $sql->rowCount () > 0) {
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$cols [] = strtolower ( $row ['Field'] );
			}
		}
		return $cols;
	}
	function save() {
		$id = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
		if (isset ( $_POST ['id'] )) {
			unset ( $_POST ['id'] );
		}
		$fields = array ();
		$values = array ();
		$notallow = array (
				"id",
				"module",
				"task",
				"xaction",
				"ext-c",
				"cat_id" 
		);
		$allow = $this->getColumns ();
		foreach ( $_POST as $field => $value ) {
			$test = strtolower ( $field );
			if (in_array ( $test, $allow )) {
				$fields [$field] = $field;
				$values [$field] = addslashes ( $this->utf2win ( $value ) );
			}
		}
		
		$fields ['active'] = 'active';
		$values ['active'] = 1;
		
		$this->exec ( "update `contacts` set {$this->A2Up($fields, $values)} where `id`='$id'" );
		
		$sql = $this->query ( "select  `name` from `contacts` where `id`='{$id}' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			if (isset ( $_POST ['url'] ) && ! empty ( $_POST ['url'] )) {
				rewriteUrls::saveUrl ( array (
						'module' => 'contacts',
						'contacts' => $id,
						
						'custom' => $_POST ['url'] 
				) );
			} elseif (isset ( $_POST ['url'] ) && empty ( $_POST ['url'] )) {
				
				rewriteUrls::saveUrl ( array (
						'module' => 'contacts',
						'contacts' => $id,
						
						'custom' => '' 
				) );
			} else {
				$url = rewriteUrls::getSingleUrl ( array (
						'module' => 'contacts',
						'contacts' => $id 
				) );
				
				if (empty ( $url )) {
					if ($this->get ( 'Pages_TypeURL' ) == 2) {
						rewriteUrls::saveUrl ( array (
								'module' => 'contacts',
								'contacts' => $id,
								
								'name' => $this->translit ( $row ['name'] ) 
						) );
					} else {
						rewriteUrls::saveUrl ( array (
								'module' => 'contacts',
								'contacts' => $id,
								'name' => '' 
						) );
					}
				}
			}
		}
		
		echo "{success:true}";
	}
	function Listing() {
		if (! isset ( $_POST ['id'] )) {
			$id = 0;
		} else {
			$id = $_POST ['id'];
		}
		$rows = 0;
		$_POST ['start'] = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$_POST ['limit'] = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		$count = $this->query ( "SELECT count(1) FROM `contacts`" );
		if ($count != false && $count->rowCount () > 0 && ($rows = $count->fetchColumn ()) > 0) {
			
			$sql_count = "SELECT * FROM `contacts`";
			$sql = $sql_count . " order by `date` desc LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'];
			$rs_count = $this->query ( $sql );
			if ($rs_count != false && $rs_count->rowCount () > 0) {
				
				$arr = array ();
				$arr2 = array ();
				
				foreach ( $rs_count->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
					$record = $obj;
					$record ['link'] = rewriteUrls::getUrl ( array (
							'module' => 'contacts',
							'contacts' => $record ['id'] 
					) );
					$record ['url'] = rewriteUrls::getSingleCustomUrl ( array (
							'module' => 'contacts',
							'contacts' => $record ['id'] 
					) );
					$record ['date2'] = $this->formatDate ( 'd micromonth Y', $record ['date'] );
					
					$record ['UpdatedDate'] = ($this->formatDate ( "d micromonth Y", $obj ['UpdatedDate'] ));
					$record ['CreatedDate'] = ($this->formatDate ( "d micromonth Y", $obj ['CreatedDate'] ));
					$record ['UpdatedDate2'] = ($this->formatDate ( "d micromonth Y H:i:s", $obj ['UpdatedDate'] ));
					$record ['CreatedDate2'] = ($this->formatDate ( "d micromonth Y H:i:s", $obj ['CreatedDate'] ));
					
					$arr [] = $this->winDecode ( $record );
				}
				$jsonresult = $this->JEncode ( $arr );
				echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
			} else {
				echo '({"total":"0", "results":""})';
			}
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	function deleteItem() {
		$sql = $this->query ( "select * from `contacts_img` where `iditem`='$_POST[id]'" );
		if ($sql != false && $sql->rowCount () > 0) {
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$dir = $_SERVER ['DOCUMENT_ROOT'] . "/files/contacts/";
				$file = "o_{$row['id']}.{$row['ext']}";
				
				if (file_exists ( $dir . $file )) {
					$dd = $dir . $file;
					@unlink ( $dd );
				}
				
				$this->exec ( "delete from `contacts_img` where `id`='$row[id]' limit 1" );
			}
		}
		$this->exec ( "delete from `contacts` where `id`='$_POST[id]'" );
		
		echo "33";
	}
	function deleteImage() {
		$id = ( int ) $_POST ['id'];
		$sql = $this->query ( "select * from `contacts_img` where `id`='$id' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$dir = $_SERVER ['DOCUMENT_ROOT'] . "/files/contacts/";
			$file = "o_{$row['id']}.{$row['ext']}";
			
			if (file_exists ( $dir . $file )) {
				$dd = $dir . $file;
				@unlink ( $dd );
			}
			
			$this->exec ( "delete from `contacts_img` where `id`='$row[id]' limit 1" );
		}
		echo "33";
	}
	function setOsnImage() {
		$id = ( int ) $_POST ['id'];
		$sql = $this->query ( "select `id`,`iditem` from `contacts_img` where `id`='$id' limit 1" );
		if ($sql != false && $sql->rowCount () > 0) {
			$row = $sql->fetch ( PDO::FETCH_ASSOC );
			$this->exec ( "update `contacts_img` set `osn`='0' where `iditem`='$row[iditem]'" );
			$this->exec ( "update `contacts_img` set `osn`='1' where `id`='$row[id]'" );
		}
	}
	function Listing_Images() {
		$id = ( int ) $_POST ['dd'];
		$_POST ['start'] = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$_POST ['limit'] = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 25;
		$count = $this->query ( "SELECT count(1) FROM `contacts_img` where `iditem`='$id'" );
		$rows = 0;
		if ($count != false && $count->rowCount () > 0) {
			$rows = $count->rowCount ();
		}
		$sql_count = "SELECT * FROM `contacts_img` where `iditem`='$id'";
		$sql = $sql_count . " LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'];
		
		$rs = $this->query ( $sql );
		if ($rs != false && $rs->rowCount () > 0) {
			$arr = array ();
			$arr2 = array ();
			
			foreach ( $rs->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
				$arr2 ['id'] = $this->win2utf ( $obj ['id'] );
				$arr2 ['image'] = "o_{$obj['id']}.{$obj['ext']}";
				$arr2 ['file'] = "o_{$obj['id']}.{$obj['ext']}";
				$arr2 ['osn'] = $obj ['osn'];
				$arr2 ['pos'] = $obj ['pos'];
				$arr [] = $arr2;
			}
			$jsonresult = $this->JEncode ( $arr );
			echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
		} 

		else {
			echo '({"total":"0", "results":""})';
		}
	}
	function NewItem() {
		$this->exec ( "insert into `contacts` (`id`, `active`) value ('', '0')" );
		$id = $this->getAdapter ()->lastInsertId ();
		if (isset ( $_POST ['copy'] )) {
			$old = isset ( $_POST ['id'] ) ? ( int ) $_POST ['id'] : 0;
			if ($old) {
				$sth = $this->Stm ( 'select * from `contacts_img` where `iditem`=?' );
				if ($sth != false && ($sth->execute ( array (
						$old 
				) )) != false && $sth->rowCount () > 0) {
					foreach ( $sth->fetchAll () as $row ) {
						$file = "files/contacts/o_{$row->id}.{$row->ext}";
						if (file_exists ( $file )) {
							$insert = $this->Stm ( "insert into `contacts_img` (`iditem`, `ext`, `osn`, `pos`) values (?, ?, ?, ?)" );
							if ($insert != false && ($insert->execute ( array (
									$id,
									$row->ext,
									$row->osn,
									$row->pos 
							) )) != false) {
								$imageId = $this->getAdapter ()->lastInsertId ();
								if ($imageId) {
									if (copy ( $file, "files/contacts/o_{$imageId}.{$row->ext}" )) {
										chmod ( "files/contacts/o_{$imageId}.{$row->ext}", 0666 );
									} else {
										$delete = $this->Stm ( "delete from `contacts_img` where `id`=?" );
										if ($delete != fale) {
											$delete->execute ( array (
													$imageId 
											) );
										}
									}
								}
							}
						}
					}
				}
			}
		}
		echo $id;
	}
	function winDecode($string) {
		//if (is_array ( $string )) {
			//$newArray = array ();
			//foreach ( $string as $name => $value ) {
				//if (is_array ( $value )) {
					//$newArray [$name] = $this->winDecode ( $value );
				//} else {
					//if (is_string ( $value )) {
						//$newArray [$name] = iconv ( "windows-1251", "utf-8", $value );
					//} else {
						//$newArray [$name] = $value;
					//}
				//}
			//}
			//return $newArray;
		//} else {
			//if (is_string ( $string )) {
				//return iconv ( "windows-1251", "utf-8", $string );
			//}
		//}
		return $string;
	}
	function ResizeImage($image_from, $image_to, $fitwidth = 450, $fitheight = 450, $quality = 100) {
		global $php_inc;
		
		$os = $originalsize = getimagesize ( $image_from );
		
		if ($originalsize [2] != 2 && $originalsize [2] != 3 && $originalsize [2] != 1 && $originalsize [2] != 6 && ($originalsize [2] < 9 or $originalsize [2] > 12)) {
			return false;
		}
		
		if ($originalsize [0] > $fitwidth or $originalsize [1] > $fitheight) {
			$h = getimagesize ( $image_from );
			if (($h [0] / $fitwidth) > ($h [1] / $fitheight)) {
				$fitheight = $h [1] * $fitwidth / $h [0];
			} else {
				$fitwidth = $h [0] * $fitheight / $h [1];
			}
			
			if ($os [2] == 1) {
				$i = @imagecreatefromgif ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				
				$trans_color = imagecolortransparent ( $i );
				$trans_index = imagecolorallocate ( $i, $trans_color ['red'], $trans_color ['green'], $trans_color ['blue'] );
				imagecolortransparent ( $i, $trans_index );
				
				imagesavealpha ( $i, true );
				imagesavealpha ( $o, true );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				imagegif ( $o, $image_to );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			} 

			elseif ($os [2] == 2 or ($os [2] >= 9 && $os [2] <= 12)) {
				$i = @ImageCreateFromJPEG ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				imagejpeg ( $o, $image_to, $quality );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			} elseif ($os [2] == 3) {
				$i = @ImageCreateFromPng ( $image_from );
				if (! $i) {
					return false;
				}
				$o = ImageCreateTrueColor ( $fitwidth, $fitheight );
				imagesavealpha ( $i, true );
				
				imagesavealpha ( $i, true );
				imagealphablending ( $o, false );
				
				imagesavealpha ( $o, true );
				imagecopyresampled ( $o, $i, 0, 0, 0, 0, $fitwidth, $fitheight, $h [0], $h [1] );
				
				imagesavealpha ( $o, true );
				imagepng ( $o, $image_to );
				chmod ( $image_to, 0777 );
				imagedestroy ( $o );
				imagedestroy ( $i );
			}
			
			return 2;
		}
		if ($originalsize [0] <= $fitwidth && $originalsize [1] <= $fitheight) {
			if ($os [2] == 1) {
				$i = @imagecreatefromgif ( $image_from );
				if (! $i) {
					return false;
				}
				imagesavealpha ( $i, true );
				imagegif ( $i, $image_to );
			} elseif ($os [2] == 3) {
				$i = @ImageCreateFromPng ( $image_from );
				if (! $i) {
					return false;
				}
				imagesavealpha ( $i, true );
				imagepng ( $i, $image_to );
			} else {
				$i = @ImageCreateFromJPEG ( $image_from );
				if (! $i) {
					return false;
				}
				imagejpeg ( $i, $image_to, $quality );
			}
			imagedestroy ( $i );
			chmod ( $image_to, 0777 );
			return 1;
		}
	}
}

?>
