<?php
class Redactor_Module_Search extends Redactor_Action {
	var $over = '';
	var $View = 'index';
	function __construct() {
	}
	function getView() {
		ob_start ();
		
		include ('Views/search/' . $this->View . '.phtml');
		$this->over = ob_get_clean ();
		return $this->over;
	}
	function q($query) {
		$modules = array (
				'�������' => array (
						'table' => 'Catalog_Items',
						'cols' => '`Id`, `Title`,	`Price`,`uf_1`',
						'module' => 'catalog',
						'where' => ' `CategoryID`!=0 and `Active`=\'1\' and' 
				),
				"������� �����" => array (
						'table' => 'pages',
						'cols' => '`Id` as `Id`, `Title` as `Title`',
						'module' => 'news',
						'where' => ' `Active`=\'1\'  and' 
				),
				
				'�������' => array (
						'table' => 'news',
						'cols' => '`id` as `Id`, `name` as `Title`',
						'module' => 'news',
						'where' => ' `active`=\'1\'  and' 
				) 
		);
		$results = array ();
		$this->getModule ( 'catalog' );
		foreach ( $modules as $moduleName => $params ) {
			$cols = $this->cols ( $params ['table'] );
			
			if (count ( $cols ) > 0) {
				$sth = $this->prepare ( "SELECT {$params['cols']} FROM `{$params['table']}` WHERE {$params['where']} (" . implode ( " OR ", $cols ) . ")" );
				$sth->execute ( array (
						":QUERY" => '%' . $this->strtolower ( $query ) . '%' 
				) );
			
				if ($sth->rowCount () > 0) {
					foreach ( $sth->fetchAll () as $row ) {
						if ($moduleName == '�������') {
							$cat = new Redactor_Module_Catalog_Item ( $row );
							$image = $cat->getImage ( $row );
							$url = $this->getUrl ( array (
									'module' => 'catalog',
									'catalog' => $row->Id 
							) );
							if ($image != false) {
								$image = '<img class="pic99" src="' . $image->getThumbUrl ( 99, 99 ) . '" alt="' . htmlspecialchars ( $row->Title, ENT_COMPAT, 'cp1251' ) . '" title="' . htmlspecialchars ( $row->Title, ENT_COMPAT, 'cp1251' ) . '"/>';
							} else {
								$image = '<img class="pic99" src="/thumbs/99x99/nophoto.png"  alt="' . htmlspecialchars ( $row->Title, ENT_COMPAT, 'cp1251' ) . '" title="' . htmlspecialchars ( $row->Title, ENT_COMPAT, 'cp1251' ) . '" />';
							}
							$results [$moduleName] [] = '<li class="item249"><a href="' . $url . '">' . $image . '</a>
			<form method="post" id="basket' . $row->Id . '" action="' . $this->getUrl ( array (
									'module' => 'basket' 
							) ) . '">
					<input type="hidden" name="add2Basket" value="true" />
						<input type="hidden" name="ItemID" value="' . $row->Id . '" />	<div>
					<p>' . $row->Title . '</p>
				</div> ' . (! empty ( $row->uf_1 ) ? '<i>' . $row->uf_1 . ' ���.</i>' : '') . ' <em>' . $row->Price . ' ���.</em> <a href="'.$url.'"
				class="blueButton1"  >������</a></form></li>';
							;
						} else {
							$results [$moduleName] [] = '<li><p><a href="' . $this->getUrl ( array (
									'module' => $params ['module'],
									$params ['module'] => $row->Id 
							) ) . '">' . $row->Title . '</a></p></li>';
						}
					}
				}
			}
		}
		return $results;
	}
	function cols($table) {
		$cols = array ();
		$sth = $this->query ( "SHOW COLUMNS FROM `{$table}` WHERE `Type` Like '%char%' or `Type` LIKE '%text%'" );
		if ($sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				
				$cols [] = "LOWER(`{$row->Field}`) LIKE :QUERY";
			}
		}
		return $cols;
	}
}
