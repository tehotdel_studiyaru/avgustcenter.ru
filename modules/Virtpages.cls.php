<?php
ini_set ( 'display_errors', 1 );
class virtpages_admin extends Redactor_Admin {
	var $over = "";
	function __construct() {
		ini_set ( "memory_limit", "78M" );
		if (! isset ( $_SESSION ['admin'] )) {
			exit ();
		}
	}
	function Update() {
		if (isset ( $_POST ['pos'] )) {
			$pos = $_POST ['pos'];
		} else {
			$pos = "";
		}
		if (isset ( $_POST ['active'] )) {
			$Active = ( int ) $_POST ['active'];
		} else {
			$Active = "";
		}
		$_POST ['id'] = ( int ) $_POST ['id'];
		$this->exec ( "update Virtpages set  `Active`='$Active' where `Id`='$_POST[id]'" );
		echo "33";
	}
	function A2Up($fields, $values) {
		$string = "";
		$i = 0;
		foreach ( $fields as $name => $value ) {
			$i ++;
			if ($i > 1) {
				$string .= ",";
			}
			
			$value = addslashes ( $value );
			$vv = isset ( $values [$name] ) ? $values [$name] : '';
			$string .= "`{$value}`='$vv'";
		}
		return $string;
	}
	function A2S($Array, $Sep = ",", $Closer = "", $Slashes = false) {
		if (is_array ( $Array )) {
			$string = "";
			$i = 0;
			foreach ( $Array as $name => $value ) {
				$i ++;
				if ($i > 1) {
					$string .= "{$Sep}";
				}
				if (is_array ( $value )) {
					$this->A2S ( $value, $Sep, $Closer, $Slashes );
				} else {
					if ($Slashes == true) {
						$value = addslashes ( $value );
					}
					$string .= "{$Closer}{$value}{$Closer}";
				}
			}
			return $string;
		}
		return $Array;
	}
	function getColumns() {
		$cols = array ();
		$sql = $this->query ( "SHOW COLUMNS FROM `Virtpages`" );
		if ($sql != false && $sql->rowCount () > 0) {
			foreach ( $sql->fetchAll ( PDO::FETCH_ASSOC ) as $row ) {
				$cols [] = $row ['Field'];
			}
		}
		return $cols;
	}
	function getCode($page) {
		$ch = curl_init ( $page );
		
		if (! curl_errno ( $ch )) {
			$info = curl_getinfo ( $ch );
			
			return $info ['http_code'];
		}
		return 'error';
	}
	function testPage($page) {
		$ch = curl_init ( 'http://' . getenv ( 'HTTP_HOST' ) . $page );
		@curl_setopt ( $ch, CURLOPT_HEADER, true ); // we want headers
		@curl_setopt ( $ch, CURLOPT_NOBODY, true ); // we don't need body
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 0 );
		$return = '';
		// ���������
		curl_exec ( $ch );
		$codeLast = 0;
		$lastUrl = 'http://' . getenv ( 'HTTP_HOST' ) . $page;
		$Virtpages = array ();
		$pages = array ();
		// ��������� ������� ������
		if (! curl_errno ( $ch )) {
			$info = curl_getinfo ( $ch );
			$codeLast = $info ['http_code'];
			if ($info ['http_code'] == 404) {
				$return = '<span style="color:red">�������� �� ���������� (' . $info ['http_code'] . ')</span>';
			} elseif (in_array ( $info ['http_code'], array (
					300,
					301,
					302,
					303,
					307 
			) )) {
				$Virtpages = array (
						$info ['url'] => $info ['url'] 
				);
				
				$testR = $this->redi ( $info ['redirect_url'], $Virtpages, $pages );
				
				if ($testR == 'WHILE') {
					$return = '<span style="color:red; font-weight:bold;">����������� ��������</span>';
					return array (
							$return,
							$lastUrl,
							$codeLast,
							$Virtpages 
					);
				} else {
					$return = '�������� ' . $info ['http_code'] . ' (' . end ( $Virtpages ) . ') <span style="border-bottom:1px dotted blue;cursor:pointer;" onclick="showLinks(this);">�������� �������</span><div style="display:none"> ' . implode ( ' -> <br/>', $Virtpages ) . '</div>';
				}
				$lastUrl = end ( $Virtpages );
			} elseif ($info ['http_code'] == 200) {
				$return = '<span style="color:green">OK (' . $info ['http_code'] . ')</span>';
			} else {
				$return = '<span style="color:red">�������� �� ������� ��������� (' . $info ['http_code'] . ')</span>';
			}
		} else {
			$return = '<span style="color:red">�������� �� ������� ���������</span>';
		}
		$codeLast = isset ( $pages [$lastUrl] ) ? $pages [$lastUrl] : $this->getCode ( $lastUrl );
		// Close handle
		curl_close ( $ch );
		return array (
				$return,
				$lastUrl,
				$codeLast,
				$Virtpages,
				$pages 
		);
	}
	function redi($page, &$Virtpages = array(), &$pages = array()) {
		if (isset ( $Virtpages [$page] )) {
			return 'WHILE';
		}
		$ch = curl_init ( $page );
		@curl_setopt ( $ch, CURLOPT_HEADER, true ); // we want headers
		@curl_setopt ( $ch, CURLOPT_NOBODY, true ); // we don't need body
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 0 );
		$Virtpages [$page] = $page;
		
		curl_exec ( $ch );
		if (! curl_errno ( $ch )) {
			$info = curl_getinfo ( $ch );
			$pages [$page] = $info ['http_code'];
			if (in_array ( $info ['http_code'], array (
					300,
					301,
					302,
					303,
					307 
			) )) {
				if (isset ( $Virtpages [$info ['redirect_url']] )) {
					return 'WHILE';
				}
				
				return $this->redi ( $info ['redirect_url'], $Virtpages );
			}
		}
		return $Virtpages;
	}
	function testing() {
		ob_start ();
		$sth = $this->prepare ( "update `Virtpages` SET `Test`=''" );
		$sth->execute ();
		$up = $this->prepare ( "update `Virtpages` SET `Test`=? WHERE `Id`=?" );
		$sth = $this->prepare ( "select * from `Virtpages`" );
		$sth->execute ();
		if ($sth->rowCount () > 0) {
			foreach ( $sth->fetchAll () as $row ) {
				$Test = array ();
				$test1 = $this->testPage ( $row->From );
				if (! empty ( $test1 [0] )) {
					if ($test1 == '<span style="color:red; font-weight:bold;">����������� ��������</span>') {
						$Test [] = $test1;
						
						$up->execute ( array (
								implode ( '<br/>', $Test ),
								$row->Id 
						) );
						continue;
					}
					
					if ($test1 [1] != 'http://' . getenv ( 'HTTP_HOST' ) . $row->To || $test1 [2] != 200) {
						$Test [] = '<span style="color:red; font-weight:bold">' . $test1 [1] . ' (' . $test1 [2] . ')</span> ' . (count ( $test1 [3] ) > 0 ? '<span style="border-bottom:1px dotted blue;cursor:pointer;" onclick="showLinks(this);">�������</span> <div style="display:none;"><p>���� ��������: ' . date ( 'd.m.Y H:i:s' ) . '</p><br/>' . $this->links ( $test1 [3], $test1 [4] ) . '</div>' : '');
					} 

					else {
						$Test [] = '<span style="color:green; font-weight:bold">' . $test1 [1] . ' (' . $test1 [2] . ')</span> ' . (count ( $test1 [3] ) > 0 ? '<span style="border-bottom:1px dotted blue;cursor:pointer;" onclick="showLinks(this);">�������</span> <div style="display:none;"><p>���� ��������: ' . date ( 'd.m.Y H:i:s' ) . '</p><br/>' . $this->links ( $test1 [3], $test1 [4] ) . '</div>' : '');
					}
				}
				// $test1 = $this->testPage ( $row->To );
				// if (! empty ( $test1 )) {
				// $Test [] = '<b>����:</b> ' . $test1;
				// }
				$up->execute ( array (
						implode ( '<br/>', $Test ),
						$row->Id 
				) );
			}
		}
		// ob_clean();
		echo json_encode ( array (
				'success' => true 
		) );
	}
	function links($a, $p) {
		$out = '<ol>';
		foreach ( $a as $link ) {
			$out .= '<li>' . $link . (isset ( $p [$link] ) ? ' (' . $p [$link] . ')' : '') . '</li>';
		}
		$out .= '</ol>';
		return $out;
	}
	function save() {
		$id = isset ( $_POST ['Id'] ) ? ( int ) $_POST ['Id'] : 0;
		if (isset ( $_POST ['Id'] )) {
			unset ( $_POST ['Id'] );
		}
		if ($id == 0) {
			$id = $this->NewItem ();
		}
		$fields = array ();
		$values = array ();
		
		$allow = $this->getColumns ();
		foreach ( $_POST as $field => $value ) {
			
			if (in_array ( $field, $allow )) {
				$fields [$field] = $field;
				$values [$field] = addslashes ( $this->utf2win ( $value ) );
			}
		}
		
		$fields ['Active'] = 'Active';
		$values ['Active'] = 1;
		
		$this->exec ( "update `Virtpages` set {$this->A2Up($fields, $values)} where `Id`='$id'" );
		echo "{success:true}";
	}
	function Listing() {
		if (! isset ( $_POST ['id'] )) {
			$id = 0;
		} else {
			$id = $_POST ['id'];
		}
		$rows = 0;
		$_POST ['start'] = isset ( $_POST ['start'] ) ? ( int ) $_POST ['start'] : 0;
		$_POST ['limit'] = isset ( $_POST ['limit'] ) ? ( int ) $_POST ['limit'] : 500;
		$params = array ();
		$dop = '';
		if (isset ( $_POST ['search'] )) {
			$dop .= ' WHERE `From` LIKE :Query OR `To` LIKE :Query';
			$params [':Query'] = '%' . $this->utf2win ( $_POST ['search'] ) . '%';
		}
		$count = $this->prepare ( "SELECT count(1) FROM `Virtpages`" . $dop );
		$count->execute ( $params );
		if ($count->rowCount () > 0 && ($rows = $count->fetchColumn ()) > 0) {
			
			$sql_count = "SELECT * FROM `Virtpages`" . $dop;
			$sql = $sql_count . " LIMIT " . ( int ) $_POST ['start'] . ", " . ( int ) $_POST ['limit'];
			$rs_count = $this->prepare ( $sql );
			$rs_count->execute ( $params );
			if ($rs_count->rowCount () > 0) {
				
				$arr = array ();
				$arr2 = array ();
				
				foreach ( $rs_count->fetchAll ( PDO::FETCH_ASSOC ) as $obj ) {
					$record = $obj;
					
					$arr [] = $this->winDecode ( $record );
				}
				$jsonresult = $this->JEncode ( $arr );
				echo '({"total":"' . $rows . '","results":' . $jsonresult . '})';
			} else {
				echo '({"total":"0", "results":""})';
			}
		} else {
			echo '({"total":"0", "results":""})';
		}
	}
	function deleteItem() {
		$_POST ['id'] = ( int ) $_POST ['id'];
		$this->query ( "delete from `Virtpages` where `Id`='$_POST[id]'" );
		echo "33";
	}
	function NewItem() {
		$this->exec ( "insert into `Virtpages` (`Id`, `Active`) value ('', '0')" );
		$id = $this->getAdapter ()->lastInsertId ();
		return $id;
	}
	function winDecode($string) {
		if (is_array ( $string )) {
			$newArray = array ();
			foreach ( $string as $name => $value ) {
				if (is_array ( $value )) {
					$newArray [$name] = $this->winDecode ( $value );
				} else {
					if (is_string ( $value )) {
						$newArray [$name] = iconv ( "windows-1251", "utf-8", $value );
					} else {
						$newArray [$name] = $value;
					}
				}
			}
			return $newArray;
		} else {
			if (is_string ( $string )) {
				return iconv ( "windows-1251", "utf-8", $string );
			}
		}
		return $string;
	}
}
